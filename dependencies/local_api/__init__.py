# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

''' API for local database '''

from typing import Union

try :
    import classes
    import filters_utils
    import utils
    import logger
except :
    from . import classes
    from . import filters_utils
    from . import utils
    from . import logger
    

log = logger.get_logger()


__version__ = "0.0.1"


class Local_API :
    def __init__(self, db_file_path: str) :
        self.db_file_path = db_file_path
        self.schema = classes.Schema_Local()
        self.db = classes.DB_Local(self.db_file_path)
        self.auth = None
        

    def read_schema(self, entity: str = None) -> dict :
        ''' 
        get the DB schema 

        Args:
            - entity: if given, return the schema only for this entity
        '''
        schema = self.schema.get()
        if entity :
            schema = utils.get_schema_entity(entity, schema)
        return schema
    

    def create(self, entity: str, parameters: dict) -> dict :
        ''' 
        create a new entity 

        Args:
            - entity: the wanted entity
            - parameters: the new entity parameters
        
        Return:
         the new entity ditionnary
        '''
        db = self.db.get()
        schema = self.schema.get()
        
        #ensure that entity is ok
        entity = utils.check_entity(entity, schema)
        
        #ensure that all needed parameters are there
        entity_schema = schema["entities"][entity]
        link_params = utils.check_parameters(entity, parameters, schema)
            
        new_entity = parameters

        #complete the dict
        new_id = len(db["All"])
        new_entity.update({"id": new_id})
        new_entity.update({"type": entity})
        for param in entity_schema :
            param_type = utils.get_var_type(entity_schema[param]["var_type"])
            if param not in new_entity :
                new_entity.update({param: param_type()})

            if param == "type" : continue

            param_dict = entity_schema[param]
            if "data" in param_dict and param_dict["data"].startswith("self.") :
                attrs = param_dict["data"].split(".")[1:]
                value = None
                attr_dict = new_entity
                for i, attr in enumerate(attrs) :
                    value = attr_dict[attr]
                    if type(value) is int and i+1 < len(attrs) :
                        attr_dict = db["All"][value]
                new_entity[param] = value

        #add to entity list in db
        db["All"].append(new_entity)
        if entity not in db["Entities"] :
            db["Entities"].update({entity: []})
        db["Entities"][entity].append(new_id)
        
        #add relation to other entities
        for link in link_params :
            link_id: int = new_entity[link]
            link_entity: dict = db["All"][link_id]

            link_entity_schema = utils.get_schema_entity(link_entity["type"], schema)
            for param in link_entity_schema :
                param_dict = link_entity_schema[param]
                if "data" not in param_dict : continue
                if entity not in param_dict["data"].split(".") : continue

                param_type = utils.get_var_type(param_dict["var_type"])
                if param_type is not list :
                    error = f"the var_type for a link operation is not supported (only list are supported)(param_type: {param_type})"
                    log.error(error)
                    raise Exception(error)
                
                if (param not in db["All"][link_id] or 
                    type(db["All"][link_id][param]) is not param_type) :
                    db["All"][link_id].update({param: [new_id]})
                    continue
                
                db["All"][link_id][param].append(new_id)


        self.db.save(db)
        return new_entity

    def find(self, entity, filters: list[list] = [], only_one: bool = False) -> Union[list[dict], dict] :
        ''' 
        find an entity 
        
        Args:
            - entity: the wanted entity type
            - filters: 
            - only_one:
        '''
        to_return = []
        db = self.db.get()
        schema = self.schema.get()

        entity = utils.check_entity(entity, schema)
        
        for item in db["Entities"][entity] :
            item = db["All"][item]
            if not filters :
                to_return.append(item)
                continue

            is_item_filter_ok = True
            for filter in filters :
                if not filters_utils.process_filter(item, filter) :
                    is_item_filter_ok = False
                    break

            if is_item_filter_ok :
                to_return.append(item)

        if not to_return : 
            log.debug("nothing to return")
            return to_return
        
        if not only_one :
            return to_return
        
        if len(to_return) == 1 :
            return to_return[-1]

        log.info(f"multiple entities ({entity}) found")
        return to_return
    

    def update(self, entity_id: int, data: dict) :
        ''' TODO '''
        db = self.db.get()
        db["All"][entity_id].update(data)
        return
    

    def delete(self, entity: str, entity_id: int) :
        ''' TODO '''
        db = self.db.get()
        db["Entities"][entity].remove(entity_id)
        db["All"][entity_id].update({"deleted": True})
        return
    
    def revive(self, entity: str, entity_id: int) :
        ''' TODO '''
        db = self.db.get()
        db["All"][entity_id].pop("deleted")
        db["Entities"][entity].append(entity_id)
        return
    

if __name__ == "__main__" :
    import os
    os.system("cls")
    print("\n____START____\n\n")
    path = r"C:\Users\Marteau\AppData\Roaming\Blender Foundation\Blender\3.6\scripts\addons\flux\database_access\local_bridge\test_local_db.json"
    lcl = Local_API(path)

    to_print = lcl.db.get()

    print(to_print)
    print()
    
    #lcl.create("Project", {"name": "Rufus"})
    #lcl.create("Sequence", {"name": "sq_test_01", "project": 0})
    #lcl.create("Shot", {"name": "sh_test_03", "sequence": 1})
    lcl.create("Shot", {"name": "azhgehsdb", "sequence": 1})

    #to_print = lcl.db.get()["All"]
    to_print = lcl.find("Shot", [["id", "is_not", 3]])

    print(to_print)
