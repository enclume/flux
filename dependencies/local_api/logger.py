# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

''' Local API logging related stuff '''

import logging


def get_logger(logger_name: str = "Local_DB") -> logging.Logger :
    '''
    get or create a Local_DB logger

    Args:
        logger_name : the name of the logger to get/create
    '''

    flux_logger = logging.getLogger(logger_name)
    flux_logger.setLevel(logging.ERROR)
    # flux_logger.setLevel(logging.DEBUG)
    if not flux_logger.hasHandlers():
        stream_handler = logging.StreamHandler()

        # TODO better name if name is __init__
        formatter = logging.Formatter('\n|%(name)-12s- %(levelname)s in %(funcName)s (%(filename)s, %(lineno)d) : %(message)s\n(%(pathname)s)')
        stream_handler.setFormatter(formatter)
        flux_logger.addHandler(stream_handler)

    return flux_logger


def get_all_loggers() :
    ''' get all loggers in a list '''
    return [logging.getLogger(name) for name in logging.root.manager.loggerDict]


def set_level_all_loggers(level: str = "INFO") :
    ''' 
    set the level of all loggers to the given level 

    Args:
        level: the level string to set (-> DEBUG - INFO - ERROR)
    '''
    level = logging.getLevelName(level.upper())
    for logger in get_all_loggers() :
        logger.setLevel(level)


if __name__ == "__main__" :
    import os
    os.system("cls")

    log = get_logger()

    log.error("test")
