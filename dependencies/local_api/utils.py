# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

''' Local API utils '''

import json
import os

from typing import Union

try :
    import logger
except :
    from . import logger


log = logger.get_logger()


def read_json(file_path: str) -> dict :
    ''' 
    load a json file from specified (absolute) path and return the correspondant dictionnary

    Args:
        - file_path: the absolute path to the json file
    '''
    if not os.path.exists(file_path) :
        error = (f"json path does not exist ({file_path})")
        log.error(error)
        raise Exception(error)
    finalDict = open(file_path, "r")
    finalDict = json.load(finalDict)
    log.debug(f"json found and loaded (json_path : {file_path})")
    return finalDict


def write_json(to_write: dict, file_path: str, indent: int = 4) :
    ''' 
    load a json file from specified (absolute) path and return the correspondant dictionnary

    Args:
        - file_path: the absolute path to the json file
    '''
    if not os.path.exists(file_path) :
        error = (f"json path does not exist ({file_path})")
        log.error(error)
        raise Exception(error)
    if type(to_write) is not dict :
        error = f"the to_write variable is not a dict (to_write type : {type(to_write)})"
        log.error(error)
        raise Exception(error)
    
    to_write = json.dumps(to_write, indent=indent)
    file = open(file_path, 'w')
    file.write(to_write)
    file.close()
    log.debug(f"json written (json_path : {file_path})")
    return


def get_default_db(as_str: bool = True) -> Union[str, dict] :
    path = os.path.dirname(__file__)
    path = os.path.abspath(path)
    path += "\\db_default.json"
    if as_str :
        f = open(path, 'r')
        return f.read()
    return read_json(path)


def get_var_type(var_type: str) -> Union[dict, float, int, list, str] :
    ''' 
    Get the variable type corresponding to the given input 

    Args:
        - var_type: _Dict, _Float, _Int, _List or _Str
    '''
    correspondances = {
        "_Dict": dict,
        "_Float": float,
        "_Int": int,
        "_List": list,
        "_Str": str
        }
    if var_type not in correspondances :
        error = f"Given var_type ({var_type}) not supported.\nSupported var_type : "
        error += ", ".join(list(correspondances.keys()))
        log.error(error)
        raise Exception(error)
    return correspondances[var_type]


def check_entity(entity: str, schema: dict) -> str :
    '''
    ensure that entity is ok

    Args:
        - entity: the string to check
        - schema: the db schema with all entities
    
    Return:
        the formated entity (capitalized)
    '''
    entity_types = list(schema["entities"].keys())
    entity = entity.capitalize()
    if entity not in entity_types :
        error = f"The given entity is not supported (entity: {entity})\nSupported entities :\n"
        error += ", ".join(entity_types)
        log.error(error)
        raise Exception(error)
    return entity


def get_schema_entity(entity: str, schema: dict) -> dict :
    ''' get the DB schema for a given entity '''
    entity = check_entity(entity, schema)
    return schema["entities"][entity]


def check_parameters(entity: str, parameters: dict, schema: dict) -> list[str] :
    ''' 
    ensure that all needed parameters are there

    Args:
        - entity: the entity
        - parameters: the dict to check
        - schema: the db schema with all entities 

    Returns:
        - link_params
    '''
    entity_schema = get_schema_entity(entity, schema)
    needed_params = []
    link_params = []
    for param in entity_schema :
        if "creation" not in entity_schema[param] : continue
        if "needed" in entity_schema[param]["creation"] :
            needed_params.append(param)
        if "link" in entity_schema[param]["creation"] :
            if "needed" not in entity_schema[param]["creation"] :
                needed_params.append(param) #because link are needed
            link_params.append(param)

    for param in needed_params :
        if param not in parameters :
            error = f"You need to have all of these parameter keys in your parameters dictionnary : "
            error += ", ".join(needed_params)
            log.error(error)
            raise Exception(error)
        
    for param in parameters :
        param_type = get_var_type(entity_schema[param]["var_type"])
        if type(parameters[param]) is not param_type :
            error = f"The type of the parameter \"{param}\" should be {param_type} (not {type(parameters[param])})"
            log.error(error)
            raise Exception(error)
    return link_params