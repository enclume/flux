# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

''' Local API classes '''

import os

try :
    import utils
    import logger
except :
    from . import utils
    from . import logger


log = logger.get_logger()


class Json_Object :
    def __init__(self, file_path: str) :
        self.file_path = file_path
        self.obj = self.revert()

    def revert(self) -> dict :
        ''' revert the dictionnary to the on-disk one '''
        if not self.file_path :
            error = "file_path is none !"
            log.error(error)
            raise Exception(error)
        self.obj = utils.read_json(self.file_path)
        return self.obj

    def get(self) -> dict :
        ''' get the dictionnary '''
        if self.obj :
            return self.obj
        return self.revert()
    
    def set_obj(self, new_obj: dict) :
        ''' set the dictionnary '''
        if type(new_obj) is not dict :
            error = f"new_obj need to be a dict ! (new_obj type: {type(new_obj)})"
            log.error(error)
            return
        self.obj = new_obj
    
    def save(self, set_new_obj: dict = None) :
        ''' save the dictionnary on disk '''
        if set_new_obj :
            self.set_obj(set_new_obj)
        if not self.obj :
            error = "obj is none !"
            log.error(error)
            raise Exception(error)
        if not self.file_path :
            error = "file_path is none !"
            log.error(error)
            raise Exception(error)
        
        return utils.write_json(self.obj, self.file_path)

class Schema_Local(Json_Object) :
    def __init__(self) :
        super().__init__(self.get_schema_file_path())

    def get_schema_file_path(self) -> str :
        ''' get the schema.json file path '''
        schema_path = os.path.dirname(__file__)
        schema_path = os.path.abspath(schema_path)
        schema_path += "\\db_schema.json"
        return schema_path

class DB_Local(Json_Object) :
    def __init__(self, db_file_path):
        super().__init__(db_file_path)