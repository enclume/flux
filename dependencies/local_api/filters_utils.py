# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

''' Local API Filters Utils '''

try :
    import utils
    import logger
except :
    from . import utils
    from . import logger



log = logger.get_logger()


comparators = {"is" : "[\"is\" description]"}

def get_comparators_print_txt(with_descr: bool = True, print_txt: bool = False) -> str :
    txt = ["Comparators list :"]
    global comparators
    for key in comparators :
        line = f" - {key}"
        if with_descr :
            descr = comparators[key]
            line += f" => {descr}"

        txt.append(line)

    txt.append("")
    txt.append("add \"_not\" after the comparator to reverse the bool output")
    txt = "\n".join(txt)
    if print_txt :
        print(txt)
    return txt


def filter_compare_is(item: dict, field: str, value: str) -> bool :
    if field not in item or item[field] != value :
        return False
    return True


def filter_compare(item: dict, filter_list: list) -> bool :
    field = filter_list[0]
    value = filter_list[-1]
    comparator = filter_list[1]

    to_return = -1

    if "is" in comparator.split("_")[0] :
        to_return = filter_compare_is(item, field, value)
    

    if to_return == -1 :
        log.warning("bizarre, bizarre...")
        return False

    if "not" in comparator.split("_") :
        return not to_return
    return to_return


def check_filter(filter_list: list, entity: str = None) -> None :
    ''' see if the filter is right formated '''
    if len(filter_list) != 3 or type(filter_list) is not list :
        error = "the filter should be a list of 3 items"
        log.error(error)
        raise Exception(error)
    
    field = filter_list[0]
    value = filter_list[2]

    comparator = filter_list[1]
    global comparators
    if comparator.split("_")[0] not in comparators :
        error = f"The comparator (\"{comparator}\") is not supported.\n"
        error += get_comparators_print_txt()
        log.error(error)
        raise Exception(error)

    if not entity :
        return
    
    #entity related things
    utils.check_entity(entity)
    return


def process_filter(item: dict, filter_list: list) -> bool :
    ''' 
    process the filter for the current item 

    Args: 
        - item: the item dictionnary
        - filter_list: a list of 3 items [field, comparator, value] 
    '''
    check_filter(filter_list)

    return filter_compare(item, filter_list)
