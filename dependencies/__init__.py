# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

import os
import sys


def is_dep_in_path() -> bool :
    """
    check if the dependencies folder is present.

    Returns: 
        True if the dependencies folder is present.
    """
    all_paths = sys.path
    is_dep_in_path = False
    for path in all_paths:
        if "flux" in path and "dependencies" in path: is_dep_in_path = True

    return is_dep_in_path


def add_dep_in_path() -> str :
    ''' add the dependencies folder path in sys.path '''
    dependencies_folder = os.path.dirname(__file__)
    dependencies_folder = os.path.abspath(dependencies_folder)
    
    sys.path.append(dependencies_folder)
    return dependencies_folder