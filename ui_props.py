# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Flux Pipeline Manager

This define all UI parameters gives dynamically to context.scene to read/write data

'''
import bpy
import json
import os
from . import ui_operators
from . import ui
from . import blender_libs
from . import database_access
from . import prefs

def update_filter(self, context):
    """
    update filter ofr load list template_list
    """

    # print("upd")
    ui_operators.loadlist_clearfill()


#  when you change the cache parameter
def update_use_kitsu_cache(self, context):
    """
    switch for kitsu cache
    """
    flux_ui = context.scene.flux_ui
    if not flux_ui.use_kitsu : return

    if flux_ui.use_kitsu_cache:
        database_access.update_cache(True)
        print("Flux : enable cache")
    else:
        database_access.update_cache(False)
        print("Flux : disable and clear cache")


def update_composer_list(self, context):
    """
    update when checkbox is change from composer list
    """
    # print("update")
    # ---
    num_base = bpy.context.scene.flux_ui_composer.num_checked

    # get the number of item checked
    num = 0
    for my_item in context.scene.flux_ui_composer_list:
        if my_item.item_load: num += 1

    # det if there is more than before
    # if num>num_base:

    # dont check anymore if there is more or less, just check
    all_ok = True
    for my_item in context.scene.flux_ui_composer_list:
        if not my_item.item_load : continue

        if (not my_item.item_is_file_exist or 
            not my_item.item_is_collection_checked or 
            not my_item.item_is_collection_ok) :

            all_ok = False

    context.scene.flux_ui_composer.ok_for_load = all_ok

    # if there is 0
    if num == 0:
        context.scene.flux_ui_composer.at_least_one_checked = False
        context.scene.flux_ui_composer.ok_for_load = False

    # update cache
    bpy.context.scene.flux_ui_composer.num_checked = num


    # only_checked = context.scene.flux_ui_composer.show_only_checked
    # context.scene.flux_ui_composer.show_only_checked = False
    #
    # # finally update the cache
    # ui_operators.update_composer_list_copy()
    #
    # context.scene.flux_ui_composer.show_only_checked = only_checked


def update_link_check(self, context):
    """
    update when checkbox tag
    """

    # dont check anymore if there is more or less, just check
    for my_item in context.scene.flux_ui_link_list:
        # should be '!= not' but as not is not suppororted, '==' is use
        if blender_libs.collection.get_layer_collection(my_item.link_collection).exclude == my_item.is_collection_checked:
            blender_libs.collection.get_layer_collection(my_item.link_collection).exclude = not my_item.is_collection_checked


def update_link_vis(self, context):
    """
    update when visibility tag
    """
    # dont check anymore if there is more or less, just check
    for my_item in context.scene.flux_ui_link_list:
        # print(my_item.link_collection)
        # print(my_item.is_collection_checked)
        if bpy.data.collections[my_item.link_collection].hide_viewport == my_item.is_visible:
            bpy.data.collections[my_item.link_collection].hide_viewport = not my_item.is_visible


def update_composer_filter_checked(self, context):
    '''
    Swirtch values btw unchecked and cheked, part 1
    '''
    if context.scene.flux_ui_composer.show_only_checked:
        context.scene.flux_ui_composer.show_only_unchecked = False


def update_composer_filter_unchecked(self, context):
    '''
    Swirtch values btw unchecked and cheked, part 2
    '''
    if context.scene.flux_ui_composer.show_only_unchecked:
        context.scene.flux_ui_composer.show_only_checked = False


def update_linkeditor_filter_checked(self, context):
    '''
    Swirtch values btw unchecked and cheked, part 1
    '''
    if context.scene.flux_ui_linkeditor.show_only_checked:
        context.scene.flux_ui_linkeditor.show_only_unchecked = False


def update_linkeditor_filter_unchecked(self, context):
    '''
    Swirtch values btw unchecked and cheked, part 2
    '''
    if context.scene.flux_ui_linkeditor.show_only_unchecked:
        context.scene.flux_ui_linkeditor.show_only_checked = False


def update_linkeditor_filter_short(self, context):
    '''
    Swirtch values btw unchecked and cheked, part 1
    '''
    if context.scene.flux_ui_linkeditor.show_short:
        context.scene.flux_ui_linkeditor.show_file = False


def update_linkeditor_filter_file(self, context):
    '''
    Swirtch values btw unchecked and cheked, part 2
    '''
    if context.scene.flux_ui_linkeditor.show_file:
        context.scene.flux_ui_linkeditor.show_short = False


def update_linkeditor_list_index(self, context):
    """
    update link editor list index
    """
    all_linked = list(context.scene.flux_ui_link_list)
    if not all_linked : return

    # when update, select only if user choose it from the list. If notm it coming fron the GET action.
    current_linked = all_linked[context.scene.flux_ui_link_list_index]

    flux_ui_linkeditor = context.scene.flux_ui_linkeditor
    if not flux_ui_linkeditor.select_by_user :
        flux_ui_linkeditor.select_by_user = True
    elif current_linked.is_collection_checked :
        bpy.ops.flux.linkeditor_action(item=current_linked.link_collection, action="SELECT_REPLACE")


    flux_ui_linkeditor.is_proxy_silentswitch = True
    flux_ui_linkeditor.is_proxy = current_linked.is_proxy
    flux_ui_linkeditor.is_proxy_silentswitch = False

    # print(json.loads(prefs.get().proxy_level_prefs)[2])

    if prefs.get().proxy_is_multilevel:
        if current_linked.is_already_setted:
            flux_ui_linkeditor.proxy_level = current_linked.proxy_level
        else:
            flux_ui_linkeditor.proxy_level = int(json.loads(prefs.get().proxy_level_prefs)[2])

    print(f"Select : {current_linked.link_collection} / {current_linked.link_file} / {current_linked.is_link_ok}")

    # debug feedback.
    if prefs.get().debug_mode:
        all_items = list(current_linked.items())
        for item in all_items:
            print(f"{item[0]:21s}= {str(item[1])}")


def update_composer_loadtype(self, context):
    if context.scene.flux_ui_composer.load_type != "list":
        return
    
    # print("list")
    current_item = bpy.context.scene.flux_ui_composer.current_user_item
    # print(current_item)
    if current_item is None or current_item == "None": 
        return
    
    current_item = json.loads(current_item)
    if not current_item["type"] == "None":
        # print(current_item["type"])
        bpy.ops.flux.refresh_composer_list(hard_refresh=True)


def update_current_category(self, context):
    """
    update when you switch from assets to shots
    """
    ui.set_cache_force_check(True)
    # print("update")


def update_link_sel_num(self, context):

    num = 0
    at_least_one_false = False
    for my_item in context.scene.flux_ui_link_list :
        if not my_item.is_sel : continue
        num += 1
        is_this_proxy_available = ui.is_proxy_available_in_folder(my_item)

        if not is_this_proxy_available : at_least_one_false = True

    context.scene.flux_ui_linkeditor.is_sel_num = num
    context.scene.flux_ui_linkeditor.is_proxy_for_all_sel = not at_least_one_false


def update_checkfiles(self, context):
    context.scene.flux_ui_utils.itempreviews_no_update = True
    bpy.context.window_manager.previews_thumbnails = bpy.context.scene.flux_ui_previews_list[bpy.context.scene.flux_ui_previews_list_index].file_name
    context.scene.flux_ui_utils.itempreviews_no_update = False


def get_proxy_level_range(self):
    # return self["proxy_level_range"]
    return self.get('proxy_level_range', 0)

def set_proxy_level_range(self, value):
    range = json.loads(prefs.get().proxy_level_prefs)
    # print(f"{range=}")
    if value < range[0]:
        value = range[0]
    if value > range[1]:
        value = range[1]
    self["proxy_level_range"] = value

# print("read at prop")
# print(bpy.context.preferences.addons["flux"].preferences)

def update_linkeditor_multilevel(self, context):
    """
    Update when we switch is proxy from link editor
    We want an operation only if multilevel is OFF. If so we switch
    If ON se dont do anything.
    """
    # print("update_linkeditor_multilevel")
    if not prefs.get().proxy_is_multilevel and not context.scene.flux_ui_linkeditor.is_proxy_silentswitch:
        bpy.ops.flux.link_proxy_apply_settings()


class FLUX_UI_PROPS(bpy.types.PropertyGroup):
    '''
    Classe storing all important datas used for UI.
    All of them have to be passed from one file to another is case of loading a new file during the process
    '''

    # flux category
    #  this storage allows to have more than just asset/shot.
    #  category is what is display on top, and it's for now only two, but can be virtually any size
    current_category: bpy.props.EnumProperty(
        name="is asset or shot",
        items=[
            ("assets", "Assets", "Asset related tasks","FILE_3D",0),
            ("shots", "Shots", "Shot related tasks", "FILE_MOVIE",1),
        ],
        update=update_current_category
    )

    # PROXY attr
    # this is the dynamic prop for ui
    is_proxy: bpy.props.BoolProperty(name="proxy", default=False)
    # this is the dynamic prop for the saved file
    is_saved_as_proxy: bpy.props.BoolProperty(name="proxy", default=False)
    # enable and level proxy. This items is getted from prefs, but needed for ui
    enable_proxy: bpy.props.BoolProperty(name="enable_proxy", default=True)
    is_proxy_multilevel: bpy.props.BoolProperty(default=True, description="is proxy multilevel")
    proxy_level: bpy.props.IntProperty(default=0, min=0, max=5, get=get_proxy_level_range, set=set_proxy_level_range, description="proxy level")

    # exemple with get set. This is not user friendly yet, as any method could restric correctly the user with the min/max,
    # but re-set after reaching out values
    # proxy_level: bpy.props.IntProperty(default=0, min=0, get=get_int, set=set_intm description="proxy level, if applicable")

    # ============================================================
    # default description of the elements containers
    #  goal is to keep it modifiable as possible, also easly modifiable input by change attribut of this obj
    #  each line is a button, who refer to a category (asset/shot for inst.)
    #   category MUST BE in the current_category list
    #   first one (id = 0) MUST BE assets, or the item used for composer, whatever the name is
    items_description: bpy.props.StringProperty(name="items description",
                                                default = ('{"assets" : ["type", "name", "task"],'
                                                            ' "shots"  : ["seq",  "shot", "task"]}')
                                                 )

    # cache for current item in json style
    current_user_item: bpy.props.StringProperty(name="current selected asset", default="None")
    current_user_item_cache: bpy.props.StringProperty(name="current selected asset", default="None")

    # cache for current version
    current_version: bpy.props.StringProperty(name="current selected asset", default="None")

    # cache for current category
    current_category_cache : bpy.props.StringProperty(name="type of category cached", default="None")

    # path for last preview used
    item_preview_path : bpy.props.StringProperty(name="preview", default="None")

    # is item locked?
    is_item_locked : bpy.props.BoolProperty(name="is item locked", default=False)

    # kitsu/db related
    use_kitsu : bpy.props.BoolProperty(name="is kitsu used for writing data", default=True)
    use_kitsu_cache: bpy.props.BoolProperty(name="Use kitsu cache", default=True, update=update_use_kitsu_cache)


class FLUX_UI_UTILS_PROPS(bpy.types.PropertyGroup):
    '''
    Secondary props used by interface
    '''
    # force no reset
    no_reset : bpy.props.BoolProperty(name="is forced reset", default=False)

    file_path_for_item_cache: bpy.props.StringProperty(default="None")

    # cache for current item in json style
    load_list_filter: bpy.props.StringProperty(name="Filter", default="", update=update_filter)

    load_list_show_comments: bpy.props.BoolProperty(default=True, description="Show comments", update=update_filter)
    load_list_show_time: bpy.props.BoolProperty(default=False, description="Show time creation", update=update_filter)
    load_list_show_user: bpy.props.BoolProperty(default=False, description="Show user", update=update_filter)
    load_list_show_status: bpy.props.BoolProperty(default=False, description="Show status", update=update_filter)

    # color for load list
    load_list_color_v : bpy.props.FloatVectorProperty(name="Color v", subtype='COLOR', default=(0.2, 0.8, 0.2), min=0.0, max=1.0)
    load_list_color_r : bpy.props.FloatVectorProperty(name="Color R", subtype='COLOR', default=(0.2, 0.2, 0.8), min=0.0, max=1.0)
    load_list_color_w : bpy.props.FloatVectorProperty(name="Color W", subtype='COLOR', default=(0.8, 0.2, 0.2), min=0.0, max=1.0)

    # link_objsvis: bpy.props.StringProperty(name="objs vis", default="")
    itempreviews_no_update:bpy.props.BoolProperty(name="is forced reset", default=False)

    # show_offline_subpanel:bpy.props.BoolProperty(default=False)


class FLUX_UI_LINKEDIT_PROPS(bpy.types.PropertyGroup):
    edit_mode: bpy.props.EnumProperty(
        name="edit mode",
        items=[
            ("manip", "Manip", "Show manip/action buttons","RESTRICT_SELECT_OFF",0),
            ("status", "Status", "Show status", "EXPERIMENTAL",1),
            ("none", "", "Show nothing", "X",2),
        ],
        update=update_current_category
    )

    is_sel_num:  bpy.props.IntProperty(name="is sel num", default=0)
    is_proxy_for_all_sel:  bpy.props.BoolProperty(default=False)
    is_proxy_list:  bpy.props.StringProperty(default="")

    # too much infos?
    # show_sidebar: bpy.props.BoolProperty(default=True)

    show_only_unchecked:  bpy.props.BoolProperty(default=False, description="Show only unchecked link", update = update_linkeditor_filter_unchecked)
    show_only_checked:  bpy.props.BoolProperty(default=False, description="Show only checked link", update = update_linkeditor_filter_checked)

    show_short:  bpy.props.BoolProperty(default=False, description="Show short name", update = update_linkeditor_filter_short)
    show_file:  bpy.props.BoolProperty(default=False, description="Show file name", update = update_linkeditor_filter_file)

    select_by_user:  bpy.props.BoolProperty(default=True)

    isolate_objsvis: bpy.props.StringProperty(name="objs vis", default="")
    isolate_current: bpy.props.StringProperty(name="objs vis", default="")

    is_proxy:  bpy.props.BoolProperty(default=False, description="is current link a Proxy?", update=update_linkeditor_multilevel)
    is_proxy_silentswitch:  bpy.props.BoolProperty(default=False, description="Ignore udpate of is proxy when true")
    proxy_level:  bpy.props.IntProperty(description="which level is the proxy?", default=0, min=0, max=5, get=get_proxy_level_range, set=set_proxy_level_range)

    show_proxy_panel: bpy.props.BoolProperty(default=True)
    show_convert_to_shot_panel: bpy.props.BoolProperty(default=False)
    show_edit_link: bpy.props.BoolProperty(default=False)

    # persisnet infos
    shotlevel_infos: bpy.props.StringProperty(default="")

    editlink_action: bpy.props.EnumProperty(
        name="editlink_action",
        default = "nothing",
        items=[
            ("nothing", "Do Nothing", "Just open Link and do nothing", "X", 0),
            ("keep", "Keep Sel", "open and edit link : erase unselected objects", "FAKE_USER_ON", 1),
            ("remove", "Remove Sel", "open and edit link : erase selected objects", "TRASH", 2),
            ]
    )

class FLUX_UI_COMPO_PROPS(bpy.types.PropertyGroup):
    '''
    Classe storing all important datas used for UI, specific to composer
    '''

    # cache for current item in json style
    current_user_item: bpy.props.StringProperty(name="current selected asset", default="None")

    # cache for current version
    current_version: bpy.props.StringProperty(name="current selected asset", default="None")

    # ui_link
    ui_link_lib_override: bpy.props.BoolProperty(default=True)

    #
    load_type: bpy.props.EnumProperty(
        name="is list or one",
        default="one",
        update=update_composer_loadtype,
        items=[
            ("list", "List", "Load from list","COLLAPSEMENU",0),
            ("one", "One", "Load from single choice", "REC",1),
        ]
    )

    # filter show
    show_only_checked: bpy.props.BoolProperty(default=False, description="Show only checked item", update = update_composer_filter_checked)
    show_only_heart: bpy.props.BoolProperty(default=False, description="Show only existing item")
    show_only_funded: bpy.props.BoolProperty(default=False, description="Show only item with final version present on disk")

    show_only_question: bpy.props.BoolProperty(default=False, description="Show only unknowns item")
    show_only_broken: bpy.props.BoolProperty(default=False, description="Show only broken item")
    show_only_unchecked: bpy.props.BoolProperty(default=False, description="Show only unchecked item", update = update_composer_filter_unchecked)

    #
    ok_for_load: bpy.props.BoolProperty(default=False)
    at_least_one_checked: bpy.props.BoolProperty(default=False)

    num_checked: bpy.props.IntProperty(default=0)


class FLUX_UI_COMPO_LIST_PROPS(bpy.types.PropertyGroup):
    '''
    Classe storing all important datas used for UI, specific to composer
    '''

    item_name: bpy.props.StringProperty(name="item name", default="", description="name of the item")
    item_is_collection_checked: bpy.props.BoolProperty(default=False, description="is collection was checked for validity?")
    item_is_collection_ok: bpy.props.BoolProperty(default=False, description="is collection checked and valid?")
    item_is_file_exist: bpy.props.BoolProperty(default=False, description="is file exist?")
    item_load: bpy.props.BoolProperty(default=False, update=update_composer_list, description="is item marked as for append/link?")


class FLUX_UI_PREVIZ_TO_SHOT_LIST(bpy.types.PropertyGroup):
    '''
    all file list for previz to shot util
    '''

    shot_name: bpy.props.StringProperty(name="item name", default="", description="name")
    shot_is_sel: bpy.props.BoolProperty(default=True, description="is sel?")
    shot_is_already_exist: bpy.props.BoolProperty(default=True, description="is exist?")
    shot_path: bpy.props.StringProperty(default="", description="path")
    shot_base_path: bpy.props.StringProperty(default="", description="path")
    file_version: bpy.props.StringProperty(name="version", default="", description="version")
    shot_item: bpy.props.StringProperty(name="shot item", default="", description="version")


class FLUX_UI_PREVIEWS_LIST_PROPS(bpy.types.PropertyGroup):
    '''
    Classe storing all important datas used for UI, specific to composer
    '''

    file_name: bpy.props.StringProperty(name="item name", default="", description="name of the item")
    file_path: bpy.props.StringProperty(name="item name", default="", description="name of the item")
    is_sel: bpy.props.BoolProperty(default=True)


class FLUX_UI_LINK_LIST_PROPS(bpy.types.PropertyGroup):
    '''
    Classe storing all important datas used for UI, specific to composer
    '''

    # link_name: bpy.props.StringProperty(name="item name", default="", description="name of the item")
    # todo add link path orig. If not the proxy path will take the path on the next load (and not on the moment itself)
    link_path: bpy.props.StringProperty(name="item path", default="", description="path of the item, proxy or not")
    link_file: bpy.props.StringProperty(name="item file", default="", description="file of the item")
    link_collection: bpy.props.StringProperty(name="item collection", default="", description="collection of the item")
    link_shortname: bpy.props.StringProperty(name="item short name", default="", description="short name")
    is_link_ok: bpy.props.BoolProperty(default=True, description="is collection ok?")
    is_collection_checked: bpy.props.BoolProperty(default=True, description="is collection checked?", update=update_link_check)
    is_visible: bpy.props.BoolProperty(default=True, description="is collection visible?", update=update_link_vis)
    is_sel: bpy.props.BoolProperty(default=False, description="If selected, actions are available for all selected links.", update=update_link_sel_num)

    # proxy datas
    is_proxy: bpy.props.BoolProperty(default=False, description="is proxy?")
    is_shot_level: bpy.props.BoolProperty(default=False, description="is shot level?")
    proxy_level: bpy.props.IntProperty(default=0, description="proxy level?")
    link_path_orig: bpy.props.StringProperty(name="item path", default="", description="path of the item, before beeing proxy")
    link_file_orig: bpy.props.StringProperty(name="item path", default="", description="file of the item, before beeing proxy")
    is_already_setted: bpy.props.BoolProperty(default=False, description="use to put default level. Set to true one per session, never set to false.")


class FLUX_UI_COMPO_LISTCOPY_PROPS(bpy.types.PropertyGroup):
    '''
    Classe storing all important datas used for UI, specific to composer
    '''

    item_name: bpy.props.StringProperty(name="item name", default="", description="name of the item")
    item_is_collection_checked: bpy.props.BoolProperty(default=False, description="is collection was checked for validity?")
    item_is_collection_ok: bpy.props.BoolProperty(default=False, description="is collection checked and valid?")
    item_is_file_exist: bpy.props.BoolProperty(default=False, description="is file exist?")
    item_load: bpy.props.BoolProperty(default=False, update=update_composer_list, description="is item marked as for append/link?")


class FLUX_UI_SC_PROPS(bpy.types.PropertyGroup):
    '''
    Store the sanity check result
    '''

    # cache for current item in json style
    is_clean: bpy.props.BoolProperty(name="", default=False)


class FLUX_UI_LoadListPropertyGroup(bpy.types.PropertyGroup):
    '''
    Used to store data for the load list dialog. have to be stored later in collectionProp
    '''
    version: bpy.props.StringProperty()
    version_int: bpy.props.StringProperty()
    comment: bpy.props.StringProperty()
    type: bpy.props.StringProperty()


class FLUX_UI_SCListPropertyGroup(bpy.types.PropertyGroup):
    '''
    Used to store data for the load list dialog. have to be stored later in collectionProp
    '''
    name: bpy.props.StringProperty()
    description: bpy.props.StringProperty(default="no description")
    report: bpy.props.StringProperty(default="to check")
    filters: bpy.props.StringProperty(default="{}")
    statut: bpy.props.BoolProperty(default=True)


# register

classes = (
    FLUX_UI_PROPS,
    FLUX_UI_UTILS_PROPS,
    FLUX_UI_SC_PROPS,
    FLUX_UI_COMPO_PROPS,
    FLUX_UI_LINKEDIT_PROPS,
    FLUX_UI_COMPO_LIST_PROPS,
    FLUX_UI_PREVIZ_TO_SHOT_LIST,
    FLUX_UI_PREVIEWS_LIST_PROPS,
    FLUX_UI_COMPO_LISTCOPY_PROPS,
    FLUX_UI_LINK_LIST_PROPS,
    FLUX_UI_LoadListPropertyGroup,
    FLUX_UI_SCListPropertyGroup,
)

# todo move that in tools panel. rename also the def
def update_previews_thumbnail(self, context):
    if context.scene.flux_ui_utils.itempreviews_no_update:
        return
    
    sel = bpy.context.window_manager.previews_thumbnails

    all_files = bpy.context.scene.flux_ui_previews_list
    for i, each in enumerate(all_files):
        if each.file_name == sel:
            bpy.context.scene.flux_ui_previews_list_index = i


# todo move that in tools panel. rename also the def
def enum_previews_from_checkfilelist(self, context):

    all_files = bpy.context.scene.flux_ui_previews_list

    pcoll = preview_collections["main"]
    enum_items = []

    for i, each in enumerate(all_files):
        # generates a thumbnail preview for a file.
        filepath = each.file_path
        name = os.path.basename(filepath)
        icon = pcoll.get(name)
        if not icon:
            if ".png" in name:
                thumb = pcoll.load(name, filepath, 'IMAGE')
            if ".mp4" in name:
                thumb = pcoll.load(name, filepath, 'MOVIE')
        else:
            thumb = pcoll[name]
        enum_items.append((name, name, "", thumb.icon_id, i))

    pcoll.previews_thumbnails = enum_items
    # pcoll.previews_thumbnails_dir = directory
    return pcoll.previews_thumbnails


preview_collections = {}
def register():
    import bpy.utils.previews
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)
    # register usual props
    bpy.types.Scene.flux_ui = bpy.props.PointerProperty(type=FLUX_UI_PROPS)
    bpy.types.Scene.flux_ui_utils = bpy.props.PointerProperty(type=FLUX_UI_UTILS_PROPS)
    bpy.types.Scene.flux_ui_loadList = bpy.props.CollectionProperty(type=FLUX_UI_LoadListPropertyGroup)
    # composer
    bpy.types.Scene.flux_ui_composer = bpy.props.PointerProperty(type=FLUX_UI_COMPO_PROPS)
    # link editor
    bpy.types.Scene.flux_ui_linkeditor = bpy.props.PointerProperty(type=FLUX_UI_LINKEDIT_PROPS)
    # sc
    bpy.types.Scene.flux_ui_sc = bpy.props.PointerProperty(type=FLUX_UI_SC_PROPS)
    # composer list / tenplate_list builder
    bpy.types.Scene.flux_ui_composer_list = bpy.props.CollectionProperty(type=FLUX_UI_COMPO_LIST_PROPS)
    bpy.types.Scene.flux_ui_composercopy_list = bpy.props.CollectionProperty(type=FLUX_UI_COMPO_LISTCOPY_PROPS)

    # previz to shot list
    bpy.types.Scene.flux_ui_tool_previz_to_shot_list = bpy.props.CollectionProperty(type=FLUX_UI_PREVIZ_TO_SHOT_LIST)

    bpy.types.Scene.flux_ui_previews_list = bpy.props.CollectionProperty(type=FLUX_UI_PREVIEWS_LIST_PROPS)

    bpy.types.Scene.flux_ui_scList = bpy.props.CollectionProperty(type=FLUX_UI_SCListPropertyGroup)
    
    # add recorded for load list index and composer
    bpy.types.Scene.flux_ui_tool_previz_to_shot_list_index = bpy.props.IntProperty()

    bpy.types.Scene.flux_ui_load_list_index = bpy.props.IntProperty()
    bpy.types.Scene.flux_ui_composer_list_index = bpy.props.IntProperty(description="Item to load(s). red heart = ok for load / hear = file exist but unchecked/bad, broken heart = file doesnt exist")
    bpy.types.Scene.flux_ui_previews_list_index = bpy.props.IntProperty(description="", update=update_checkfiles)
    bpy.types.Scene.flux_ui_composerload_list_index = bpy.props.IntProperty()
    bpy.types.Scene.flux_ui_sc_list_index = bpy.props.IntProperty()

    bpy.types.Scene.flux_ui_link_list = bpy.props.CollectionProperty(type=FLUX_UI_LINK_LIST_PROPS)
    bpy.types.Scene.flux_ui_link_list_index = bpy.props.IntProperty(update=update_linkeditor_list_index)

    # start preview-icons fonction
    bpy.types.WindowManager.previews_thumbnails = bpy.props.EnumProperty(
        items=enum_previews_from_checkfilelist,
        update=update_previews_thumbnail
    )
    pcoll = bpy.utils.previews.new()
    # pcoll.previews_thumbnails_dir = ""
    pcoll.previews_thumbnails = ()

    preview_collections["main"] = pcoll
    # end preview-icons fonction


def unregister():
    # start preview-icons fonction
    del bpy.types.WindowManager.previews_thumbnails
    for pcoll in preview_collections.values():
        bpy.utils.previews.remove(pcoll)
    preview_collections.clear()
    # end preview-icons fonction

    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)

    del bpy.types.Scene.flux_ui
    del bpy.types.Scene.flux_ui_utils
    del bpy.types.Scene.flux_ui_loadList
    del bpy.types.Scene.flux_ui_scList