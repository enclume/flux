# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Flux Pipeline Manager

high level proxy bridge config <> ui

Pour les beaux yeaux de Guillaume, qui veut pas de bpy ds ses affaires.
'''
import os

from . import config
from . import prefs

def get_from_item_path(item_path: str, level=0, check=False):
    """
    get proxy path from item path, reading preferences from disc
    Args:
        item_path: normal item path
        level: level, if applicable

    Returns: proxy path

    """
    # prefs.get().proxy_enable
    # prefs.get().proxy_is_multilevel
    # prefs.get().proxy_tag
    # prefs.get().proxy_level_prefs

    proxy_path = config.path.get_proxy_from_item_path(item_path,
                                         is_proxy_multilevel=prefs.get().proxy_is_multilevel,
                                         proxy_level=level,
                                         proxy_part=prefs.get().proxy_tag)

    if check:
        if not os.path.exists(proxy_path): return False

    return proxy_path


def is_proxy_from_item_path(item_path):
    result = config.path.is_proxy_from_item_path(item_path=item_path,
                                        proxy_part=prefs.get().proxy_tag)

    # is_proxy_multilevel = prefs.get().proxy_is_multilevel,

    return result