# Flux Pipeline Manager
## Changelog
Addon for blender 3.6.
(c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

## [0.8.3] - 2023-12-19

### fixes
- minor bug fixes and stability.
- Last release before dev pause. Last polish.

## [0.8.2] - 2023-12-19

### fixes
- bug fixes, stability, deep tests and corrections.

## [0.8.1] - 2023-12-13

### added
- Tool : delete unsel cams

### fixes
- bug fixes, stability

## [0.8.0] - 2023-12-12

### added
- Proxy enable
- Proxy total workflow support
- Shotgrid support on top of kitsu
- Improve prefs
- Documentation

### fixes
- lots of bug fixes...


## [0.7.0] - 2023-10-31

### added
- mew UI for choose ui panels
- Link Editor first launch! 
  - list all link collection in the scene
  - allow to select, isolate, reload each link
  - allow to detect if link is in the pipeline, and wich item it is
  - get link from selection
  - switch visibility and check
- offline operation (publish and new multiple previews) moved to main UI
- standalone feature : launch standalone_script_exe.py

### fixes
- bug fixes with new preview workflow

## [0.6.17] - 2023-10-30

### fixes
- bug fixes

## [0.6.16] - 2023-10-30

### fixes
- bug fixes

## [0.6.15] - 2023-10-26

### added
- New workflow to add comment/previews after publish/reiew
- New workflow to add mitliple previews

## [0.6.14] - 2023-10-26

### fixes
- bug fixes

## [0.6.13] - 2023-10-26

### added
- Geo node toggle BB
- proxy code ok

## [0.6.12] - 2023-10-25

### added
- Batch Tool (standalone)

## [0.6.9] - 2023-10-12

### fixed
- Various Fix bugs

## [0.6.9] - 2023-10-12

### fixed
- Fix bugs on layout tool

## [0.6.8] - 2023-10-11

### added
- Layout render and export, alembic export

## [0.6.7] - 2023-10-11

### added
- Allow Playblast preview range 

## [0.6.6] - 2023-10-10

### fixed
- minor bug fix 

## [0.6.5] - 2023-10-10

### added
- add publish offline

### fixed
- review/publish save also current wip, avoid time different btw wip/review/publish

## [0.6.4] - 2023-10-09

### added
- add clean layout render and SC animatic

### fixed
- good naming for clean layout render file
- Update SC-aqnimatic, animatic switcher

## [0.6.3] - 2023-10-05

### fixed
-  fix playblast context issue

## [0.6.2] - 2023-10-01

### fixed
-  fix bookmarks bugs

## [0.6.1] - 2023-10-01

### fixed
-  fix bookmarks bugs

## [0.6.0] - 2023-10-01

### added
-  UI : bookmarks feature for item/composer
-  Sanity check operational for most of the tasks. Miss topology tasks
-  Bake scatter final and operational

### fixed
-  fix playblast context issue

## [0.5.27] - 2023-09-27

### added
- track revision for publish


## [0.5.26] - 2023-09-27

### added
- sc visibility : add driver detection
- video for publish

### fixed
- Correct bug : revision + preview + artist level at kitsu = error

## [0.5.25] - 2023-09-25 

### added
- sanity : add autofix for animatic
- sanity : add pop up ability when cannot autofix
- sanity : apply it to animatic

## [0.5.24] - 2023-09-20, 

### fixed
- bugfix with animatic on shot with new files

### added
- Increment for playblast

## [0.5.23] - 2023-09-20, 

### fixed
- stability general

## [0.5.22] - 2023-09-20, 

### improved
- UI sanity check

## [0.5.21] - 2023-09-19, 

### added
- sanity check open!

## [0.5.20] - 2023-09-19, 

### fixed
- fix get_versionning_tree remove_empty

## [0.5.19] - 2023-09-17, 

### fixed
- fix bug in composer when new scene. Still to improve.

## [0.5.18] - 2023-09-15, 

### fixed
- startup : fix the get colormanagement path methode and add path verification

## [0.5.17] - 2023-09-14, 

### fixed
- playblast render : fix playblast crop issue

## [0.5.16] - 2023-09-14, 

### fixed
- bug fixes with all status from kitsu now correctly handled

## [0.5.15] - 2023-09-14, 

### Changed
- add force confo
- sanity check : add render params to check

## [0.5.14] - 2023-09-13, 

### Added
- sanity check : layout check functions

### fixed 
- UI bugfixes regression
- on server ID detection

## [0.5.13] - 2023-09-13, 

### fixed 
- UI bugfixes regression

## [0.5.12] - 2023-09-13, 

### Changed
- bakescatter : expandable UI
- UI: wip+1 enable also outside pipeline 
### fixed
- UI : comment stability improved
- Composer : better catch of possibilities with current_item unfill by user

## [0.5.11] - 2023-09-12, 

### added
- auto ACES switcher

### fixed
- fix post opengl render to avoid double color transformation

## [0.5.10] - 2023-09-12, 

### fixed
- fix animatic library path in animatic switcher

## [0.5.9] - 2023-09-12, 

### Added
- tools : baker scatter operational
- prefsf : add the ability to check/uncheck the check update at startup

## [0.5.8] - 2023-09-12, 

### fixed
- fix startup

## [0.5.7] - 2023-09-12, 

### fixed
- fix startup

## [0.5.6] - 2023-09-10, 

### added
- startup : add colormanagement check
- item operators : add item_op_open_blend_folder

### Fixed
- composer : bugfixes
### wip
- tools : baker scatter

## [0.5.5] - 2023-09-08, 

### fixed
- multiple little fixes

## [0.5.4] - 2023-09-08, 

### added
- startup

### fixed
- ID methode fix

## [0.5.3] - 2023-09-08, 

### internal
code cleaning and stability

## [0.5.2] - 2023-09-05, 

### Fixed
composer: stability, bug fixes
### internal
code cleaning, 

## [0.5.1] - 2023-09-05, 

### Fixed
composer: stability, bug fixes
### internal
code cleaning, stability

## [0.5.0] - 2023-09-01, 

### Added
- Composer : choose from list for batch process of import
- auto fix master collection

### wip
- Sanity Check : work and cleaning

## [0.4.24] - 2023-08-23, 

### Fixed
- regression flux+kitsu+revision playblast

## [0.4.23] - 2023-08-23, 

### Fixed
- regression with save/wip+1, revision, no wip

### added
- Sanity Check : add sc_check_master_collection
- Sanity Check : add check_report option
- Sanity Check : add auto fix handling

## [0.4.22] - 2023-08-??, 

### added
- Sanity Check : add update_final_is_clean
- Sanity Check : Add bpy.types.Scene.flux_ui_sc.is_clean for sanity check
- Sanity Check : add filters feature

### fixed
- fix startup add_all_check_elements log error
- fix _temp_ error

## [0.4.21] - 2023-08-??, 

### fixed
- bug fixes

### added
- Sanity Check : add filter based on current item

## [0.4.20] - 2023-08-??, 

### fixed
- bug fixes

## [0.4.19] - 2023-08-??, 

### fixed
- bug fixes

### added
- Load List : Add filter with user, time, status 

## [0.4.18] - 2023-08-??, 

### added
- First open Beta with comment on local/kitsu
- flux playblast : add openfile property
- flux playblast : improvement

### wip
- sanity check

## [0.4.13] - 2023-08-10

This summaries also previous changes made

### Added

- Better tools turnable
- GeoNode frustum

- UI more clear when new file from current
- save as allow you to start from scratch

### Fixed

- UI with no more lag

## [0.3.5] - 2023-07-05

new usable version, end of checkpoint for july

### Added

- Scene Composer usable in vanilla style
- Report bug

### Fixed

- Status ok on publish
- list of comment on load list : only last comment display
- Lot of bug fixes


## [0.3.0] - 2023-06-29

New release for demo

### Added

- Turn Table Process for Asset
- Setup New Item (Modeling, surfacing, etc...)
- New Tools Windows framework

### Fixed

- A lot of stuffs


## [0.2.2] - 2023-06-28

Fix release.

### Fixed
- UI : work with all special cases of loading (wip older than reviews, no wip present, etc...)

## [0.2.0] - 2023-06-23

First Working release.
All basic features for kitsu communication/file system core/item read and save

## [0.0.0] - yyyy-mm-dd

Template

### Added

### Fixed

### Changed

### Removed
