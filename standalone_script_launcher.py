''' executable point for standalone scripts, usefull to have here for relatives imports of flux libs '''

import os
import standalone_scripts as std_scripts


if __name__ == "__main__" :
    os.system("cls")


std_manager = std_scripts.standalone_scripts_manager()

#std_manager.register("test_std_script")
std_manager.register("batch_blender_modif", "batch blender modif", True)
std_manager.register("get_published_items", "get published items", True) 
std_manager.register("local_DB_editor", "local DB editor", True) 

std_manager.draw() 