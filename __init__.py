# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

bl_info = {
    "name" : "Flux",
    "author" : "Paul Jadoul, Cédric Nicolas, Guillaume Geelen",
    "description" : "",
    "blender" : (3, 6, 0),
    "version" : (0, 8, 3),
    "location" : "View3D",
    # "warning" : "This is an alpha (but working) of mutli project. Dont deploy to rufus for now.",
    "category" : "Pipeline",
}

import bpy

from . import database_access
# database_access.db_access_startup()

# ---------------
# importer casual
from . import addon_updater_ops
import inspect
import glob
import os
# ----

# importer front end
from . import ui_operators
from . import ui
#from .items_operators import op_bake_scatter
from . import prepare_item
from . import ui_items_operators_call
from .sanity_check_operators import ui_SCops
from . import prefs
from . import startup
from . import avoid_spam_cls
from . import config
from . import ui_edit_link
# set logger
from . import flux_logger
flux_logger.set_level_all_loggers("INFO")
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(ui_edit_link)
importlib.reload(flux_logger)

importlib.reload(ui_operators)
importlib.reload(ui)
importlib.reload(prepare_item)
importlib.reload(ui_items_operators_call)
importlib.reload(ui_SCops)
importlib.reload(prefs)
importlib.reload(startup)
importlib.reload(avoid_spam_cls)
importlib.reload(config)
#importlib.reload(op_bake_scatter)

# to change lace to have dyn max slider of proxies
from . import ui_props
importlib.reload(ui_props)

def update_backend_debug(self, context):
    if prefs.get().debug_mode:
        prefs.get().debug_frontend_mode = True
        level = prefs.get().debug_backend_mode
        print("update logger to ", level)
        flux_logger.set_level_all_loggers(level)
    else:
        prefs.get().debug_frontend_mode = False
        prefs.get().debug_backend_mode = "ERROR"

tprint = avoid_spam_cls.timed_print()

# class FLUX_Item_Bookmarks(bpy.types.PropertyGroup):
#     # Settings
#     item: bpy.props.StringProperty(default="")


def get_projects_list_enum(self, context):
    # list = ["rufus", "rufus_test"]
    config_path_root = bpy.utils.script_path_user() + '/addons/flux/project_config'
    end_file = "_config.json"
    config_path_root = config_path_root + "/*" + end_file
    all_files = glob.glob(config_path_root)
    # print(all_files)

    list = []
    for each in all_files:
        file = os.path.basename(each)
        list.append(file.replace(end_file, ""))

    enum_items = []
    for i, each in enumerate(list):
        enum_items.append((each, each.capitalize(), "", "FILE_MOVIE", i))
    # print(enum_items)
    return enum_items

def update_project_infos_db(self, context):
    """
    Update project db infos.
    This moduiles are needed in other part of the code, so they are externalised
    """
    ui_operators.update_project_infos_db(self, context)


def update_project_custom_db(self, context):

    if prefs.get().db_choice == "custom":
        prefs.get().db_custom_host = prefs.get().db_host
        prefs.get().db_custom_type = prefs.get().db_type
        config.set_project_db(prefs.get().db_host, prefs.get().db_type)

def update_project_infos(self, context):
    """
    Update project name and root infos.
    This moduiles are needed in other part of the code, so they are externalised
    """

    ui_operators.update_project_infos(self, context)


def get_enum_db_type():

    all_types = database_access.get_supported_db_api()

    result = []
    for i, each in enumerate(all_types):
        result.append((each, each.capitalize(), "", "", i))
    return result


#PREFERENCES
class FLUX_Preferences(bpy.types.AddonPreferences):
    bl_idname = __package__

    is_db_logged: bpy.props.BoolProperty(
        default=False,
        description="is logged"
    )

    show_composer: bpy.props.BoolProperty(
        default=False,
        description="Show Composer or not"
    )

    show_sanity_check: bpy.props.BoolProperty(
        default=False,
        description="Show sanity check or not"
    )

    show_linkeditor: bpy.props.BoolProperty(
        default=False,
        description="Show Link editor or not"
    )

    show_tools: bpy.props.BoolProperty(
        default=True,
        description="Show tools"
    )

    check_linkeditor_atstart: bpy.props.BoolProperty(
        default=True,
        description="Show Link editor or not"
    )

    Use_ui_cache: bpy.props.BoolProperty(
        default=True,
        description="use cache for storing disk acces. Safer and faster for the UI, but slower to update disk change"
    )

    debug_mode: bpy.props.BoolProperty(
        default=False,
        description="print debug messages",
        update = update_backend_debug
    )

    debug_frontend_mode: bpy.props.BoolProperty(
        default=False,
        description="print debug messages for Front-End"
    )

    debug_backend_mode: bpy.props.EnumProperty(
        items=(
            ('DEBUG', "DEBUG", ""),
            ('INFO', "INFO", ""),
            ('ERROR', "ERROR", ""),
            )
        ,default="DEBUG"
        ,description="print debug messages for Back-End"
        ,update=update_backend_debug
    )

    # login vars start
    db_auto_connect: bpy.props.BoolProperty(
        default=False,
    )

    db_custom_host: bpy.props.StringProperty(
        name="Custom Host",
        default="",
    )
    db_custom_type: bpy.props.StringProperty(
        name="Custom Type",
        default="",
    )

    db_host: bpy.props.StringProperty(
        name="Host",
        default="",
        update=update_project_custom_db,
    )

    db_type: bpy.props.EnumProperty(
        name="Host Type",
        items=[("kitsu", "Kitsu", "", "", 0),
               ("shotgrid", "Shotgrid", "", "", 1)],
        update=update_project_custom_db,
    )
    # items = get_enum_db_type()

    db_choice: bpy.props.EnumProperty(
        name="Db choice",
        items=[("main", "Main", "", "EVENT_A", 0),
               ("alternative", "Alternative", "", "EVENT_B", 1),
               ("custom", "Custom", "", "EVENT_C", 2)],
        update = update_project_infos_db
    )

    db_email: bpy.props.StringProperty(
        name="Email",
        default="",
    )

    # total name, in all format. full name is not recorded everywhere as is in Kitsu
    db_fullname: bpy.props.StringProperty(
        name="full_name",
        default="",
    )

    db_firstname: bpy.props.StringProperty(
        name="full_name",
        default="",
    )

    db_lastname: bpy.props.StringProperty(
        name="full_name",
        default="",
    )
    # --- end of total name

    # todo add keyring/bcrypt support
    db_password: bpy.props.StringProperty(
        name="Password", default="", options={"HIDDEN", "SKIP_SAVE"}, subtype="PASSWORD"
    )
    # login vars end
    #

    project_file_type: bpy.props.EnumProperty(
        items=[("internal", "Internal", "", "APPEND_BLEND", 0),("remote", "Remote", "", "FILEBROWSER", 1)],
        update = update_project_infos
    )

    project_name: bpy.props.StringProperty(
        name="Project Name (full/db)",
        default="None",
    )

    project_name_short: bpy.props.StringProperty(
        name="Project Name (short)",
        default="None",
    )

    projects_list: bpy.props.EnumProperty(
        items=get_projects_list_enum,
        update=update_project_infos,
    )

    production_settings_file: bpy.props.StringProperty(
        name="Production Settings File",
        subtype= "FILE_PATH",
        update=update_project_infos
    )

    # projects_list: bpy.props.EnumProperty(
    #     items=(
    #         ('UP', "Up", ""),
    #         ('DOWN', "Down", ""),
    #         ('REMOVE', "Remove", ""),
    #         ('ADD', "Add", "")),
    # )

    project_path_root: bpy.props.StringProperty(
        default="None",
        subtype="FILE_PATH",
    )

    root_ok: bpy.props.BoolProperty(
        default=False,
    )

    wip_comment_local: bpy.props.BoolProperty(
        default=True,
        description = "Read/Write comment on Local drive instead of Db. Otherwise write on Kitsu Db"
    )

    check_update_startup: bpy.props.BoolProperty(
        default=True,
        description = "Check if there is new version of flux at startup"
    )

    check_color_management: bpy.props.BoolProperty(
        default=True,
        description = "Check Color management at startup"
    )

    filter_task_item: bpy.props.BoolProperty(
        default=True,
        description = "Filter item from database to folder. Hide db task that doesnt have correspondance with folders"
    )

    # bookmarks
    # bookmarks: bpy.props.CollectionProperty(
    #     type=FLUX_Item_Bookmarks
    # )
    item_bookmarks: bpy.props.StringProperty(
        default="[]"
    )

    composer_bookmarks: bpy.props.StringProperty(
        default="[]"
    )

    # --------------------
    #PROXY PREFERENCES
    proxy_enable: bpy.props.BoolProperty(default = True)
    proxy_tag: bpy.props.StringProperty(default = "PRX")
    proxy_is_multilevel: bpy.props.BoolProperty(default = True)
    proxy_level_prefs: bpy.props.StringProperty(default = "[0,2,2]")

    # dev options
    # show_dev_options : bpy.props.BoolProperty(default = False)
    # dev_options_refresh_edit_link_settings : bpy.props.BoolProperty(default = False)

    # --------------------
    #ADDON UPDATER PREFERENCES
    auto_check_update : bpy.props.BoolProperty(
        name = "Auto-check for Update",
        description = "If enabled, auto-check for updates using an interval",
        default = False,
    )

    updater_interval_months : bpy.props.IntProperty(
        name='Months',
        description = "Number of months between checking for updates",
        default=0,
        min=0
    )
    updater_interval_days : bpy.props.IntProperty(
        name='Days',
        description = "Number of days between checking for updates",
        default=7,
        min=0,
    )
    updater_interval_hours : bpy.props.IntProperty(
        name='Hours',
        description = "Number of hours between checking for updates",
        default=0,
        min=0,
        max=23
    )
    updater_interval_minutes : bpy.props.IntProperty(
        name='Minutes',
        description = "Number of minutes between checking for updates",
        default=0,
        min=0,
        max=59
    )




    def draw(self, context):
        # --------------
        # Login.
        # --------------

        layout = self.layout

        col = layout.row()
        # help on proxy
        op = col.operator(ui.FLUX_OP_Help.bl_idname, text="Help on Prefs", icon="QUESTION", emboss=True)
        op.what = "INSTALL"

        op = col.operator(ui.FLUX_OP_Help.bl_idname, text="General Help", icon="QUESTION", emboss=True)
        op.what = ""

        box = layout.box()
        box.label(text="DataBase Login and Host Settings", icon="URL")

        # check
        # print("> ", prefs.get().db_email)
        if not len(prefs.get().db_email):
            prefs.get().is_db_logged = False
            prefs.update_db_name(False)

        # --------------
        # if need to login
        # if not prefs.is_db_auth():
        if prefs.is_debug_mode() : 
            tprint.print(f"{prefs.get().is_db_logged=}")
            tprint.print(f"{prefs.get().is_db_logged=}")
        if not prefs.get().is_db_logged:
            row = box.column(align=True)

            row.prop(self, "db_choice")
            row2 = row.column(align=True)
            row2.prop(self, "db_host")
            row2.prop(self, "db_type")
            if self.db_choice == "custom" : row2.enabled = True
            else : row2.enabled = False
            row.prop(self, "db_email")
            row.prop(self, "db_password")
            box.row().operator(ui_operators.FLUX_DB_session_start.bl_idname, text="Login", icon="PLAY")
            # ---
            op = box.row().operator(ui_operators.FLUX_DB_session_end.bl_idname, text="Clear Current User Data", icon="TRASH")
            op.empty_all_prefs = True

        # if already login
        else:
            # row = box.row()
            # row.prop(self, "db_host")
            # row.enabled = False

            row = box.column(align=True)

            row.label(text=f"Db Choice  : {prefs.get().db_choice.capitalize()}", icon="QUESTION")
            row.label(text=f"Host at  : {prefs.get().db_host}", icon="QUESTION")
            row.label(text=f"Host Type  : {prefs.get().db_type.capitalize()}", icon="QUESTION")
            row.label(text=f"Logged in: {prefs.get().db_email}", icon="USER")
            row.label(text=f"Project: {prefs.get().project_name}", icon="FILE_VOLUME")
            box.row().operator(
                ui_operators.FLUX_DB_session_end.bl_idname, text="Logout", icon="PANEL_CLOSE"
            )
            box.row().operator(
                ui_operators.FLUX_DB_clearCache.bl_idname, text="refresh cache", icon="FILE_REFRESH"
            )
            # print(prefs.get().db_email)

        # column.prop(self, 'production_settings_file', expand=True)
        boxPrj = layout.box()
        column1 = boxPrj.column()
        column1_rows = column1.row()
        column1_rows.prop(self, 'project_file_type', text="Choose project", expand=True)
        # print(self.project_file_type)

        column1_div = column1.row()

        if self.project_file_type == "internal":
            column1_div.prop(self, 'projects_list', text="Choose project", expand=False)
        else:
            column1_div.prop(self, 'production_settings_file', text="Choose project File", expand=False)
        column1_div.operator(FLUX_PT_refreshConfigBd.bl_idname, text="", icon="FILE_REFRESH")

        # help on proxy
        op = column1_div.operator(ui.FLUX_OP_Help.bl_idname, text="", icon="QUESTION", emboss=True)
        op.what = "CONFIG"

        column = boxPrj.column()
        column.label(text='Current Project:')
        column.prop(self, 'project_name')
        column.prop(self, 'project_name_short')
        column.prop(self, 'project_path_root')
        if not self.root_ok: column.label(text=f"Error with : " + self.project_path_root, icon="ERROR")
        column.enabled = False
        column = layout.column()
        column.prop(self, 'wip_comment_local', text="Read/Write Local Wip comments")
        column.prop(self, 'filter_task_item', text="Filter Tasks Items")

        # advanced prefs
        column.label(text=f"")
        column.label(text=f"-Advanced Prefs-")
        column.prop(self, 'Use_ui_cache', text="use UI cache")
        column.prop(self, 'debug_mode', text="Debug Mode")

        boxDebug = column.box()
        boxDebug.prop(self, 'debug_frontend_mode', text="Debug Front-End Mode")
        boxDebug.prop(self, 'debug_backend_mode', text="Debug Back-End Mode")
        # boxDebug.label(text="Debug Mode", icon="URL")
        # boxDebug.label(text=f"")
        boxDebug.enabled = self.debug_mode

        column.operator(FLUX_PT_reportBug.bl_idname, text="Report a bug", icon="ORPHAN_DATA")
        print_op = column.operator(FLUX_PT_reportBug.bl_idname, text="Print Debug Infos", icon="EYEDROPPER")
        print_op.only_print = True

        column.prop(self, 'check_update_startup', text="Check new version at startup")
        column.prop(self, 'check_color_management', text="Check color management at startup")
        column.prop(self, 'check_linkeditor_atstart', text="Check Links for link editor at Load")

        column.separator()

        # boxDev = column.box()
        # boxDevCol = boxDev.column()
        # boxDevCol.prop(self, 'show_dev_options', text="Show dev options", toggle=True)
        # if self.show_dev_options:
        #     boxDevCol.prop(self, 'dev_options_refresh_edit_link_settings', text="Edit Link : Allow reload Script")


        addon_updater_ops.update_settings_ui(self,context)


class FLUX_PT_reportBug(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.ui_reportbug"
    bl_label = "Flux UI report bug"
    bl_options = {"INTERNAL"}
    bl_description = ("report a bug ")

    only_print: bpy.props.BoolProperty(default=False)

    def execute(self, context: bpy.types.Context):

        if self.only_print:

            print("----DEBUG INFOS START----")

            toprint = bpy.context.preferences.addons["flux"].preferences
            prefs.get_bpy_attributs(toprint, True)
            toprint = bpy.context.scene.flux_ui
            prefs.get_bpy_attributs(toprint, True)

            print("----DEBUG INFOS END----")

            return {"FINISHED"}

        else:

            import webbrowser
            import urllib.parse
            recipient = 'incoming+enclume-flux-46461988-8ihkwo48hbbfl6ak02dhl0q3j-issue@incoming.gitlab.com'
            # recipient = 'nicolasced@yahoo.fr'
            subject = 'my_bug'
            recipient = urllib.parse.quote(recipient)
            body = "This is were I describe my bug"
            webbrowser.open('mailto:?to=' + recipient + '&subject=' + subject + '&body=' + body, new=1)

            return {"FINISHED"}


class FLUX_PT_refreshConfigBd(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.ui_refresh_config_db"
    bl_label = "Flux UI refresh"
    bl_options = {"INTERNAL"}
    bl_description = ("refresh db")

    def execute(self, context: bpy.types.Context):

        # cache update
        file_indicator = prefs.get_file_indicator()
        config.set_project_config(file_indicator)

        update_project_infos("", "REFRESH")
        print("refresh")
        return {"FINISHED"}


#REGISTER UNREGISTER
register_classes = [
    # FLUX_Item_Bookmarks,
    FLUX_PT_reportBug,
    FLUX_PT_refreshConfigBd,
    FLUX_Preferences,
    ui.FLUX_PT_SessionManagementPanel,
    ui.FLUX_PT_ItemManagementPanel,
    ui.FLUX_OP_Help,
    ui.FLUX_PT_ItemManagementPanel_subPanelOffline,
    ui.FLUX_MT_ItemMenu,
    ui.FLUX_MT_ComposerMenu,
    ui.FLUX_OP_ItemMenu_Exe,
    ui_items_operators_call.FLUX_PT_ItemOpsPanel,
    ui_SCops.FLUX_PT_SanityCheckOpsPanel,
    ui_SCops.FLUX_SC_Popup,
    ui_SCops.FLUX_SC_PanelRefresh,
    ui_SCops.FLUX_SC_AutoFixElement,
    ui_SCops.FLUX_SC_RunElementList,
    ui_SCops.FLUX_UL_SanityCheckElementList,
    ui.FLUX_PT_SceneComposerPanel,
    ui.FLUX_PT_LinkEditorPanel,
    ui_operators.FLUX_DISK_AppendLink,
    ui_operators.FLUX_OT_LINK_refresh,
    ui_operators.FLUX_LINK_op,
    ui_operators.FLUX_LINK_PROXY_switch,
    ui_operators.FLUX_LINK_PROXY_printInfos,
    ui_operators.FLUX_LINK_PROXY_refresh_props,
    ui_operators.FLUX_LINK_PROXY_asset2shot,
    ui_operators.FLUX_LINK_PROXY_shot2asset,
    ui_operators.FLUX_ConfirmDialogBoxOperator,
    ui_operators.FLUX_DB_session_start,
    ui_operators.FLUX_DB_session_end,
    ui_operators.FLUX_DISK_ActionComfirm,
    ui_operators.FLUX_DB_choose_from_list,
    ui_operators.FLUX_UL_itemsLoadList,
    ui_operators.FLUX_UL_composerList,
    ui_operators.FLUX_UL_linkList,
    ui_operators.FLUX_DB_clearCache,
    ui_operators.FLUX_ITEM_BKM_AddRemove,
    ui_operators.FLUX_ITEM_BKM_Call,
    ui_operators.FLUX_OT_RefreshComposerList,
    ui_operators.FLUX_OT_CheckComposerFilesCollection,
    ]

register_classes.extend(startup.startup_register_classes)

# ------------------------------------------
# get dynamically files in items_operator
# ------------------------------------------
items_ops_path = bpy.utils.script_path_user() + '\\addons\\flux\\items_operators\\'
all_files = glob.glob(f"{items_ops_path}*.py")
all_base_files = []
for each in all_files:
    base_file = os.path.basename(each)
    if base_file.startswith("op_"):
        all_base_files.append(base_file.split(".")[0])
# print(f"{all_base_files=}")
# register_classes = []
for file in all_base_files:
    for name, cls in inspect.getmembers(importlib.import_module("flux.items_operators." + file), inspect.isclass):
        # print("-->>>  " , name, cls)
        register_classes.append(cls)

# for e in classes:
#     print("------->>>  ", e)
# ------------------------------------------

from bpy.app.handlers import persistent
from .items_operators import draw_message

@persistent
def handle_load_post(self, context) :

    log.debug('Start of handle_load_post')

    # ============================
    # likk editor refresh
    # if we want to clear
    if prefs.is_show_linkeditor():
        if prefs.get().check_linkeditor_atstart:
            bpy.ops.flux.linkeditor_refresh()
        else:
            bpy.context.scene.flux_ui_link_list.clear()
    # ============================

    draw_message.remove_warning_text()
    draw_message.add_warning_text()
    return

@persistent
def handle_load_pre(self, context) :
    log.debug('Start of handle_load_pre')
    draw_message.remove_warning_text()
    return


def register():

    # first exe manage
    first_exe = False
    if '_first_exe_cache' not in globals():
        first_exe = True
    global _first_exe_cache

    # addon updater
    addon_updater_ops.register(bl_info)
    for cls in register_classes:
        bpy.utils.register_class(cls)

    bpy.app.handlers.load_post.append(handle_load_post)
    bpy.app.handlers.load_pre.append(handle_load_pre)

    # config init.
    file_indicator = prefs.get_file_indicator()
    config.set_project_config(file_indicator)
    db_host = bpy.context.preferences.addons["flux"].preferences.db_host
    db_api = bpy.context.preferences.addons["flux"].preferences.db_type
    if db_host and db_api :
        config.set_project_db(db_host, db_api)

    if not first_exe:
        login = bpy.context.preferences.addons["flux"].preferences.db_email
        password = bpy.context.preferences.addons["flux"].preferences.db_password
        database_access.db_login(db_host, login, password)

    # try to connect
    if database_access.is_db_auth():
        # cl = database_access.get_db_current_user()
        # print("Flux : Can connect as ", cl["email"])

        # cache update
        # file_indicator = prefs.get_file_indicator()
        # config.set_project_config(file_indicator)

        prefs.get().is_db_logged = True
        # prefs.update_db_name(cl)
    else:
        print("Flux : Not connected to db/kitsu")
        prefs.get().is_db_logged = False
        prefs.update_db_name(False)

    # ui props
    ui_props.register()
    # edit link
    ui_edit_link.register()

    # update infos
    update_project_infos("", "REFRESH")

def unregister():
    addon_updater_ops.unregister()
    for cls in reversed(register_classes):
        bpy.utils.unregister_class(cls)
    # handlers
    try :
        bpy.app.handlers.load_post.remove(handle_load_post)
        bpy.app.handlers.load_pre.remove(handle_load_pre)
        pass
    except :
        pass

    # prop
    ui_props.unregister()
    # edit link
    ui_edit_link.unregister()

print("--FLUX lunched--")
# print(__file__)

#startup things here
startup.launch_startup()

