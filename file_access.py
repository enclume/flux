# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen


''' File and folder management 
    
        -> save/copy/create new... '''

import os

try:
    from . import flux_logger 
    from . import config
    
    #reload lib
    import importlib
    importlib.reload(flux_logger)
    importlib.reload(config)
except :
    import flux_logger
    import config


# initialize logger
log = flux_logger.get_flux_logger()


def get_versionning_tree(path: str, remove_empty: bool = False) -> dict :
    ''' 
    get the versionning folder tree of a given item path 

    Args:
        - path: the root path of the versionning
        - remove_empty: if True, remove empty folders

    Return:
        the versionning folder tree
    '''

    if not os.path.exists(path) :
        log.debug(f"path does not exist\n   ->(path : {path})")
        return False
    if not os.path.isdir(path) :
        # cedric add
        if not path: return False
        path = "\\".join(path.split("\\")[:-1])
    if not os.path.isdir(path) :
        log.error(f"path is not a directory\n   ->(path : {path})")
        raise Exception

    dirList_v = os.listdir(path)

    if "v01" not in dirList_v :
        log.debug(f"not able to find v01 folder\n   ->(given path : {path})")
        return False
    
    final_dict = {}

    for vdir in dirList_v :
        if not vdir.startswith("v") or len(vdir) != 3 or not vdir[1:].isdigit() : continue
        if not os.path.isdir(path + "\\" + vdir) : continue
        
        dirList_r = os.listdir(path + "\\" + vdir)
        r_dict = {}
        for rdir in dirList_r :
            if not rdir.startswith("r") or len(rdir) != 4 or not rdir[1:].isdigit() : continue
            if not os.path.isdir(path + "\\" + vdir + "\\" + rdir) : continue
            
            dirList_w = os.listdir(path + "\\" + vdir + "\\" + rdir)
            w_list = []
            for wdir in dirList_w :
                if not wdir.startswith("w") or len(wdir) != 5 or not wdir[1:].isdigit() : continue
                if not os.path.isdir(path + "\\" + vdir + "\\" + rdir + "\\" + wdir) : continue

                w_list.append(wdir)

            r_dict.update({rdir: w_list})

        final_dict.update({vdir: r_dict})

    log.debug(f"final_dict : {final_dict}, remove_empty : {remove_empty}")

    if not remove_empty :
        return final_dict
    
    #exemple :
    #final_dict = {'v01': {'r001': ['w0001'], 'r002': ['w0001']}}

    cleaned_final_dict = {}

    for vdir in final_dict :
        vdir_path = path + "\\" + vdir
        vdir_children = {}

        for rdir in final_dict[vdir]:
            rdir_path = vdir_path + "\\" + rdir
            rdir_children = []

            for wdir in final_dict[vdir][rdir]:
                wdir_path = rdir_path + "\\" + wdir
                if os.listdir(wdir_path) :
                    rdir_children.append(wdir)

            if rdir_children :
                vdir_children.update({rdir : rdir_children})
                continue
            
            for item in os.listdir(rdir_path) :
                if not os.path.isdir(f"{rdir_path}\\{item}") and item.endswith(".blend") :
                    vdir_children.update({rdir : rdir_children})
                    break

        if vdir_children :
            cleaned_final_dict.update({vdir : vdir_children})
            continue
        
        for item in os.listdir(vdir_path) :
            if not os.path.isdir(f"{vdir_path}\\{item}") and item.endswith(".blend") :
                cleaned_final_dict.update({vdir : vdir_children})
                break


    if not cleaned_final_dict :
        log.debug(f"all values of final_dict are None (final_dict : {cleaned_final_dict})")
        return False
    
    log.debug(f"final_dict after removing empty folder : {cleaned_final_dict}")
    return cleaned_final_dict


#-------------------------------------------------

def get_item_path_last_version(path: str, version_letter: str = "w", as_int: bool = False) -> dict :
    ''' 
    get the 3 versionning directory to last wip file of a given item path 

    Args:
        - path: the path pointing to the versionning file tree (at this path, there is normally at least the v01 folder)
        - version_letter: the letter of the last version to look for
        - as_int: return the values of the final dict as int (and not as str)

    Return:
        a dict : {"v": "v**", "r": "r***", "w": "w****"}
    '''

    versionning_tree = get_versionning_tree(path, True)
    if not versionning_tree :
        log.debug(f"versionning_tree is {versionning_tree}")
        return False
    
    all_is_None = True

    for value in versionning_tree.values() :
        if value : 
            all_is_None = False
            break

    if all_is_None :
        log.debug("all values of versionning_tree are None")
        return False


    version = ""
    revision = ""
    work = ""

    for dir_v in reversed(versionning_tree) :
        version = dir_v
        if version_letter == "v" :
            if ".blend" in str(os.listdir("\\".join([path, version]))) :
                if as_int :
                    version = int(version[1:])

                log.debug(f"return v: {version}")
                return {"v": version}
            continue

        for dir_r in reversed(versionning_tree[dir_v]) :
            revision = dir_r
            if version_letter == "r" and ".blend" in str(os.listdir("\\".join([path, version, revision]))) :
                if as_int :
                    version = int(version[1:])
                    revision =  int(revision[1:])

                log.debug(f"return v: {version}, r: {revision}")
                return {"v": version, "r": revision}

            if version_letter == "w" and versionning_tree[dir_v][dir_r] and len(versionning_tree[dir_v][dir_r]) and versionning_tree[dir_v][dir_r][-1] :
                work = versionning_tree[dir_v][dir_r][-1] 
                if ".blend" in str(os.listdir("\\".join([path, version, revision, work]))) :
                    if as_int :
                        version = int(version[1:])
                        revision =  int(revision[1:])
                        work =  int(work[1:])

                    log.debug(f"return v: {version}, r: {revision}, w: {work}")
                    return {"v": version, "r": revision, "w": work}
                
    log.info("nothing to return")
    return False


# TODO : handle multiple found id case
def get_item_id(id_path: str, item_name: str, item_type: str, item_project: str = "") -> str :
    ''' 
    get the id of a given item 

    Args:
        - project_config
        - item_name
        - item_type
        - item_category
        - item_project

    Return:
        item_id
    '''
   
    item_id = None
    item_path = id_path
    found_id = []  

    #start collect all relevant dir name
    for dir_asset in os.listdir(item_path) :
        if item_name in dir_asset and item_project in dir_asset :
            if not item_type :
                found_id.append(dir_asset)
            elif item_type and item_type.split("_")[-1] in dir_asset :
                found_id.append(dir_asset)

    #yes, just one !
    if len(found_id) == 1 :
        to_return = found_id[0].split("_")[2]
        log.debug(f"return {to_return} -> len(found_id) == 1")
        return to_return
    
    #find nothing, return 0000
    if len(found_id) == 0 :
        log.debug("return \"0000\" -> len(found_id) == 0")
        return "0000"
    
    #there is more than 1     

    #keep only "short" -> ["azerty", "azerty00"] -> keep azerty
    found_id_short = []
    found_id_short_equal = {}
    for dirname in found_id :
        new_short = "_".join(dirname.split("_")[3:])
        found_id_short.append(new_short)
        if new_short == item_name :
            found_id_short_equal.update({dirname: new_short})


    if len(found_id_short_equal) == 1 :
        to_return = str(list(found_id_short_equal.keys())[0]).split("_")[2]
        log.debug(f"return {to_return} -> len(found_id_short_equal) == 1")
        return to_return

    #test inside folder
    if len(found_id_short_equal) != 0 :
        max_dir_items = [None, 0]
        for dirname in found_id_short_equal.keys() :
            item_nbr = len(os.listdir(item_path + "\\" + dirname))
            if item_nbr > max_dir_items[1] :
                max_dir_items = [dirname, item_nbr]

        if max_dir_items[0] :
            to_return = max_dir_items[0].split("_")[2]
            log.debug(f"return {to_return} (test inside folder -> max_dir_items[0])")
            return to_return

    
    if len(found_id_short_equal) > 1 :
        printList = list(found_id_short_equal.keys())
        if len(printList) > 5 :
            printList = f"   ->({printList[:6]} + {len(printList) - 5} other)"
        else :
            printList = f"   ->({printList})"

        to_return = str(list(found_id_short_equal.keys())[0]).split("_")[2]
        log.info(f'multiple correspondances found for the {item_type} {item_name} \n{printList}\n   -> arbitrary return the first : {to_return}')
        return to_return



    printList = list(found_id)
    if len(printList) > 5 :
        printList = f"   ->({found_id[:6]} + {len(printList) - 5})"
    else :
        printList = f"   ->({found_id})"

    log.info(f'multiple correspondances found for the {item_type} {item_name} \n{printList}\n   -> arbitrary return the first : {found_id[0].split("_")[2]}')
    return found_id[0].split("_")[2]
    
    if item_id == None :
        log.info(f"return None\n   ->(item_name : {item_name}, item_type : {item_type}, item_project : {item_project})")
    return item_id


#-------------------------------------------------
#very high level -> directly call by ui

def get_item_path(item_data: dict, with_version: bool = True) :
    '''
    get the file path of an item from user input 

    Args:
        - item_data: the item_data dictionnary
        - with_version: if True, return file path (and file name) with versionning\n
                        if False, return file path without file name and versionning\n
                        if 'both', return a list with the two version posibilities [False, True]

    Return:
        the file path and / or the file name\n
        False if can't find the last wip file of the given item (-> all bool option by default and item_data["version"] == "")
    '''
    
    if type(item_data) is not dict :
        log.error(f"get_item_path need a dict in entry, not a {type(item_data)}")
        raise Exception("get_item_path is not a dict")
    
    item_data_wip = item_data.copy()

    if "category" not in item_data_wip.keys() :
        log.error(f"get_item_path need a key \"category\" into the entry dict")   
        raise Exception("need category")

    project_config = config.get_project_config()
    if not project_config :
        error = f"The project config is not setuped ! (project_config = {project_config})"
        log.error(error)

    fullPath_unversionned = config.path.get_filepath_from_item_data(item_data_wip, with_version = False)

    if not with_version :
        log.debug("return : " + str(fullPath_unversionned))
        return fullPath_unversionned


    if "version" in item_data_wip.keys() and item_data_wip["version"] :
        item_data_wip["version"] = config.path.convert_version_to_dict(item_data_wip["version"])
    else :
        last_version = get_item_path_last_version(path = fullPath_unversionned)
        item_data_wip.update({"version": last_version})

    fullPath = config.path.get_filepath_from_item_data(item_data_wip, with_version = with_version)

    if str(with_version).lower() == "both" :
        log.debug("return : " + str([fullPath_unversionned, fullPath]))
        return [fullPath_unversionned, fullPath]

    log.debug("return : " + str(fullPath))
    return fullPath


def get_item_last_version(item_data: dict, version_letter: str = "w", as_int: bool = False) -> dict :
    ''' 
    get the 3 versionning directory to last wip file of ( TODO -> a given item path or) a given item_data

    Args:
        - TODO path: the path pointing to the versionning file tree
        - item_data: the item_data dictionnary
        - version_letter: the letter of the last version to look for
        - as_int: return the values of the final dict as int (and not as str)

    Return:
        a dict : {"v": "v**", "r": "r***", "w": "w****"}
    '''    
    
    if type(item_data) is not dict :
        log.error(f"get_item_last_version need a dict in entry, not a {type(item_data)}")
        raise Exception
    item_data_wip = item_data.copy()
     
    path = get_item_path(item_data = item_data_wip, with_version = False)
  
    return get_item_path_last_version(path = path, version_letter = version_letter, as_int = as_int)


def get_item_versionning_tree(item_data: dict, remove_empty: bool = True) -> dict :
    ''' 
    get the versionning tree of a given item 

    Args:
        - item_data: the item_data dictionnary
        - remove_empty: remove empty folders

    Return:
        a dictionnary with all the versionning folder tree of the given item {v: {r: [w]}}
    '''

    item_data_wip = item_data.copy()
    fullPath = get_item_path(item_data_wip, with_version = False)

    return get_versionning_tree(path = fullPath, remove_empty = remove_empty)


def version_str_to_int(version_str):
    """
    convert return of veriosn tre as int.
    version_str_to_int(w0001) return 1 as int
    version_str_to_int(r005) return 5 as int
    Args:
        version_str: str versionning (can be v##, r##, w##)

    Returns: int of version

    """

    if version_str[0] not in ["v", "r", "w"]:
        raise Exception(f"not a valid version str {version_str}")

    version_int = int(version_str[1:])

    return version_int


def get_item_versions(item_data: dict, as_int: bool = True, remove_empty: bool = True) -> list :
    '''
    get all sub-version directly in a given version folder

    Args:
        - item_data: the item_data dictionnary
        - as_int: return the values of the final dict as int (and not as str)
        - remove_empty: remove folders with no .blend of the final list
    Return:
        a list with all folders depending on the given item version
    '''
    item_data_wip = item_data.copy()
    versionning_tree = get_item_versionning_tree(item_data_wip, remove_empty)
    version = {}

    if not "version" in item_data_wip.keys() :
        item_data_wip.update({"version" : ""})
    if item_data_wip["version"] :
        version = item_data_wip["version"]
        version = config.path.convert_version_to_dict(version)

    final_list = []

    if "r" in version.keys() and version["v"] in versionning_tree.keys() and version["r"] in versionning_tree[version["v"]].keys():
        final_list = versionning_tree[version["v"]][version["r"]]
    
    elif "v" in version.keys() and version["v"] in versionning_tree.keys() :
        final_list = versionning_tree[version["v"]].keys()
    else :
        final_list = versionning_tree.keys()
    
    if not final_list :
        return False
    
    final_list = list(final_list)

    if remove_empty :
        with_version = True
        if not version :
            with_version = False

        version_path = get_item_path(item_data_wip, with_version=with_version)
        version_path = "\\".join(version_path.split("\\")[:-1])

        is_empty = []

        for dir in final_list :
            if ".blend" not in str(os.listdir(version_path + "\\" + dir)):
                is_empty.append(dir)

        for dir in is_empty :
            final_list.remove(dir)   

        if not final_list :
            return False

    if as_int :
        for i, item in enumerate(final_list) :
            final_list[i] = version = int(item)

    log.debug("return : " + str(final_list))
    return final_list
   

def create_needed_folders(dirPath: str, is_file_path: bool = False):
    ''' 
    create all needed folders to match the given path 

    Args:
        - dirPath : the given path
        - is_file_path : if True, the last \element of the given path will be ignored (default False)

    Return:
        True if the folders are successfully created
    '''

    if is_file_path :
        dirPath = "\\".join(dirPath.split("\\")[:-1])

    '''if not os.path.isdir(dirPath) :
        raise Exception(f"ERROR in create_needed_folders : dirPath is not a directory path\n   ->(dirPath : {dirPath})")'''
    if os.path.exists(dirPath) :
        log.info("dirPath already exist")
        return True
    
    os.makedirs(dirPath)
    return os.path.exists(dirPath)


def get_new_item_save_path(basePath: str, new_version: str = "w") -> str :
    ''' 
    get the file path of a potential new save item 

    Args:
        - basePath : the path of the (blend) file to save
        - new_version : "v", "r" or "w", the letter of the version parameter to increment

    Return:
        the file path of a potential new save item
    '''
    
    fullPath = config.path.get_new_item_save_data(basePath, version_to_increment=new_version)
    
    # deso guillaume, mais je speed...
    log.debug("return : " + str(fullPath))
    return fullPath


def get_item_path2(item_data: dict,
                   only_base_folder = False,
                   only_file_path = False,
                   proxy=False) -> str :
    """
    version is given with "version" key in item_data dict as array like item_data["version"] = [1,2,3] or item_data["version"] = [1]
    If version is given     : return hypotethic file item path and/or base folder from item, from the current input, whatever the file exist or not
    if version is NOT given : return the last vesion of exisiting path, or False if nothing exist.
    Args:
        - item_data: item data in dict *without project*
        - only_base_folder: return only base folder
        - only_file_path: return only file path
        - proxy: return the proxy file

    Returns:
           list: [base_folder_path, file_item_path]
        or list: [base_folder_path, file_item_path, proxy_path]
        or list: [file_item_path, proxy_path]
        or str : file_item_path
        or str : base_folder_path

    """

    # checkers
    if only_base_folder and only_file_path:
        log.error("both only's are not possible, override only_base_folder with False")
    
    with_version = 'both'
    
    if only_base_folder : with_version = False
    if only_file_path : with_version = True

    # print(f"{with_version=}")
    result = get_item_path(item_data, with_version = with_version)


    # todo add this in config file
    if proxy:
        if not only_base_folder and not only_file_path:
            file_path = result[-1]
        if only_file_path: file_path = result

        # config.path.get_proxy_from_item()
        if file_path:
            proxy_path = file_path.replace("_v01", "_PRX_v01")
        else:
            proxy_path = False

        if not only_base_folder and not only_file_path:
            result.append(proxy_path)
        if only_file_path: result = [result, proxy_path]

    # print(f"{result=}")

    return result

def get_item_from_path(path: str) -> dict :
    '''
    get item dict from path

    Args:
        - path: path to item
    '''
    return config.path.get_item_data_from_path(path)

    base = os.path.basename(path)
    base_array = base.split("_")

    # check
    if base_array[0] != "RF" : return False
    if len(base_array) > 1 :
        if base_array[1] == "MS" : return False

    type_from_file = base_array[1]
    name_from_file = base_array[3]
    task_from_file = base_array[4]

    type = ""
    if type_from_file == "CH": type = "character"
    if type_from_file == "PR": type = "prop"
    if type_from_file == "BG": type = "background"
    if type_from_file == "EL": type = "element"

    name = name_from_file

    task = ""
    if task_from_file == "rig": task = "rigging"
    if task_from_file == "model": task = "modeling"
    if task_from_file == "setdressing": task = "set dressing"
    if task_from_file == "lookdev": task = "surfacing"

    item = {}
    item["project"] = "rufus"
    item["type"] = type.capitalize()
    item["name"] = name.capitalize()
    item["task"] = task.capitalize()

    return item

if __name__ == "__main__" :
    os.system("cls")
    print(get_item_path({"project": "rufus", "category": "assets", "type": "prop", "name": "ztest3", "task": "modeling", "version": [1, 1, 1]}))

    print(get_item_path({"project": "rufus", "category": "assets", "type": "Character", "name": "rufus", "task": "modeling", "version": [1, 1]}))
    
    print(get_item_path({"project": "rufus", "category": "assets", "type": "Character", "name": "rufus", "task": "rigging", "version": ""}))
    
    print(get_item_path({"project": "rufus", "category": "shots", "seq": "sq0000", "shot": "sq0000_sh0000", "task": "Layout", "version": [1, 1, 1]}))
    