# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
call for items operators, in a dyn way.

'''

import bpy
import json
import os

from .config import path as path_from_config
from . import prefs
from .items_operators import op_hello_world # import of exemple
from .items_operators import op_flux_render
from .items_operators import op_make_turntable
from .items_operators import op_setup_lookdev

from . import ui as main_ui

# todo make it specific to current project with ui_tools_{projectName}
from .items_operators.ui import rufus

#reload lib
import importlib
importlib.reload(main_ui)
importlib.reload(rufus)


class FLUX_PT_ItemOpsPanel(bpy.types.Panel):
    """Creates a Panel with custom operator in it"""
    bl_label = "Flux Tools"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        if prefs.get().is_db_logged:
            if prefs.is_show_tools(): return True
            else: return False
        else: return False

    def draw(self, context: bpy.types.Context):

        cat = bpy.context.scene.flux_ui.current_category
        current_item = bpy.context.scene.flux_ui.current_user_item

        # todo cache
        # cache
        # refresh = main_ui.get_global_ui_refresh()
        # print(f"> {refresh=}")
        #
        # global _flux_ui_cat
        # global _flux_ui_current_item
        #
        # if '_flux_ui_cat' not in globals():
        #     refresh = True
        #
        # print(f">> {refresh=}")
        # if refresh:
        #     # acces to flux_ui context
        #     cat = bpy.context.scene.flux_ui.current_category
        #     current_item = bpy.context.scene.flux_ui.current_user_item
        #     _flux_ui_cat = cat
        #     _flux_ui_current_item = current_item
        # else:
        #     cat = _flux_ui_cat
        #     current_item = _flux_ui_current_item

        text = ":("
        item_valid = False
        if current_item != None and current_item != "None":
            current_item = json.loads(current_item)
            item_valid = main_ui.is_item_valid(current_item)
        if item_valid:
            text = ":) > " + cat.capitalize()
            for each in current_item.keys():
                if each not in ["name", "seq"]:
                    text += " > " + current_item[each].capitalize()
        # init layout
        layout = self.layout
        box = layout.box()
        row = box.row(align=True)
        row.label(text=text, icon="MODIFIER")

        # help
        from . import ui
        op = row.operator(ui.FLUX_OP_Help.bl_idname, text="", icon="HELP", emboss=False)
        op.what = "TOOLS"

        #-- stop if not valid

        # todo check here?
        # if not item_valid: return None

        # run custom ui
        rufus.ui(box, cat, current_item)

        # cache update
        # if refresh:
        #     main_ui.set_global_ui_refresh(False)

        return None
def register():
    print("REG")

def unregister():
    print("UN   REG")
