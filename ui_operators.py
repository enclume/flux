# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Flux Pipeline Manager

manage Database acces, operator level, connection to UI.
'''

import json
import bpy
import shutil
import os
import shutil

import ast
from typing import Set, Dict

# import ui_props
from .config import path as path_from_config
from .config import prefs as  prefs_from_config

from . import ui_props
from . import proxy
from . import prefs
from . import ui
from . import database_access
from . import file_access
from . import blender_libs
from . import config
from .blender_libs import render
from .blender_libs.animatics import switcher as animatics_switcher
from .items_operators import draw_message

from . import prepare_item
from pathlib import Path
# reload lib
import importlib

importlib.reload(ui_props)
importlib.reload(path_from_config)
importlib.reload(prefs_from_config)
importlib.reload(prefs)
importlib.reload(ui)
importlib.reload(database_access)
importlib.reload(file_access)
importlib.reload(blender_libs)
importlib.reload(render)
importlib.reload(animatics_switcher)
importlib.reload(draw_message)
importlib.reload(prepare_item)
importlib.reload(proxy)


def lock_item():
    """
    this is call when the item is detected as locked.
    alternatively :
        bpy.context.scene.flux_ui.is_item_locked = False
        allow to detect it.
    Returns:

    """
    print("Lock this file")
    draw_message.add_warning_text()


def loadlist_clearfill(item: dict = None, current_version: dict = None) -> None :
    """
    construct the list of files for the load from list
    Args:
        - item: ...
        - current_version: ...
        if any of this arg is None, it goes by itself

    Returns:
        Nothing, but store result in bpy.context.scene.flux_ui_loadList
    """
    scene = bpy.context.scene
    # in case we are going here from update
    if not item:
        # item = json.loads(scene.flux_ui.current_user_item)
        # item["category"] = scene.flux_ui.current_category
        item = ui.get_current_user_item(get_category=True)
    if not current_version: current_version = json.loads(scene.flux_ui.current_version)

    # clear
    scene.flux_ui_loadList.clear()
    # search

    flux_ui_utils = scene.flux_ui_utils
    search_pattern = flux_ui_utils.load_list_filter
    show_comment = flux_ui_utils.load_list_show_comments
    show_user = flux_ui_utils.load_list_show_user
    show_time = flux_ui_utils.load_list_show_time
    show_status = flux_ui_utils.load_list_show_status

    is_proxy = scene.flux_ui.is_proxy
    proxy_level = scene.flux_ui.proxy_level

    # get versions
    all_versions = get_all_files_from_last(item, is_proxy, proxy_level)
    # print(all_versions)

    # ----
    # local_path = file_access.get_item_path(item, with_version=False)
    local_path = file_access.get_item_path2(item, only_base_folder=True)

    # exemple of what I need from get item path
    # [file_access.get_item_path(temp_item, with_version=False), file_access.get_item_path(temp_item, with_version=True)]

    comments = database_access.get_comment(scene.flux_ui.current_category, item, local=["w"], local_path=local_path)

    # from pprint import pprint
    # pprint(comments)

    # boxcol = box.column(align=False)
    for e in all_versions:
        # print(f"{e=}")

        # build string to display
        # state 0 1 2 for v r w
        # adj_factor = 1.3
        adj_factor = 1.17
        text = "v" + str(e[0])
        if len(e) > 1:
            text += " / r" + str(e[1])
            adj_factor = 1.2
            if len(e) > 2:
                text += " / w" + str(e[2])
                adj_factor = 1.0

        if not is_proxy:
            # --------------------
            # add comments
            text_comment = ""
            is_from_old = 0
            print_is_from=False
            for i, comment in enumerate(comments):
                # check if there is valid comment
                # print(f"{comment=}")
                if comment[2] and comment[2] != "None":
                    # -----------------------
                    # version is from wich revision?
                    # guess the newer one to futur print
                    # from is only here in the comment if it was written before as a revision.
                    if "from" in list(comment[2].keys()) and len(e) == 1:
                        # print(comment[2])
                        print_is_from = True
                        is_from = comment[2]["from"]

                        # ------------------------------------------------------------
                        # fix for old, comming from bug. This is only for secu reason.
                        if "r" in is_from:
                            is_from = is_from[1:]
                        # ------------------------------------------------------------

                        if int(is_from) > int(is_from_old):
                            is_from_old = is_from
                        else:
                            is_from = is_from_old

                        # remove it fopr further list
                        comment[2].pop("from")

                    # -----------------------
                    list_from_comment = list(comment[2].values())

                    if len(list_from_comment) == len(e):
                        egual = True
                        for j in range(len(e)):
                            if e[j] != list_from_comment[j]:
                                egual = False
                        # do it if all is equal
                        if egual:
                            # if comment[1].strip() != "None":

                            # show in format
                            text_comment = ""

                            switches =[show_comment, show_user, show_time, show_status]
                            switches_sum = sum(switches)

                            content =[comment[1], comment[5], comment[4], comment[3]]

                            add_slash = False
                            for i, switch in enumerate(switches):
                                if switch:
                                    if i == 0:
                                        if comment[1].strip() != "None" or switches_sum>1:
                                            text_comment += content[i]

                                    elif i == 2:
                                        text_comment += content[i][2:-3].replace("T", "@")
                                    else:
                                        text_comment += content[i]

                                    if len(text_comment):
                                        text_comment += " | "
                                        add_slash = True

                            if add_slash:
                                text_comment = text_comment[:-3]

            # print(print_is_from)
            if print_is_from:
                # print(f"{is_from=}")
                text += " (r" + str(is_from) + ")"

        else:
            text_comment = "(proxy)"

        # add final comment, only the last one
        if len(text_comment):
            adj = int(20.0*adj_factor)
            text = text.ljust(adj, " ")
            text += "   > " + text_comment

        # ----
        do_it = False
        if not len(search_pattern):
            do_it = True
        else:
            if search_pattern in text:
                do_it = True
        # go for it
        if do_it:
            newItem = bpy.context.scene.flux_ui_loadList.add()
            newItem.version = text
            newItem.version_int = str(e)


def get_all_files_from_last(item: dict, is_proxy: bool, proxy_level: int): # TODO cleanup
    '''
    get all files strated from the last one, in correct order.
    Args:
        item:
        last_version:

    Returns:

    '''

    tree = file_access.get_item_versionning_tree(item)
    # print(f"{tree=}")

    versions_to_check = []

    for v_str in tree.keys():
        v = int(v_str[1:])

        if tree[v_str]:
            for r_str in tree[v_str]:
                r = int(r_str[1:])
                if tree[v_str][r_str]:
                    for w_str in tree[v_str][r_str]:
                        w = version_str_to_int(w_str)
                        if w: versions_to_check.append([v, r, w])
                versions_to_check.append([v, r])

        versions_to_check.append([v])

    # reverse it
    versions_to_check.reverse()
    # print(versions_to_check)\
    final_versions_list = []
    for version in versions_to_check:
        full_item = dict(item)
        full_item["version"] = version

        # print(f"{full_item=}")
        file_path = file_access.get_item_path2(full_item, only_file_path=True)
        # print(f"{file_path=}")

        if is_proxy:
            # file_path = config.path.get_proxy_from_item_path(file_path)
            file_path = proxy.get_from_item_path(file_path, level=proxy_level)

        exist = os.path.exists(file_path)
        # print(f"{exist=}")

        if exist:
            final_versions_list.append(version)

    return final_versions_list


def version_str_to_int(input: str):
    """
    convert v01 to 1 (same for r and w)
    return None if input is None
    Args:
        input:

    Returns:

    """
    if input == None:
        return False
    else:
        try:
            return int(input[1:])
        except:
            return None


def get_preview_file(path: str) -> list[str] :
    """
    get preview files as array if they exist. return None if no files.Ext can be jpg, jpeg, mp4
    Args:
        path: base file path to check

    Returns:
        array or None
    """
    folder = os.path.dirname(path)
    file = os.path.basename(path)

    file_parts = file.split(".")

    result = []
    for i in range(1, 99):

        for new_ext in ["jpg", "jpeg", "mp4"]:

            new_file = file_parts[0] + "_preview" + str(i).zfill(2) + "." + new_ext
            mew_path = folder + "\\" + new_file

            if os.path.exists(mew_path):
                result.append(mew_path)

    if len(result):
        return result
    return None


def set_preview_file(path: str, video: bool = False) -> list[str] :
    """
    Set preview file path for future writing
    Args:
        path: base file path of blend file
    Returns:
        array or None
    """
    folder = os.path.dirname(path)
    file = os.path.basename(path)

    file_parts = file.split(".")

    result = []
    i = 1
    new_file = file_parts[0] + "_preview" + str(i).zfill(2)
    new_path = folder + "\\" + new_file

    # if video
    if video and ".mp4" not in new_path:
        new_path = new_path + ".mp4"

    result.append(new_path)

    return result


def get_item_dir_from_filepath(file_path: str, is_base_dir: bool = False) :

    print("get_item_dir_from_filepath : You shouldnt be here!")

    if is_base_dir:
        return file_path

    file_path_directory = os.path.dirname(file_path)
    path = Path(file_path_directory)
    return str(path.parents[2])


def is_file_exist(item: dict, force_v: bool = False) -> bool :
    """
    Convinient way to check if file exist.

    Args:
        force_v : force v1 test, override the version
        item (): item to test

    """
    if force_v:
        item['version'] = [1]

    try:
        file_path = file_access.get_item_path2(item, only_file_path=True)
    except:
        file_path = False
        return -1

    if file_path and os.path.exists(file_path):
            return True
    return False


def update_composer_list_copy(update_back = False):
    """
    For filter option in the composer list, allow to copy from list to listCopy or reverse
    Args:
        update_back (): copy from listCopy to list
    """
    # I know, its not really pythonic, but I cannt find the way to have a dynamic acces to all keys

    return

    if not update_back : bpy.context.scene.flux_ui_composercopy_list.clear()
    else : bpy.context.scene.flux_ui_composer_list.clear()

    if not update_back: all_items = bpy.context.scene.flux_ui_composer_list
    else: all_items = bpy.context.scene.flux_ui_composercopy_list

    for my_item in all_items:

        if not update_back : newItem = bpy.context.scene.flux_ui_composercopy_list.add()
        else : newItem = bpy.context.scene.flux_ui_composer_list.add()

        newItem.item_name = my_item.item_name
        newItem.item_is_collection_checked = my_item.item_is_collection_checked
        newItem.item_is_collection_ok = my_item.item_is_collection_ok
        newItem.item_is_file_exist = my_item.item_is_file_exist
        newItem.item_load = my_item.item_load

    # print("copy list")
    # print(len(bpy.context.scene.flux_ui_composer_list))
    # print(len(bpy.context.scene.flux_ui_composer_list))


def get_status_list_enum(self, context):
    ''' get the status list from the config '''
    all_status = config.get_status_list()
    result = []
    for i, status_desc in enumerate(all_status):
        result.append((status_desc[0], status_desc[0], status_desc[1], status_desc[2], i))

    # print(result)
    return result


def find_last_proxy_file(item: dict, current_version: dict = False, level: int = 0):
    """
    find last existing proxy file, parsing reverse from the last r/w
    Args:
        item: current item
        current_version: if already setted. Keep false for detecting inside

    Returns:
        file path
    """
    if not current_version:
        current_version = file_access.get_item_last_version(item, as_int=True)
        # print(current_version)


    tree = file_access.get_item_versionning_tree(item)
    if not tree:
        return False
    tree = tree['v01']
    # reverse array
    tree_r = list(tree.keys())[::-1]

    current_version = {}
    current_version["v"] = 1

    # process, reverse way
    for r in tree_r:
        tree_w = tree[r][::-1]

        current_version['r'] = file_access.version_str_to_int(r)
        for w in tree_w:

            current_version['w'] = file_access.version_str_to_int(w)
            # rec version
            item['version'] = current_version
            # check file
            file_path = file_access.get_item_path2(item, proxy=False, only_file_path=True)
            # print(f"{file_path=}")
            file_path = proxy.get_from_item_path(file_path, level=level)
            if os.path.exists(file_path):
                return file_path

    # =================== NEW ALGO TO PARSE
    # https: // www.geeksforgeeks.org / python - reverse - dictionary - keys - order /



def get_enum_wip_type(self, context):
    """
    return enum different list if proxy or not
    """
    if bpy.context.scene.flux_ui.is_proxy:
        result = [
            ("saveas", "Save (usual behaviour)", "save Current Opened file as wip file", "FILE_TICK", 0),
            ("new", "New Wip From Blank Scene", "Create New file, Discard Current Opened", "FILE_NEW", 1),
        ]
    else:
        result = [
            ("saveas", "Save As +1 (usual behaviour)", "save Current Opened file as new file", "FILE_TICK", 0),
            ("new", "New Wip From Blank Scene", "Create New file, Discard Current Opened", "FILE_NEW", 1),
        ]

    return result


def update_project_infos_db(self, context):
    """
    Update project name and root infos.
    """
    project_db_config = prefs.get_project_config_dict()["project_data"]["db"]

    db_choice = prefs.get().db_choice

    if db_choice in ["main", "alternative"]:
        prefs.get().db_host = project_db_config[db_choice]['host']
        prefs.get().db_type = project_db_config[db_choice]['api']
        config.set_project_db(prefs.get().db_host, prefs.get().db_type)
    else:
        prefs.get().db_host = prefs.get().db_custom_host
        prefs.get().db_type = prefs.get().db_custom_type


def update_project_infos(self, context):
    """
    Update project name and root infos
    """

    # flux session end
    stop_ses = True
    if type(context) is str and context == "REFRESH" :
        stop_ses = False

    if prefs.get().is_db_logged and stop_ses:
        print("FORCE END SESSION")
        bpy.ops.flux.db_session_end()

    # go
    flux_pref = bpy.context.preferences.addons["flux"].preferences
    if flux_pref.project_file_type == "remote":
        file = flux_pref.production_settings_file
        if not file :
            return None
        if not os.path.exists(file):
            flux_pref.production_settings_file = ""
            return None

    # --base config --
    project_config = prefs.get_project_config_dict()
    prefs.get().project_name = project_config["project_data"]["project_name"]
    prefs.get().project_name_short = project_config["project_data"]["project_name_short"]
    # prefs.get().db_host = project_config["project_data"]["db_url"]
    root = project_config["project_data"]["default_root"]
    prefs.get().project_path_root = root

    # update db
    update_project_infos_db("", "")

    # -- proxy --
    # print("update_project_infos")
    # print(project_config["project_data"]["proxy"])
    # print(project_config["project_data"]["proxy"]['enable'])

    prefs.get().proxy_enable = project_config["project_data"]["proxy"]['enable']
    prefs.get().proxy_tag = project_config["project_data"]["proxy"]['tag']
    prefs.get().proxy_is_multilevel = project_config["project_data"]["proxy"]['is_multilevel']

    lv_min = project_config["project_data"]["proxy"]['level_min']
    lv_max = project_config["project_data"]["proxy"]['level_max']
    lv_def = project_config["project_data"]["proxy"]['level_default']
    prefs.get().proxy_level_prefs = str([lv_min, lv_max, lv_def])

    # selected_prj = flux_pref.projects_list
    # print("UPDATE PROJECT:" + selected_prj)
    # flux_pref.project_name = selected_prj
    # root = path_from_config.get_project_root(selected_prj)
    # flux_pref.project_path_root = root

    if os.path.exists(root):
        prefs.get().root_ok = True
    else:
        prefs.get().root_ok = False



# todo replace by a scene prop
def linkEditor_persistent_infos(op: str, value: str = ""):
    """
    Set of basic operation to operate with the linkEditor_persistent_infos object

    Args:
        op ():
        value ():

    Returns:

    """
    pi_obj_name = "FLUX_linkEditor_persistent_infos"
    supported_op = ["exist", "create", "get", "set"]

    # check if obj exist
    if op == "exist":
        obj = bpy.context.scene.objects.get(pi_obj_name)
        if not obj : return False
        return True

    # create valif object
    if op == "create":
        # create
        pi_obj = bpy.data.objects.new(pi_obj_name, None)
        bpy.context.collection.objects.link(pi_obj)
        # add prop
        pi_obj["infos"] = ""
        return pi_obj

    # get infos
    if op == "get":
        obj = bpy.context.scene.objects.get(pi_obj_name)
        if not obj : return False
        infos = obj["shotlevel_infos"]
        return infos

    # set with value
    if op == "set":
        obj = bpy.context.scene.objects.get(pi_obj_name)
        if not obj: return False
        obj["shotlevel_infos"] = value
        return True

    # else
    raise Exception(f"op not supported (supported_op : {supported_op})")


class FLUX_ConfirmDialogBoxOperator(bpy.types.Operator):
    """Really?"""
    bl_idname = "flux.confirm_dialog"
    bl_label = "Do you really want to do that?"
    bl_options = {'BLOCKING', 'INTERNAL'}

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        self.report({'INFO'}, "YES!")
        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)

    def draw(self, context):
        # self.layout.label(self.message)
        self.layout.label("TEST")


class FLUX_DB_session_start(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.db_session_start"
    bl_label = "Flux DB Session Start"
    bl_options = {"INTERNAL"}
    bl_description = (
        "Logs in to server with the credentials that are defined in the "
        "addon preferences. Session is valid until Blender closes"
    )

    from_prefs: bpy.props.BoolProperty(default=True)

    def execute(self, context: bpy.types.Context) -> Set[str]:

        # get dict config
        dict_config = self.get_config()

        # root = path_from_config.get_project_root()
        root = prefs.get_project_path_root()

        # context.preferences.addons["flux"].preferences.project_path_root = path_from_config.get_project_root()
        context.preferences.addons["flux"].preferences.project_path_root = prefs.get_project_path_root()
        if os.path.exists(root):
            prefs.get().root_ok = True
        else:
            prefs.get().root_ok = False


        # if at least one of the info is an empty string
        if not len(dict_config["db_email"]) or not len(dict_config["db_password"]) or not len(dict_config["db_host"]):

            if not self.from_prefs: self.open_addon_prefs()
            self.report({'ERROR'}, 'Please fill host, email, password')
            # update data
            prefs.update_db_name(False)
            prefs.get().is_db_logged = False
            return {"CANCELLED"}

        # from .flux import *
        # try it
        try:
            # update data base
            update_project_infos("", "REFRESH")
            # check tag validity
            # print(prefs.get().proxy_tag)
            if prefs.get().proxy_enable:
                if prefs.get().proxy_is_multilevel:
                    if "#" not in prefs.get().proxy_tag:
                        self.report({'ERROR'}, f'With mutlilevel proxy, # should indicate the digit.')
                        return {"CANCELLED"}
                else:
                    if "#" in prefs.get().proxy_tag:
                        self.report({'ERROR'}, f'With mono level proxy, # should NOT indicate the digit.')
                        return {"CANCELLED"}


            database_access.db_login(dict_config["db_email"], dict_config["db_password"])
            client = database_access.get_db_current_user()
            # update data
            prefs.update_db_name(client)

            # TODO replace this by a proper function
            if prefs.get().project_file_type == "internal":
                file_indicator = prefs.get().projects_list
            else:
                file_indicator = prefs.get().production_settings_file


            if not file_indicator or file_indicator == "None" :
                self.report({'ERROR'}, f'Please select a project config (file_indicator : {file_indicator})')

            config.set_project_config(file_indicator)

            prefs.get().is_db_logged = True


        except:

            if not self.from_prefs: self.open_addon_prefs()
            self.report({'ERROR'}, 'Something get wrong with your Kitsu login. Please check your login/password.')
            # context.preferences.addons["flux"].preferences.project_path_root =  "None"
            # update data
            prefs.get().is_db_logged = False
            prefs.update_db_name(False)
            return {"CANCELLED"}

        if prefs.get().check_update_startup:
            try:
                bpy.context.scene.display_settings.display_device = "ACES"
            except:
                bpy.ops.flux.sc_popup_message("INVOKE_DEFAULT", message=f"Cannot Put ACES as display device")


        print("Start Flux session")

        return {"FINISHED"}


    def open_addon_prefs(self):
        '''Open addon prefs windows with focus on current addon'''
        # from .__init__ import bl_info
        wm = bpy.context.window_manager
        wm.addon_filter = 'All'
        if not 'COMMUNITY' in wm.addon_support:  # reactivate community
            wm.addon_support = set([i for i in wm.addon_support] + ['COMMUNITY'])
        wm.addon_search = "Flux"
        bpy.context.preferences.active_section = 'ADDONS'
        # bpy.ops.preferences.addon_expand(module=__package__)
        bpy.ops.screen.userpref_show('INVOKE_DEFAULT')


    def get_config(self) -> Dict[str, str]:
        addon_prefs = prefs.get()
        # print("addon prefs read : ", addon_prefs)
        # addon_prefs = context.preferences.addons["flux"].preferences
        return {
            "db_host": addon_prefs.db_host,
            "db_email": addon_prefs.db_email,
            "db_password": addon_prefs.db_password,
        }


class FLUX_DB_session_end(bpy.types.Operator):
    """
    Ends the Session which is stored in blender_kitsu addon preferences.
    """

    bl_idname = "flux.db_session_end"
    bl_label = "Flux DB Session End"
    bl_options = {"INTERNAL"}
    bl_description = "Logs active user out"

    empty_all_prefs: bpy.props.BoolProperty(default=False)

    def execute(self, context: bpy.types.Context) -> Set[str]:
        if self.empty_all_prefs:
            prefs.get().db_email = ""
            prefs.get().db_password = ""
            return {"FINISHED"}

        database_access.db_end_user_session()
        prefs.get().is_db_logged = False
        ui.reset_user_item()
        print("End Flux session")

        return {"FINISHED"}


class FLUX_DB_choose_from_list(bpy.types.Operator):
    """
    Generic class to display list of element via button + search pop
    disp_items      : final element to display.
                      This is NOT a user var, as it get his vars from ui.set_enum_formated
    what_to_display : str , input to choose what to display.
                      Call ui.set_var(what) and pass it though global var
                        This is really fucked up from blender, not thanks! Lost 2 hours on that. Fucking morons.
    """

    bl_idname = "flux.db_get_list"
    bl_label = "Flux Db get list"
    bl_description = "Get List from button, et cool c'est vendredi"
    bl_property = "disp_items"

    disp_items: bpy.props.EnumProperty(items=ui.set_enum)
    what: bpy.props.StringProperty(name="None")
    composer: bpy.props.BoolProperty(default=False)

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        # return prefs.session_auth(context)
        return True

    def execute(self, context: bpy.types.Context) -> Set[str]:

        # get cat
        if self.composer:
            cat = "assets"
            context.scene.flux_ui_composer.ok_for_load = False
            # print("here")
        else:
            cat = bpy.context.scene.flux_ui.current_category

        # set the current user item
        ui.set_current_user_item(cat, self.what, self.disp_items, for_composer=self.composer)

        print(f"{self.what}  -  {self.disp_items}")

        # only for assets, refresh if we change the type, by choosing the level of choice in the list
        #  purpose of this part is to be modular as possible and *may be* put it in a separate def later if needed
        #  refresh choice level for now fives the level of refresh (here 0 = type when cat = assets)
        if cat == "assets":

            # refresh differently from composer
            refresh_choice_level = 0
            if context.scene.flux_ui_composer.load_type == "list": refresh_choice_level = 2

            if not self.composer:
                # current_item = json.loads(bpy.context.scene.flux_ui.current_user_item)
                current_item = ui.get_current_user_item()
            else:
                # current_item = json.loads(bpy.context.scene.flux_ui_composer.current_user_item)
                current_item = ui.get_current_user_item(for_composer=True)
            current_item_keys = list(current_item.keys())

            if self.what == current_item_keys[refresh_choice_level]:
                for i, each in enumerate(current_item_keys):
                    if i > refresh_choice_level:
                        ui.set_current_user_item(cat, current_item_keys[i], "None", for_composer=self.composer)
        if cat == "shots":
            pass

        # refresh list in case of composer / type / list
        if self.composer:
            if context.scene.flux_ui_composer.load_type == "list":
                # in case of type
                if self.what == "type":
                    bpy.ops.flux.refresh_composer_list(hard_refresh=True)
                # in case of task
                if self.what == "task":
                    bpy.ops.flux.refresh_composer_list(hard_refresh=True)

        # refresh faster
        prefs.ui_redraw()

        # debug
        if prefs.is_debug_mode():
            self.report({'INFO'}, "You've selected: %s = %s" % (self.what, self.disp_items))

        return {"FINISHED"}

    def invoke(self, context, event):
        ui.spopup_set_items_list(self.what, self.composer)
        context.window_manager.invoke_search_popup(self)
        return {"FINISHED"}




class FLUX_DISK_ActionComfirm(bpy.types.Operator):
    bl_idname = "flux.disk_action_confirm"
    bl_label = "Confirm Action for Flux Main Panel"
    bl_options = {"REGISTER", "UNDO"}
    bl_description = "Actually do Disk Action -or not- after confirmation"

    # action/subaction describe the purpose of this op. Load, save, and precise sub action of it (review/pub, etc...)
    action: bpy.props.StringProperty(default="None")
    sub_action: bpy.props.StringProperty(default="None")
    is_proxy: bpy.props.BoolProperty(default=False, description="save as proxy file")
    is_proxy_multilevel: bpy.props.BoolProperty(default=False)
    proxy_level: bpy.props.IntProperty(default=0)

    # todo confirm proxy <> non proxy
    proxy_confirm_enable: bpy.props.BoolProperty(default=False, description="need to confirm?")
    proxy_confirm: bpy.props.BoolProperty(default=False, description="confirm save")
    # proxy confirm
    # if context.scene.flux_ui.is_saved_as_proxy != context.scene.flux_ui.is_proxy:
    #     proxy_confirm_enable = True

    # current file path
    file_path: bpy.props.StringProperty(default="None")
    file_path_metadata: bpy.props.StringProperty(default="None")

    # preview
    preview_path_open: bpy.props.BoolProperty(default=True, description="Open preview file after creation")
    preview_path_do: bpy.props.BoolProperty(default=True, description="Create Preview mp4")
    preview_path: bpy.props.StringProperty(default="")

    # =============================================
    # force confo
    force_confo_ability: bpy.props.BoolProperty(default=False, description="Force confo Ability")
    force_confo: bpy.props.BoolProperty(default=False, description="Force confo")
    # =============================================

    # current file path
    all_load_list_array: bpy.props.StringProperty(default="None")

    # user freidnly print
    action_desc: bpy.props.StringProperty(default="None")

    post_comment: bpy.props.BoolProperty(default=True, description="Post Comment on DB")

    # kitsu infos on status and comment. Status is wip by default as it s the most used in the UI
    # task_status_name: bpy.props.StringProperty( default="WIP")
    task_status_name: bpy.props.EnumProperty(name="List of kitsu tasks",
        items=get_status_list_enum)

    user_comment: bpy.props.StringProperty(default="None")

    # current version detected / setted
    version_v: bpy.props.IntProperty(default=0)
    version_r: bpy.props.IntProperty(default=0)
    version_w: bpy.props.IntProperty(default=0)

    # preview file and publish
    publish_preview_file_exist: bpy.props.BoolProperty(default=False)
    publish_preview_file_exist_str: bpy.props.StringProperty(default="YES")
    publish_preview_file_path: bpy.props.StringProperty(default="None")
    publish_anyway: bpy.props.BoolProperty(default=False)

    publish_from_review: bpy.props.BoolProperty(default=True)

    # create new file/task from current opened scene
    new_file_type: bpy.props.EnumProperty(name="New file type", items=[
        ("new", "New File", "Create New file, Discard Current Opened", "FILE_NEW", 0),
        ("current", "Save As Current", "save Current Opened file as new file", "FILE_TICK", 1),
    ]
                                          )
    # wip file type
    wip_file_type: bpy.props.EnumProperty(name="Wip file type", items=get_enum_wip_type)

    # check if the current file/task can (or not) be created
    #  Used only in case of new/task
    #  asset modeling need surfacing and shot blocking need layout and so on
    is_disk_action_need_dependencies: bpy.props.BoolProperty(default=False,
                                                             description="Is this task need dependencies")
    is_disk_action_need_dependencies_open: bpy.props.BoolProperty(default=False,
                                                                  description="Is this dependencies need to be open")
    disk_action_needed_file_base: bpy.props.StringProperty(default="None", description="path of dependencies, basename")
    disk_action_needed_file: bpy.props.StringProperty(default="None", description="path of dependencies")
    disk_action_needed_file_exist: bpy.props.BoolProperty(default=True, description="Is dependencies file exist")
    disk_action_needed_file_exist_str: bpy.props.StringProperty(default="YES", description="Is dependencies file exist")

    is_diskAction_ok: bpy.props.BoolProperty(default=True, description="Final action perform?")
    is_diskAction_message: bpy.props.StringProperty(default="None", description="user message")
    # keep in case... unused for now
    is_diskAction_anyway: bpy.props.BoolProperty(default=False,
                                                 description="Proceed anyway")

    is_diskAction_confirm: bpy.props.BoolProperty(default=False,
                                                 description="Confirm proceed")


    # check if file exist (not use for now but will be in future publish v02 possibles
    # is_file_exist: bpy.props.StringProperty( default="No")

    def is_proxy_switched_type(self, context) :
        # todo warning aussi when switch from one proxy level to another
        is_proxy = context.scene.flux_ui.is_proxy
        if context.scene.flux_ui.is_saved_as_proxy != is_proxy:
            if is_proxy:
                return "toProxy"
            else:
                return "fromProxy"

        return False

    def execute_file_new(self, context, cat: str, current_item: dict):
        """
        New file execution
        Args:
            context: bpy
            cat: explicit enough...
            current_item: explicit enough...

        Returns: None

        """
        print(f"new file = {self.file_path}")
        print(f"{self.is_disk_action_need_dependencies=}")
        print(f"{self.is_disk_action_need_dependencies_open=}")
        print(f"{self.disk_action_needed_file=}")
        print(f"{self.disk_action_needed_file_exist=}")
        print(f"{self.is_diskAction_ok=}")
        print(f"{self.is_diskAction_message=}")
        print(f"{self.new_file_type=}")

        # untouched current item
        current_item_orig = dict(current_item)

        # go
        if self.new_file_type == "new":
            if self.is_disk_action_need_dependencies:
                if not self.disk_action_needed_file_exist:
                    # here, we cannot proceed
                    if not self.is_diskAction_anyway:
                        self.report({'ERROR'}, 'Cant proceed. Needed file doesnt exist')
                        return {"CANCELLED"}
                    # here we override and come back to normal scene
                    else:
                        if self.is_disk_action_need_dependencies_open:
                            self.is_disk_action_need_dependencies_open = False
                        else:
                            self.report({'ERROR'}, 'Cant proceed. Needed file doesnt exist')
                            return {"CANCELLED"}

        # go
        # if we start a scene from scratch, or from dep
        if self.new_file_type == "new":
            # if dep need to be open
            if self.is_disk_action_need_dependencies_open:
                load_from = self.disk_action_needed_file
            # get template if need dep and NOT need dep open
            else:
                load_from = prefs_from_config.get_disk_action_template(cat)
                print(f"{load_from=}")

            prepare_item.create_item(load_from, self.file_path, cat)

        elif cat.lower() == "shots":
            self.proceed_animatic_switch()

        file_access.create_needed_folders(self.file_path, is_file_path=True)

        # -- buffer --
        context.scene.flux_ui.current_user_item = json.dumps(current_item_orig)
        context.scene.flux_ui.current_category_cache = context.scene.flux_ui.current_category

        bpy.ops.wm.save_mainfile(filepath=self.file_path)

        # print(">>>", [self.version_v, self.version_r, self.version_w])

        # set kitsu
        if bpy.context.scene.flux_ui.use_kitsu:
            # versionning = [1, 1, 1]
            versionning = [self.version_v, self.version_r, self.version_w]
            database_access.set_formated_comment(cat, current_item, self.task_status_name, self.user_comment, versionning,
                                                 user = prefs.get().db_fullname, local=self.file_path_metadata)

        # bpy.ops.r
        # prepare_item.FLUX_prepare_item

        print("Create new file")


    def proceed_animatic_switch(self, context):
        animatic_switch = animatics_switcher.replace_animatic_filepath()
        # print(f"{animatic_switch=}")

        if animatic_switch:
            self.report({'INFO'}, 'Animatic switch proceeded')
        else:
            self.report({'WARNING'}, 'Animatic switch cant proceed. File process ok, no animatic file founded.')


    def execute_file_load(self, context):
        """
        Load file execution
        file_path is given, no need to pass args
        """
        # test if file exist, and may be load it
        file_exist = os.path.exists(self.file_path)

        if not file_exist :
            raise Exception(f"ERROR : Cannot open file : {self.file_path}")

        try:
            bpy.ops.wm.open_mainfile(filepath=self.file_path)
        except:
            self.report({"ERROR"}, f"{self.file_path} is invalid file")
            return False

        # todo check if this should be passed when cat = asset?
        self.proceed_animatic_switch(context)

        # check if item should be locked
        file_path_for_item = self.file_path
        matching_r = file_access.get_new_item_save_path(file_path_for_item, new_version="r")
        matching_v = file_access.get_new_item_save_path(file_path_for_item, new_version="v")

        if not matching_r or not matching_v:
            is_matching_r_exist, is_matching_v_exist = True, True
        else:
            is_matching_r_exist = os.path.exists(matching_r)
            is_matching_v_exist = os.path.exists(matching_v)

        context.scene.flux_ui.is_item_locked = False
        if is_matching_v_exist or is_matching_r_exist:
            if not self.is_proxy:
                context.scene.flux_ui.is_item_locked = True
                lock_item()

        is_saved_as_proxy = context.scene.flux_ui.is_saved_as_proxy
        print(f"{is_saved_as_proxy=}")
        print(f"{self.is_proxy=}")

        return True


    def execute_file_saveAs(self, context, cat: str, current_item: dict):
        """
        Save as execution
        Args:
            context : bpy.context
            cat: explicit enough...
            current_item: explicit enough...

        Returns: None

        """
        print(f"new file = {self.file_path}")

        file_access.create_needed_folders(self.file_path, is_file_path=True)
        bpy.ops.wm.save_mainfile(filepath=self.file_path)

        if context.scene.flux_ui.use_kitsu and self.post_comment and not self.is_proxy:
            # print(f"{comment=}")
            # print(f"{self.task_status_name=}")
            # comment_local_path = get_item_dir_from_filepath(self.file_path)

            comment_local_path = False
            if prefs.get().wip_comment_local:
                comment_local_path = get_item_dir_from_filepath(self.file_path)

            final_current_item = dict(current_item)
            if self.force_confo_ability and self.force_confo:
                final_current_item["task"]

            versionning = [self.version_v, self.version_r, self.version_w]

            database_access.set_formated_comment(cat, final_current_item, self.task_status_name, self.user_comment, versionning,
                                                 user=prefs.get().db_fullname, local=comment_local_path)

        print("Save Wip +1 Done...")


    def execute_file_review(self, context, cat: str, current_item: dict):
        """
        review file execution
        Args:
            context : bpy.context
            cat: explicit enough...
            current_item: explicit enough...

        Returns: None

        """
        # ----
        current_open_path = bpy.data.filepath
        bpy.ops.wm.save_mainfile(filepath=current_open_path, check_existing=True)
        # ----

        print(f"new file = {self.file_path}")
        if self.preview_path_do and not self.is_proxy:

            # check if there is camera
            if context.scene.camera is None:
                self.report({'ERROR'}, 'You need a camera to generate preview')
                return {"CANCELLED"}

            # preview path
            preview_path = set_preview_file(self.file_path)[0]
            print(f"preview path = {preview_path}")

            # -- OLD--
            # name alone
            # only_file = os.path.basename(self.file_path)
            # only_file = only_file.split(".")[0]

            # final_preview_path = blender_libs.render.launch_flux_render(render_path=preview_path, burn_data=only_file, render_params=None, animation=True, openGL=True)
            
            final_preview_path = render.launch_flux_playblast(render_path = preview_path, render_params = None, animation=True, openfile=self.preview_path_open)

            # ---
            print(f"preview path from revision playblast = {final_preview_path}")
            # if self.preview_path_open:
            #     os.startfile(final_preview_path)

            self.preview_path = final_preview_path
        else:
            self.preview_path = ""

        # new_version = file_access.get_item_last_version(self.file_path, version_letter="r")
        bpy.ops.wm.save_mainfile(filepath=self.file_path, check_existing=True)

        # lock
        context.scene.flux_ui.is_item_locked = True

        if context.scene.flux_ui.use_kitsu and self.post_comment and not self.is_proxy:
            # print(f"{self.task_status_name=}")
            final_current_item = dict(current_item)
            if self.force_confo_ability and self.force_confo:
                final_current_item["task"] = "Conformation"
                print("Force Confo")
                # print(f"{final_current_item=}")
                # print(self.task_status_name)
            versionning = [self.version_v, self.version_r]
            user = prefs.get().db_fullname
            database_access.set_formated_comment(cat, final_current_item, self.task_status_name, self.user_comment, versionning, 
                                                 user=user, preview_path=self.preview_path, revision=self.version_r)

        print("Save review Done...")


        # if proxy, if choose by user, publish on the go...
        if self.is_proxy:
            if self.publish_from_review:
                print(f"{current_item=}")
                self.invoke_save_publish(context, current_item, {"v": 1}, True, context.scene.flux_ui.proxy_level)
                self.execute_file_publish(context, cat, current_item)




    def execute_file_publish(self, context, cat: str, current_item: dict):
        """
        publish file execution
        Args:
            context : bpy.context
            cat: explicit enough...
            current_item: explicit enough...

        Returns: None
        """
        # ----
        current_open_path = bpy.data.filepath
        bpy.ops.wm.save_mainfile(filepath=current_open_path, check_existing=True)
        # ----

        # get version r. Used in preview file if done and comment
        get_version_r = file_access.get_item_last_version(dict(current_item), as_int=True, version_letter="r")
        # print(f"{get_version_r=}")
        # ------ preview
        if not self.is_proxy:
            if self.publish_preview_file_exist:

                preview_path = set_preview_file(self.file_path, video=True)[0]
                shutil.copyfile(self.publish_preview_file_path, preview_path)
                print(f"Copy preview file {self.publish_preview_file_path} to {preview_path}")

            else:
                # if we dont publish any we go to check
                if not self.publish_anyway and self.preview_path_do:

                    review_item = dict(current_item)
                    review_item["version"] = get_version_r
                    last_review_path = file_access.get_item_path2(review_item, only_file_path=True)
                    print(f"{last_review_path=}")

                    preview_path = set_preview_file(last_review_path, video=False)[0]

                    final_preview_path = render.launch_flux_playblast(render_path=preview_path,
                                                                                     render_params=None, animation=True,
                                                                                     openfile=self.preview_path_open)

                    # ------------------

                    preview_publish_path = set_preview_file(self.file_path, video=True)[0]
                    shutil.copyfile(final_preview_path, preview_publish_path)
                    print(f"preview path from revision playblast = {final_preview_path}")
                    print(f"Copy preview file {final_preview_path} to {preview_publish_path}")


        # ---- go
        print(f"new file = {self.file_path}")

        # new_version = file_access.get_item_last_version(self.file_path, version_letter="r")
        bpy.ops.wm.save_mainfile(filepath=self.file_path, check_existing=True)

        # lock
        # bpy.ops.wm.read_homefile(app_template="")
        context.scene.flux_ui.is_item_locked = True

        print(f"{self.preview_path=}")

        if context.scene.flux_ui.use_kitsu and self.post_comment and not self.is_proxy:
            final_current_item = dict(current_item)
            if self.force_confo_ability and self.force_confo:
                final_current_item["task"] = "Conformation"
                print("Force Confo")

            user = prefs.get().db_fullname

            database_access.set_formated_comment(cat, final_current_item, self.task_status_name, self.user_comment, [self.version_v], 
                                                 from_r=get_version_r["r"], user=user, preview_path=self.preview_path, revision=self.version_r)

        print("Save publish Done...")


    def execute_file_publish_offline(self, context, cat, current_item):
        """
        publish file execution
        Args:
            context : bpy.context
            cat: explicit enough...
            current_item: explicit enough...

        Returns: None
        """
        doIt = True
        if not self.is_diskAction_confirm:
            doIt = False
        if not self.publish_preview_file_exist and not self.publish_anyway:
            doIt = False
        if not doIt:
            self.report({"ERROR"}, "Canceled. Need confirmation")
            return {'CANCELLED'}


        # get version r. Used in preview file if done and comment
        get_version_r = file_access.get_item_last_version(dict(current_item), as_int=True, version_letter="r")
        current_item["version"] = [1]
        current_item_r = dict(current_item)
        current_item_r["version"] = get_version_r
        get_file_r = file_access.get_item_path2(dict(current_item_r), only_file_path=True)
        get_file_v = file_access.get_item_path2(dict(current_item), only_file_path=True)
        # print(f"{get_file_r=}")
        # print(f"{get_file_v=}")
        # ------ preview
        # ------ preview
        if self.publish_preview_file_exist:

            preview_path = set_preview_file(self.file_path, video=True)[0]
            shutil.copyfile(self.publish_preview_file_path, preview_path)
            print(f"Copy preview file {self.publish_preview_file_path} to {preview_path}")

        else:
            # if we dont publish any we go to check
            if not self.publish_anyway:
                self.report({"ERROR"}, "Stop")
                return {'CANCELLED'}


        # ---- go
        # print(f"new file = {self.file_path}")

        # new_version = file_access.get_item_last_version(self.file_path, version_letter="r")
        shutil.copyfile(get_file_r, get_file_v)

        

        if context.scene.flux_ui.use_kitsu and self.post_comment:
            # convert revision to correct number, without characters
            # print(f"{get_version_r['r']=}")
            get_v = get_version_r['r'][1:]
            get_v = str(int(get_v))
            # print(f"{get_v=}")

            final_current_item = dict(current_item)
            if self.force_confo_ability and self.force_confo:
                final_current_item["task"] = "Conformation"
                print("Force Confo")

            user = prefs.get().db_fullname
            database_access.set_formated_comment(cat, final_current_item, self.task_status_name, self.user_comment, [1], from_r=get_v, 
                                                 user=user, preview_path=self.preview_path, revision=self.version_r)

        print("Save publish Done...")

    def draw_loadlist(self, context, layout):
        """
        internal proc for the load list popup dialogue
        Args:
            context: from bpy
            layout: from bpy
        """

        col = layout.column()
        box = col.box()
        boxcol = box.column(align=False)

        # search box
        boxcol.prop(context.scene.flux_ui_utils, "load_list_filter", text="", icon="VIEWZOOM", icon_only=True)

        # draw list
        boxcol.template_list("FLUX_UL_itemsLoadList", "LoadList", bpy.context.scene, "flux_ui_loadList",
                             bpy.context.scene, "flux_ui_load_list_index", rows=1, type="DEFAULT",
                             sort_lock=True)

        if not context.scene.flux_ui.is_proxy:
            rowLoadList = boxcol.row(align=True)
            rowLoadList.prop(context.scene.flux_ui_utils, "load_list_show_comments", text="Show Comments")
            rowLoadList.prop(context.scene.flux_ui_utils, "load_list_show_user", text="Show User")
            rowLoadList.prop(context.scene.flux_ui_utils, "load_list_show_time", text="Show Time")
            rowLoadList.prop(context.scene.flux_ui_utils, "load_list_show_status", text="Show Status")

        row = layout.row()
        row.label(text="")

        self.action_desc = "Choose oldies from list..."


    def execute(self, context: bpy.types.Context):

        try:
            bpy.ops.file.make_paths_absolute()
        except:
            pass

        # ==========================
        # confirm states
        if self.action == "SAVE":
            #
            if self.is_proxy_switched_type(context):
                if not self.proxy_confirm:
                    self.report({"ERROR"}, "Action not confirmed. Stopped!")
                    return {"CANCELLED"}
        # ==========================


        # init message
        draw_message.remove_warning_text()
        context.scene.flux_ui.is_item_locked = False

        # record datas to prevent new scene erase data. is_log should be always 1.
        is_log = prefs.get().is_db_logged
        if context.scene.flux_ui.use_kitsu: cache = context.scene.flux_ui.use_kitsu_cache
        current_item = json.loads(context.scene.flux_ui.current_user_item)

        cat = context.scene.flux_ui.current_category
        current_item["category"] = cat

        # untouched
        current_item_orig = dict(current_item)

        # force no reset to keep values on UI
        bpy.context.scene.flux_ui_utils.no_reset = True

        # if not self.is_diskAction_ok:
        #     return {'CANCELLED'}

        # ======================================================
        # create new
        if self.action == "NEW":
            # --->go
            self.execute_file_new(context, cat, current_item)

        # ======================================================
        # MAIN LOAD
        if self.action == "LOAD":
            # --------------------------------------------------
            # last wip
            if self.sub_action == "WIP_LAST":
                print("Try to Load Last wip...")
            # --------------------------------------------------
            # from list
            if self.sub_action == "LIST":
                user_selected_version = context.scene.flux_ui_loadList[
                    context.scene.flux_ui_load_list_index].version_int
                user_selected_version = ast.literal_eval(user_selected_version.split(">")[0])

                current_item["version"] = user_selected_version
                # self.file_path = file_access.get_item_path(current_item, with_version=True)
                self.file_path = file_access.get_item_path2(current_item, only_file_path=True)

                if self.is_proxy:
                    # self.file_path = config.path.get_proxy_from_item_path(self.file_path)
                    self.file_path = proxy.get_from_item_path(self.file_path)

                # print(self.is_proxy)
                # print(self.file_path)

                print("Try to Load Last from list...")

            # --->go
            if not self.execute_file_load(context):
                return {"CANCELLED"}

        # ======================================================
        # save
        if self.action == "SAVE":
            # --------------------------------------------------
            if self.sub_action == "AS+1":
                # --->go
                if self.wip_file_type == "saveas":
                    context.scene.flux_ui.is_saved_as_proxy = self.is_proxy
                    self.execute_file_saveAs(context, cat, current_item)
                elif self.wip_file_type == "new":
                    self.new_file_type = "new"
                    self.execute_file_new(context, cat, current_item)
            # --------------------------------------------------
            if self.sub_action == "REVIEW":
                # --->go
                self.execute_file_review(context, cat, current_item)
            # --------------------------------------------------
            if self.sub_action == "PUBLISH":
                # --->go
                self.execute_file_publish(context, cat, current_item)
            if self.sub_action == "PUBLISH_OFFLINE":
                # --->go
                self.execute_file_publish_offline(context, cat, current_item)

            # save +1 wip
        # ======================================================
        # reset values
        context.scene.flux_ui.current_user_item = json.dumps(current_item_orig)
        context.scene.flux_ui.current_category = cat
        context.scene.flux_ui.is_proxy = self.is_proxy
        context.scene.flux_ui.current_category_cache = context.scene.flux_ui.current_category
        prefs.get().is_db_logged = is_log
        if context.scene.flux_ui.use_kitsu: context.scene.flux_ui.use_kitsu_cache = cache
        prefs.ui_redraw()
        context.scene.flux_ui_utils.no_reset = False

        ui.set_cache_force_check(True)

        # update warning message
        if context.scene.flux_ui.is_item_locked:
            draw_message.add_warning_text(context)
        else:
            draw_message.remove_warning_text()

        try:
            bpy.ops.file.make_paths_absolute()
        except:
            pass

        return {"FINISHED"}


    def draw_proxy_confirm_save(self, context, layout, direction: str):
        """
        Draw the inside confirm box for switch type proxy/non proxy
        Args:
            context: ...
            layout: ...
            direction: "toproxy" or anything else
        """
        colChoice = layout.column()
        colChoice_box = colChoice.box()
        colChoice_box_col = colChoice_box.column()
        if direction == "toProxy":
            msg = "You will switch from non-proxy to proxy file type. Are you sure?"
        else:
            msg = "You will switch from proxy to non-proxy file type. Are you sure?"
        colChoice_box_col.label(text=msg, icon="ERROR")
        colChoice_box_col.prop(self, "proxy_confirm", text="Confirm save", toggle=True, icon="QUESTION")

    def draw(self, context):

        layout = self.layout

        # ======================================================
        # if we add a desc
        if self.action_desc != "None":
            col = layout.column()
            col.label(text=self.action_desc)
            if self.is_proxy:
                box_prx = col.box()
                box_prx_col = box_prx.column(align=True)
                box_prx_col.label(text=" > Proxy Wip files are always created at last wip Level")
                box_prx_col.label(text=" > Proxy file cannot be without non-proxy file")
                box_prx_col.label(text=" > Please review+publish them to deploy a proxy")
                box_prx.enabled = False

        if self.sub_action == "PUBLISH_OFFLINE":
            col = layout.column()
            col.label(text="  ** \u26A0 File will be COPIED from last review \u26A0 **")

        if self.action == "SAVE":
            # --------------------------------------------------
            if self.sub_action == "AS+1":
                colChoice = layout.column()
                colChoice.prop(self, "wip_file_type", text="New File Type", expand=True)
                colChoice.label(text="")

                # proxy confirm
                # colChoice.label(text="ADD HERE")

        # ======================================================
        if self.action == "NEW":
            colNew = layout.column()
            # colCom.prop(self, "new_file_from_current_scene", text="New File Task From Current Open Scene")
            colNew.prop(self, "new_file_type", text="New File Type", expand=True)
            colNew.label(text="")

        # ======================================================
        # if load list, ask for version
        if self.action == "LOAD":
            # --------------------------------------------------
            if self.sub_action == "LIST":

                self.draw_loadlist(context, layout)

        # ======================================================
        # status and comment.
        if self.sub_action != "LIST" and not self.is_proxy:
            # ---
            if self.action == "SAVE" or self.action == "NEW":
                colEnable = layout.column()
                colEnable.prop(self, "post_comment", text="Post Comment/Status to DB", toggle=True)

            colStatus = layout.column()
            colStatus.prop(self, "task_status_name", text="Status")
            colStatus.enabled = False
            if self.sub_action == "REVIEW" or self.sub_action == "PUBLISH" or self.sub_action == "PUBLISH_OFFLINE" or self.sub_action == "AS+1":
                colStatus.enabled = True

            # ---
            colCom = layout.column()
            colCom.prop(self, "user_comment", text="Comment")
            if self.action == "LOAD":
                colCom.enabled = False

            if not self.post_comment:
                colCom.enabled = False
                colStatus.enabled = False

            if self.force_confo_ability:
                colCom.prop(self, "force_confo", text="Force Conformation")


        if self.action == "SAVE":
            # --------------------------------------------------
            if self.sub_action == "AS+1":
                if self.wip_file_type == "new":
                    colChoice2 = layout.column()
                    colChoice2.label(
                        text=" !! Wip file will be created from Blank/Template/Empty scene from scratch !!")
                    colChoice2.label(text=" !! Current Open File is ignored !!")
                else:
                    is_proxy_switched_type = self.is_proxy_switched_type(context)
                    if is_proxy_switched_type:
                        self.draw_proxy_confirm_save(context, layout, is_proxy_switched_type)

        # ======================================================
        if self.sub_action == "REVIEW":
            colCom = layout.column()
            if self.is_proxy:
                colCom.prop(self, "publish_from_review", text="Publish on the way")
            else:
                colCom.prop(self, "preview_path_do", text="Render Preview File")
                col2Com = layout.column()
                col2Com.prop(self, "preview_path_open", text="Open Preview File After Creation")
                col2Com.enabled = self.preview_path_do

        # publish
        if self.sub_action == "PUBLISH" and not self.is_proxy:
            colComL = layout.column()
            colCom1 = colComL.column()
            # colCom1.label(text="")
            colCom1.label(text="PREVIEW FROM REVIEW NEEDED : ")

            colCom1.prop(self, "publish_preview_file_path", text="Needed Preview")
            colCom1.prop(self, "publish_preview_file_exist_str", text="Preview Exist?")

            # no preview file exist
            if not self.publish_preview_file_exist:

                # publish anyway
                if self.preview_path_do:
                    self.publish_anyway = False

                # ------
                col2Com0 = colComL.column()
                col2Com0.label(text="Preview file from Last review doesn't exist")
                col2Com1 = colComL.column()
                col2Com1.prop(self, "preview_path_do", text="Render Preview File")
                col2Com2 = colComL.column()
                col2Com2.prop(self, "preview_path_open", text="Open Preview File After Creation")
                col2Com2.enabled = self.preview_path_do
                # ------
                col2Com3 = colComL.column()
                col2Com3.label(text="or")
                col2Com3.prop(self, "publish_anyway", text="Publish anyway? (without preview)")
                col2Com3.enabled = not self.preview_path_do

            # colCom.label(text="")

        if self.sub_action == "PUBLISH_OFFLINE":

            colComL = layout.column()
            colCom1 = colComL.column()
            # colCom1.label(text="")
            colCom1.label(text="PREVIEW FROM REVIEW NEEDED : ")

            colCom1.prop(self, "publish_preview_file_path", text="Needed Preview")
            colCom1.prop(self, "publish_preview_file_exist_str", text="Preview Exist?")

            col2Com3 = colComL.column()
            # no preview file exist
            if not self.publish_preview_file_exist:
                col2Com3.prop(self, "publish_anyway", text="Publish anyway? (without preview)")

            # col2Com3.label(text="")
            col2Com3.separator()
            col2Com3.prop(self, "is_diskAction_confirm", text="Confirm Publish OFFLINE")


        # ======================================================
        if self.action == "NEW":
            if self.is_disk_action_need_dependencies:
                if self.new_file_type == "new":
                    # colNew.label(text="")
                    colNew.label(text="THIS FILE NEED : ")
                    colNew.label(text=self.is_diskAction_message)

                    self.disk_action_needed_file_base = os.path.basename(self.disk_action_needed_file)

                    colNew.prop(self, "disk_action_needed_file_base", text="File Needed")
                    if not self.disk_action_needed_file_exist: self.disk_action_needed_file_exist_str = "NO"
                    colNew.prop(self, "disk_action_needed_file_exist_str", text="File Needed Exist?")

                    # if not self.disk_action_needed_file_exist:
                    #     col.prop(self, "is_diskAction_anyway", text="Proceed anyway")

                    colNew.label(text="")

    def invoke_new(self, context, item: dict, cat: str):
        item["version"] = [1, 1, 1]
        current_version = {"v": 1, "r": 1, "w": 1}

        # infos of file path
        item_path_infos = file_access.get_item_path2(item)
        self.file_path = item_path_infos[1]
        self.file_path_metadata = item_path_infos[0]
        # ---
        self.action_desc = "You will create a new folder/file for the task : " + item["task"]
        self.task_status_name = config.get_status_list(get_default="wip")

        # check dependencies
        previous_task_needed = prefs_from_config.get_disk_action_from_task(cat, item)

        # print("----")
        # print(f"> {previous_task_needed=}")

        if previous_task_needed:
            # need dep?
            self.is_disk_action_need_dependencies = True
            # is the file need to be open (if not its needed in setup_task)
            if previous_task_needed[1]:
                self.is_disk_action_need_dependencies_open = True

            previous_task_needed[0]["version"] = [1]
            # self.disk_action_needed_file = file_access.get_item_path(previous_task_needed[0], with_version=True)
            self.disk_action_needed_file = file_access.get_item_path2(previous_task_needed[0], only_file_path=True)
            print(f"> {self.disk_action_needed_file=}")

            if os.path.exists(self.disk_action_needed_file):
                self.disk_action_needed_file_exist = True
            else:
                self.disk_action_needed_file_exist = False
                self.is_diskAction_ok = False
                self.is_diskAction_message = item["task"] + " task need a previous task : " + \
                                             previous_task_needed[0]["task"]

        # print(f"{self.is_disk_action_need_dependencies=}")
        # print(f"{self.is_disk_action_need_dependencies_open=}")
        # print(f"{self.disk_action_needed_file=}")
        # print(f"{self.disk_action_needed_file_exist=}")
        # print(f"{self.is_diskAction_ok=}")
        # print(f"{self.is_diskAction_message=}")


    def invoke_load_last_wip(self, context, item: dict, cat: str, current_version: dict, is_proxy: bool, proxy_level: int):

        # test if there is newer r than w
        test_version_base = dict(current_version)
        # remove w
        del test_version_base["w"]
        # set var
        is_newer_present = False
        # test arbitrarly on 3 versions up
        test_version = dict(test_version_base)
        test_item = dict(item)
        for i in range(1, 4):
            test_version["r"] = test_version_base["r"] + i
            test_item["version"] = test_version
            # test_file_path = file_access.get_item_path(test_item, with_version=True)
            test_file_path = file_access.get_item_path2(test_item, only_file_path=True)
            if os.path.exists(test_file_path):
                is_newer_present = True
                break

        # error and stop if needed
        if is_newer_present:
            self.report({"ERROR"}, "There is Revision newer than wip. Fix your hierarchy or use Load...")
            return {'CANCELLED'}

        # proxy
        if is_proxy:
            # print(f"{current_version=}")

            self.file_path = find_last_proxy_file(item, current_version, level=proxy_level)
            self.action_desc = "Load last Proxy wip : " + os.path.basename(self.file_path)
            return {'STOP'}

        # self.file_path = file_access.get_item_path(item)
        self.file_path = file_access.get_item_path2(item, only_file_path=True)
        self.action_desc = "Load last wip : " + os.path.basename(self.file_path)

        # determine the read of local comment or not
        local = None
        local_path = None
        if prefs.get().wip_comment_local:
            local = ["w"]
            # local_path = file_access.get_item_path(item, with_version=False)
            local_path = file_access.get_item_path2(item, only_base_folder=True)
            # print(f"{local_path=}")

        comments = database_access.get_comment(cat, item,
                                               [current_version["v"], current_version["r"],
                                                current_version["w"]],
                                               force_last_with_no_match_versionning=True,
                                               local=local,
                                               local_path=local_path)

        # if no version matching
        # print(comments)
        self.user_comment = comments[-1][1]
        if comments[-1][2] == "None":
            self.user_comment += " (unsure version comment)"

        # print(f"item : {item}")
        print(f"file_path : {self.file_path}")


    def invoke_load_list(self, context, item, current_version):

        # freeze color
        context.scene.flux_ui_utils.load_list_color_v = (0.2, 0.8, 0.2)
        context.scene.flux_ui_utils.load_list_color_r = (0.2, 0.2, 0.8)
        context.scene.flux_ui_utils.load_list_color_w = (0.8, 0.2, 0.2)

        # file path
        # self.file_path = file_access.get_item_path(item, with_version=False)
        self.file_path = file_access.get_item_path2(item, only_base_folder=True)
        # print(f"{self.file_path=}")

        # if no w -----
        if not current_version:
            current_version = file_access.get_item_last_version(item, as_int=True,
                                                                version_letter="r")

            # ------
            if not current_version:
                current_version = file_access.get_item_last_version(item, as_int=True,
                                                                    version_letter="v")

                if not current_version:
                    item_copy = dict(item)
                    item_copy["version"] = [1]
                    self.file_path = file_access.get_item_path2(item_copy, only_base_folder=True)
                    if self.file_path:
                        current_version = {'v': 1}
        # print(f"{current_version=}")

        # record the list
        loadlist_clearfill(item, current_version)
        # bpy.ops.flux.loadlist_clear_fill('EXEC_DEFAULT')

        # record it for future purpose though load_list_clear_fill
        context.scene.flux_ui.current_version = json.dumps(current_version)

    def invoke_save_wip(self, context, item: dict, current_version: dict, is_proxy: bool, proxy_level: int):

        # --
        increase_w = True
        print(f"{item=}")
        # file_path = file_access.get_item_path(dict(item))
        file_path = file_access.get_item_path2(dict(item), only_file_path=True)
        print(f"{file_path=}")

        # wip exist
        if file_path:

            # proxy
            if is_proxy:
                # file_path = config.path.get_proxy_from_item_path(file_path)
                file_path = proxy.get_from_item_path(file_path, level=proxy_level)
                # print("--PROXy--", file_path)
                self.file_path = file_path
                self.action_desc = "PROXY from current Wip : " + os.path.basename(file_path)
                return {'STOP'}

            get_version = file_access.get_item_last_version(item, as_int=True, version_letter="r")
            # print(f"{get_version=}")
            # print(f"> {current_version=}")

            # ---
            if get_version:
                if get_version["r"] > current_version["r"]:
                    get_version["r"] += 1
                    get_version["w"] = 1

                    new_item = dict(item)
                    new_item["version"] = dict(get_version)

                    current_version = get_version
                    # file_path = file_access.get_item_path(dict(new_item))
                    file_path = file_access.get_item_path2(dict(new_item), only_file_path=True)
                    increase_w = False
        # no wip at all
        else:

            # if noi file, and proxy, we should inform user and stop here
            if proxy:
                self.report({"ERROR"}, "Proxy can't be save if a non-proxy file does not exist.")
                return {'CANCELLED'}

            get_version = file_access.get_item_last_version(item, as_int=True, version_letter="r")
            # print(f"{get_version=}")
            get_version["r"] += 1
            get_version["w"] = 1

            new_item = dict(item)
            new_item["version"] = dict(get_version)

            current_version = dict(get_version)
            # file_path = file_access.get_item_path(dict(new_item))
            file_path = file_access.get_item_path2(dict(new_item), only_file_path=True)
            increase_w = False
        # if wip but not matching to last r
        # print(f"{file_path=}")
        # print(f">> {current_version=}")
        self.file_path = file_path

        # print(f"{self.file_path=}")
        self.user_comment = "None"
        matching_r = file_access.get_new_item_save_path(self.file_path, new_version="r")
        matching_v = file_access.get_new_item_save_path(self.file_path, new_version="v")

        # is file exist ?
        is_matching_r_exist = os.path.exists(matching_r)

        # for now we always erase the v1
        # is_matching_v_exist = os.path.exists(matching_v)
        is_matching_v_exist = False

        # if v exist, go for v+1
        if is_matching_v_exist:
            # current_version["v"] += 1
            current_version["r"] += 1
            current_version["w"] = 1
            item["version"] = dict(current_version)
            # self.file_path = file_access.get_item_path(dict(item))
            self.file_path = file_access.get_item_path2(dict(item), only_file_path=True)

            self.action_desc = "New Review folder tree created : " + str(
                current_version["r"]) + "   --   " + os.path.basename(self.file_path)

        # else stay in the same r, w+1
        else:
            # if r exist, go for r+1
            if is_matching_r_exist:
                current_version["r"] += 1
                current_version["w"] = 1

                item["version"] = dict(current_version)
                # self.file_path = file_access.get_item_path(dict(item))
                self.file_path = file_access.get_item_path2(dict(item), only_file_path=True)

                self.action_desc = "New Review folder tree created : " + str(
                    current_version["r"]) + "   --   " + os.path.basename(self.file_path)
            # else stay in the same r, w+1
            else:
                # print(f"{item=}")
                # self.file_path = file_access.get_item_path(item)
                # print(f"{self.file_path=}")
                if increase_w:
                    current_version["w"] += 1
                    self.file_path = file_access.get_new_item_save_path(self.file_path)
                else:
                    self.file_path = file_path

                self.action_desc = "Save as New wip  : " + str(
                    current_version["w"]) + "   --   " + os.path.basename(self.file_path)

        self.task_status_name = config.get_status_list(get_default="wip")
        print("save +1 : ", self.file_path)


    def invoke_save_review(self, context, item: dict, current_version: dict, is_proxy: bool, proxy_level: int):

        # last = file_access.get_item_path(item, with_version=True)
        last = file_access.get_item_path2(dict(item), only_file_path=True)
        self.file_path = file_access.get_new_item_save_path(last, new_version="r")
        # print("review", self.is_proxy,  self.file_path)

        # if proxy
        if is_proxy:
            if not os.path.exists(self.file_path):
                self.report({"ERROR"}, "Proxy can't be save if a non-proxy file does not exist.")
                return {'CANCELLED'}

            # self.file_path = config.path.get_proxy_from_item_path(self.file_path)
            self.file_path = proxy.get_from_item_path(self.file_path, level=proxy_level)
            self.action_desc = "Save PROXY Review as : " + str(current_version["r"]) + "   --   " + os.path.basename(self.file_path)
        else:
            self.action_desc = "Save Review as : " + str(current_version["r"]) + "   --   " + os.path.basename(self.file_path)

        self.user_comment = "None"
        self.task_status_name = config.get_status_list(get_default="review")


    def invoke_save_publish(self, context, item: dict, current_version: dict, is_proxy: bool, proxy_level: int):

        # self.file_path = file_access.get_item_path(dict(item), with_version=True)
        # print(item)
        file_path = file_access.get_item_path2(dict(item), only_file_path=True)
        print(f"{file_path=}")

        # if no w
        if not file_path:
            tree = file_access.get_item_last_version(dict(item), version_letter="r", as_int=True)
            item_copy = dict(item)
            item_copy["version"] = [1, tree["r"]]
            file_path = file_access.get_item_path2(item_copy, only_file_path=True)
            current_version = {}
            current_version["r"] = tree["r"]
            current_version["v"] = 1

        self.file_path = file_path
        self.file_path = file_access.get_new_item_save_path(self.file_path, new_version="v")
        print("invoke publish : ", self.file_path)

        if is_proxy:
            if not os.path.exists(self.file_path):
                self.report({"ERROR"},
                            "Proxy cannot only be save if a non-proxy file exist. No publish file exist = no proxy")
                return {'CANCELLED'}

            # self.file_path = config.path.get_proxy_from_item_path(self.file_path)
            self.file_path = proxy.get_from_item_path(self.file_path, level=proxy_level)
        print(f"{file_path=}")


        if self.sub_action == "PUBLISH_OFFLINE":
            self.is_diskAction_confirm = False
            self.action_desc = "COPY from review, Publshed as : " + str(
                current_version["v"]) + "   --   " + os.path.basename(
                self.file_path)
        else:
            self.action_desc = "Save Publshed as : " + str(current_version["v"]) + "   --   " + os.path.basename(
                self.file_path)

        if is_proxy:
            return True

        self.user_comment = "None"
        self.task_status_name = config.get_status_list(get_default="publish")

        # preview handle
        get_version = file_access.get_item_last_version(dict(item), as_int=True, version_letter="r")
        # print(f"{get_version=}")

        review_item = dict(item)
        review_item["version"] = get_version
        last_review_path = file_access.get_item_path2(review_item, only_file_path=True)
        preview_path = set_preview_file(last_review_path, video=True)[0]
        print(f"{preview_path=}")

        self.publish_preview_file_exist = False
        self.publish_preview_file_exist_str = "NO"
        self.publish_preview_file_path = preview_path

        if os.path.exists(preview_path):
            self.publish_preview_file_exist = True
            self.publish_preview_file_exist_str = "YES"


    def invoke(self, context, event):
        # init
        self.is_proxy = context.scene.flux_ui.is_proxy
        self.proxy_multilevel = prefs.get().proxy_is_multilevel
        self.proxy_level = context.scene.flux_ui.proxy_level
        self.proxy_confirm = False

        proxy_level_str = ""
        if self.proxy_multilevel and self.is_proxy:
            proxy_level_str = f" level={self.proxy_level}"
        print(f"FLUX DISK ACTION CONFIRM: {self.action} / {self.sub_action} / proxy : {self.is_proxy}{proxy_level_str}")


        # start
        cat = context.scene.flux_ui.current_category
        context.scene.flux_ui.is_item_locked = False

        item = ui.get_current_user_item(get_category=True)

        # if shot, no proxy!
        if cat == "shots":
            self.is_proxy = False

        # print(f"item at invoke : {item}")

        # current version if needed
        current_version = file_access.get_item_last_version(item, as_int=True)
        # print(f"start {current_version=}")

        # default base
        self.wip_file_type = "saveas"

        # ======================================================
        # create new
        if self.action == "NEW":
            self.invoke_new(context, item, cat)

            current_version = {"v": 1, "r": 1, "w": 1}
        # ======================================================
        # load
        if self.action == "LOAD":
            # last one
            # --------------------------------------------------
            if self.sub_action == "WIP_LAST":
                result = self.invoke_load_last_wip(context, item, cat, current_version, self.is_proxy, self.proxy_level)
                if result == {'STOP'}:
                    return context.window_manager.invoke_props_dialog(self, width=500)
                if result == {'CANCELLED'}:
                    return {"CANCELLED"}

            # --------------------------------------------------
            # list
            if self.sub_action == "LIST":
                self.invoke_load_list(context, item, current_version)

        # ======================================================
        # save
        if self.action == "SAVE":

            # --------------------------------------------------
            # force confo ability
            self.force_confo_ability = False
            if item['task'] == "Polish":
                self.force_confo_ability = True

            # --------------------------------------------------
            # auto next
            if self.sub_action == "AS+1":
                result = self.invoke_save_wip(context, item, current_version, self.is_proxy, self.proxy_level)
                if result == {'STOP'}:
                    return context.window_manager.invoke_props_dialog(self, width=500)
                if result == {'CANCELLED'}:
                    return {"CANCELLED"}
            # --------------------------------------------------
            # review
            if self.sub_action == "REVIEW":
                result = self.invoke_save_review(context, item, current_version, self.is_proxy, self.proxy_level)
                if result == {'CANCELLED'}:
                    return {"CANCELLED"}
            # --------------------------------------------------
            # publish
            if self.sub_action == "PUBLISH" or self.sub_action == "PUBLISH_OFFLINE":
                result = self.invoke_save_publish(context, item, current_version, self.is_proxy, self.proxy_level)
                if result == {'CANCELLED'}:
                    return {"CANCELLED"}

        # final record only if we parsed it
        # print(f"end : {current_version=}")
        self.version_v = current_version["v"]
        if "r" in list(current_version.keys()):
            self.version_r = current_version["r"]
        if "w" in list(current_version.keys()): self.version_w = current_version["w"]

        # force redraw
        prefs.ui_redraw()

        # print(f"Flux file path :  {self.file_path}")

        # always on
        self.post_comment = True

        # end, finally
        return context.window_manager.invoke_props_dialog(self, width=500)


class FLUX_UL_itemsLoadList(bpy.types.UIList):
    '''
    Load list builder
    '''

    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):

        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            # print(item.version)

            str_version = str(item.version).split(">")[0].strip()
            version_array = str_version.split("/")
            letter_version = "v"
            icon = "FUND"
            if len(version_array) == 2:
                letter_version = "r"
                icon = "HEART"
            if len(version_array) == 3:
                letter_version = "w"
                icon = "FILE"

            split = layout.split(factor=0.02)
            # split.label(text="Index: %d" % (index))
            split.prop(context.scene.flux_ui_utils, "load_list_color_" + letter_version, text="")
            # static method UILayout.icon returns the integer value of the icon ID
            # "computed" for the given RNA object.
            split.prop(item, "version", text="", emboss=False, icon=icon)


class FLUX_UL_composerList(bpy.types.UIList):
    '''
    Composer List Builder
    '''
    # VGROUP_EMPTY = 1 << 0

    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):

        if self.layout_type in {'DEFAULT', 'COMPACT'}:

            if prefs.is_debug_mode():
                print(f"FLUX_UL_composerList, draw : {item.item_name}")

            # when drawing the list, show to the user if there is a v1 or not via icon.
            current_composer_item = bpy.context.scene.flux_ui_composer.current_user_item
            # print(current_composer_item)
            icon = "QUESTION"
            file_exist=False
            if current_composer_item != "None":
                current_composer_item = json.loads(current_composer_item)
                if current_composer_item["task"] != "None":

                    current_composer_item["version"] = [1]

                    current_composer_item_copy = dict(current_composer_item)
                    current_composer_item_copy["category"] = "assets"
                    current_composer_item_copy["name"] = item.item_name

                    if item.item_is_file_exist:
                        file_exist = True

                    icon = "ORPHAN_DATA"
                    if file_exist:
                        icon = "HEART"
                        if item.item_is_collection_checked:
                            if item.item_is_collection_ok:
                                icon = "FUND"

                        file_exist = True

            # -------------------------
            # final draw

            row = layout.row(align=True)

            # load ability
            icon_load = "PANEL_CLOSE"
            if file_exist:
                icon_load = "CHECKBOX_DEHLT"
                if item.item_load: icon_load = "CHECKBOX_HLT"
            row.prop(item, "item_load", text="", emboss=False, icon=icon_load)

            # -----------------------
            icon_col = "QUESTION"
            if not file_exist: icon_col = "PANEL_CLOSE"
            if item.item_is_collection_checked:
                if item.item_is_collection_ok: icon_col = "OUTLINER_COLLECTION"
                else: icon_col = "LIBRARY_DATA_BROKEN"

            subrow = row.row(align=True)
            subrow.prop(item, "item_is_collection_ok", text="", emboss=False, icon=icon_col)
            subrow.enabled = False

            # name of asset
            row.prop(item, "item_name", text="", emboss=False, icon=icon)

            # enable ot not
            if not file_exist:
                layout.enabled=False


    def filter_items(self, context, data, propname):
        # This function gets the collection property (as the usual tuple (data, propname)), and must return two lists:
        # * The first one is for filtering, it must contain 32bit integers were self.bitflag_filter_item marks the
        #   matching item as filtered (i.e. to be shown), and 31 other bits are free for custom needs. Here we use the
        #   first one to mark VGROUP_EMPTY.
        # * The second one is for reordering, it must return a list containing the new indices of the items (which
        #   gives us a mapping org_idx -> new_idx).
        # Please note that the default UI_UL_list defines helper functions for common tasks (see its doc for more info).
        # If you do not make filtering and/or ordering, return empty list(s) (this will be more efficient than
        # returning full lists doing nothing!).

        # get criterai of filter
        only_checked = context.scene.flux_ui_composer.show_only_checked
        show_only_unchecked = context.scene.flux_ui_composer.show_only_unchecked
        only_heart = context.scene.flux_ui_composer.show_only_heart
        only_funded = context.scene.flux_ui_composer.show_only_funded
        # ---
        show_only_broken = context.scene.flux_ui_composer.show_only_broken
        show_only_question = context.scene.flux_ui_composer.show_only_question

        # compose list
        composer_list_full = getattr(data, propname)

        # init helpers
        helper_funcs = bpy.types.UI_UL_list

        # Default return values.
        flt_flags = []
        flt_neworder = []

        # Filtering by name
        if self.filter_name:
            flt_flags = helper_funcs.filter_items_by_name(self.filter_name, self.bitflag_filter_item, composer_list_full, "item_name",
                                                          reverse=self.use_filter_sort_reverse)
        # or not
        if not flt_flags:
            flt_flags = [self.bitflag_filter_item] * len(composer_list_full)

        # ------------------------
        # Filter by criteria
        #   to_keep is the full list of index to keep. None mean all.
        to_remove = []
        for idx, item in enumerate(composer_list_full):

            # only checked filter
            if not item.item_load and only_checked:
                to_remove.append(idx)

            # only unchecked
            if item.item_load and show_only_unchecked:
                to_remove.append(idx)
            if not item.item_is_file_exist and show_only_unchecked:
                to_remove.append(idx)

            # only heart (file exist)
            if not item.item_is_file_exist and only_heart:
                to_remove.append(idx)

            # unly fund (file exist and collection ok)
            if not item.item_is_collection_ok and only_funded:
                to_remove.append(idx)

            # only question
            if item.item_is_collection_checked and show_only_question:
                to_remove.append(idx)

            # only broken
            if item.item_is_file_exist and show_only_broken:
                to_remove.append(idx)

        # finally remove/keep if needed
        if to_remove:
            for idx, item in enumerate(composer_list_full):
                if idx in to_remove:
                    flt_flags[idx] = 0

        # -------------------------------------
        # kkep that as inspiration
        # PLEASE DO NOT DELETE!!!! I dont udenrstan dreally this codem but tweak it till get the best of it
        # for idx, vg in enumerate(composer_list_full):
        #     if vgroups_empty[vg.index][0]:
        #         flt_flags[idx] |= self.VGROUP_EMPTY
        #         if self.use_filter_empty and self.use_filter_empty_reverse:
        #             flt_flags[idx] &= ~self.bitflag_filter_item #bremove it
        #     elif self.use_filter_empty and not self.use_filter_empty_reverse:
        #         flt_flags[idx] &= ~self.bitflag_filter_item
        # -------------------------------------

        # NO REORDER FOR NOW

        # flt_neworder = [flt_neworder[0], flt_neworder[1]]

        # Reorder by name or average weight.
        # if self.use_order_name:
        #     flt_neworder = helper_funcs.sort_items_by_name(composer_list_full, "name")
        # elif self.use_order_importance:
        #     _sort = [(idx, vgroups_empty[vg.index][1]) for idx, vg in enumerate(composer_list_full)]
        #     flt_neworder = helper_funcs.sort_items_helper(_sort, lambda e: e[1], True)

        return flt_flags, flt_neworder


class FLUX_UL_linkList(bpy.types.UIList):
    '''
    Composer List Builder
    '''
    # VGROUP_EMPTY = 1 << 0
    def remap(self, old_val, old_min, old_max, new_min, new_max):
        ''' description TODO '''

        if old_val < old_min: return new_min
        if old_val > old_max: return new_max

        return (new_max - new_min) * (old_val - old_min) / (old_max - old_min) + new_min

    def draw_lines(self, context, icon: str, row, item):

        if context.scene.flux_ui_linkeditor.show_short:
            if icon : row.prop(item, "link_shortname", text="", emboss=False, icon=icon)
            else : row.prop(item, "link_shortname", text="", emboss=False)
        elif context.scene.flux_ui_linkeditor.show_file:
            # context.scene.flux_ui_linkeditor.show_short = False
            if icon: row.prop(item, "link_file", text="", emboss=False, icon=icon)
            else: row.prop(item, "link_file", text="", emboss=False)
        else:
            if icon: row.prop(item, "link_collection", text="", emboss=False, icon=icon)
            else: row.prop(item, "link_collection", text="", emboss=False)

    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):

        # print(context.scene.flux_ui_linkeditor.is_sel_num)

        # manage status list
        global _flux_ui_cache_status_list
        if '_flux_ui_cache_status_list' not in globals():
            _flux_ui_cache_status_list = [-1]

        if self.layout_type in {'DEFAULT', 'COMPACT'}:

            # if prefs.is_debug_mode():
            #     print(f"FLUX_UL_composerList, draw : {item.link_name}")

            row = layout.row(align=True)

            # draw algo, for manip mode
            draw_it = True
            n_sel = context.scene.flux_ui_linkeditor.is_sel_num
            if n_sel or context.scene.flux_ui_linkeditor.edit_mode == "none":
                draw_it = False

            # load ability
            # icon_load = "CHECKBOX_HLT"
            icon_load = "CHECKBOX_DEHLT"
            if item.is_collection_checked: icon_load = "CHECKBOX_HLT"
            row.prop(item, "is_collection_checked", text="", emboss=False, icon=icon_load)

            icon_view = "HIDE_ON"
            if item.is_visible: icon_view = "HIDE_OFF"
            row.prop(item, "is_visible", text="", emboss=False, icon=icon_view)

            # print(f"{context.scene.flux_ui_linkeditor.is_proxy_list=}")
            if context.scene.flux_ui_linkeditor.is_proxy_list:
                is_proxy_list = ast.literal_eval(context.scene.flux_ui_linkeditor.is_proxy_list)
            else:
                is_proxy_list = []
            # is_proxy_list = json.loads(is_proxy_list)
            # print(f"{is_proxy_list=}")
            # print(f"{item=}")
            # print()
            # if item in is_proxy_list:
            #     print("--------IM----------")


            show_proxy = True
            # icon = "NONE"
            icon = "MESH_UVSPHERE"
            if item.is_proxy:
                icon = "MESH_CUBE"

            # proxy_list is build at refresh and contains all the index that have the condition ui.is_proxy_available_in_folder
            # it replace the line above :
            # if not ui.is_proxy_available_in_folder(item):
            if not index in is_proxy_list:
                # icon="BLANK1"
                icon="BLANK1"
                show_proxy = False
                # print(index)

            row2 = row.column()
            row2.enabled = draw_it
            op = row2.operator(FLUX_LINK_PROXY_switch.bl_idname, text="", icon=icon)
            # op.item = item
            op.idx = index
            if not show_proxy:
                row2.enabled = False

            # if manip
            if context.scene.flux_ui_linkeditor.edit_mode == "manip" or context.scene.flux_ui_linkeditor.edit_mode == "none":
                # name of asset
                icon = None
                if item.is_shot_level:
                    icon = "FILE_MOVIE"
                self.draw_lines(context, icon, row, item)
                # self.draw_lines(context, icon, row, item)

                if draw_it:

                    isolate_icn = "RADIOBUT_ON"
                    draw_isolate = True
                    if context.scene.flux_ui_linkeditor.isolate_objsvis:
                        draw_isolate = False
                        isolate_icn = "CANCEL"
                        item_recorded = context.scene.flux_ui_linkeditor.isolate_current
                        if item_recorded == item.link_collection:
                            draw_isolate = True

                    row2 = row.row(align=True)

                    if draw_isolate:
                        op = row2.operator(FLUX_LINK_op.bl_idname, text="", icon=isolate_icn)
                        op.item = item.link_collection
                        op.action = "ISOLATE"
                    op = row2.operator(FLUX_LINK_op.bl_idname, text="", icon="RESTRICT_SELECT_OFF")
                    op.item = item.link_collection
                    op.action = "SELECT"

                    if not item.is_collection_checked: row2.enabled = False

                    op = row.operator(FLUX_LINK_op.bl_idname, text="", icon="FILE_REFRESH")
                    op.item = item.link_path
                    op.action = "RELOAD"

            # if kitsu
            else:

                # if this, then refresh status
                if len(_flux_ui_cache_status_list) == 1:
                    if _flux_ui_cache_status_list[0] == -1:
                        # print("draw item refresh")
                        status = "?"
                else:
                    # print(f"draw iten {index} et link {_flux_ui_cache_status_list[index]}")
                    # print(f"{_flux_ui_cache_status_list}")
                    if index > len(_flux_ui_cache_status_list)-1:
                        status = "?"
                    else:
                        status = _flux_ui_cache_status_list[index]

                w = context.region.width
                f = self.remap(w, 400, 1000, 0.8, 0.95)
                row_split = row.split(factor = f)
                row_split_line = row_split.row()
                row_split.alignment = "RIGHT"

                icon = None
                if item.is_shot_level:
                    icon = "FILE_MOVIE"
                self.draw_lines(context, icon, row_split_line, item)
                #
                # current_item = file_access.get_item_from_path(item.link_path)
                # if current_item and database_access.is_db_auth():
                #     status = database_access.get_comment("assets", current_item, versionning=None, local=None, only_last=True)
                # else:
                #     status = False

                status_msg = "?"
                if status:
                    # status_msg = status[0][3]
                    status_msg = status
                row_split.label(text=status_msg)

            icon_view = "DOT"
            if item.is_sel: icon_view = "CHECKMARK"
            row.prop(item, "is_sel", text="", emboss=False, icon=icon_view)


    def filter_items(self, context, data, propname):
        # This function gets the collection property (as the usual tuple (data, propname)), and must return two lists:
        # * The first one is for filtering, it must contain 32bit integers were self.bitflag_filter_item marks the
        #   matching item as filtered (i.e. to be shown), and 31 other bits are free for custom needs. Here we use the
        #   first one to mark VGROUP_EMPTY.
        # * The second one is for reordering, it must return a list containing the new indices of the items (which
        #   gives us a mapping org_idx -> new_idx).
        # Please note that the default UI_UL_list defines helper functions for common tasks (see its doc for more info).
        # If you do not make filtering and/or ordering, return empty list(s) (this will be more efficient than
        # returning full lists doing nothing!).

        # get criterai of filter
        only_checked = context.scene.flux_ui_linkeditor.show_only_checked
        show_only_unchecked = context.scene.flux_ui_linkeditor.show_only_unchecked
        # ---
        # compose list
        composer_list_full = getattr(data, propname)

        # init helpers
        helper_funcs = bpy.types.UI_UL_list

        # Default return values.
        flt_flags = []
        flt_neworder = []

        # Filtering by name
        if self.filter_name:
            flt_flags = helper_funcs.filter_items_by_name(self.filter_name, self.bitflag_filter_item, composer_list_full, "link_collection",
                                                          reverse=self.use_filter_sort_reverse)
        # or not
        if not flt_flags:
            flt_flags = [self.bitflag_filter_item] * len(composer_list_full)

        # ------------------------
        # Filter by criteria
        #   to_keep is the full list of index to keep. None mean all.
        to_remove = []
        for idx, item in enumerate(composer_list_full):

            # only checked filter
            if not item.is_collection_checked and only_checked:
                to_remove.append(idx)

            # only unchecked
            if item.is_collection_checked and show_only_unchecked:
                to_remove.append(idx)

        # finally remove/keep if needed
        if to_remove:
            for idx, item in enumerate(composer_list_full):
                if idx in to_remove:
                    flt_flags[idx] = 0

        return flt_flags, flt_neworder


class FLUX_OT_CheckComposerFilesCollection(bpy.types.Operator):
    bl_idname = "flux.check_composer_files_collections"
    bl_label = "Check Composer Collections in Files"
    bl_options = {"INTERNAL"}
    bl_description = ("Check is collections from file(s) in composer are ok")


    def execute(self, context):

        # when drawing the list, show to the user if there is a v1 or not via icon.
        current_composer_item = bpy.context.scene.flux_ui_composer.current_user_item
        current_composer_item = json.loads(current_composer_item)
        current_composer_item["category"] = "assets"
        current_composer_item["version"] = [1]

        load_type = context.scene.flux_ui_composer.load_type

        if load_type == "list":

            all_ok = True
            at_least_one_checked = False
            for my_item in context.scene.flux_ui_composer_list:

                # is the one we want to load?
                if my_item.item_load:

                    # foce it for now.
                    my_item.item_is_collection_checked = False

                    # print(" check flux_ui_composer_list : ", my_item.item_name, my_item.item_load, my_item.item_is_collection_checked)
                    if not my_item.item_is_collection_checked:
                        # print("we need to check ", my_item.item_name)

                        # -------
                        current_composer_item_copy = dict(current_composer_item)
                        current_composer_item_copy["name"] = my_item.item_name

                        # print(current_composer_item_copy)
                        file_path = file_access.get_item_path2(current_composer_item_copy, only_file_path=True)

                        # we should be here at this point, but just for double checking
                        if os.path.exists(file_path):
                            at_least_one_checked = True
                            if blender_libs.is_collection_in_file(file_path):
                                # icon_col = "OUTLINER_COLLECTION"
                                #print("collection OK")
                                my_item.item_is_collection_checked = True
                                my_item.item_is_collection_ok = True
                            else:
                                #print("collection NOT OK")
                                # icon_col = "PANEL_CLOSE"
                                my_item.item_is_collection_checked = True
                                my_item.item_is_collection_ok = False
                                all_ok = False

                        # file doesnt exist
                        else:
                            all_ok = False

            # refresh ok for load
            #print(f"{all_ok=}")
            if not at_least_one_checked: all_ok = False
            context.scene.flux_ui_composer.ok_for_load = all_ok
            context.scene.flux_ui_composer.at_least_one_checked = at_least_one_checked

        else:
            at_least_one_checked = True
            file_path = file_access.get_item_path2(current_composer_item, only_file_path=True)

            # we should be here at this point, but just for double checking
            if os.path.exists(file_path):
                if blender_libs.is_collection_in_file(file_path):
                    # icon_col = "OUTLINER_COLLECTION"
                    # print("collection OK")
                    all_ok = True
                else:
                    all_ok = False

            # file doesnt exist
            else:
                all_ok = False

            # refresh ok for load
            # print(f"{all_ok=}")
        context.scene.flux_ui_composer.ok_for_load = all_ok
        context.scene.flux_ui_composer.at_least_one_checked = at_least_one_checked

        return {'FINISHED'}


class FLUX_OT_RefreshComposerList(bpy.types.Operator):
    bl_idname = "flux.refresh_composer_list"
    bl_label = "Simple Modal Operator"
    bl_options = {"INTERNAL"}
    bl_description = ("Refresh the list of asset in the scene composer")

    hard_refresh: bpy.props.BoolProperty(default=True, description="clear list before building in")

    def execute(self, context):
        # ui_operators.composerlist_clearfill()

        if prefs.is_debug_mode(): print(f"{self.hard_refresh=}")

        at_least_one_error = False
        # check if we can go on
        composer_item = json.loads(context.scene.flux_ui_composer.current_user_item)
        # print(f"{composer_item=}")
        if composer_item["type"] == "None":
            print("Please enter type")
            return {'FINISHED'}

        # do we check the file?
        check_exist_file=False
        if composer_item["task"] != "None":
            check_exist_file=True

        # go on
        # ----

        # get project name and asset list from db.
        #  this is hard reset
        assets_name = database_access.get_assets_names(composer_item["type"])

        if prefs.is_debug_mode(): print(f"{assets_name=}")

        # print(f"{assets_name=}")

        # go for it
        # clear all input of list
        context.scene.flux_ui_composer_list.clear()
        for asset_name in assets_name:
            newItem = context.scene.flux_ui_composer_list.add()
            newItem.item_name = asset_name
            newItem.item_load = 0

            if check_exist_file:
                #
                composer_item_copy = dict(composer_item)
                composer_item_copy["category"] = "assets"
                composer_item_copy["name"] = asset_name

                is_file = is_file_exist(composer_item_copy, force_v=True)

                if is_file:
                    newItem.item_is_file_exist = True

                if is_file == -1:
                    at_least_one_error = True
                    print(" FLUX : This file produce an error : ", composer_item_copy)


                # print(f"{asset_name=} - {newItem.item_is_file_exist=}")

            # newItem.version_int = str(e)

        # --
        context.scene.flux_ui_composer.ok_for_load = False
        context.scene.flux_ui_composer.at_least_one_checked = False

        if at_least_one_error:
            self.report({'WARNING'}, "At least one file produce an error. Please check the console")

        # finally, go to a copy
        context.scene.flux_ui_composer.show_only_funded = 0
        context.scene.flux_ui_composer.only_funded = 0
        context.scene.flux_ui_composer.show_only_checked = 0

        # print("finish")

        update_composer_list_copy()

        return {'FINISHED'}
    

class FLUX_DB_clearCache(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.db_clear_cache"
    bl_label = "Flux DB Clear Cache"
    bl_options = {"INTERNAL"}
    bl_description = ("Flux DB/Disk Clear Cache")

    def execute(self, context: bpy.types.Context) -> Set[str]:
        database_access.clear_cache()
        ui.set_cache_force_check(True)
        print("Flux : Cache Db/Disk refreshed")
        return {"FINISHED"}


class FLUX_ITEM_BKM_AddRemove(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.item_bkm_addremove"
    bl_label = "Flux Item Bookmark Add remove"
    bl_options = {"INTERNAL"}
    bl_description = ("Flux Item Bookmark Add remove")

    for_composer : bpy.props.BoolProperty(
        default=False,
    )

    def execute(self, context: bpy.types.Context) -> Set[str]:

        ex = prefs.bookmarks_is_item_exist(self.for_composer)
        # print(ex)
        if ex:
            prefs.bookmarks_remove(self.for_composer)
        else:
            prefs.bookmarks_add(self.for_composer)

        # force save user pref
        bpy.ops.wm.save_userpref()

        # print out
        bk = prefs.bookmarks_get(self.for_composer)
        print("Add/Remove : ", bk)

        return {"FINISHED"}


class FLUX_ITEM_BKM_Call(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.item_bkm_call"
    bl_label = "Flux Item Bookmark Call"
    bl_options = {"INTERNAL"}
    bl_description = ("Flux Item Bookmark call")

    item : bpy.props.StringProperty(default="")
    for_composer: bpy.props.BoolProperty(default=False)

    def execute(self, context: bpy.types.Context) -> Set[str]:

        # print("--")
        # print(self.for_composer)
        # print(self.item)

        # remove
        if self.item == "REMOVE":
            prefs.bookmarks_clear(self.for_composer)
            return {"FINISHED"}

        #add
        item = json.loads(self.item)

        if not self.for_composer:
            bpy.context.scene.flux_ui.current_category = item["cat"]
        else:
            if "[" in item["name"]:
                bpy.context.scene.flux_ui_composer.load_type = "list"
            else:
                bpy.context.scene.flux_ui_composer.load_type = "one"

        is_compo_list = False
        if item["cat"] == "assets":
            if "[" in item["name"]:
                is_compo_list = True

        item.pop("cat")
        item.pop("project")


        if self.for_composer:

            # care about list
            copy_item = dict(item)
            if is_compo_list:
                current_item = json.loads(bpy.context.scene.flux_ui_composer.current_user_item)
                copy_item["name"] = current_item["name"]

            bpy.context.scene.flux_ui_composer.current_user_item = json.dumps(copy_item)
        else:
            bpy.context.scene.flux_ui.current_user_item = json.dumps(item)
        # force ui

        cat = bpy.context.scene.flux_ui.current_category
        if self.for_composer: cat = "assets"

        for each in item:
            # care about list
            doIt = True
            if is_compo_list and each == "name":
                doIt = False

            # print(each, item[each], doIt)
            if doIt:
                ui.set_current_user_item(cat, each, item[each], for_composer=self.for_composer)

        # car bout list
        if is_compo_list:
            bpy.ops.flux.refresh_composer_list(hard_refresh=True)

            items = ast.literal_eval(item["name"])
            # print(items)
            for my_item in bpy.context.scene.flux_ui_composer_list:
                #print(my_item.item_name)
                if my_item.item_name in items:
                    my_item.item_load = 1
                else:
                    my_item.item_load = 0

        return {"FINISHED"}


class FLUX_DISK_AppendLink(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.disk_append_link"
    bl_label = "Flux DB Append or link"
    bl_options = {"INTERNAL"}
    bl_description = ("Append or Link")

    link: bpy.props.BoolProperty(default=True)
    link_lib_override: bpy.props.BoolProperty(default=True)

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context) -> Set[str]:
        current_item = ui.get_current_user_item(for_composer=True)

        print("Append or link")

        # update current item
        current_item["version"] = [1]
        current_item["category"] = ui.enum_members_from_instance(bpy.context.scene.flux_ui, "current_category")[0]

        # print(f"{current_item=}")

        # project_name set sur rufus
        # item_data -> y mettre la même chose que pour "get_item_path()"
        # link prend l'option de l'opérateur
        # parent_col -> y mettre le nom de la collection dans laquelle va atterrir l'import (peut rester sur none)

        current_items = []
        load_type = bpy.context.scene.flux_ui_composer.load_type
        # list load
        if load_type == "list":

            for my_item in bpy.context.scene.flux_ui_composer_list:
                if my_item.item_load:
                    # print("---> ", my_item.item_name, my_item.item_load)
                    current_item["name"] = str(my_item.item_name)
                    current_items.append(dict(current_item))

        # one load
        else:
            current_items = [current_item]

        # final process for all
        load_it = True
        result = False
        for item in current_items:
            print("------------------------------------------------------------------------------------------------")
            print("--Load--", item)
            if load_it:
                result = blender_libs.import_item(item_data=item, link=self.link,
                                                  make_lib_override=self.link_lib_override, parent_col=None)
                print(f"{result=}")
            else:
                result = "ok"

        if not result:
            self.report({'ERROR'}, 'cannot find a v1 published for the current item')
            return {"CANCELLED"}

        return {"FINISHED"}


class FLUX_OT_LINK_refresh(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.linkeditor_refresh"
    bl_label = "Flux Link editor refresh"
    bl_options = {"INTERNAL"}
    bl_description = ("Flux Link editor refresh")

    def execute(self, context: bpy.types.Context) -> Set[str]:

        # Initialize a list to store all collections and their respective file paths
        #  will be fill with the def
        all_collections_info = {}

        is_proxy_list = []

        global _flux_ui_cache_status_list
        _flux_ui_cache_status_list = []

        def get_linked_collections(collection=None):

            # if no collection, then its scene collections
            if not collection:
                collection = bpy.context.scene.collection

            # process
            for sub_collection in collection.children:
                # print(f"{sub_collection=}")
                # print(f"{sub_collection.override_library=}")

                file_path = None
                if sub_collection.override_library:

                    # --keep that for educ purpose--
                    # print("--")
                    # print(sub_collection.override_library.properties)
                    #
                    # print("---")
                    # p = sub_collection.override_library.properties
                    # for e in list(p):
                    #     print(e.operations.items())
                    #     op = e.operations.items()
                    #     for each_op in op:
                    #         print("------")
                    #         print(each_op[1].operation)

                    if sub_collection.override_library.reference:
                        file_path = sub_collection.override_library.reference.library.filepath
                    else:
                        file_path = False
                    # file_path = sub_collection.library.filepath
                if file_path:
                    # print(sub_collection.name)
                    # print(file_path)
                    # print(all_collections_info)
                    prt = blender_libs.collection.get_parent(sub_collection)
                    prt_override = False
                    if prt:
                        if prt.override_library:
                            prt_override = True

                    # print(f"{sub_collection.name=} - {prt=}")
                    if not prt_override:
                        if not sub_collection.name in list(all_collections_info.keys()):
                            # if not file_path in list(all_collections_info.values()):
                            all_collections_info[sub_collection.name] = file_path
                get_linked_collections(sub_collection)

        # launch and fill the dict
        get_linked_collections()

        # Print the list of all collections and their file paths
        context.scene.flux_ui_link_list.clear()

        # info = name (of collection)
        # all_collections_info[info] = path

        # get if there is shot level
        shotlevel_infos = context.scene.flux_ui_linkeditor.shotlevel_infos
        if shotlevel_infos:
            shotlevel_infos = json.loads(shotlevel_infos)
        # print(f"{shotlevel_infos=}")


        for i, info in enumerate(list(all_collections_info.keys())):
            # print(f"name: {info}")
            # print(f"path: {all_collections_info[info]}")


            # add new item. all we be recorded there
            newItem = context.scene.flux_ui_link_list.add()
            # declare, just to be clea. Im sick, need something simple :(
            path = all_collections_info[info]
            collection_name = info


            # record.
            # path, collection info
            newItem.link_path = path
            newItem.link_collection = collection_name
            # short name

            # print(f"{all_collections_info=}")
            # print(f"{all_collections_info[info]=}")
            link_item = file_access.get_item_from_path(all_collections_info[info])
            if link_item:
                val = list(link_item.values())
                link_shortname = f"{val[2]} || {val[1][0:4]} / {val[3]}"
                newItem.link_shortname = link_shortname
            else:
                link_shortname = "( " + info + " )"
                newItem.link_shortname = link_shortname

            # link_ok, collection checked (just get the prop)
            newItem.is_link_ok = True
            newItem.is_collection_checked = not blender_libs.collection.get_layer_collection(info).exclude

            # proxy
            proxy_infos = proxy.is_proxy_from_item_path(all_collections_info[info])
            # is proxy?
            newItem.is_proxy = bool(proxy_infos)

            # print(f"proxy: {proxy_infos}")

            # if is proxy
            if proxy_infos:
                newItem.proxy_level = proxy_infos[1]
                newItem.link_path_orig = proxy_infos[0]
                newItem.link_file_orig = os.path.basename(proxy_infos[0])

                newItem.link_file = os.path.basename(all_collections_info[info])
            # if it's not
            else:
                newItem.link_file = os.path.basename(all_collections_info[info])

            # if shot level?
            if shotlevel_infos:
                if collection_name == shotlevel_infos[0]:
                    newItem.is_shot_level=True

            if ui.is_proxy_available_in_folder(newItem):
                is_proxy_list.append(i)

            # status if needed
            # print(context.scene.flux_ui_linkeditor.edit_mode)
            if context.scene.flux_ui_linkeditor.edit_mode == "status":

                current_item = file_access.get_item_from_path(newItem.link_path)
                if current_item and database_access.is_db_auth():
                    # print(f"{current_item=}")
                    if 'name' in list(current_item.keys()):
                        status = database_access.get_comment("assets", current_item, versionning=None, local=None, only_last=True)
                        status = status[0][3]
                    else:
                        status = "unknown"
                else:
                    status = False

                # print(status)
                _flux_ui_cache_status_list.append(status)

            if prefs.get().debug_mode:
                print(f"======ADD ITEM=====")
                print(f"{path=}")
                print(f"{collection_name=}")
                print(f"{link_shortname=}")
                print(f"{proxy_infos=}")
                print(f"{newItem.is_proxy=}")
                print(f"{newItem.is_shot_level=}")

        context.scene.flux_ui_linkeditor.is_proxy_list = str(is_proxy_list)

        return {"FINISHED"}

class FLUX_LINK_op(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.linkeditor_action"
    bl_label = "Flux Link editor Isolate/Select/reload"
    bl_options = {"INTERNAL"}
    bl_description = ("Isolate Select or reload current sel item")

    item: bpy.props.StringProperty(name="path", default="", description="path")
    action: bpy.props.StringProperty(name="", default="", description="path")

    # todo button bottom with names
    # todo take in account failed link/warning in the UI
    # todo take instanced collection and other type of link?

    # https://docs.blender.org/api/current/bpy.types.ID.html#bpy.types.ID.asset_clear
    # bpy.data.libraries['RF_BG_0000_ztest8_setdressing_v01.blend']

    def execute(self, context: bpy.types.Context) -> Set[str]:

        # Initialize a list to store all collections and their respective file paths
        #  will be fill with the def
        # print(self.item)
        # print(self.action)

        # n_sel = context.scene.flux_ui_linkeditor.is_sel_num
        items = []
        # print(self.item)
        if self.item:
            items = [self.item]
        else:
            for my_item in context.scene.flux_ui_link_list:
                # should be '!= not' but as not is not suppororted, '==' is use
                if my_item.is_sel:
                    if self.action == "RELOAD": items.append(my_item.link_path)
                    else:   items.append(my_item.link_collection)

        # init for isolate case
        values = {}
        protected = []
        hidden = []
        # get all scene collections
        scene_collections = context.scene.collection.children_recursive
        isolate_values_recorded = False
        if self.action == "ISOLATE":
            # get value recorded if any
            rec_values = context.scene.flux_ui_linkeditor.isolate_objsvis
            # if values, already recorded, we back to normal.
            if rec_values:
                isolate_values_recorded = True
                rec_values = json.loads(rec_values)
                # print("REC")
                # print(rec_values)
                for key, value in rec_values.items():
                    col = bpy.data.collections[key]
                    col.hide_viewport = bool(value)

                # empty it
                context.scene.flux_ui_linkeditor.isolate_objsvis = ""
                return {"FINISHED"}
            # if not, we record, and isolate
            else:
                # first record state for all collections
                for each in scene_collections:
                    values[each.name] = each.hide_viewport
                # print(values)
                context.scene.flux_ui_linkeditor.isolate_objsvis = json.dumps(values)

                for item in items:
                    item_col = bpy.data.collections[item]
                    item_childs = item_col.children_recursive
                    item_childs+= blender_libs.collection.get_all_parents(item_col, include_input=True)
                    # item_childs.append(item_col)

                    for each in item_childs:
                        if each not in protected:
                            protected.append(each)
                            # print(f"protect ", each, " from ", item)

                    # record only one as it will be use only in isolate unique mode
                    context.scene.flux_ui_linkeditor.isolate_current = item

                # finally show them
                for each in scene_collections:
                    if each not in protected:
                        each.hide_viewport = True

        # get from selection
        elif self.action == "GET":
            # say to NOT select obj in viewport
            context.scene.flux_ui_linkeditor.select_by_user = False
            obj = bpy.context.selected_objects
            if obj:
                obj = obj[0]
            else:
                # if no sel objs, see collections selected in outliner
                for area in context.screen.areas:
                    if area.type == 'OUTLINER':
                        with context.temp_override(area=area):
                            print(f"{context.selected_ids=}")
                            context.selected_ids = [bpy.data.collections['RF_CH_0015_koala_rig']]
                            result = [item for item in context.selected_ids if isinstance(item, bpy.types.Collection)]
                if result:
                    obj = result[0]
                else:
                    self.report({'ERROR'}, 'Selected obj/collection')
                    return {"CANCELLED"}
            # print(obj)
            if obj.override_library:
                file_path = obj.override_library.reference.library.filepath
                # print(file_path)

                all_linked = list(bpy.context.scene.flux_ui_link_list)
                current_linked = None
                if all_linked:
                    # print(all_linked)
                    for i, each in enumerate(all_linked):
                        if file_path == each.link_path:
                            # print("find", i, each, each.link_collection)
                            self.report({'INFO'}, 'Find  : '+  each.link_collection)
                            break
                    # finally select in UI
                    bpy.context.scene.flux_ui_link_list_index = i

                # if all_linked:
                #     current_linked = all_linked[bpy.context.scene.flux_ui_link_list_index]
            else:
                self.report({'ERROR'}, 'Selected object is not from a LINK !')
                return {"CANCELLED"}

            return {"FINISHED"}

        # process
        # print("---------------------START--------------------")
        # print(f"{items=}")
        # print(f"{self.action=}")
        for item in items:

            # print(item)
            # select\
            if self.action == "SELECT_REPLACE":
                for obj in bpy.context.selected_objects:
                    obj.select_set(False)

            # select
            if self.action == "SELECT" or self.action == "SELECT_REPLACE":

                col = bpy.data.collections[item]
                objs = col.all_objects
                # print(list(objs))
                for obj in list(objs):
                    try:
                        obj.select_set(True)
                    except:
                        print("Cannot select : " + str(obj.name) + " (need ot be adress in a futur release...)")
            # reload
            elif self.action == "RELOAD":
                print("RELOAD")
                file = os.path.basename(item)
                bpy.data.libraries[file].reload()
                # bpy.data.libraries[file].filepath = "W:\01_PRODUCTIONS\012_RUFUS\1_PRE\1_CH\RF_CH_0000_rufuswheelwithanimals\12_RIG\v01\RF_CH_0000_rufuswheelwithanimals_rig_v01.blend"

            elif self.action == "REMOVE":
                print("REMOVE")
                file = os.path.basename(item)
                bpy.data.libraries.remove(library=bpy.data.libraries[file], do_unlink=True)
                # bpy.data.libraries[file].filepath = "W:\01_PRODUCTIONS\012_RUFUS\1_PRE\1_CH\RF_CH_0000_rufuswheelwithanimals\12_RIG\v01\RF_CH_0000_rufuswheelwithanimals_rig_v01.blend"
                idx = context.scene.flux_ui_link_list_index

                # refresh
                bpy.ops.flux.linkeditor_refresh()

                all_linked = list(context.scene.flux_ui_link_list)
                if idx > len(all_linked)-1:
                    idx = len(all_linked)-1

                if all_linked:
                    context.scene.flux_ui_link_list_index = idx
        return {"FINISHED"}

    @classmethod
    def description(cls, context, event):

        opt = getattr(event, "action")
        if opt == "RELOAD" : return f"Reload link"
        elif opt == "REMOVE" : return f"Remove Selected link"
        elif opt == "SELECT" or opt == "SELECT_REPLACE": return f"Select objects in Link"
        elif opt == "ISOLATE" : return f"Isolate Link in scene"


class FLUX_LINK_PROXY_switch(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.link_proxy_apply_settings"
    bl_label = "Flux Proxy Operation"
    bl_options = {"INTERNAL"}
    bl_description = ("Proxy state. Sphere = non-proxy, Cube = Proxy. Clic to switch state.")

    item: bpy.props.StringProperty(name="", default="", description="path")
    idx: bpy.props.IntProperty(default=-1, description="path")

    # this is use to change the description in the UI
    indep_button: bpy.props.BoolProperty(default=False, description="")

    @classmethod
    def description(cls, context, event):

        opt = getattr(event, "indep_button")
        if opt: return f"Apply proxy settings on slelected link in list."

    def execute(self, context: bpy.types.Context) -> Set[str]:
        # todo not multilevel compatible
        # get user selected
        all_linked = list(context.scene.flux_ui_link_list)
        if self.idx >= 0:
            i = self.idx
        else:
            i = context.scene.flux_ui_link_list_index
        current_linked = all_linked[i]

        # get user settings
        is_proxy = context.scene.flux_ui_linkeditor.is_proxy
        proxy_level = context.scene.flux_ui_linkeditor.proxy_level

        all_sel = []
        for each in all_linked:
            if each.is_sel:
                all_sel.append(each)

        # get selected
        if all_sel:
            links = all_sel
        else:
            # futur loop
            links = [current_linked]

        # prgoress icon
        wm = bpy.context.window_manager
        wm.progress_begin(0, len(links))

        self.report({'INFO'}, "Proceed to switch Proxy<>non-Proxy")

        # ---------------------
        # process in links
        for i, link in enumerate(links):
            # print(f"{link=}")
            # print(f"{link.link_path=}")
            # print(f"{link.link_file=}")

            # get path
            path = link.link_path
            path_orig = link.link_path_orig
            file = link.link_file_orig
            if not file:
                file = link.link_file
            # print(f"{link.is_shot_level=}")
            if link.is_shot_level:
                infos = context.scene.flux_ui_linkeditor.shotlevel_infos
                infos = json.loads(infos)
                # print(f"{infos=}")
                file = infos[1]


            # if we go from a button, determine if we are proxy of not
            if self.idx >= 0:
                is_proxy = proxy.is_proxy_from_item_path(path)
                is_proxy = bool(is_proxy)
                is_proxy = not is_proxy

            # ------------------------------------
            # switch orig > proxy
            if is_proxy:

                # this allow to swtich from one proxy level to another
                #  if its a proxy, get the non-proxy path
                is_proxy = proxy.is_proxy_from_item_path(path)
                if is_proxy:
                    path = is_proxy[0]

                # get proxy path from non-prxy
                proxy_path = proxy.get_from_item_path(path, proxy_level)
                # print(f"{proxy_path=}")

                # refresh
                bpy.data.libraries[file].filepath = proxy_path
                link.is_proxy = True

                # record the orig path for later
                link.link_path = proxy_path                       # proxy path to current path
                link.link_path_orig = path                        # recorded path to orig
                link.link_file = os.path.basename(proxy_path)     # update the file name
                link.link_file_orig = os.path.basename(path)      # update the file name
                link.proxy_level = proxy_level                    # update proxy level
                link.is_already_setted = True                     # this will never be setted to false.
            # switch proxy > orig
            else:
                bpy.data.libraries[file].filepath = path_orig
                link.is_proxy = False

                # record back the orig path
                link.link_path = path_orig                        # back path orig
                link.link_path_orig = ""                          # empty orig. no need anumore
                link.link_file = os.path.basename(path_orig)      # back file
                link.link_file_orig = ""                          # empty orig. no need anumore

            # reload to take change effective
            bpy.data.libraries[file].reload()

            # print(bpy.data.libraries[file].filepath)
            # print(list(bpy.data.libraries))

            wm.progress_update(i)

        # force to redraw the UI, if we are in button mode
        if self.idx >= 0:
            if self.idx == context.scene.flux_ui_link_list_index:
                context.scene.flux_ui_linkeditor.is_proxy_silentswitch = True
                context.scene.flux_ui_linkeditor.is_proxy = is_proxy
                context.scene.flux_ui_linkeditor.is_proxy_silentswitch = False

        # reset to default
        self.idx = -1

        wm.progress_end()

        return {"FINISHED"}

    def invoke(self, context, event):

        # if not multilevel
        if not prefs.get().proxy_is_multilevel:
            return self.execute(context)

        # if multilevel and hand selection
        if self.idx >= 0:

            all_linked = list(context.scene.flux_ui_link_list)
            i = self.idx
            current_linked = all_linked[i]

            if not current_linked.is_proxy:
                return context.window_manager.invoke_props_dialog(self)
            else:
                return self.execute(context)
        # if multilevel and not going though operator in UIlist
        else:
            return self.execute(context)

    def draw(self, context):
        # todo better text on window
        layout = self.layout
        col = layout.column()
        # self.layout.label("TEST")
        col.prop(context.scene.flux_ui_linkeditor, "proxy_level", text="Level?")


class FLUX_LINK_PROXY_asset2shot(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.link_proxy_asset_to_shot"
    bl_label = "Flux Link editor asset to shot"
    bl_options = {"INTERNAL"}
    bl_description = ("Convert link : asset to shot : Copy the current asset, and his proxies to the shot level directory and update the link")

    dest_path_exist: bpy.props.BoolProperty(default=False)
    do_copy: bpy.props.BoolProperty(default=True)
    shot_message: bpy.props.StringProperty(default="None")

    def execute(self, context: bpy.types.Context) -> Set[str]:
        # print("asset 2 shot")
        #
        # current_item = ui.get_current_user_item(get_category=True)
        # print(current_item)
        #
        # # error handle
        # #  -- we are in shot
        # if current_item['category'] == "assets":
        #     self.report({"ERROR"}, f"Cannot be in assets")
        #     return {"FINISHED"}
        #
        # #  -- multisel is on
        all_linked = list(context.scene.flux_ui_link_list)
        # for each in all_linked:
        #     if each.is_sel:
        #         self.report({"ERROR"}, f"Convertion only work on the current select, not in multi-selection")
        #         return {"FINISHED"}
        #
        # # go
        current_linked = all_linked[context.scene.flux_ui_link_list_index]
        #
        # #  -- copy only from is proxy off
        # if current_linked.is_proxy:
        #     self.report({"ERROR"}, f"Oonvert only with non proxy. Proxies will be copied")
        #     return {"FINISHED"}
        #
        #
        # # ----------------------
        # # 1- get source and destination
        orig_path = current_linked.link_path
        print(f"{orig_path=}")
        # current_item['task'] = "proxy"
        # current_item['version'] = [1]
        # dest_item_path = file_access.get_item_path2(current_item, only_file_path=True)
        # print(f"{dest_item_path=}")

        dest_item_path = self.dest_item_path

        # 2- set persisitent infos
        # if not linkEditor_persistent_infos("exist"):linkEditor_persistent_infos("create")
        # set infos , as array
        #  0 : link_collection
        #  1 : link_file
        #  2 : link_path
        infos = [str(current_linked.link_collection), str(current_linked.link_file), str(current_linked.link_path)]
        # linkEditor_persistent_infos("set", json.dumps(infos))
        print(f"{infos=}")
        context.scene.flux_ui_linkeditor.shotlevel_infos = json.dumps(infos)

        wm = bpy.context.window_manager
        wm.progress_begin(0, 100)

        if self.do_copy:
            # 3- check if proxies exist
            proxies = ui.is_proxy_available_in_folder(current_linked)
            print(f"{proxies=}")

            # set files to copy
            to_copy = [(orig_path, dest_item_path)]

            if isinstance(proxies, list):
                for proxy_level in proxies:
                    orig_prx = proxy.get_from_item_path(orig_path, proxy_level)
                    dest_prx = proxy.get_from_item_path(dest_item_path, proxy_level)
                    to_copy.append((orig_prx, dest_prx))
            else:
                if proxies:
                    orig_prx = proxy.get_from_item_path(orig_path)
                    dest_prx = proxy.get_from_item_path(dest_item_path)
                    to_copy.append((orig_prx, dest_prx))

            # print(f"{to_copy=}")

            # return {"FINISHED"}
            # 4- copy file at shot level
            # todo create folder if doesnt exist
            # todo test if file exist
            print("copy file(s)...")
            for i, copy in enumerate(to_copy):
                print(f"{copy=}")

                # test if dir exist, if not crate it
                dest_dir = os.path.dirname(copy[1])
                ex = os.path.exists(dest_dir)
                # print(f"{ex=}")
                if not ex:
                    os.makedirs(dest_dir)

                # copy it
                shutil.copyfile(copy[0], copy[1])

                wm.progress_update(i)

        # 4- set new infos in scene, in link editor
        current_linked.is_shot_level = True
        current_linked.link_path = dest_item_path

        file = current_linked.link_file
        if "proxy" in file:
            infos = context.scene.flux_ui_linkeditor.shotlevel_infos
            if infos:
                # print(type(infos))
                infos = json.loads(infos)
                file =os.path.basename(infos[2])
            else:
                self.report({"ERROR"}, f"Spomething went wront")
                return {"CANCELLED"}

        print(f"{file=}")
        # print("infos")
        print(f"{context.scene.flux_ui_linkeditor.shotlevel_infos=}")

        bpy.data.libraries[file].filepath = dest_item_path
        bpy.data.libraries[file].reload()

        if self.do_copy:
            self.report({"INFO"}, f"Convertion to shot level asset for {str(len(to_copy))} proxys at current shot")
        else:
            self.report({"INFO"}, f"Convertion to shot level asset done at current shot")

        wm.progress_end()

        return {"FINISHED"}

    def invoke(self, context, event):

        current_item = ui.get_current_user_item(get_category=True)
        print(current_item)

        # error handle
        #  -- we are in shot
        if current_item['category'] == "assets":
            self.report({"ERROR"}, f"Cannot be in assets")
            return {"FINISHED"}

        #  -- multisel is on
        all_linked = list(context.scene.flux_ui_link_list)
        for each in all_linked:
            if each.is_sel:
                self.report({"ERROR"}, f"Convertion only work on the current select, not in multi-selection")
                return {"CANCELLED"}

        # check if we are not at previz level
        project_config = prefs.get_project_config_dict()
        previz_tag = project_config["project_data"]["previz_tag"]
        # print(f"{previz_tag=}")

        if not "shot" in list(current_item.keys()):
            self.report({"ERROR"}, f"You are not in the good context")
            return {"CANCELLED"}

        if previz_tag in current_item['shot']:
            self.report({"ERROR"}, f"Convert to shot level cannot be done on previz shot. Use 'Convert to shot layout' tool")
            return {"CANCELLED"}

        # go
        current_linked = all_linked[context.scene.flux_ui_link_list_index]

        #  -- copy only from is proxy off
        if current_linked.is_proxy:
            self.report({"ERROR"}, f"Convert only with non proxy. Proxies will be copied")
            return {"FINISHED"}


        # ----------------------
        # 1- get source and destination
        orig_path = current_linked.link_path
        print(f"{orig_path=}")
        current_item['task'] = "proxy"
        current_item['version'] = [1]
        dest_item_path = file_access.get_item_path2(current_item, only_file_path=True)
        print(f"{dest_item_path=}")

        self.dest_item_path = dest_item_path

        self.current_link_name = current_linked.link_collection
        self.current_link_file = current_linked.link_file

        # ---
        if os.path.exists(dest_item_path):
            self.dest_path_exist=True
            self.do_copy=False
        else:
            self.dest_path_exist=False
            self.do_copy=True

        self.report({'INFO'}, "Proceed to switch Proxy<>non-Proxy")

        return context.window_manager.invoke_props_dialog(self, width=500)


    def draw(self, context):
        layout = self.layout
        col = layout.column()
        # self.layout.label("TEST")

        col.label(text="Current Link Name : " + self.current_link_name)
        col.label(text="Current Link File : " + self.current_link_file)

        col.label(text="Copy to : " + self.shot_message)

        # - box -
        box = col.box()
        col2 = box.column()
        if not self.dest_path_exist:
            col2.label(text="Asset item will be copied, with proxies, at shot level")
        else:
            col2.label(text="Shot level files already exist.")
            col2.prop(self, "do_copy", text="Overwrite file(s) with new copy?")


class FLUX_LINK_PROXY_shot2asset(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.link_proxy_shot_to_asset"
    bl_label = "Flux Link editor shot to asset"
    bl_options = {"INTERNAL"}
    bl_description = ("Convert link : shot to asset : update the current shot level link to asset level.")

    def execute(self, context: bpy.types.Context) -> Set[str]:
        print("shot 2 asset")

        current_item = ui.get_current_user_item(get_category=True)
        print(f"{current_item=}")

        # error handle
        #  -- we are in shot
        if current_item['category'] == "assets":
            self.report({"ERROR"}, f"Cannot be in assets")
            return {"FINISHED"}

        #  -- multisel is on
        all_linked = list(context.scene.flux_ui_link_list)
        for each in all_linked:
            if each.is_sel:
                self.report({"ERROR"}, f"Convertion only work on the current select, not in multi-selection")
                return {"FINISHED"}

        # go
        current_linked = all_linked[context.scene.flux_ui_link_list_index]

        #  -- copy only from is proxy off
        if current_linked.is_proxy:
            self.report({"ERROR"}, f"Convert only with non proxy. Proxies will be copied")
            return {"FINISHED"}

        # ----------------------
        # todo put that in def with proxy on shot level libe 3502
        # 1- get persisitent infos
        #  0 : link_collection
        #  1 : link_file
        #  2 : link_path
        infos = context.scene.flux_ui_linkeditor.shotlevel_infos
        # print(f"{infos=}")
        infos = json.loads(infos)

        # 2- set new infos in scene, in link editor
        current_linked.is_shot_level = False
        current_linked.link_path = infos[2]
        file = infos[1]
        current_linked.link_file = file

        #---
        # todo same as 3152. put in def
        link_item = file_access.get_item_from_path(infos[2])
        if link_item:
            val = list(link_item.values())
            link_shortname = f"{val[2]} || {val[1][0:4]} / {val[3]}"
            current_linked.link_shortname = link_shortname
        #---

        if "proxy" in file:
            if infos:
                # print(type(infos))
                file =os.path.basename(infos[2])
            else:
                self.report({"ERROR"}, f"Spomething went wront")
                return {"CANCELLED"}

        bpy.data.libraries[file].filepath = infos[2]
        bpy.data.libraries[file].reload()

        # reset infos
        context.scene.flux_ui_linkeditor.shotlevel_infos = ""

        return {"FINISHED"}

    def invoke(self, context, event):
        self.report({'INFO'}, "Proceed... Please wait")
        return self.execute(context)

class FLUX_LINK_PROXY_printInfos(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.link_proxy_print_infos"
    bl_label = "Flux Link editor print infos"
    bl_options = {"INTERNAL"}
    bl_description = ("print infos for debug about the current selected link")

    def execute(self, context: bpy.types.Context) -> Set[str]:
        print("---------------------------------------------------------------------------------")
        all_linked = list(context.scene.flux_ui_link_list)
        current_linked = all_linked[context.scene.flux_ui_link_list_index]
        print(f"--Selected n={context.scene.flux_ui_link_list_index}, {current_linked}--")
        print("---------------------------------------------------------------------------------")
        all_itemsValues = list(current_linked.items())
        for each in all_itemsValues:
            print(f"{each[0]} = {each[1]}")
        print("---------------------------------------------------------------------------------")
        print(f"all libraries keys={list(bpy.data.libraries.keys())}")
        print("---------------------------------------------------------------------------------")

        self.report({"INFO"}, "debug infos printed in console")
        return {"FINISHED"}

class FLUX_LINK_PROXY_refresh_props(bpy.types.Operator):
    """
    Starts the Session, which  is stored in blender_kitsu addon preferences.
    Authenticates user with server until session ends.
    Host, email and password are retrieved from blender_kitsu addon preferences.
    """

    bl_idname = "flux.link_proxy_refresh_props"
    bl_label = "Flux Link editor refresh props"
    bl_options = {"INTERNAL"}
    bl_description = ("refresh proxy settings from link")

    # item: bpy.props.StringProperty(name="path", default="", description="path")
    # action: bpy.props.StringProperty(name="", default="", description="path")

    def execute(self, context: bpy.types.Context) -> Set[str]:
        ui_props.update_linkeditor_list_index("", context)

        return {"FINISHED"}