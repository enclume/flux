''' dont import this file, it's just a dev unit, will be deleted at the end '''

import os
import json

import config
import database_access
#import file_access

try :
    import requests
except :
    import pip


    

def get_item_naming_issues(item) -> str:
    item = item.split("_")

    if len(item) < 4 :
        return "  -> the name should have this structure : [PROJECT]_[TYPE]_[ID]_[item_name]"
    
    item_project = item[0]
    item_type = item[1]
    item_id = item[2]
    item_name = "_".join(item[3:])

    if item_project == "RF" :
        item_project = None
    else :
        item_project = f"  -> \"{item_project}\" should be \"RF\""

    if len(item_type) == 2 and item_type.isalpha() and item_type.isupper():
        item_type = None
    else :
        item_type = f"  -> \"{item_type}\" should have exactly 2 non-numerical characters in uppercase"

    if len(item_id) == 4 and item_id.isdigit() :
        item_id = None
    else :
        item_id = f"  -> \"{item_id}\" should have exactly 4 digits"

    if item_name.islower() and " " not in item_name :
        item_name = None
    elif item_name.isdigit() :
        item_name = None
    else :
        item_name = f"  -> \"{item_name}\" should be in lowercase with no blank space"

    item_data = [item_project, item_type, item_id, item_name]
    final_return = []

    for data in item_data:
        if data != None:
            final_return.append(data)


    if final_return :
        return "\n".join(final_return)
    
    return None
  

def get_all_naming_issues():

    base_path = r"W:\01_PRODUCTIONS\012_RUFUS\1_PRE"

    for dir_type in os.listdir(base_path):
        path_type = base_path + "\\" + dir_type

        for dir_item in os.listdir(path_type) :
            report = get_item_naming_issues(dir_item)

            if report :
                print()
                print(f"item [{dir_item}] naming issue :")
                print(report)

    return




if __name__ == '__main__' :
    

    '''
    project_config = path_from_config.project_config

    print(file_access.get_item_versionning_tree({"project": "rufus", "category": "assets", "type": "Character", "name": "Rufus", "task": "modeling"}, remove_empty = False))
    print()
    print(file_access.get_item_versionning_tree({"project": "rufus", "category": "assets", "type": "Character", "name": "Rufus", "task": "modeling"}, remove_empty = True))
    print()

    #test()'''

    import flux_logger
    flux_logger.set_level_all_loggers("WARNING")
    config.get_project_config("rufus")

    os.system("cls")
    print("___start___")

    #w_ref = "W:\\01_PRODUCTIONS\\012_RUFUS\\0_DEV\\00_PIPELINE\\colormanagement"
    #os.startfile(w_ref)
    #get_all_naming_issues()
    #print(True + True + False + False)

    if False :
        filetree = config.path.class_tree.file_tree(config.get_project_config("rufus"))
        #tree.print_tree()
        json_tree = filetree.get_tree()
        json_tree = json.dumps(json_tree, indent=1)
        #print(json_tree)
        finalpath = filetree.get_path({"category": "assets", "type": "Character", "name": "Rufus", "task": "modeling"})
        print(finalpath)
    elif False :
        path = r'W:\01_PRODUCTIONS\012_RUFUS\1_PRE\2_BG\RF_BG_0000_ztest6\14_SET_DRESSING\v01\r001\w0003\RF_BG_0000_ztest6_setdressing_v01_r001_w0003.blend'
        print(config.path.get_proxy_from_item_path(path))
    else :
        config.get_project_config('rufus')
        item_data = {"project": "rufus", "category": "assets", "type": "prop", "name": "ztest3", "task": "modeling", "version": [1, 1, 1]}
        item_data = {"project": "rufus", "category": "shots", "seq": "sq0000", "shot": "sq0000_sh0000", "task": "Layout", "version": [1, 1, 1]}
        item_data["version"] = config.path.convert_version_to_dict(item_data["version"])
        item_file_extention = ".blend"
        with_version = True
        #path_a = config.path.old_get_filepath_from_item_data(item_data, item_file_extention, with_version)
        #path_b = config.path.get_filepath_from_item_data(item_data, item_file_extention, with_version)
        azer = r"W:\01_PRODUCTIONS\012_RUFUS\2_PROD\02_ANIM\sq0000_anim\sq0000_sh0000_anim\01_LAYOUT\v01\r001\w0001\sq0000_sh0000_layout_v01_r001_w0001.blend"
        path_c = config.path.get_item_data_from_path(azer)

        #print(path_a)
        #print(path_b)
        print(path_c)
        print()