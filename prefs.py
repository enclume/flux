# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Flux Pipeline Manager

Manage User Prefs
Low Level preference access
'''
import json

import bpy
# try: from . import prefs
from . import config
from . import flux_logger

log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)

# save template if future error. Collapse context for now
# def get(context: bpy.types.Context) -> bpy.types.AddonPreferences:

def get() -> bpy.types.AddonPreferences:
    """
    get all preferences
    """

    # print("session is auth try : ", bpy.context.preferences.addons["flux"].preferences.session)

    return bpy.context.preferences.addons["flux"].preferences


def get_file_indicator() -> str :
    ''' get a project config file indicator from the user prefs '''

    if get().project_file_type == "internal":
        file_indicator = get().projects_list
    else:
        file_indicator = get().production_settings_file

    if not file_indicator or file_indicator == "None" :
        log.error(f'Please select a project config (file_indicator : {file_indicator})')

    return file_indicator


def get_project_config_dict() -> dict :
    ''' get the project dict based on a file indicator from the user prefs '''

    file_indicator = get_file_indicator()
    # print(f"from prefs : {file_indicator=}")

    return config.get_project_config(file_indicator)


def get_project_path_root() -> str :
    """
    return the name of the current df project name
    """
    return get().project_path_root


def get_project_name(db=False) -> str :
    """
    return the name of the current df project name
    """

    prj = get().project_name_short
    if db:
        prj = get().project_name

    #
    return prj


def is_debug_mode() -> bool :
    '''
    Check debug mode state

    '''
    if get().debug_mode:
        if get().debug_frontend_mode:
            return True
        else:
            return False
    else:
        return False


def is_use_ui_cache() -> bool :
    '''
    use ui cache

    '''
    if bpy.context.preferences.addons["flux"].preferences.Use_ui_cache:
        return True
    else:
        return False


def is_show_linkeditor() -> bool :
    '''
    show linkeditor or not

    '''
    if bpy.context.preferences.addons["flux"].preferences.show_linkeditor:
        return True
    else:
        return False

def is_show_tools() -> bool:
    '''
    show composer or not

    '''
    if bpy.context.preferences.addons["flux"].preferences.show_tools:
        return True
    else:
        return False

def is_show_composer() -> bool:
    '''
    show composer or not

    '''
    if bpy.context.preferences.addons["flux"].preferences.show_composer:
        return True
    else:
        return False
    

def is_show_sanity_check() -> bool :
    ''' show sanity check or not '''
    if bpy.context.preferences.addons["flux"].preferences.show_sanity_check:
        return True
    
    return False


def ui_redraw() -> None :
    """
    Forces blender to redraw the UI.
    """
    for screen in bpy.data.screens:
        for area in screen.areas:
            area.tag_redraw()


def get_bpy_attributs(what, print_out:bool =False) -> dict :
    """
    print out/return  element of a bpy. something
    Thx guillaume!!
    Args:
        what: the bpy.somthing to get/print
        print_out: print out. Use of debug infos

    Returns: dict of result

    """
    r = {}

    for pref in dir(what):
        if not pref.startswith("__") and not "db_password" in pref:
            r[pref] = getattr(what, pref)
            if print_out:
                print(f"{pref} : {getattr(what, pref)}")

    return r


def update_db_name(client):
    """
    Update db with the current name.
    Args:
        client ():
    """
    if not client:
        get().db_fullname = ""
        get().db_firstname = ""
        get().db_lastname = ""
    else:
        get().db_fullname = client["full_name"]
        get().db_firstname = client["first_name"]
        get().db_lastname = client["last_name"]


# ---------------------------------------
# bookmarks sub proc.
#   for composer is .... for composer ;)
# ---------------------------------------
def bookmarks_get(for_composer: bool = False) :
    """
    Get all bookarks entries
    Args:
        where (): all bkms entryes al list(dict)

    Returns:

    """
    if for_composer:
        bkm = get().composer_bookmarks
    else:
        bkm = get().item_bookmarks
    bkm = json.loads(bkm)

    # print(f"{bkm=}")

    return bkm


def bookmarks_is_item_exist(for_composer: bool = False) -> bool :
    """
    Is current item exist?
    Args:
        where ():

    Returns:Book

    """
    bkms = bookmarks_get(for_composer)
    item = bookmarks_get_current_item(for_composer)
    for bkm in bkms:
        if item == bkm:
            return True

    return False


def bookmarks_remove(for_composer: bool = False) -> bool :
    """
    remove current item if its in the bookmark
    Args:
        where ():

    Returns:Bool

    """
    bkms = bookmarks_get(for_composer)
    item = bookmarks_get_current_item(for_composer)

    to_remove = ""
    for bkm in bkms:
        if item == bkm:
            to_remove = item

    # print(bkms)
    # print(to_remove)

    if to_remove:
        bkms.remove(to_remove)
        if for_composer:
            get().composer_bookmarks = json.dumps(bkms)
        else:
            get().item_bookmarks = json.dumps(bkms)
        return True
    else:
        return False


def bookmarks_add(for_composer: bool = False) :
    """
    Add entry to the bookmark with current item
    Args:
        where ():
    """
    item = bookmarks_get_current_item(for_composer, as_string=True)

    bkm = bookmarks_get(for_composer)
    bkm.append(item)

    if for_composer:
        get().composer_bookmarks = json.dumps(bkm)
    else:
        get().item_bookmarks = json.dumps(bkm)


def bookmarks_clear(for_composer: bool = False) :
    """
    Clear all bookmarks entries
    Args:
        where ():
    """
    print("remove", for_composer)
    if for_composer:
        get().composer_bookmarks = "[]"
    else:
        get().item_bookmarks = "[]"


def bookmarks_get_current_item(for_composer: bool = False, as_string: bool = True) -> dict :
    """
    get current item in bookmark format. Wich is dict current item with cat and proj.
    Args:
        where ():
        as_string (): return as string

    Returns:Item in format

    """
    if for_composer:
        item = json.loads(bpy.context.scene.flux_ui_composer.current_user_item)
        item["cat"] = "assets"

        # if list
        if bpy.context.scene.flux_ui_composer.load_type == "list":
            loaded_items = []
            for my_item in bpy.context.scene.flux_ui_composer_list:
                if my_item.item_load:
                    loaded_items.append(my_item.item_name)

            item["name"] = str(loaded_items)

    else:
        item = json.loads(bpy.context.scene.flux_ui.current_user_item)
        item["cat"] = bpy.context.scene.flux_ui.current_category

    # remove categroy if any
    if "category" in list(item.keys()):
        item.pop("category")

    item["project"] = get_project_name()
    if as_string:
        item = json.dumps(item)

    return item

