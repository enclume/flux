# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

import bpy

import os
import filecmp
import json
import shutil
import urllib.request

from .. import bl_info

# initialize logger
from .. import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)

def get_dir_files(dir_path, as_list = False):
    ''' description TODO '''
    if not os.path.exists(dir_path) :
        log.error(f"dir_path does not exist\n->({dir_path})")
        return False
    
    dir_files = {}
    if as_list :
        dir_files = []
    
    def recursive_get_dir_files(dir_path, dir_files) :
        ''' description TODO '''
        for item in os.listdir(dir_path):
            item_path = dir_path + "\\" + item
            
            if os.path.isdir(item_path) :
                recursive_get_dir_files(item_path, dir_files)
                continue

            if as_list :
                dir_files.append(item)
            else :
                dir_files.update({item: item_path})
                    
                
    recursive_get_dir_files(dir_path, dir_files)

    return dir_files


def get_dir_folders(dir_path, as_list = True, with_root = True):
    ''' description TODO '''
    if not os.path.exists(dir_path) :
        log.error(f"dir_path does not exist\n->({dir_path})")
        return False
    
    dir_folders = {}
    if as_list :
        dir_folders = []
        
    if with_root :
        if as_list :
            dir_folders.append("ROOT")
        else :
            dir_folders.update({"ROOT": dir_path})
    
    def recursive_get_dir_folders(dir_path, dir_folders) : 
        ''' description TODO '''   
        for dir in os.listdir(dir_path):
            subdir_path = dir_path + "\\" + dir
            if not os.path.isdir(subdir_path) : continue

            if as_list :
                dir_folders.append(dir)
            else :
                dir_folders.update({dir: subdir_path})
                
            recursive_get_dir_folders(subdir_path, dir_folders)
                       
    recursive_get_dir_folders(dir_path, dir_folders)

    return dir_folders


def compare_file_hierarchy(test, ref):
    ''' description TODO '''
    if not os.path.exists(test) :
        log.error(f"dir_path does not exist\n->({test})")
    if not os.path.exists(ref) :
        log.error(f"dir_path does not exist\n->({ref})")
    if not os.path.exists(test) or not os.path.exists(ref) :
        return False
    
    test_dir_folders = get_dir_folders(test)
    test_dir_files = get_dir_files(test)
    
    ref_dir_folders = get_dir_folders(ref)
    ref_dir_files = get_dir_files(ref)
    
    if test_dir_folders != ref_dir_folders :
        log.debug("folders lists are not the same\n-> test\n-> ref")
        return False
    
    if test_dir_files.keys() != ref_dir_files.keys() :
        log.debug("files lists are not the same\n-> test\n-> ref")
        return False
    
    for ref_item in ref_dir_files.keys():
        ref_path = ref_dir_files[ref_item]
        test_path = test_dir_files[ref_item]
        
        if not filecmp.cmp(test_path, ref_path) :
            log.debug(f"the two files {ref_item} are not the same\n-> test\n-> ref")
            return False
    
    log.debug("the two file hierarchies are the same !\n-> test\n-> ref")
    return True


def replace_file_hierarchy(bad, good):
    ''' description TODO '''
    log.debug("replace_file_hierarchy")
    log.debug(f"bad : {bad}")
    log.debug(f"good : {good}")
    #shutil.rmtree(bad)
    shutil.copytree(good, bad, dirs_exist_ok=True)
    log.debug("done")
    return

def get_flux_last_tag():
    ''' description TODO '''
    api_link = r"https://gitlab.com/api/v4/projects/46461988"
    api_link = api_link + r"/repository/tags"

    with urllib.request.urlopen(api_link) as response:
        html = response.read()

    html = json.loads(html)
    last_tag = html[0]["name"]

    return last_tag

def get_flux_current_tag():
    ''' description TODO '''
    current_tag = ""

    for digit in list(bl_info["version"]):
        current_tag += f".{digit}"

    return current_tag[1:]