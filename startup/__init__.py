# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
module used to handle a blender startup
'''

import bpy
import os

from . import fct_utils
from . import class_startup_manager as cls_sm
from .. import blender_libs
from .. import prefs

# initialize logger
from .. import flux_logger 
log = flux_logger.get_flux_logger()


#reload lib
import importlib
importlib.reload(flux_logger)
importlib.reload(blender_libs)
importlib.reload(fct_utils)
importlib.reload(cls_sm)

startuper = None

class FLUX_STARTUP_Flux_Update(bpy.types.Operator):
    bl_idname = "flux.startup_flux_update"
    bl_label = "startup flux update message"
    bl_options = {"REGISTER"}
    bl_description = "startup flux update message"

    auto_resolve: bpy.props.BoolProperty(default=True, description="Auto Resolve")

    def execute(self, context):
        #print("FLUX_STARTUP_Flux_Update executed !")
        return {"FINISHED"}
    
    def draw(self, context):
        layout = self.layout
        col = layout.column()
        if startuper.uptodate :
            self.auto_resolve = False
            col.label(text="Flux is up to date !")
            return
        
        col.label(text="There is a new version of Flux !")
        #col.prop(self, "auto_resolve", text="update Flux", expand=True)
    
    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self, width=500)
    

class FLUX_STARTUP_Popup(bpy.types.Operator):
    bl_idname = "flux.startup_popup"
    bl_label = "startup message"
    bl_options = {"REGISTER"}
    bl_description = "startup message"

    auto_resolve: bpy.props.BoolProperty(default=True, description="Auto Resolve")

    def execute(self, context):
        if not self.auto_resolve :
            return {"FINISHED"}

        if not prefs.get().check_color_management:
            return {"FINISHED"}

        install_path = blender_libs.get_colormanagement_install_path()

        w_ref = "W:\\01_PRODUCTIONS\\012_RUFUS\\0_DEV\\00_PIPELINE" # TODO hard coded

        instructions = "W:\\01_PRODUCTIONS\\012_RUFUS\\0_DEV\\00_PIPELINE\\Flux\\colormanagement_instructions.txt" # TODO hard coded

        stop_exe = False

        if not os.path.exists(install_path) :
            stop_exe = True
            self.report({"ERROR"}, f"{install_path} does not exist")
        if not os.path.exists(w_ref) :
            stop_exe = True
            self.report({"ERROR"}, f"{w_ref} does not exist")

        w_ref_cm = os.path.join(w_ref, "colormanagement")
        if not os.path.exists(w_ref_cm) :
            stop_exe = True
            self.report({"ERROR"}, f"{w_ref_cm} does not exist")

        if stop_exe :
            return {"FINISHED"}
        
        try :
            fct_utils.replace_file_hierarchy(install_path, w_ref)
            self.report({"INFO"}, f"Colormanagement successfully fixed !")
            return {"FINISHED"}
        except Exception as e:
            log.info(f"can't do the colormanagement fix by script :\n{e}")

        if not os.path.exists(instructions) :
            self.report({"ERROR"}, f"{instructions} does not exist")
            return {"FINISHED"}

        os.startfile(install_path)
        os.startfile(w_ref)

        os.startfile(instructions)

        return {"FINISHED"}
    
    def draw(self, context):
        layout = self.layout
        col = layout.column()
        
        if startuper.colormanagement == True :
            self.auto_resolve = False
            col.label(text="All is fine")
            return
        
        if startuper.colormanagement :
            self.auto_resolve = False
            col.label(text=f"there is an issue with the colormanagement procedure :\n{startuper.colormanagement}")
            return
        
        col.label(text="You don't have the right colormanagement !")
        col.prop(self, "auto_resolve", text="Fix it", expand=True)
    
    def invoke(self, context, event):

        # add to prevent flooding
        if not prefs.get().check_color_management:
            return {"FINISHED"}

        return context.window_manager.invoke_props_dialog(self, width=500)

def launch_startup():
    ''' description TODO '''
    global startuper
    startuper = cls_sm.startup_manager()

    blender_libs.timer.run_fct(startuper.start_Flux_Update)
    blender_libs.timer.run_fct(startuper.start_Flux_Popup)
    blender_libs.timer.run_fct(startuper.set_display_device)
    return


startup_register_classes = [FLUX_STARTUP_Flux_Update, FLUX_STARTUP_Popup]