# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

import bpy
import os

from . import fct_utils
from .. import blender_libs
from .. import prefs

# initialize logger
from .. import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)
importlib.reload(fct_utils)
importlib.reload(prefs)

class startup_manager:
    ''' description TODO '''
    def __init__(self, name = "startup_manager"):
        self.name = name
        self.current_version = fct_utils.get_flux_current_tag()
        self.last_version = fct_utils.get_flux_last_tag()
        self.uptodate = self.is_flux_uptodate()
        self.colormanagement = self.is_colormanagement_ok()
    

    def is_flux_uptodate(self):
        ''' description TODO '''
        if self.current_version == self.last_version :
            print(f"flux is up to date (v{self.last_version}) :)")
            return True
        print(f"flux is not up to date :(")
        print(f"current v{self.current_version}, new v{self.last_version}")
        return False
    

    def is_colormanagement_ok(self):
        ''' description TODO '''
        install_path = blender_libs.get_colormanagement_install_path()
        install_path += "\\colormanagement"
        w_ref = "W:\\01_PRODUCTIONS\\012_RUFUS\\0_DEV\\00_PIPELINE\\colormanagement" # TODO hard coded

        if not os.path.exists(w_ref) or not os.path.exists(install_path) :
            log.error(f"can't proceed.\nw_ref exist : {os.path.exists(w_ref)}\ninstall_path exist : {os.path.exists(install_path)}")
            to_return = ["path_issue", ":"]
            if not os.path.exists(w_ref) : to_return.append("w_ref")
            if not os.path.exists(install_path) : to_return.append("install_path")
            to_return = " ".join(to_return)
            return to_return
        
        return fct_utils.compare_file_hierarchy(install_path, w_ref)
    

    def start_Flux_Update(self):
        ''' description TODO '''
        if self.uptodate :
            return
        if not prefs.get().check_update_startup:
            return
        bpy.ops.flux.startup_flux_update("INVOKE_DEFAULT")


    def start_Flux_Popup(self):
        ''' description TODO '''
        if self.colormanagement == True :
            log.debug("colormanagement is OK !")
            return
        bpy.ops.flux.startup_popup("INVOKE_DEFAULT")


    def set_display_device(self):
        ''' description TODO '''
        context = bpy.context
        #print("set_display_device")
        #print(context)
        if "filepath" in dir(context) and context.filepath.startswith("W:\\01_PRODUCTIONS\\012_RUFUS"): # TODO hard coded
            context.scene.display_settings.display_device = "ACES"