# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
This is the core for proxy edit linked.

Its a total copy of the edit link buildin blender. No shame...
'''


import bpy
import logging
import os

from bpy.app.handlers import persistent

logger = logging.getLogger('flux_edit_linked')

refresh = True
# uncomment this to not reset when reload script
# refresh = False

if refresh:
    settings = {
        "original_file": "",
        "linked_file": "",
        "linked_objects": [],
        "linked_nodes": []
        }


@persistent
def linked_file_check(context: bpy.context):
    if settings["linked_file"] != "":
        if os.path.samefile(settings["linked_file"], bpy.data.filepath):
            logger.info("Editing a linked library.")
            bpy.ops.object.select_all(action='DESELECT')
            for ob_name in settings["linked_objects"]:
                bpy.data.objects[ob_name].select_set(True) # XXX Assumes selected object is in the active scene
            # if len(settings["linked_objects"]) == 1:
            #     context.view_layer.objects.active = bpy.data.objects[settings["linked_objects"][0]]
        else:
            # For some reason, the linked editing session ended
            # (failed to find a file or opened a different file
            # before returning to the originating .blend)
            settings["original_file"] = ""
            settings["linked_file"] = ""

    # pass


def draw_proxy_link_edit(context, layout):
    """
    draw the edit link part
    """
    # print(f"{settings=}")
    # print(f"{context.scene.flux_ui_link_list_index=}")

    line_box_line2 = layout.column(align=True)

    en = False
    # name = "None"
    # filepath = "None"
    if context.active_object:
        if context.active_object.override_library:
            en = True
            # name = context.active_object.override_library.reference.name
            # filepath = context.active_object.override_library.reference.library.filepath

    # --
    if settings["original_file"]:
        line_box_line2.label(text=f"You'r editing a link")
        filepath = settings["linked_file"]
    else:
        all_linked = list(context.scene.flux_ui_link_list)
        # print(f"{all_linked}")
        if not all_linked:
            line_box_line2.label(text=f"No link recorded and no edit neither...")
            return

        current_linked = all_linked[context.scene.flux_ui_link_list_index]
        filepath = current_linked.link_path
        name = current_linked.link_collection

        line_box_line2.label(text=f"Sel link Obj : {name}")
    line_box_line2.label(text=f"From Path : ...{os.path.basename(filepath)}")

    # --
    if settings["original_file"]:
        line_box_line2.operator(FLUX_OBJ_OT_ReturnToOriginal.bl_idname, icon="LOOP_BACK", text="Back to Orig")
        en = True
    else:
        op = line_box_line2.operator(FLUX_OBJ_OT_EditLinked.bl_idname, icon="LINK_BLEND", text="Edit Link")
        op.action = context.scene.flux_ui_linkeditor.editlink_action
    # self.draw_common(scene, layout, props)
    # print(bpy.types.Scene.flux_ui_linkeditor.shotlevel_infos)
    line_box_line2_row = line_box_line2.row(align=True)
    line_box_line2_row.prop(context.scene.flux_ui_linkeditor, "editlink_action", expand=True)
    if settings["original_file"]:line_box_line2_row.enabled = False

    # --------------------------------
    line_box_line2.enabled = en
    # print(f"{settings=}")
    # print(f"{filepath=}")

def get_sel_from_link():
    sel_objs = bpy.context.selected_objects
    sel_objs_ref_name = []
    for obj in sel_objs:
        sel_objs_ref_name.append(obj.override_library.reference.name)

    return sel_objs_ref_name

def action_to_link_obj(objs, action):

    # ---------------------------
    print(f"{action=}")

    # first check if objects exist
    final_objs = []
    for obj in objs:
        if bpy.data.objects.get(obj):
            final_objs.append(obj)
        else:
            print("This obj don'et exist in this scene : ", obj)

    # get back
    objs = final_objs

    # ---------------------------objects
    for obj in objs:
        bpy.data.objects[obj].select_set(True)
        print("select : ", obj)

        # remove selected objs
        if action == "remove":
            data_objs = bpy.data.objects
            data_objs.remove(data_objs[obj], do_unlink=True)

    if action == "keep":
        for each_obj in list(bpy.data.objects):
            if not each_obj.name in objs:
                data_objs = bpy.data.objects
                data_objs.remove(data_objs[each_obj.name], do_unlink=True)


class FLUX_OBJ_OT_EditLinked(bpy.types.Operator):
    """Edit Linked Library"""
    bl_idname = "flux.edit_linked"
    bl_label = "Edit Linked Library"
    bl_description = "Open and edit selected link."

    use_autosave: bpy.props.BoolProperty(
            name="Autosave",
            description="Save the current file before opening the linked library",
            default=True)
    use_instance: bpy.props.BoolProperty(
            name="New Blender Instance",
            description="Open in a new Blender instance",
            default=False)

    # action are nothing/keep/remove
    action: bpy.props.StringProperty(default="nothing")


    @classmethod
    def poll(cls, context: bpy.context):
        return True

    def execute(self, context: bpy.context):

        global flux_editlink_origfile

        # get object name from linked selection
        sel_objs_ref_name = get_sel_from_link()

        # -------------
        # this is the old method heriteed from the addon I copy all this classes.
        #   the problem is the object you select is not necessary the good one,
        #   specially if the link and link in it....
        # target = context.active_object
        #
        # if target.instance_collection and target.instance_collection.library:
        #     targetpath = target.instance_collection.library.filepath
        #     settings["linked_objects"].extend({ob.name for ob in target.instance_collection.objects})
        # elif target.library:
        #     targetpath = target.library.filepath
        #     settings["linked_objects"].append(target.name)
        # elif target.override_library:
        #     target = target.override_library.reference
        #     targetpath = target.library.filepath
        #     settings["linked_objects"].append(target.name)

        # ..so switch to that method
        all_linked = list(context.scene.flux_ui_link_list)
        current_linked = all_linked[context.scene.flux_ui_link_list_index]
        targetpath = current_linked.link_path
        target = current_linked.link_collection

        if targetpath:

            # flux_editlink_origfile = bpy.data.filepath
            # flux_editlink_origfile = "test"

            logger.debug(target + " is linked to " + targetpath)

            if self.use_autosave:
                if not bpy.data.filepath:
                    # File is not saved on disk, better to abort!
                    self.report({'ERROR'}, "Current file does not exist on disk, we cannot autosave it, aborting")
                    return {'CANCELLED'}
                print("save file")
                bpy.ops.wm.save_mainfile()

            settings["original_file"] = bpy.data.filepath
            # Using both bpy and os abspath functions because Windows doesn't like relative routes as part of an absolute path
            settings["linked_file"] = os.path.abspath(bpy.path.abspath(targetpath))

            if self.use_instance:
                import subprocess
                try:
                    subprocess.Popen([bpy.app.binary_path, settings["linked_file"]])
                except:
                    logger.error("Error on the new Blender instance")
                    import traceback
                    logger.error(traceback.print_exc())
            else:
                bpy.ops.wm.open_mainfile(filepath=settings["linked_file"])

            logger.info("Opened linked file!")
        else:
            self.report({'WARNING'}, target.name + " is not linked")
            logger.warning(target.name + " is not linked")

        # ---------------
        # operation on the new objs
        action_to_link_obj(sel_objs_ref_name, self.action)

        return {'FINISHED'}


class FLUX_OBJ_OT_ReturnToOriginal(bpy.types.Operator):
    """Load the original file"""
    bl_idname = "flux.edit_link_return_to_original"
    bl_label = "Return to Original File"

    use_autosave: bpy.props.BoolProperty(
            name="Autosave",
            description="Save the current file before opening original file",
            default=True)

    @classmethod
    def poll(cls, context: bpy.context):
        return (settings["original_file"] != "")

    def execute(self, context: bpy.context):
        if self.use_autosave:
            bpy.ops.wm.save_mainfile()

        bpy.ops.wm.open_mainfile(filepath=settings["original_file"])

        settings["original_file"] = ""
        settings["linked_objects"] = []
        logger.info("Back to the original!")
        return {'FINISHED'}


class FLUX_VIEW3D_PT_PanelLinkedEdit(bpy.types.Panel):
    bl_label = "Flux Edit Linked Library"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_category = "Item"
    bl_context = 'objectmode'
    bl_options = {'DEFAULT_CLOSED'}

    @classmethod
    def poll(cls, context: bpy.context):
        return (context.active_object is not None) or (settings["original_file"] != "")

    def draw_common(self, scene, layout, props):
        if props is not None:
            props.use_autosave = scene.use_autosave
            props.use_instance = scene.use_instance

            layout.prop(scene, "use_autosave")
            layout.prop(scene, "use_instance")

    def draw(self, context: bpy.context):
        scene = context.scene
        layout = self.layout
        layout.use_property_split = False
        layout.use_property_decorate = False
        icon = "PANEL_CLOSE"

        target = None

        target = context.active_object.instance_collection

        if settings["original_file"] == "" and (
                (target and
                target.library is not None) or
                context.active_object.library is not None or
                (context.active_object.override_library is not None and
                context.active_object.override_library.reference is not None)):

            if (target is not None):
                props = layout.operator("object.edit_linked", icon="LINK_BLEND",
                                        text="Edit Library: %s" % target.name)
            elif (context.active_object.library):
                props = layout.operator("object.edit_linked", icon="LINK_BLEND",
                                        text="Edit Library: %s" % context.active_object.name)
            else:
                props = layout.operator("object.edit_linked", icon="LINK_BLEND",
                                        text="Edit Override Library: %s" % context.active_object.override_library.reference.name)

            self.draw_common(scene, layout, props)

            if (target is not None):
                layout.label(text="Path: %s" %
                            target.library.filepath)
            elif (context.active_object.library):
                layout.label(text="Path: %s" %
                            context.active_object.library.filepath)
            else:
                layout.label(text="Path: %s" %
                            context.active_object.override_library.reference.library.filepath)

        elif settings["original_file"] != "":

            if scene.use_instance:
                layout.operator("wm.return_to_original",
                                text="Reload Current File",
                                icon="FILE_REFRESH").use_autosave = False

                layout.separator()

                # XXX - This is for nested linked assets... but it only works
                # when launching a new Blender instance. Nested links don't
                # currently work when using a single instance of Blender.
                if context.active_object.instance_collection is not None:
                    props = layout.operator("object.edit_linked",
                            text="Edit Library: %s" % context.active_object.instance_collection.name,
                            icon="LINK_BLEND")
                else:
                    props = None

                self.draw_common(scene, layout, props)

                if context.active_object.instance_collection is not None:
                    layout.label(text="Path: %s" %
                            context.active_object.instance_collection.library.filepath)

            else:
                props = layout.operator("wm.return_to_original", icon="LOOP_BACK")
                props.use_autosave = scene.use_autosave

                layout.prop(scene, "use_autosave")

        else:
            layout.label(text="%s is not linked" % context.active_object.name,
                        icon=icon)


addon_keymaps = []
classes = (
    FLUX_OBJ_OT_EditLinked,
    # NODE_OT_EditLinked,
    FLUX_OBJ_OT_ReturnToOriginal,
    FLUX_VIEW3D_PT_PanelLinkedEdit,
    # NODE_PT_PanelLinkedEdit,
    # TOPBAR_MT_edit_linked_submenu
    )


def register():
    bpy.app.handlers.load_post.append(linked_file_check)

    for c in classes:
        bpy.utils.register_class(c)

    bpy.types.Scene.flux_editlink_use_autosave = bpy.props.BoolProperty(
            name="Autosave",
            description="Save the current file before opening a linked file",
            default=True)

    bpy.types.Scene.flux_editlink_use_instance = bpy.props.BoolProperty(
            name="New Blender Instance",
            description="Open in a new Blender instance",
            default=False)



def unregister():

    bpy.app.handlers.load_post.remove(linked_file_check)

    del bpy.types.Scene.flux_editlink_use_autosave
    del bpy.types.Scene.flux_editlink_use_instance

    for c in reversed(classes):
        bpy.utils.unregister_class(c)


if __name__ == "__main__":
    register()
