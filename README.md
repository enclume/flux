## What is Flux

FLUX is an Animation Pipeline Manager for Blender 3.6.
It contains :
- An asset loader/saver : Versionning, post for review, post for publish, manage wip file, last and list all...
- A json containtaining all data-structure and project prefs for Multiproject and deploy easly
- A versatile link editor (to manage link in the scene)
- A Scene composer, to append/link in mass
- A versatile sanity check
- A versatile toolbox

## Credit

- It's created by Cedric Nicolas (nicolasced@gmail.com / www.cedriclefox.com) and Guillaume Geelen (geelenguillaume@hotmail.fr)
- It's use for Rufus production, by NextFrame Animation
- Consulting : Paul Jadoul from Enclume Animation Studio.

## Limitations

- Passwords are not encrypted
- Multiproject is still under dev/test. Specially Proxy support.
- Shotgrid was not heavily tested.
- Local db was not heavily tested.

 
