import bpy
import json
import os

def find_path_in_map(filepath):
    ''' description TODO '''
    correspondance_map = "blender_libs\\relocate_old_r_link_map.json"
    correspondance_map = open(correspondance_map, "r")
    correspondance_map = json.load(correspondance_map)
    
    map = correspondance_map

    if "\\03_ASSETS\\" not in filepath :
        return None
    
    filepath = filepath.split("\\")
    filepath = filepath[filepath.index("03_ASSETS"):-1]
    filepath = "\\".join(filepath)
    
    pathlist = []
    
    for path in map.keys() :
        if filepath in path :
            pathlist.append(path)
            
            
    final_path = map[pathlist[0]].split("\\")
    
    final_blend = final_path[-1] + "_rig_v01.blend"
    
    final_path.append("12_RIG")
    final_path.append("v01")
    final_path.append(final_blend)
    
    final_path = "\\".join(final_path)   
    
    if not os.path.exists(final_path) : 
        return None        
    
    return final_path


def relocate_link(lib):
    ''' description TODO '''
    if "\\03_ASSETS\\" not in lib.filepath :
        return False
        
    new_path = find_path_in_map(lib.filepath)
    print(str(lib).split("\"")[1])
    
    if not new_path :
        print("can't find a W:\\ path\n")
        return False
    
    print(f"old : {lib.filepath}")
    print(f"new : {new_path}")
    
    lib.filepath = new_path
    
    lib.reload()
    
    print()
    return True


def relocate_all_link():    
    ''' description TODO '''
    for lib in bpy.data.libraries :
        relocate_link(lib)

    print("all link are relocated")
    return

if __name__ == "__main__" :
    os.system("cls")
    
    correspondance_map = "blender_libs\\relocate_old_r_link_map.json"
    correspondance_map = open(correspondance_map, "r")
    correspondance_map = json.load(correspondance_map)

    print(correspondance_map)