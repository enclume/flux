import bpy
import os
import shutil

try :
    from . import utils
except :
    import utils
from ... import file_access
from ...config import path as path_from_config

from ... import sanity_check_operators

# initialize logger
from ... import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)
importlib.reload(utils)
importlib.reload(file_access)
importlib.reload(path_from_config)
importlib.reload(sanity_check_operators)


# TEST_RF_ep310580_sh0040_0_c_blocking_v03.blend

def get_animatic_name(old_animatic_name, ep):
    ''' get the good animatic name relative to the bad given one '''
    animatic_name = old_animatic_name[:-4]
    animatic_ext = old_animatic_name.split(".")[-1]
    animatic_name = animatic_name.split("_")
    if len(animatic_name) == 2 :
        animatic_name = [animatic_name[-1]]
    if len(animatic_name) == 1 :
        if not ep :
            raise Exception(f"ep is empty, abort\n   ->({old_animatic_name=})")
        animatic_name.append("sq" + ep[-4:])
        animatic_name.append("sh" + animatic_name[0][-4:])
        animatic_name[0] = "RF"
    else :
        animatic_name[0] = "RF"
        animatic_name[1] = "sq" + animatic_name[1][-4:]
        animatic_name[2] = "sh" + animatic_name[2][-4:]

    while "animatic" in animatic_name :
        animatic_name.remove("animatic")

    animatic_name.append("animatic")
    animatic_name = "_".join(animatic_name)
    animatic_name += "." + animatic_ext

    return animatic_name


def get_new_path_from_old_name(old_animatic_name, ep):
    return utils.get_animatic_fullpath_from_name(get_animatic_name(old_animatic_name, ep))


def get_new_path_from_old_path(old_path):
    animatic_name = old_path.split("\\")[-1]
    ep = ""

    for item in old_path.split("\\"):
        if item.startswith("ep") and item[2:].isdigit():
            ep = item

    # print(f"{ep=}")
    if not ep:
        return False

    return utils.get_animatic_fullpath_from_name(get_animatic_name(animatic_name, ep))


def copy_animatic_new_loc(old_path: str, new_path: str = None):
    '''
    Copy an animatic mp4 from the old path to a new path (given or found)

    Args:
        - old_path : the current animatic path
        - new_path : (default None) if given, pass the shearch of the good new_path

    '''
    if not new_path :
        animatic_name = old_path.split("\\")[-1]
        ep = ""

        for item in old_path.split("\\"):
            if item.startswith("ep") and item[2:].isdigit():
                ep = item

        new_path = get_new_path_from_old_name(animatic_name, ep)

    #print(f"old_path : {old_path}")
    #print(f"new_path : {new_path}")

    if not os.path.exists(new_path) :
        new_folder_path = "\\".join(new_path.split("\\")[:-1])

        if not os.path.exists(new_folder_path) :
            os.makedirs(new_folder_path)

        # -> copy
        shutil.copy2(old_path, new_path)


    log.debug(f"make copy from {old_path} to {new_path}")
    return new_path


def moulinette_copy_all_old_to_new(base_sq_path, all_sq):
    '''
    To execute outside of blender, in general don't use it

    Return :
        -
    '''

    for sq_id in all_sq :
        sq_data = {"sq_id": sq_id}

        for ep in os.listdir(base_sq_path) :
            ep_path = base_sq_path + "\\" + ep

            if not ep.startswith("ep") or not ep.endswith(sq_id) :
                continue

            
            print(ep)
            shots = []
            animatics = []

            if "01_BRIEFING" in os.listdir(ep_path): 
                #print(f"01_BRIEFING for {ep}")
                current_path = ep_path +"\\01_BRIEFING"
                animatics = os.listdir(current_path)
                print(f"   {current_path}")
            elif "001_BRIEFING" in os.listdir(base_sq_path + "\\" + ep):
                current_path = ep_path +"\\001_BRIEFING"
                animatics = os.listdir(current_path)
                print(f"   {current_path}")

            elif False :
                for sh in os.listdir(base_sq_path + "\\" + ep) :
                    if sh.startswith("sh"):
                        if "01_BRIEFING" in os.listdir(base_sq_path + "\\" + ep + "\\" + sh):
                            shots.append(sh)
                        elif "001_BRIEFING" in os.listdir(base_sq_path + "\\" + ep + "\\" + sh):
                            shots.append(sh)
                        else :
                            #print(f'do nothing for {ep} {sh}')
                            pass
            
            if not animatics :
                print(f"   no animatics for {ep}\n")
                continue

            for animatic in animatics.copy() :
                if not animatic.endswith(".mp4") : 
                    animatics.remove(animatic)

            for a, animatic in enumerate(animatics) :
                animatics[a] = current_path + "\\" + animatic
                print(animatics[a])
                copy_animatic_new_loc(current_path + "\\" + animatic)

            animatics_new = []
            for animatic in animatics :
                animatic_name = get_animatic_name(animatic, ep)
                animatics_new.append(animatic_name)

            #print(animatics_new)
            #print()
            sq_data.update({"animatics": animatics})

            #sq_data.update({ep: shots})
            print()  

    return True


def get_animatic_path_in_blend_paths() -> str :
    ''' 
    To execute in a blend file.
    Get the animatic filepath.

    Return :
        - True if succesfull
        - False if dont find
    '''

    all_path = bpy.utils.blend_paths(absolute=True)
    current_path = bpy.data.filepath

    animatic_path = []
        
    for path in all_path :
        if path.startswith("W:\\") and path.endswith(".mp4") :        
            if path not in animatic_path:
                animatic_path.append(path)

    if not animatic_path :
        log.info("animatic_path is empty")  
        return False 
    
    if len(animatic_path) == 1:
        return animatic_path[0]
    
    print("multiple animatic_path found")

    to_remove = [] # remove doublons
    for i, item_i in enumerate(animatic_path) :
        for j, item_j in enumerate(animatic_path) :
            if j != i and item_j == item_i and item_j not in to_remove :
                to_remove.append(item_j)

    for item in to_remove :
        animatic_path.remove(item)

    animatic_path = animatic_path[0]
                    
    return animatic_path


def set_animatic_path(old_path, new_path) :
    for data_item_name in dir(bpy.data) :
        data_item = getattr(bpy.data, data_item_name)
        if "<bpy_collection[" not in str(data_item) or len(data_item) <= 0 or not hasattr(data_item[0], "filepath") :
            continue
        
        for subitem in data_item :
            if subitem.filepath == old_path :
                subitem.filepath = new_path
    
    for sequence in bpy.context.scene.sequence_editor.sequences : 
        if hasattr(sequence, "filepath") and sequence.filepath == old_path:
            sequence.filepath = new_path

    return


def replace_animatic_filepath():
    ''' 
    To execute in a blend file.
    Replace all animatic filepath from old to new place.

    Return :
        True if succesfull
    '''

    log.debug("start replacing")
    bpy.ops.file.make_paths_absolute()

    # check if all needed data are ok    
    current_item = sanity_check_operators.fct.fct_utils.get_current_item()

    if not current_item : 
        log.info(f"can't get the current item (current_item = {current_item})")
        return False
    
    if current_item["category"] != "shots" : 
        log.debug("the current item category is not shots, fail of the switch")
        return True
    
    # check if it's a bad animatics case
    if utils.get_is_bad_animatic(sq = current_item["seq"][-4:], sh = current_item["shot"][-4:]) :
        log.debug(f"It's a bad animatic case, just waiting for a final file")
        return True
    
    # get right animatic path
    good_animatic_path = utils.get_animatic_fullpath_from_data(seq = current_item["seq"][-4:], shot = current_item["shot"][-4:], file_extension = "mov")
    
    current_animatic_path = get_animatic_path_in_blend_paths()

    if current_animatic_path == False :
        log.debug(f"can't find current_animatic_path (current_animatic_path = {current_animatic_path})")
        return False

    if not os.path.exists(good_animatic_path) :
        log.error(f"good_animatic_path doesn't exist !\n->({good_animatic_path})")
        return False
    
    if not os.path.exists(current_animatic_path) :
        log.error(f"current_animatic_path doesn't exist !\n->({current_animatic_path})")
        return False
    
    if current_animatic_path == good_animatic_path :
        log.info(f"current animatic path is already the good one !")
        return True

    set_animatic_path(current_animatic_path, good_animatic_path)

    bpy.ops.file.make_paths_absolute()
    
    log.info("animatic filepaths succesfully replaced in this file")
    return True



if __name__ == '__main__' :
    os.system("cls")
    print("___start___")

    base_sq_path = r"W:\01_PRODUCTIONS\012_RUFUS\2_PROD"

    all_sq = ["0010", "0020", "0030", "0050", "0060", "0090", "0130", "0170", "0190", "0225", "0240", "0280", "0285", "0300", "0325", "0335", 
            "0340", "0380", "0410", "0420", "0440", "0460", "0470", "0480", "0510", "0540", "0560", "0580", "0590", "0600", "0610", "0620", 
            "0640", "0650", "0655", "0660", "0670", "0680", "0690", "0700", "0730", "0750", "0755", "0760", "0770", "0780", "0790", "0800", 
            "0850", "0860", "0870", "0880", "0910", "0920", "0930", "0990", "1080", "1100", "1110", "1170", "1180", "1190"]

    #moulinette_copy_all_old_to_new(base_sq_path, all_sq)
