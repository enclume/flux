
import os
import shutil

try :
    from . import utils
except :
    import utils

# initialize logger
#from ... import flux_logger 
#log = flux_logger.get_flux_logger()

#reload lib
import importlib
#importlib.reload(flux_logger)
importlib.reload(utils)

def set_animatic_file_to_location(source_file, debug = False) :
    ''' copy paste the source file at the right location (replace if needed) '''
    print("_______________________________________________")
    source_animatic_name = source_file.split("\\")[-1]
    source_path = source_file
    print(f"copy :\n{source_path}\n->")

    destination_path = utils.get_animatic_fullpath_from_animatic_bad_name(source_animatic_name)

    if destination_path == False :       
        print("destination_path is False, pass this file")
        return

    # test is folders exist
    folder_path = "\\".join(destination_path.split("\\")[:-1])
    if not os.path.exists(folder_path) :
        print(f"makedirs : {folder_path}")
        if not debug :
            os.makedirs(folder_path)

    # -> copy
    print(f"{destination_path}")
    if not debug :
        shutil.copy2(source_path, destination_path)

    return


def set_animatic_file_in_folder_to_location(source_folder, debug = False):
    ''' run set_animatic_file_to_location for all source files directly in the given folder '''
    errors = []
    for file in os.listdir(source_folder) :
        file_path = os.path.join(source_folder, file)
        if not os.path.isfile(file_path) : continue
        if file.startswith("°") : continue

        try :
            set_animatic_file_to_location(file_path, debug)
        except Exception as e:
            error = f"an error occured for the file \"{file}\" :\n{e}"
            #log.error(error)
            errors.append(error)

    if errors :
        print(errors)
        return errors
    return False


def batch_animatic_file_setter(source_folder, debug = False) :
    ''' 
    run set_animatic_file_in_folder_to_location for all source folders directly in the given folder\n
    folders named "_ignore" will be ignored
    '''
    ignore_list = ["_ignore"]
    errors = []
    for folder in os.listdir(source_folder) :
        if folder in ignore_list : continue

        folder_path = os.path.join(source_folder, folder)
        if not os.path.isdir(folder_path) : continue

        try :
            error = set_animatic_file_in_folder_to_location(folder_path, debug)
            if error :
                errors.append(error)
        except Exception as e:
            error = f"an error occured for the folder \"{folder}\" :\n{e}"
            #log.error(error)
            errors.append(error)

    if errors :
        return errors
    return False


if __name__ == "__main__" :

    os.system("cls")
    print("___START___\n")

    master_folder = None
    single_folder = None
    single_file = None
  
    
    #master_folder = "C:\\Users\\Marteau\\Desktop\\animatic_source"
    #single_folder = "C:\\Users\\Marteau\\Desktop\\animatic_source\\RUF_0480"
    single_file = "C:\\Users\\Marteau\\Desktop\\animatic_source\\RUF_0620\\RUF_0620_0120_V3.mov"

    if master_folder : 
        batch_animatic_file_setter(master_folder, debug=False)

    if single_folder : 
        set_animatic_file_in_folder_to_location(single_folder, debug=False)

    if single_file : 
        set_animatic_file_to_location(single_file, debug=False)


    
    print("\n____END____")