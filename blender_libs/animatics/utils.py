import os
import shutil

try :
    from ... import file_access
    get_file_access = True
except :
    get_file_access = False
#from ...config import path as path_from_config


# initialize logger
#from ... import flux_logger 
#log = flux_logger.get_flux_logger()

#reload lib
import importlib
#importlib.reload(flux_logger)
#importlib.reload(file_access)
#importlib.reload(path_from_config)


def get_bad_animatics_data():
    ''' get a dict with all sequences and shots with animatic not ready '''
    bad = {
        "sq" : [
            "0050", 
            "0690", 
            "0730", 
            "1080",
            "1100",
            "1110"
        ],
        "sh" : {
            "0010": [
                "0190"
            ],
            "0480": [
                "0120"
            ],
            "0540": [
                "0260"
            ],
            "0580": [
                "0400",
                "0550",
                "0590",
                "0640"
            ],
            "0590": [
                "0100"
            ],
            "0600": [
                "0090", 
                "0100"
            ],
            "0610": [
                "0050", 
                "0110"
            ]
        }
    }
    return bad


def get_is_bad_animatic(sq: str, sh: str):
    ''' return True if sq or sh are in the dict with all sequences and shots with animatic not ready '''
    if type(sq) is not str or len(sq) != 4 : raise Exception(f"the variable \"sq\" need to be a string with a length of 4\n   ->(sq = {sq}, type = {type(sq)})")
    if type(sh) is not str or len(sh) != 4 : raise Exception(f"the variable \"sh\" need to be a string with a length of 4\n   ->(sh = {sh}, type = {type(sh)})")

    bad_dict = get_bad_animatics_data()

    if sq in bad_dict["sq"] : return True

    if sq not in bad_dict["sh"].keys() : return False

    if sh in bad_dict["sh"][sq] : return True

    return False


def get_formated_animatic_number(nbr) :
    if type(nbr) not in [int, float, str] :
        error = f"nbr {nbr} should be an int, float or string not a {type(nbr)}"
        #log.error(error)
        raise TypeError(error)
    if type(nbr) is float :
        nbr = int(nbr)

    formated_nbr = str(nbr).zfill(4)
    #log.debug(f"return {formated_nbr}")
    return formated_nbr


def get_formated_animatic_file_extension(file_extension: str):
    formated_file_extension = file_extension.replace(".", "")
    return formated_file_extension


def format_animatic_data(seq, shot, file_extension) :
    seq = get_formated_animatic_number(seq)
    shot = get_formated_animatic_number(shot)
    file_extension = get_formated_animatic_file_extension(file_extension)
    return seq, shot, file_extension


def get_animatic_name_from_data(seq: str, shot: str, file_extension: str = "mp4") :
    return f"RF_sq{seq}_sh{shot}_animatic.{file_extension}"


def get_animatic_fullpath_from_data(seq: str, shot: str, file_extension: str = "mp4") :
    ''' get the animatic fullpath from animatic data (sequence, shot and file_extension) '''
    seq, shot, file_extension = format_animatic_data(seq, shot, file_extension)
    animatic_name = get_animatic_name_from_data(seq, shot, file_extension)
    if get_file_access : # TODO hard coded
        path = file_access.get_item_path({"project": "rufus", "category": "shots", "seq": seq, "shot": shot, "task": "animatic"}, with_version=False)
    else :
        path = f"W:\\01_PRODUCTIONS\\012_RUFUS\\2_PROD\\02_ANIM\\sq{seq}_anim\\sq{seq}_sh{shot}_anim\\00_ANIMATIC"
    fullpath = os.path.join(path, animatic_name)
    return fullpath


def get_animatic_fullpath_from_name(animatic_name: str) :
    ''' get the animatic fullpath from a standardized animatic name '''
    seq = animatic_name.split("_")[1]
    shot = seq + "_" + animatic_name.split("_")[2] # TODO hard coded
    path = file_access.get_item_path({"project": "rufus", "category": "shots", "seq": seq, "shot": shot, "task": "animatic"}, with_version=False)
    fullpath = os.path.join(path, animatic_name)
    return fullpath


def get_animatic_data_from_bad_name(bad_animatic_name: str, separator: str = "_") :
    ''' work for files like "RUF_0335_0010_v3.mov" '''
    all_item = bad_animatic_name.split(".")[:-1] # remove extention
    all_item = ".".join(all_item) # recompile name
    all_item = all_item.split(separator)
    numerics = []

    for item in all_item :
        if not item.isdigit() : continue
        numerics.append(item)

    if len(numerics) != 2 : 
        error = f"len(numerics) != 2, can't deduce seq and shot\n   ->(numerics : {numerics})"
        #log.error(error)
        raise Exception(error)

    seq = numerics[0]
    shot = numerics[1]
    file_extension = bad_animatic_name.split(".")[-1]

    seq, shot, file_extension = format_animatic_data(seq, shot, file_extension) 
    return seq, shot, file_extension

def get_animatic_fullpath_from_animatic_bad_name(bad_animatic_name: str, separator: str = "_") :
    seq, shot, file_extension = get_animatic_data_from_bad_name(bad_animatic_name, separator)
    if get_is_bad_animatic(seq, shot) :
        print("It's a bad animatic !")
        return False
    fullpath = get_animatic_fullpath_from_data(seq, shot, file_extension)
    return fullpath
