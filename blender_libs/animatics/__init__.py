
import os

from . import replacer
from . import switcher
from . import utils

from ... import file_access

# initialize logger
from ... import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)
importlib.reload(replacer)
importlib.reload(switcher)
importlib.reload(utils)


if __name__ == "__main__" :

    os.system("cls")

    master_folder = None
    single_folder = None
    single_file = None
    
    master_folder = "C:\\Users\\Marteau\\Desktop\\animatic_source"

    if master_folder : 
        replacer.batch_animatic_file_setter(master_folder)

    if single_folder : pass

    if single_file : pass