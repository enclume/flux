# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Blender libs scene functions
'''

import bpy

# initialize logger
from .. import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)


#============================
#scene.flux_ui_data related
def flux_ui_data_transfer(scene_from: bpy.types.Scene, scene_to: bpy.types.Scene) -> bool :
    ''' Transfer all flux related data from a scene to another '''
    flux_ui_data_from = scene_from.flux_ui
    flux_ui_data_to = scene_to.flux_ui
    
    for prop in flux_ui_data_from.__annotations__.keys():
        setattr(flux_ui_data_to, prop, getattr(flux_ui_data_from, prop))
        
    return True


def flux_ui_data_get(scene_from: str = "") -> dict :
    ''' Get all flux related data from a scene '''
    if not scene_from :
        scene_from = bpy.context.scene

    flux_ui_data = scene_from.flux_ui
    final_dict = {}
    
    for prop in flux_ui_data.__annotations__.keys():
        final_dict.update({prop: getattr(flux_ui_data, prop)})
        
    return final_dict


def flux_ui_data_set(prop_dict: dict, scene_to: str = "") -> bool :
    ''' Set all flux related data in a scene '''
    if not scene_to :
        scene_to = bpy.context.scene
        
    flux_ui_data = scene_to.flux_ui
    for key in prop_dict :
        value = prop_dict[key]
        setattr(flux_ui_data, key, value)

    return True


#============================
#scene related
def get(scene_name: str, create_scene: bool = True) -> bpy.types.Scene :
    '''
    get a scene by name (create it if it doesn't exist)

    Args:
        - scene_name : the name of the wanted scene
        - create_scene : if True, will create the scene if it doesn't exist

    Return:
        scene
    '''
    wanted_scene = bpy.data.scenes.get(scene_name)
    
    if wanted_scene : 
        return wanted_scene
    if not create_scene :
        return False
    
    wanted_scene = bpy.data.scenes.new(scene_name)
    
    return wanted_scene


def remove(scene) -> bool :
    '''
    remove a scene

    Args:
        - scene : the scene or the scene name
    '''
    if type(scene) == str :
        scene = bpy.data.scenes.get(scene)
    elif type(scene) != bpy.types.Scene :
        log.error(f"scene type must be a \"str\" or a \"bpy.types.Scene\"\n   -> scene type : {type(scene)}")
        return False

    if not scene :
        log.info("no scene to remove")
        return False

    bpy.data.scenes.remove(scene)

    return True


def set_active(scene) :
    ''' 
    set the given scene active in the context.window 

    Args:
        - scene : the scene to set active (or his name)

    Return:
        True if the scene is successfully set active
    '''

    if type(scene) is str :
        scene = bpy.data.collections.get(scene)
    elif type(scene) != bpy.types.Scene :
        log.error(f"scene type must be a \"str\" or a \"bpy.types.Scene\"\n   -> scene type : {type(scene)}")
        return False

    if not scene :
        log.info("no scene to set active")
        return False
    
    bpy.context.window.scene = scene
    return True


def append(append_path: str, scene_name: str) -> bpy.types.Scene :
    '''
    append a scene from a path by name

    Args:
        - append_path : the path of the blender source file 
        - scene_name : the name of the scene to append

    Return:
        scene
    '''

    if bpy.data.scenes.get(scene_name) :
        log.info("there is already a scene with this name -> return it with no append")
        return bpy.data.scenes.get(scene_name)
    
    bpy.ops.wm.append(
                filepath=append_path.split("\\")[-1],
                directory=append_path + "\\Scene\\",
                filename=scene_name
                )   
     
    return bpy.data.scenes.get(scene_name)


def append_all(append_path: str) -> list :
    '''
    append all scenes from a path

    Args:
        - append_path : the path of the blender source file 

    Return:
        a list of scene
    '''
    with bpy.data.libraries.load(append_path, link=False) as (data_from, data_to):
        data_to.scenes = data_from.scenes
        
    scenes_list = [bpy.data.scenes[scene.name] for scene in data_to.scenes]

    if len(scenes_list) == 1:
        scenes_list = scenes_list[0]

    return scenes_list