# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Blender libs collection functions
'''

import bpy

# reload lib
import importlib

def instance(source, new_name, collection=False):
    """
    Instance object, fast way.
    Args:
        source (): can by object blender type or string
        new_name (): new name, as string
        collection (): parent collection if any. False = active collection

    Returns:

    """

    # ---
    # print(type(source))
    if isinstance(source, str):
        source = bpy.data.objects[source]

    # duplication
    dup = bpy.data.objects.new(new_name, source.data)
    # when you create a new object manually this way it's not part of any collection, add it to the active collection so you can actually see it in the viewport
    if not collection:
        bpy.context.collection.objects.link(dup)
    else:
        if type(collection) == "str":
            col = bpy.data.collections[collection]
        else:
            col = collection
        col.objects.link(dup)

    return dup


def instance_col(source_collection, new_name, collection=False):
    """
    instance collection to a empty object
    Args:
        source_collection: source collection to instance
        new_name: name of the output objects
        collection (): parent collection if any. False = active collection

    Returns:

    """
    # print(type(source))
    if isinstance(source_collection, str):
        source_collection = bpy.data.collections[source_collection]

    dup = bpy.data.objects.new(new_name, None)  # Create new empty object
    dup.empty_display_type = 'PLAIN_AXES'

    if not collection:
        bpy.context.collection.objects.link(dup)
    else:
        if type(collection) == "str":
            col = bpy.data.collections[collection]
        else:
            col = collection
        col.objects.link(dup)

    dup.instance_type = 'COLLECTION'
    dup.instance_collection = source_collection

    return dup

