# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

import bpy

# initialize logger
from .. import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)



class run_fct():
    ''' description TODO '''
    def __init__(self, function, max_iteration = 50):
        self.function = function
        self.current_iteration = 0
        self.max_iteration = max_iteration
        self.last_error = None
        log.debug(f"timer_run_fct set for {function}")

        self.start_timer()

    def timer_fct(self):
        ''' description TODO '''
        if self.current_iteration >= self.max_iteration :
            log.error(f"time out for the function : {self.function}\nlast error : {self.last_error}")
            return
        self.current_iteration += 1
        try :
            self.function()
            return
        except Exception as error:
            self.last_error = error
            return 0.1
    
    def start_timer(self):
        ''' description TODO '''
        self.current_iteration = 0
        bpy.app.timers.register(self.timer_fct)