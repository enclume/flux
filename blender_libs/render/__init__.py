# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
add a custom overlay to expose data in a render
'''

import bpy
import datetime
import os

from . import utils
from . import render_parameters

from ...items_operators import draw_message
from ... import blender_libs
from ... import prefs


# initialize logger
from ... import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)
importlib.reload(utils)
importlib.reload(render_parameters)
importlib.reload(draw_message)
importlib.reload(blender_libs)
importlib.reload(prefs)


def get_metadata(render_scene, vse_scene, burn_data) -> str :
    ''' get the metadata text (used in the VSE scene) for the current file and the current frame '''
    project = "Rufus" # TODO hard coded
    
    date = datetime.datetime.now().strftime("%d-%m-%Y %H:%M")
    
    basePath = bpy.context.blend_data.filepath
    if burn_data == None :
        filename = (basePath.split("\\")[-1]).replace(".blend", "")
        metadata = filename
    else :
        metadata = burn_data

    artist = prefs.get().db_fullname

    if not artist :
        artist = "artist_name"

    cam_name = render_scene.camera.name
    
    frame = vse_scene.frame_current
    
    text_data = [project, date, metadata, artist, cam_name, frame]
    
    return f"{text_data[0]} {text_data[1]} - {text_data[2]} - {text_data[3]} - {text_data[4]} - f {text_data[5]:04d}"
    

class vse_text_handler :
    ''' class used to udate the text on frame change during the render '''
    def __init__(self, render_scene, vse_scene, vse_text, burn_data: str):
        self.render_scene = render_scene
        self.vse_scene = vse_scene
        self.vse_text = vse_text
        self.burn_data = burn_data
        
    def set_vse_txt(self, scene, context):
        self.vse_text.text = get_metadata(self.render_scene, self.vse_scene, self.burn_data)
        return
    
    def add_handler(self):
        bpy.app.handlers.frame_change_pre.append(self.set_vse_txt)
        return
    
    def remove_handler(self):
        bpy.app.handlers.frame_change_pre.remove(self.set_vse_txt)
        return
    

class render_progress_handler :
    ''' class used to show and udate a progress bar on frame change during the render '''
    def __init__(self, frame_start, frame_end):
        self.frame_start = frame_start
        self.frame_end = frame_end
        self.draw_msg = draw_message.DrawMsg_3Dview(context = bpy.context, 
                                                    message=self.get_message(), 
                                                    color_RGBA=[0.05, 0.5, 1, 0.9])

    def get_progress_bar(self, current_percent, size = 10):
        current_percent = int((current_percent / 100) * size)
        final = ""
        for i in range(size) :
            if i < current_percent :
                final += "\u2588"
            else :
                final += "\u2581"

        return f"\u2999{final}\u2999"

    def get_message(self):
        current_frame = bpy.context.scene.frame_current
        render_range = self.frame_end - self.frame_start
        current_state = current_frame - self.frame_start
        current_percent = (current_state / render_range) * 100

        progress_bar = self.get_progress_bar(current_percent)

        turn = ["\\", "|", "/", "-"]

        turn = turn[current_frame % len(turn)]


        return f"{turn} Render in progress {current_percent:.1f}% {progress_bar}"

    def update_progress(self, context = bpy.context, *args, **kwargs):
        self.draw_msg.update_message(context=context, new_message=self.get_message())
        return

    def launch_progressbar(self, context = bpy.context):
        bpy.app.handlers.frame_change_post.append(self.update_progress)
        return
    
    def kill_progressbar(self, context = bpy.context):
        self.draw_msg.remove_handle()
        bpy.app.handlers.frame_change_post.remove(self.update_progress)
        return


def setup_vse_scene(vse_scene, render_scene, burn_data, layers = ["scene", "sound"], movie_filepath = None, sound_filepath = None) -> bool :
    ''' 
    Setup the VSE scene 

    Args :
        - vse_scene : the VSE scene
        - render_scene : the initial scene to render
        - burn_data : If not none, override default burn data
        - layers : needed vse layer
        - movie_filepath : movie filepath for the movie layer (can stay none if there is no movie layers)
    '''
    log.debug("\n  setup_vse_scene")
    
    # setup VSE scene parameters
    vse_scene.render.resolution_x = render_scene.render.resolution_x
    vse_scene.render.resolution_y = render_scene.render.resolution_y + 200
    vse_scene.frame_start = render_scene.frame_start
    vse_scene.frame_end = render_scene.frame_end
    
    frame_start = vse_scene.frame_start
    frame_end = vse_scene.frame_end + 1
    
    VSE_sequencer = vse_scene.sequence_editor
    # handle VSE_sequencer already present  
    if not VSE_sequencer :
        VSE_sequencer = vse_scene.sequence_editor_create()
    else :
        for item in VSE_sequencer.sequences :
            VSE_sequencer.sequences.remove(item)
    

    # add background layer
    bg = VSE_sequencer.sequences.new_effect("Black background", "COLOR", channel=1, frame_start=frame_start, frame_end=frame_end)

    # add scene layer
    if "scene" in layers :
        scene = VSE_sequencer.sequences.new_scene("Scene", render_scene, channel=2, frame_start=frame_start)

    # add movie layer
    if "movie" in layers :
        if not movie_filepath :
            log.error("there is no movie_filepath for the movie layer")
            raise Exception
        
        movie = VSE_sequencer.sequences.new_movie("movie", movie_filepath, channel=3, frame_start=frame_start, fit_method='ORIGINAL')

    # add texte layer
    txt = VSE_sequencer.sequences.new_effect("Text", "TEXT", channel=4, frame_start=frame_start, frame_end=frame_end)

    # add sound layer
    if "sound" in layers and sound_filepath :
        VSE_sequencer.sequences.new_sound("sound", filepath=sound_filepath, channel=5, frame_start=frame_start)
    elif "sound" in layers and len(bpy.data.sounds) > 0 :
        for sound in bpy.data.sounds :            
            for sequence in render_scene.sequence_editor.sequences_all:
                if sequence.type == "SOUND" and sequence.sound == sound:
                    VSE_sequencer.sequences.new_sound("sound", filepath=sound.filepath, channel=5, frame_start=int(sequence.frame_start))
                    break

    # setupe layers parameters              
    bg.color = [0, 0, 0]
    txt.align_x = "RIGHT"
    txt.align_y = "BOTTOM"
    txt.location = [0.99, 0.02]
    txt.font_size = 30
    
    txt.text = get_metadata(render_scene, vse_scene, burn_data)
    
    return True


def remove_vse_scene(vse_scene):
    ''' 
    remove the VSE scene 

    Args :
        vse_scene : the VSE scene
    '''
    log.debug("\n  remove_vse_scene")
    
    bpy.data.scenes.remove(vse_scene)
    
    log.debug(f"  {True}")
    return True


def render_metadata_on_movie(user_scene, movie_filepath: str, render_params: dict, burn_data = None) -> str :
    '''
    setup a vse scene to add a metadata overlay on an existing movie

    Args:
        - user_scene : the initial scene (bpy.context.scene most of the time)
        - movie_filepath : the path to the movie (the movie name must have "_temp_")
        - burn_data : default None (-> use the default one), metadata to burn in the render

    Return: 
        render_params
    '''

    if "_temp_" not in movie_filepath.split("\\")[-1] :
        movie_filename = movie_filepath.split("\\")[-1]
        log.error(f"the movie filename must have \"_temp_\"\n   -> movie filename : {movie_filename}\n   -> movie filepath : {movie_filepath}")
        return False

    # create VSE scene
    vse_scene = blender_libs.scene.get("VSE_render")
    
    bpy.ops.sequencer.refresh_all()

    utils.set_obj_parameters(vse_scene, render_params)
    setup_vse_scene(vse_scene, user_scene, burn_data, layers=["movie", "sound"], movie_filepath=movie_filepath, sound_filepath=movie_filepath)

    vse_scene.render.use_file_extension = user_scene.render.use_file_extension
    vse_scene.render.filepath = user_scene.render.filepath.replace("_temp_", "")
    final_return = vse_scene.render.filepath

    text_updater = vse_text_handler(render_scene = user_scene, 
                                    vse_scene = vse_scene, 
                                    vse_text = vse_scene.sequence_editor.sequences["Text"], 
                                    burn_data = burn_data) 
    text_updater.add_handler()

    # render via VSE scene
    bpy.context.window.scene = vse_scene
    vse_scene.display_settings.display_device = "sRGB"
    vse_scene.view_settings.view_transform = "Standard"

    bpy.ops.render.opengl(animation=True, sequencer=True, write_still=True)
    bpy.context.window.scene = user_scene
    
    # remove VSE scene
    text_updater.remove_handler()
    blender_libs.scene.remove(vse_scene)

    # cleanup
    for sound in bpy.data.sounds[:] :
        if sound.filepath == movie_filepath :
            bpy.data.sounds.remove(sound)

    return final_return


def remove_overlay_set_camera_view(context = bpy.context, force_material_preview = False) :
    initial_things = {}
    for area in context.screen.areas :        
        if area.type == 'VIEW_3D' :
            for space in area.spaces :
                if space.type != 'VIEW_3D' : continue
                initial_things.update({space : [space.region_3d.view_perspective, 
                                                space.overlay.show_overlays, 
                                                space.region_3d.view_matrix.copy(), 
                                                space.region_3d.view_rotation.copy(), 
                                                space.shading.type]})
                space.region_3d.view_perspective = 'CAMERA'
                if space.shading.type != "MATERIAL" :
                    if force_material_preview or space.shading.type != "RENDERED" : 
                        space.shading.type = "MATERIAL"
                space.overlay.show_overlays = False

        if area.type == 'DOPESHEET_EDITOR' :                        
            for region in area.regions :
                if region.type != 'WINDOW' : continue
                override = bpy.context.copy()                               
                override = {'region': region, 'area': area}
                bpy.ops.anim.previewrange_clear(override)

    return initial_things


def new_remove_overlay_set_camera_view(context = bpy.context, force_material_preview = False) :
    initial_things = {}
    for area in context.screen.areas :   
           
        if area.type == 'VIEW_3D' :  
            for space in area.spaces :
                if space.type != 'VIEW_3D' : continue
                space_dict = {
                    "region_3d" : {
                        "view_matrix" : space.region_3d.view_matrix.copy(),
                        "view_rotation" : space.region_3d.view_rotation.copy(),
                        "view_perspective" : space.region_3d.view_perspective
                    },
                    "overlay" : {"show_overlays" : space.overlay.show_overlays},
                    "shading" : {"type" : space.shading.type}
                }

                initial_things.update({space : space_dict})
                space.region_3d.view_perspective = 'CAMERA'
                if space.shading.type not in ["MATERIAL"] :
                    if force_material_preview or space.shading.type != "RENDERED" : 
                        space.shading.type = "MATERIAL"
                space.overlay.show_overlays = False

        if area.type == 'DOPESHEET_EDITOR' :                        
            for region in area.regions :
                if region.type != 'WINDOW' : continue
                override = bpy.context.copy()                               
                override = {'region': region, 'area': area}
                bpy.ops.anim.previewrange_clear(override)

    return initial_things


def reset_overlay_view(initial_things, context = bpy.context) :
    ''' reset the thing modified with the remove_overlay_set_camera_view function '''
    for area in context.screen.areas :
        if area.type != 'VIEW_3D' : continue

        for space in area.spaces :
            if space.type != 'VIEW_3D' or space not in initial_things : continue
            space.region_3d.view_perspective = initial_things[space][0]
            space.overlay.show_overlays = initial_things[space][1]
            space.region_3d.view_matrix = initial_things[space][2]
            space.region_3d.view_rotation = initial_things[space][3]
            space.shading.type = initial_things[space][4]
    return


def new_reset_overlay_view(initial_things, context = bpy.context) :
    ''' reset the thing modified with the remove_overlay_set_camera_view function '''

    for space in initial_things :
        
        space.region_3d.view_perspective = initial_things[space][0]
        space.overlay.show_overlays = initial_things[space][1]
        space.region_3d.view_matrix = initial_things[space][2]
        space.region_3d.view_rotation = initial_things[space][3]
        space.shading.type = initial_things[space][4]
    return


def layout_needs() :
    '''
    OK | Clean layout render
    a render of each layout in colour without anything else obstructing the image.
    -> mov output
    -> with no metadata 
    -> hide CHAR & PROP colection
    -> render viewport openGL

    | Alpha images of the shot
    .jpg images in black and white, in which all 3D assets in the scene are black and the sky, or areas where there is no asset visible, white.
    ->  viewport shading
        -> Lighting: Flat, 
        -> Color: Single and black. (HSV = 0,0,0), 
        -> Background: Viewport and white. (HSV = 0,0,1), 
        -> UNCHECK outline.
    -> film transparent = False
    -> output : JPEG, RGB 90%
    -> no metadata
    -> name "RF_ep210480_sh0120_0_MATTEALPHA_v01_"
    -> hide CHAR & PROP colection
    -> render viewport openGL

    | Export the camera in alembic format
    matte painting needs the camera object from our layout files. They use its coordinates and settings in Nuke.
    (-> change clipping for the export ? from [0.1m - 10000m] to [idem]*100?) -> need to be sure that near and far are = in layout, lighting and rendering 
    -> select Dolly_camera
    -> camera settings : clipping
    -> export Alembic : 
        -> Only “Selected objects”
        -> uncheck all option in Object Options and in the Particle Systems
    -> name "RF_ep210480_sh0120_0_camera_v01.abc"

    '''
    return


def launch_flux_playblast(render_path: str = None, render_params: dict = None, 
                          animation=True, with_metadata = True,
                          burn_data: str = None, openfile: bool = True, allow_preview_range=False) -> str :
    ''' 
    Launch an OpenGL playblast based on the preview viewport with burn data added outside of the picture 

    Args:
        - render_path : default None (-> by default à coté of the blender file), the path of the final render output
        - render_params : default None (-> take the default render scene parameters), the renders parameters to have
        - animation : default True, render the whole animation or if False, a still picture
        - burn_data : default None (-> use the default one), metadata to burn in the render

    Return: 
        The render filepath
    '''

    log.info("launch_flux_render (playblast)")

    user_scene = bpy.context.scene

    use_preview_range = False
    if user_scene.use_preview_range and allow_preview_range:
        use_preview_range = [user_scene.frame_preview_start, user_scene.frame_preview_end, user_scene.frame_start, user_scene.frame_end]
        user_scene.frame_start = use_preview_range[0]
        user_scene.frame_end = use_preview_range[1]

    # handle render parameters
    if not render_params :
        render_params = render_parameters.get_default()

    # setup user_scene render parameters
    user_scene_init_params = utils.get_obj_parameters(user_scene, render_params)
    utils.set_obj_parameters(user_scene, render_params)
    render_cam = user_scene.camera
    render_cam_init_use_DOF = render_cam.data.dof.use_dof
    render_cam.data.dof.use_dof = False

    user_scene.render.engine = "BLENDER_EEVEE"

    if not animation :
        user_scene.render.image_settings.file_format = "PNG"
    user_scene.render.use_file_extension = True
    render_item_extension = "." + user_scene.render.frame_path().split(".")[-1]
    user_scene.render.use_file_extension = False
    fileformat = user_scene.render.image_settings.file_format

    if render_path :
        user_scene.render.filepath = render_path + "_temp_" + render_item_extension
    else :
        user_scene.render.filepath = user_scene.render.filepath + "flux_playblast_temp_" + render_item_extension

    if animation:
        if not user_scene.render.is_movie_format :
            user_scene.render.filepath = user_scene.render.filepath.replace(render_item_extension, "")
            user_scene.render.filepath += "_####" + render_item_extension

    user_scene.render.use_compositing = False
    user_scene.render.use_sequencer = False

    # remove overlay and set camera view
    initial_things = remove_overlay_set_camera_view(bpy.context)

    # no animaton = no metadat!!
    if not animation:
        with_metadata = False

    if not with_metadata :
        user_scene.render.filepath = user_scene.render.filepath.replace("_temp_", "")

    render_filepath = user_scene.render.filepath

    # render playblast
    print(f"{animation=}")
    bpy.ops.render.opengl(animation = animation, write_still=True, view_context=True) #'INVOKE_DEFAULT',

    if with_metadata :
        # start of post render
        print("post render start")
        render_metadata_on_movie(user_scene=user_scene, movie_filepath=render_filepath, render_params=render_params, burn_data=burn_data)

        os.remove(render_filepath)

        render_filepath = render_filepath.replace("_temp_", "")

    # reset user_scene render parameters
    render_cam.data.dof.use_dof = render_cam_init_use_DOF
    utils.set_obj_parameters(user_scene, user_scene_init_params)
    
    if openfile :
        try :
            os.startfile(render_filepath)
        except :
            log.error(f"can't open the playblast : {render_filepath}")

    user_scene.frame_current = user_scene.frame_start
    reset_overlay_view(initial_things, bpy.context)

    if use_preview_range:
        user_scene.frame_preview_start = use_preview_range[0]
        user_scene.frame_preview_end = use_preview_range[1]
        user_scene.frame_start = use_preview_range[2]
        user_scene.frame_end = use_preview_range[3]

    return render_filepath


def layout_clean_render(render_path: str = None, openfile: bool = False):
    ''' Clean layout render : a openGL render of the layout in colour without anything else obstructing the image.
    \n->(mov output, with no metadata, CHAR & PROP collection hidden)

    '''
    # hide CHAR & PROP colection
    init_things = {}
    for col in bpy.context.scene.collection.children_recursive :
        if col.name in ["CHAR", "PROP"] :
            log.info(f"hide collection \"{col.name}\"")
            init_things.update({col : {"hide_viewport" : col.hide_viewport, "hide_render" : col.hide_render}})
            col.hide_viewport = True
            col.hide_render = True


    render_params = render_parameters.get(name="rufus_clean_layout") # TODO hard coded
    animation = True
    with_metadata = False
    render_filepath = launch_flux_playblast(render_path, render_params, animation, with_metadata, openfile = False)

    for item in init_things.keys() :
        for attr in init_things[item] :
            value = init_things[item][attr]
            setattr(item, attr, value)

    return render_filepath


def layout_alpha_images_render(render_path: str = None):
    ''' 
    Alpha images of the shot : .jpg images in black and white, in which all 3D assets in the scene are black and the sky, or areas where there is no asset visible, white.
    \n->  viewport shading : [Lighting: Flat, Color: Single and black. (HSV = 0,0,0), Background: Viewport and white. (HSV = 0,0,1), UNCHECK outline.]
    \n-> film transparent = False, output : JPEG, RGB 90%, no metadata, name "RF_ep210480_sh0120_0_MATTEALPHA_v01_", hide CHAR & PROP collection, render viewport openGL 
    '''
    # hide CHAR & PROP colection
    init_things = {}
    for col in bpy.context.scene.collection.children_recursive :
        if col.name in ["CHAR", "PROP"] :
            log.info(f"hide collection \"{col.name}\"")
            init_things.update({col : {"hide_viewport" : col.hide_viewport, "hide_render" : col.hide_render}})
            col.hide_viewport = True
            col.hide_render = True

    render_params = render_parameters.get(name="rufus_layout_alpha") # TODO hard coded
    
    """
    """

    log.info("launch_flux_render (playblast)")

    user_scene = bpy.context.scene    

    # setup user_scene render parameters
    user_scene_init_params = utils.get_obj_parameters(user_scene, render_params)
    utils.set_obj_parameters(user_scene, render_params)
    render_cam = user_scene.camera
    render_cam_init_use_DOF = render_cam.data.dof.use_dof
    render_cam.data.dof.use_dof = False

    user_scene.render.use_file_extension = True
    render_item_extension = "." + user_scene.render.frame_path().split(".")[-1]
    user_scene.render.use_file_extension = False
    fileformat = user_scene.render.image_settings.file_format

    user_scene.render.filepath = render_path + "_####" + render_item_extension

    user_scene.render.use_compositing = False
    user_scene.render.use_sequencer = False

    # remove overlay and set camera view
    initial_things = remove_overlay_set_camera_view(bpy.context)

    #
    display_init_params = {}
    if "display" in render_params :
        for area in bpy.context.screen.areas :        
            if area.type != 'VIEW_3D' : continue
            for space in area.spaces :
                if space.type != 'VIEW_3D' : continue
                space_init = utils.get_obj_parameters(space, render_params["display"])
                display_init_params.update({space : space_init})
                utils.set_obj_parameters(space, render_params["display"])


    render_filepath = user_scene.render.filepath

    # render playblast
    bpy.ops.render.opengl(animation = True, write_still=True, view_context=True) #'INVOKE_DEFAULT', 

    # reset user_scene render parameters
    if display_init_params :
        for item in display_init_params :
            utils.set_obj_parameters(item, display_init_params[item])

    
    render_cam.data.dof.use_dof = render_cam_init_use_DOF
    utils.set_obj_parameters(user_scene, user_scene_init_params)

    user_scene.frame_current = user_scene.frame_start
    reset_overlay_view(initial_things, bpy.context)

    """
    """

    for item in init_things.keys() :
        for attr in init_things[item] :
            value = init_things[item][attr]
            setattr(item, attr, value)

    return render_filepath


def launch_flux_render(render_path: str = None, 
                       render_params: dict = None, 
                       animation=True, 
                       openGL: bool = False, 
                       burn_data: str = None) -> str:
    ''' 
    Launch a render (with Eevee/Cycles or OpenGL) with burn data added outside of the picture 

    Args:
        - render_path : default None (-> by default à coté of the blender file), the path of the final render output
        - render_params : default None (-> take the default render scene parameters), the renders parameters to have
        - animation : default True, render the whole animation or if False, a still picture
        - openGL : default False, if True use OpenGL for the render
        - burn_data : default None (-> use the default one), metadata to burn in the render

    Return: 
        The render filepath 
    '''

    log.info("launch_flux_render")
    user_scene = bpy.context.scene

    if not render_params :
        render_params = render_parameters.get_default()
    else :
        if "render" not in render_params.keys():
            render_params.update({"render": {"use_sequencer" : True}})
        elif "use_sequencer" not in render_params["render"].keys():
            render_params["render"].update({"use_sequencer" : True})
        else :
            render_params["render"]["use_sequencer"] = True
        
    # setup user_scene render parameters
    user_scene_init_params = utils.get_obj_parameters(user_scene, render_params)
    utils.set_obj_parameters(user_scene, render_params)

    # create VSE scene
    #vse_scene = get_scene("VSE_render")
    vse_scene = blender_libs.scene.get("VSE_render")
    bpy.ops.sequencer.refresh_all()
    utils.set_obj_parameters(vse_scene, render_params)
    setup_vse_scene(vse_scene, user_scene, burn_data)

    vse_scene.render.use_file_extension = True
    render_item_extension = "." + vse_scene.render.frame_path().split(".")[-1]
    vse_scene.render.use_file_extension = False
    fileformat = vse_scene.render.image_settings.file_format

    if render_path :
        vse_scene.render.filepath = render_path + render_item_extension
    else :
        vse_scene.render.filepath = vse_scene.render.filepath + "flux_render" + render_item_extension

    if fileformat != "FFMPEG" and fileformat != "AVI_RAW" and fileformat != "AVI_JPEG" :
            vse_scene.render.filepath = vse_scene.render.filepath.replace(render_item_extension, "")
            vse_scene.render.filepath += "_####" + render_item_extension

    final_return = vse_scene.render.filepath

    text_updater = vse_text_handler(render_scene = user_scene, vse_scene = vse_scene, vse_text = vse_scene.sequence_editor.sequences["Text"], burn_data = burn_data) 
    text_updater.add_handler()

    progress_bar = render_progress_handler(user_scene.frame_start, user_scene.frame_end)
    progress_bar.launch_progressbar()
    
    # render via VSE scene
    bpy.context.window.scene = vse_scene

    bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)

    user_scene.render.use_compositing = False
    user_scene.render.use_sequencer = False

    if openGL :
        bpy.ops.render.opengl(animation=animation, sequencer=True, write_still=True)
    else :
        bpy.ops.render.render(animation=animation, write_still=True, use_viewport=True)

    bpy.context.window.scene = user_scene
    progress_bar.kill_progressbar()
    
    # remove VSE scene
    text_updater.remove_handler()
    remove_vse_scene(vse_scene)
    
    # reset user_scene render parameters
    utils.set_obj_parameters(user_scene, user_scene_init_params)
   
    return final_return




if __name__ == '__main__' :
    os.system("cls")
    print("___start___\n")
    
    launch_flux_render(render_params=render_parameters.get_default())

    print("\n____end____\n")