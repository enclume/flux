# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
description TODO
'''

# initialize logger
from ... import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)


def get_max_lenght_string(string: str, max_len = 40) :
    ''' return the string entry with "..." at the end if the string is longer than "max_len" '''
    string = str(string)
    if len(string) > max_len : string = string[:max_len] + " ..."
    return string


def get_obj_parameters(obj, params: dict) -> dict :
    ''' loop trough a dictionnary to get the attribut of an object '''
    log.debug("\n  get_obj_parameters")
    final_params = {}

    if not obj :
        log.error(f"obj is {obj}")

    if not params :
        log.error(f"params is {params}")
    
    def recursive_loop_get_obj_parameters(obj, param_dict):
        ''' recursive fuction for get_obj_parameters '''
        final_dict = {}
        for key in param_dict :
            if param_dict[key] == "_pass" : continue

            try :
                value = getattr(obj, key)
            except Exception as e :
                obj = get_max_lenght_string(obj)
                key = get_max_lenght_string(key)
                log.error(f"{e} (obj : {obj}, key : {key})")
                continue

            if type(param_dict[key]) is dict :
                value = recursive_loop_get_obj_parameters(value, param_dict[key])
                
            if value != "_pass" and value != {} :
                final_dict.update({key : value})
                                    
        return final_dict

    final_params = recursive_loop_get_obj_parameters(obj, params)
    
    log.debug(f"  {final_params}")
    return final_params


def set_obj_parameters(obj, params: dict):
    ''' loop trough a dictionnary to set the attribut of an object '''
    log.debug("\n  set_obj_parameters")

    def recursive_loop_set_obj_parameters(obj, params):
        ''' recursive fuction for set_obj_parameters '''
        for key in params :
            value = params[key]
            
            if value == "_pass" : continue
            if type(value) is type({}) :
                try :
                    new_obj = getattr(obj, key)
                except Exception as e :
                    obj = get_max_lenght_string(obj)
                    key = get_max_lenght_string(key)
                    log.error(f"{e} (obj : {obj}, key : {key})")
                    
                recursive_loop_set_obj_parameters(new_obj, value)
                continue
            try :
                setattr(obj, key, value)
            except:
                obj = get_max_lenght_string(obj)
                key = get_max_lenght_string(key)
                value = get_max_lenght_string(value)
                log.error(f"can't setattr (obj : {obj}, key : {key}, value : {value})")

        return True
    
    recursive_loop_set_obj_parameters(obj, params)
            
    log.debug(f"  {True}")
    return True