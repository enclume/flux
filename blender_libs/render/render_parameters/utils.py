# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
utils functions to handle dictionnary and json of scene render preferences
'''

import os
try :
    import mathutils
    get_mathutils = True
except :
    get_mathutils = False

from .... import config


# initialize logger
from ... import flux_logger 
log = flux_logger.get_flux_logger()


#reload lib
import importlib
importlib.reload(flux_logger)
importlib.reload(config)


def get_formated_dict_from_json(json_dict) :
    final_dict = {}

    def recursive_get_formated_dict_from_json(json_dict, final_dict) :
        to_format = {"_None" : None, "_False" : False, "_True": True}
        for key in json_dict:
            value = json_dict[key]

            if value == "_pass" : continue

            if type(value) is dict :
                value = {}
                recursive_get_formated_dict_from_json(json_dict[key], value)

            elif value in to_format.keys() :
                value = to_format[value]
            elif type(value) is str and value.startswith("_color") and get_mathutils:
                colors = value[(value.find("(") + 1):value.find(")")]
                colors = colors.split(", ")
                colors = [float(c) for c in colors]

                value = mathutils.Color((colors[0], colors[1], colors[2]))

            final_dict.update({key : value})

        return
    
    recursive_get_formated_dict_from_json(json_dict, final_dict)

    log.debug(f"return {final_dict}")
    return final_dict


def overhide_data_on_dict(base_dict, overhide_dict) :
    final_dict = base_dict.copy()

    def recursive_overhide_data_on_dict(final_dict, overhide_dict) :
        for key in overhide_dict :
            value = overhide_dict[key]
            if value == "_pass" : continue

            if value in ["_remove", "_delete"] and key in final_dict :
                final_dict.pop(key)
                continue

            if key not in final_dict :
                final_dict.update({key : value})
                continue

            if type(value) is dict :
                if type(final_dict[key]) is not dict :
                    final_dict[key] = value
                    continue

                recursive_overhide_data_on_dict(final_dict[key], overhide_dict[key])
                continue

            if key in final_dict and final_dict[key] != value :
                final_dict[key] = value
                continue
    
    recursive_overhide_data_on_dict(final_dict, overhide_dict)
    return final_dict


def get_dict_from_json_name(name: str = "default") :
    path = name + ".json"
    path = os.path.join("blender_libs", "render", "render_parameters", path)
    json_dict = config.utils.load_json_file(path) 
    return get_formated_dict_from_json(json_dict)