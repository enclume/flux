# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
description TODO
'''

from . import utils

# initialize logger
from ... import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)
importlib.reload(utils)

def get_default() :
    ''' get default render parameters '''
    return utils.get_dict_from_json_name()


def get(name: str = "default", add_to_default = True) :
    ''' get render parameters by name '''
    if not add_to_default :
        return utils.get_dict_from_json_name(name)
    
    default = utils.get_dict_from_json_name()
    if name == "default" :
        return default
    
    params = utils.get_dict_from_json_name(name)

    return utils.overhide_data_on_dict(default, params)