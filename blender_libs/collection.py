# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Blender libs collection functions
'''

import bpy

# initialize logger
from .. import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)

def get(col_name: str, parent_col = None, scene = None, create_col: bool = True) -> bpy.types.Collection :
    '''
    get the collection by his name (create it if don't exist)

    Args:
        - col_name : the name of the wanted collection
        - parent_col : the name of the parent collection if create_col is True (default is the scene collection)
        - scene : the name of the scene if create_col is True (default is bpy.context.scene)
        - create_col : if True, create the collection if does'nt exist (default is True)

    Return:
        collection
    '''
    col = bpy.data.collections.get(col_name)
    
    if col : 
        return col
    if not create_col :
        return False
    
    col = bpy.data.collections.new(col_name)

    if scene == None :
        scene = bpy.context.scene
    
    if not parent_col :
        scene.collection.children.link(col)
        return col
    
    # TODO if parent_col :
    return col


def set_active(collection: str) -> bool :
    '''
    set active a collection

    Args:
        - collection : the collection to set active or his name

    Return:
        True if the collection is succesfully set active
    '''
    if type(collection) == str :
        collection = bpy.data.collections.get(collection)
    elif type(collection) != bpy.types.Collection :
        log.error(f"collection type must be a \"str\" or a \"bpy.types.Collection\"\n   -> collection type : {type(collection)}")
        return False

    if not collection :
        log.error("can't find the collection")
        return False

    layer_collection = bpy.context.view_layer.layer_collection.children[collection.name]
    bpy.context.view_layer.active_layer_collection = layer_collection

    return True


def pack_all_in(col_name: str, col_exclude: list = [], unlink_old_col: bool = True, scenefrom = None, sceneto = None) :
    '''
    pack all collections (in a scene) in a specified collection

    Args:
        - col_name : the name of the collection to pack all in
        - col_exclude : collections to exclude
        - unlink_old_col :
        - scenefrom :
        - sceneto :

    Return:
        col_pack
    '''

    if scenefrom == None :
        scenefrom = bpy.context.scene

    if sceneto == None :
        sceneto = bpy.context.scene

    # get col by col_name -> create it if needed
    col_pack = get(col_name, scene=sceneto)
    
    for col in scenefrom.collection.children :
        if col.name == col_name or col.name in col_exclude : continue
        
        if unlink_old_col :
            scenefrom.collection.children.unlink(col)
        #if col not in col_pack.children :
        try :
            col_pack.children.link(col)
        except:
            pass
    
    return col_pack


def append(import_path: str, import_col_name: str, parent_col_name: str = None) :
    '''
    append a collection

    Args:
        - import_path : the path of the blender source file 
        - import_col_name : the name of the collection to append
        - parent_col_name : the name of the future parent collection (bpy.context.scene.collection by default)

    Return:
        the imported collection
    '''
    if parent_col_name == None or not bpy.data.collections.get(parent_col_name):
        parent_col = bpy.context.scene.collection
    else :
        parent_col = bpy.data.collections[parent_col_name]
        set_active(parent_col.name)
    
    try :
        if True :
            bpy.ops.wm.append(
                filepath = import_path.split("\\")[-1],
                directory = import_path + "\\Collection\\",
                filename = import_col_name
                )   

    except Exception as err :
        log.error(f"fail to append the collection \"{import_col_name}\"\n   ->(parent_col_name : {parent_col_name}, import_path : {import_path})\nException :\n{err}")
        raise

    if not bpy.data.collections.get(import_col_name) :
        log.error(f"fail to link the collection \"{import_col_name}\"\n   ->(parent_col_name : {parent_col_name}, link_path : {import_path})")
        raise Exception
    
    return bpy.data.collections[import_col_name]


def link(link_path: str, link_col_name: str, parent_col_name: str = None, instance_collections: bool = True, path_absolute = True) :
    '''
    link a collection

    Args:
        - link_path : the path of the blender source file 
        - link_col_name : the name of the collection to link
        - parent_col_name : the name of the future parent collection (bpy.context.scene.collection by default)

    Return:
        the imported collection
    '''
    if parent_col_name == None or not bpy.data.collections.get(parent_col_name):
        parent_col = bpy.context.scene.collection
    else :
        parent_col = bpy.data.collections[parent_col_name]
        set_active(parent_col_name)
    
    try :
        if True :
            bpy.ops.wm.link(
                filepath = link_path.split("\\")[-1],
                directory = link_path + "\\Collection\\",
                filename = link_col_name,
                instance_collections = instance_collections
                )   
        else :
            with bpy.data.libraries.load(link_path, link = True) as (data_from, data_to):
                data_to.collections = [link_col_name]

            for col in data_to.collections :
                instance = bpy.data.objects.new(col.name, None)
                instance.instance_type = 'COLLECTION'
                instance.instance_collection = col
                # link hard collection to blend file
                parent_col.objects.link(instance)

    except Exception as err :
        log.error(f"fail to link the collection \"{link_col_name}\"\n   ->(parent_col_name : {parent_col_name}, link_path : {link_path})\nException :\n{err}")
        raise
    
    if not bpy.data.collections.get(link_col_name) :
        log.error(f"fail to link the collection \"{link_col_name}\"\n   ->(parent_col_name : {parent_col_name}, link_path : {link_path})")
        raise Exception
    
    if path_absolute :
        bpy.ops.file.make_paths_absolute()
    
    try :
        return bpy.data.objects[link_col_name]
    except :
        return bpy.data.collections[link_col_name]


def delete_hierarchy(collection) :
    ''' delete the collection from the hierarchy '''
    for obj in collection.all_objects :
        obj.animation_data_clear()
        bpy.data.objects.remove(obj)

    for col in collection.children_recursive :
        bpy.data.collections.remove(col)

    bpy.data.collections.remove(collection)
    return True


def delete_hierarchy_recursive(collection_to_delete):
    """
    another delete methode for collection and children, that doesnt crash in my script :/
    Args:
        collection: collection to delete
    """
    # Try to find the collection by name
    if isinstance(collection_to_delete, str):
        collection_to_delete = bpy.data.collections[collection_to_delete]

    # proceed
    if collection_to_delete:
        # Recursively delete sub-collections and objects
        def delete_recursive(collection):
            for sub_collection in collection.children:
                delete_recursive(sub_collection)
                bpy.data.collections.remove(sub_collection)

            for obj in collection.objects:
                bpy.data.objects.remove(obj)

        # Start the recursive deletion
        delete_recursive(collection_to_delete)

        # Remove the top-level collection from the scene
        bpy.data.collections.remove(collection_to_delete)




def get_master_name_from_path(path: str = None, override_task: str = None) -> str :
    ''' 
    get the name of the master collection 

    Args:
        - path : the path of what is the name deduced (bpy.context.blend_data.filepath by default)
        - override_task : override the task (initially inferred from the path)
    
    Return:
        the name of the master collection 
    '''
    if not path :
        path = bpy.context.blend_data.filepath

    master_col_name = []

    for item in path.split("\\")[-1].split("_") :
        if item.startswith("v") and item[1].isdigit() and item[2].isdigit() :
            break
        master_col_name.append(item)


    if override_task :
        master_col_name[-1] = override_task


    master_col_name = "_".join(master_col_name)

    log.debug(f"return : {master_col_name}")
    return master_col_name


def set_master(scene = None, path = None) :
    ''' 
    set the master collection in the blend current file 

    Args:
        - scene : the scene where the master collection will be set (bpy.context.scene by default)
        - path : the path of what the name of the master collection is deduced (bpy.context.blend_data.filepath by default)
    
    Return:
        the master collection 
    '''
    if not scene :
        scene = bpy.context.scene

    master_col_name = get_master_name_from_path(path = path)

    if master_col_name in scene.collection.children :
        return scene.collection.children[master_col_name]

    if len(scene.collection.children) == 1 :
        master_col = scene.collection.children[0]
        master_col.name = master_col_name
    else :
        master_col = get(master_col_name, scene = scene)

        if master_col_name not in scene.collection.children :
            scene.collection.children.link(master_col)

        for col in scene.collection.children_recursive :
            if col.name != master_col_name :
                col.collection.children.unlink(master_col)

        for col in scene.collection.children :
            if col.name == master_col_name : continue
            if col.name not in master_col.children :
                master_col.children.link(col)
            scene.collection.children.unlink(col)

    log.debug(f"master collection \"{master_col_name}\" setuped")
    return master_col


def get_all_parents(collection, return_name=False, include_input=False):
    """
    Get all parents of a collection, in order, from bottom to high
    Args:
        collection (): start collrction

    Returns: array or None

    """
    def recursive_parents(collection, parents):
        #print(f"look for rec parents: {collection=}")
        for parent in bpy.data.collections:
            if not parent.children or collection.name not in parent.children.keys() : 
                continue
            parents.append(parent)
            recursive_parents(parent, parents)

    # Check if the collection exists
    if isinstance(collection, str):
        collection = bpy.data.collections.get(collection)

    if not collection :
        return None  # Return None if the collection doesn't exist

    parents = []
    if include_input:
        parents = [collection]
    recursive_parents(collection, parents)

    if return_name:
        return [parent.name for parent in parents]
    
    return [parent for parent in parents]

    

def get_top_parent(collection) :
    '''
    Return the top of all embeded collections
    Args:
        collection (): start parsing

    Returns:top collection

    '''

    all_parents = get_all_parents(collection)
    if all_parents:
        return all_parents[-1]

    return None



def get_all_parents(collection, return_name=False, include_input=False):
    """
    Get all parents of a collection, in order, from bottom to high
    Args:
        collection (): start collrction

    Returns: array or None

    """
    def recursive_parents(collection, parents):
        # print(f"look for rec parents: {collection=}")
        for parent in bpy.data.collections:
            if not parent.children or collection.name not in parent.children.keys() : 
                continue
            parents.append(parent)
            recursive_parents(parent, parents)

    # Check if the collection exists
    if isinstance(collection, str) :
        collection = bpy.data.collections.get(collection)

    if not collection:
        return None  # Return None if the collection doesn't exist

    parents = []
    if include_input:
        parents = [collection]
        
    recursive_parents(collection, parents)
    if return_name :
        return [parent.name for parent in parents]

    return [parent for parent in parents]

def get_parent(collection):
    """
    Get immediate collection parent of a collection
    Args:
        collection: collection to parse

    Returns:immediate parent. Or None...

    """

    # Check if the collection exists
    if isinstance(collection, str) :
        collection = bpy.data.collections.get(collection)

    # check
    for parent in bpy.data.collections:
        for child in list(parent.children):
            if collection == child:
                # found it
                return parent

    # return if not found
    return None


def get_top_parent(collection):
    '''
    Return the top of all embeded collections
    Args:
        collection (): start parsing

    Returns:
        top collection

    '''

    all_parents = get_all_parents(collection)
    if all_parents :
        return all_parents[-1]

    return None


def get_layer_collection(collName, layerColl=None):
    """
    To retreive .exclude param and others, collection have to be founded as layer collection.
    This procedure give it, by name.
    Args:
        collName: The name to get from, as str.
        layerColl: inside use for recursive algo.

    Returns: layerCollection

    """
    if not layerColl:
        layerColl = bpy.context.view_layer.layer_collection

    found = None
    if (layerColl.name == collName):
        return layerColl
    for layer in layerColl.children:
        found = get_layer_collection(collName, layer)
        if found:
            return found