# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Blender bpy related functions
'''

import bpy
import math
import mathutils
import os

from . import collection
from . import scene
from . import object
from . import timer
from .. import file_access

# initialize logger
from .. import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)
importlib.reload(collection)
importlib.reload(file_access)
importlib.reload(scene)
importlib.reload(object)
importlib.reload(timer)


def get_bpy_context():
    ''' get bpy.context if possible, else return False '''
    try :
        return bpy.context
    except :
        log.debug("can't access bpy.context")
        return False


def is_collection_in_file(file_path, collection_name = False):
    """
    check if collection exist in a blender file
    Args:
        file_path: file to check
        collection_name: collection name to check. False will test the collectiom deducted from file path without version

    Returns: Bool

    """
    item_col_master_name = collection_name

    if not collection_name:
        item_col_master_name = collection.get_master_name_from_path(file_path)

    with bpy.data.libraries.load(file_path) as (data_from, _):
        # file name, list of collections
        # print("------")
        # print(item_col_master_name)
        # print([c for c in data_from.collections])
        # print(data_from.collections)

        if item_col_master_name in data_from.collections : 
            return True
        return False


#library stuff
def import_item(item_data: dict, link: bool = True, parent_col: str = None, make_lib_override: bool = True) :
    '''
    import (append or link) an item from the given item_data dict

    Args:
        - item_data:
        - link:
        - parent_col:

    Return:
        the imported collection
    '''
    log.debug(f"start importing an item\n   ->(item_data : {item_data})\n   ->(link : {link})\n   ->(parent_col : {parent_col})")

    item_path = file_access.get_item_path(item_data = item_data)

    if not os.path.exists(item_path) :
        log.error(f"path does not exist\n   ->(path : {item_path})\n   ->(item_data : {item_data})")
        return False
    
    item_col_master_name = collection.get_master_name_from_path(item_path)

    if link :
        link_result = collection.link(link_path = item_path, link_col_name = item_col_master_name, parent_col_name = parent_col)
        if make_lib_override :
            bpy.ops.object.make_override_library()
        return link_result

    return collection.append(import_path = item_path, import_col_name = item_col_master_name, parent_col_name = parent_col)
    
    
def get_colormanagement_install_path():
    ''' description TODO '''
    install_path = bpy.app.binary_path
    install_path = install_path.split("\\")[:-1]
    #blender_version = 3.6
    blender_version = bpy.app.version
    blender_version = [str(blender_version[0]), str(blender_version[1])]

    '''
    for char in install_path[-1] :
        if char.isdigit() : blender_version.append(char)
        if len(blender_version) == 2 : break

    if not blender_version :
        for char in install_path[-2] :
            if char.isdigit() : blender_version.append(char)
            if len(blender_version) == 2 : break
    '''

    if not blender_version :
        log.error("can't find the blender_version ")
        return False

    blender_version = ".".join(blender_version)

    install_path.extend([str(blender_version), "datafiles"])
    install_path = "\\".join(install_path)

    return install_path


def library_get_from_selected_object() :
    ''' description TODO '''
    obj = bpy.context.object
    if not obj :
        log.info(f"bpy.context.object is {obj}")
        return False

    lib_list = []

    for lib in bpy.data.libraries :
        if f"bpy.data.objects[\'{obj.name}\']" in str(lib.users_id) :
            lib_list.append(lib)
   
    return lib_list[0]


def library_get_from_filepath(filepath: str) :
    ''' description TODO '''
    for lib in bpy.data.libraries :
        if filepath == lib.filepath :
            return lib
    
    lib_list = []

    for lib in bpy.data.libraries :
        if filepath in lib.filepath :
            lib_list.append(lib)

    if len(lib_list) == 1 :
        return lib_list[0]

    return False


def library_replace_link(library, new_filepath: str) :
    ''' description TODO '''
    library.filepath = new_filepath
    library.reload()
    return True


#other
def world_link(link_path: str, world_name: str) :
    '''
    link the given world in the current blend file

    Args:
        link_path :
        world_name :

    Return:
        world
    '''
    bpy.ops.wm.link(
            filepath = link_path.split("\\")[-1],
            directory = link_path + "\\World\\",
            filename = world_name
            )   
        
    return bpy.data.worlds[world_name]


def find_cam_location(cam, asset_collection) :
    '''
    find the camera location to frame all object in the asset collection

    Args:
        cam : the camera
        asset_collection : the collection with the objects to frame

    Return:
        a vector of the new camera location
    '''
    translate_vector = cam.location.copy()
    translate_vector.normalize()

    vert_fov_vector = mathutils.Vector((0.0, math.cos(cam.data.angle/2), math.sin(cam.data.angle/2)))

    top_loc = mathutils.Vector((0.0, 0.0, 0.0))

    for obj in asset_collection.all_objects :
        if obj.type != "MESH" : continue

        for bb in obj.bound_box :   
            bb_vector = mathutils.Vector((bb[0], bb[1], bb[2]))   
            bb_vector *= obj.scale
            bb_vector += obj.location
            if bb_vector[1] < top_loc[1] :
                top_loc[1] = bb_vector[1]
            if bb_vector[2] > top_loc[2] :
                top_loc[2] = bb_vector[2]
                    
    top_loc *= 1.6

    cam_loc = cam.location
    new_loc = mathutils.geometry.intersect_line_line(top_loc, (top_loc + vert_fov_vector), mathutils.Vector((0.0, 0.0, 0.0)), translate_vector)
    
    new_loc = (new_loc[0] + new_loc[1]) * 0.5

    return new_loc


def get_current_selection(context = bpy.context) :
    ''' 
    get a list of selected object 
    \n(-> bpy.context.selected_objects)
    '''
    return context.selected_objects


def set_selection(to_set: list = None) :
    ''' 
    set the select state of all objects in the scene based on an object list
     (in the list -> selected, else not) 

    Args:
        - to_set : the list of the future selection
    '''
    if to_set == [] or to_set == None :
        for obj in bpy.data.objects : obj.select_set(False)
        log.debug("to_set is empty, all objects set to be unselected")
        return True

    if type(to_set) in [dict] :
        log.error(f"the type of to_set variable is {type(to_set)}")
        return False
    if type(to_set) != list :
        to_set = [to_set]

    for obj in bpy.data.objects :
        if obj in to_set or obj.name in to_set :
            obj.select_set(True)
            continue
        obj.select_set(False)

    return True
   

if __name__ == '__main__' :
    os.system("cls")

    print("\n---START---\n")
    
    print("\n----END----\n")