# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
subprocess open blender fuctions
'''

import os
import subprocess

def get_subprocess_args(blender_path: str, file_path: str, 
                        script: str, debug_borne: str = "###borne###", 
                        background: bool = True) -> list :
    
    ''' description TODO '''

    printBorne = f'print(\"{debug_borne}\")'
    args_list = [] 

    args_list.append(blender_path)      # blender exe path
    if background :
        args_list.append("-b")          # background execution
    args_list.append(file_path)         # blendfile path
    args_list.append("--python-expr")   # execute a python expression inside blender
    args_list.append(f"{printBorne}; {script}; {printBorne}") # python expression to execute inside blender

    return args_list

def openBlender_exeScript(blender_path: str = r"C:\Program Files\Blender Foundation\Blender 3.3\blender-3.3.exe", 
                          file_path: str = "", script='print(\"default script\")', debug: bool = False):
    '''
    Open a blend file in a subprocess and run a script inside

    Args:
        - blender_path: the path to the blender.exe
        - file_path: the path to the file to open
        - script: the script to execute
    '''
    if not file_path :
        raise Exception("ERROR in openBlender_exeScript : file_path need to be given") 
    
    if not os.path.exists(blender_path) :
        raise Exception("ERROR in openBlender_exeScript : blender_path does not exist")
    
    if not os.path.exists(file_path) :
        raise Exception("ERROR in openBlender_exeScript : file_path does not exist") 

    Borne = '###borne###'
    subprocess_args = get_subprocess_args(blender_path, file_path, script, Borne, background=False)

    process = subprocess.Popen(subprocess_args, stdin=subprocess.DEVNULL, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True)
    out = process.stdout.readlines()
    out = "  |  ".join(out)

    if "Error:" in str(out) : 
        print(out)
        
    elif debug : 
        borne = out.find(Borne)
        out = out[(borne + len(Borne)):]
        borne = out.find(Borne)
        out = out[:(borne - 5)]
        
        print('\n__Blender log (' + file_path.split("\\")[-1] + ') :')
        print(out)

    process.terminate()


def openBlender_exeScript_background(blender_path: str, file_path: str, script='print(\"default script\")', debug: bool = False):
    '''
    Open a blend file in a subprocess and run a script inside

    Args:
        - blender_path: the path to the blender.exe
        - file_path: the path to the file to open
        - script: the script to execute
    '''    
    if not os.path.exists(blender_path) :
        raise Exception("ERROR in openBlender_exeScript : blender_path does not exist")
    
    if not os.path.exists(file_path) :
        raise Exception("ERROR in openBlender_exeScript : file_path does not exist") 

    Borne = '###borne###'
    subprocess_args = get_subprocess_args(blender_path, file_path, script, Borne)
    process = subprocess.Popen(subprocess_args, stdin=subprocess.DEVNULL, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True)
    
    out = process.stdout.readlines()
    out = "  |  ".join(out)
    if "Error:" in str(out) : 
        print(out)
        
    elif debug : 
        borne = out.find(Borne)
        out = out[(borne + len(Borne)):]
        borne = out.find(Borne)
        out = out[:(borne - 5)]
        
        print('\n__Blender log (' + file_path.split("\\")[-1] + ') :')
        print(out)
    
    process.terminate()


def blender_render_commandline(blender_path: str = r"C:\Program Files\Blender Foundation\Blender 3.3\blender-3.3.exe", 
                               file_path: str = "", debug: bool = False):
    '''
    Open a blend file in a subprocess in background and render the animation

    Args:
        - blender_path: the path to the blender.exe
        - file_path: the path to the file to open
    '''    
    
    if not file_path :
        raise Exception("ERROR in openBlender_exeScript : file_path need to be given") 
    
    if not os.path.exists(blender_path) :
        raise Exception("ERROR in openBlender_exeScript : blender_path does not exist")
    
    if not os.path.exists(file_path) :
        raise Exception("ERROR in openBlender_exeScript : file_path does not exist") 

    process = subprocess.Popen([
        blender_path, 
        "-b",
        file_path,
        "-a",
        ], stdin=subprocess.DEVNULL, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True)
    out = process.stdout.readlines()
    out = "  |  ".join(out)

    if debug or True : 
        print('\n__Blender log (' + file_path.split("\\")[-1] + ') :\n')
        print(out)
    if "Error:" in out : print(out[out.find("Error:")-2:])
    
    process.terminate()