'''
Create a report of all published shot, for all tasks, in csv format.
'''
import tkinter as tk
from standalone_scripts import ui

import csv
import os
from pathlib import Path 

import database_access
import file_access


def get_all_shots_data(seqs: list) -> list :
    ''' get all shots data from a DB '''
    to_return = []
    # seq
    for seq in seqs:
        shots = database_access.get_shots(seq)
        # shots
        for shot in shots:
            tasks = database_access.get_shots_tasks(seq, shot, filter=True)
            # tasks
            for task in tasks:
                current_item = {}
                current_item["category"] = "shots"
                current_item["version"] = [1]
                current_item["seq"] = seq
                current_item["shot"] = shot
                current_item["task"] = task

                path = file_access.get_item_path2(current_item, only_file_path=True)
                # record if needed
                if os.path.exists(path):
                    to_return.append([seq, shot, task, path])
                    print(f"{path} EXIST")

    return to_return


def write_csv_file(export_path: Path, data: list) :
    ''' 
    write a csv file at the given location 

    Args:
        - export_path : the path where the csv file will be write
        - data : a list of data. Each item will be a line in the csv file
    '''
    with open(export_path, mode='w', newline='') as csv_file :
        csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        for each in data :
            csv_writer.writerow(each)


def main(host: str, log_in: str, password: str, export_path: str) :
    ''' description TODO '''

    # go
    print("---PROCESS---")

    database_access.db_login(log_in, password, False)
    # vars
    #export = "D:/test2.csv"
    export = export_path
    seqs = database_access.get_seqs()

    shots_data = get_all_shots_data(seqs)
    
    print(f"\n_> write CSV file (-> {export_path})")
    write_csv_file(export, shots_data)
    print("-----END-----")


def draw(tk_parent: tk.Widget = None) :
    need_mainloop = False

    if not tk_parent :
        tk_parent = tk.Tk()
        tk_parent.title('Blender Batch Script')
        tk_parent.minsize(200, 200)
        need_mainloop = True

    STR_host = tk.StringVar(value="https://nextframes-animation-studio.cg-wire.com/api")
    STR_log_in = tk.StringVar(value="nicolasced@gmail.com")
    STR_password = tk.StringVar(value="realK_117")
    STR_export_path = tk.StringVar(value="D:\\")
    STR_csv_name = tk.StringVar(value="published_items_export")

    BOO_explorer = tk.BooleanVar(value=True)

    FRA_properties = tk.Frame(tk_parent)
    FRA_properties.pack(fill="both")


    host = ui.labeled_entry(FRA_properties, "DB host :", STR_host)
    export_path = ui.browse_path_entry(FRA_properties, "CSV export path :", STR_export_path)
    csv_name = ui.labeled_entry(FRA_properties, "name of the CSV file :", STR_csv_name)

    FRA_boolean = tk.Frame(FRA_properties)
    FRA_boolean.pack(fill="both")
    CHK_explorer = tk.Checkbutton(FRA_boolean, text="show file in explorer", variable=BOO_explorer)
    CHK_explorer.grid(padx=0, pady=2, row=0, column=0, sticky="w")



    def launch_script() :
        host = STR_host.get()
        log_in = STR_log_in.get()
        password = STR_password.get()
        export_path = STR_export_path.get() + "\\" + STR_csv_name.get() + ".csv"

        main(host, log_in, password, export_path)

        if BOO_explorer.get() : 
            os.startfile(STR_export_path.get())

    BTN_execute = tk.Button(tk_parent, width=30, height=2, text='Run Script', command=launch_script)
    BTN_execute.pack(padx=2, pady=6, fill='y')

    if need_mainloop :
        tk_parent.mainloop()



def execute() :
    draw()
