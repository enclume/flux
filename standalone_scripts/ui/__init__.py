''' ui generics stuff for flux standalone scripts '''

import tkinter as tk
from tkinter import Widget
from tkinter import filedialog
from typing import Union

from standalone_scripts.ui.class_tk_cache import tk_cache

import flux_logger
# initialize logger
log = flux_logger.get_flux_logger("flux standalone")
log.setLevel("INFO")


def format_paths(path: Union[str, list[str]], slash: str = "\\") -> Union[str, list[str]] :
    ''' 
    format all given path (a string or a list of strings) to have slashs equal to the give one 
    Args:
        - path: a string or a list of strings
        - slash: the wanted slash ("\\" or "/")
    '''
    if type(path) not in [str, list] : return
    slashs = ["\\", "/"]
    if slash not in slashs : 
        log.error(f"slash should be \"\\\" or \"/\", not \"{slash}\".\n-> slash set to \"\\\"")
        slash = "\\"
    good = slash
    bad = slashs.index(good)
    bad = slashs[not bad]

    if type(path) is str :
        return path.replace(bad, good)
    if type(path) is list :
        good_list = []
        for bad_path in path :
            good_list.append(format_paths(bad_path, slash=slash))

        return good_list


def browse(STR_path: tk.StringVar, initialdir: str = None, dir_not_file: bool = False) -> str :
    ''' 
    browse function for browse button, ask through file dialog a path to the user 

    Args:
        - STR_path: the tkinter string var to update
        - initialdir: the initial path to show in the file dialogue
        - dir_not_file: if True, ask for a directory, else for a file

    Return:
        the choosen path
    '''

    if dir_not_file :
        path = filedialog.askdirectory(initialdir=initialdir)
    else :
        path = filedialog.askopenfilename(initialdir=initialdir)
    path = format_paths(path)    
    STR_path.set(path)
    return path


class custom_tk_widget :
    def __init__(self, master: Widget):
        ''' 
        base class for custom tk widgets 

        Args:
            - master: the tkinter parent widget
        '''
        self.master = master


class scrollable_frame(custom_tk_widget) :
    ''' 
    description TODO 
    scrollable_frame.frame can be referenced as master for other tk widget
    '''
    def __init__(self, master: Widget, padx: int = 0, pady: int = 0) :
        super().__init__(master)

        bg_color = 'white'

        self.mainFrame = tk.Frame(self.master, background=bg_color)
        self.mainFrame.pack(fill='both', padx=padx, pady=pady)

        self.scrollFrame = tk.Canvas(self.mainFrame, highlightthickness=0, background=bg_color)
        self.scrollFrame.pack(side='left', fill='both', expand=True)
        self.scrollBar = tk.Scrollbar(self.mainFrame, background=bg_color)
        self.scrollBar.pack(side='right', fill='y', expand=False)

        self.scrollFrame.config(yscrollcommand = self.scrollBar.set)
        self.scrollBar.config(command = self.scrollFrame.yview)
        self.scrollFrame.xview_moveto(0)
        self.scrollFrame.yview_moveto(0)

        self.frame = tk.Frame(self.scrollFrame, background=bg_color)
        self.frame_id = self.scrollFrame.create_window((0,0), window=self.frame, anchor='nw')

        self.wgts = [self.mainFrame, self.scrollFrame, self.scrollBar, self.frame]

        def _configure_frame(event):
            # Update the scrollbars to match the size of the inner frame.
            size = (self.frame.winfo_reqwidth(), self.frame.winfo_reqheight())
            self.scrollFrame.config(scrollregion=f"0 0 {size[0]} {size[1]}")
            if self.frame.winfo_reqwidth() != self.scrollFrame.winfo_width():
                # Update the canvas's width to fit the inner frame.
                self.scrollFrame.config(width=self.frame.winfo_reqwidth())
        self.frame.bind('<Configure>', _configure_frame)

        def _configure_canvas(event):
            if self.frame.winfo_reqwidth() != self.scrollFrame.winfo_width():
                # Update the inner frame's width to fill the canvas.
                self.scrollFrame.itemconfigure(self.frame_id, width=self.scrollFrame.winfo_width())
        self.scrollFrame.bind('<Configure>', _configure_canvas)

        self.scrollFrame.config(border=1, relief='flat')


class labeled_entry(custom_tk_widget):
    def __init__(self, master: Widget, label_txt: str, entry_variable: tk.StringVar, pad: int = 2, width_entry: int = 80):
        ''' 
        description TODO 

        Args:
            - master: the tkinter parent widget
            - label_txt: 
            - entry_variable: 
            - pad: 
            - width_entry: 
        '''
        super().__init__(master)

        self.container = tk.Frame(self.master)
        self.container.pack(fill="both", expand=True)
        self.label = tk.Label(self.container, text=label_txt)
        self.label.pack(padx=0, pady=pad, side='left', fill='y')
        self.entry = tk.Entry(self.container, textvariable=entry_variable, width=width_entry)
        self.entry.pack(padx=pad, pady=pad, fill='x', expand=True)


class browse_path_entry(custom_tk_widget):
    def __init__(self, master: Widget, label_txt: str, entry_variable: tk.StringVar, pad: int = 2, width_entry: int = 80, 
                 initial_dir_path: str = None, dir_not_file: bool = True):
        ''' 
        description TODO 

        Args:
            - master: the tkinter parent widget
            - label_txt: 
            - entry_variable: 
            - pad: 
            - width_entry: 
            - initial_dir_path: 
            - dir_not_file: 
        '''
        super().__init__(master)

        self.container = tk.Frame(self.master)
        self.container.pack(fill="both", expand=True)
        self.label = tk.Label(self.container, text=label_txt)
        self.label.pack(padx=0, pady=pad, side='left', fill='y')
        self.entry = tk.Entry(self.container, textvariable=entry_variable, width=width_entry)
        self.entry.pack(padx=pad, pady=pad, side='left', fill='x', expand=True)
        self.browse = tk.Button(self.container, text='browse path', command=lambda: browse(entry_variable, initial_dir_path, dir_not_file))
        self.browse.pack(padx=pad, pady=pad, side='right', fill='y')