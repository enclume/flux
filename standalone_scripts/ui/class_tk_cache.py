
import json
#import tkinter as tk
from tkinter import StringVar

import flux_logger

# initialize logger
log = flux_logger.get_flux_logger("flux standalone")
log.setLevel("INFO")

class tk_cache :
    ''' class used to manage a cache in a tkinter applications '''
    def __init__(self, var: StringVar = None) :
        ''' 
        class used to manage a cache in tkinter applications 
        
        Args: 
            - var : a tkinter StringVar used to store the cache (in a json way)
        '''
        self.var = var
        if not(var and type(var) is StringVar) : 
            self.var = StringVar()
            self.var.set("{}")

        if self.var.get()[0] != "{" :
            self.var.set("{" + self.var.get())
        if self.var.get()[-1] != "}" :
            self.var.set(self.var.get() + "}")

    def get(self, key: str = None) -> dict :
        ''' 
        get the cache in a python dictionnary (from the json formated string) 

        Args:
            - key: the specific dictionnary key to return (if not set, return the dictionnary)
        '''
        cache = json.loads(self.var.get())
        if not key :
            return cache
        if key not in cache :
            log.info(f"key \"{key}\" not in cache")
            return 
        return cache[key]
    
    def set(self, obj: dict) :
        ''' set the cache and store it in a json formated string '''
        self.var.set(json.dumps(obj)) 

    def update(self, obj: dict) :
        ''' update the dictionnary cache (like a \"dict.update()\") '''
        cache = self.get()
        cache.update(obj)
        self.set(cache)

    def update_key(self, key: str, value) :
        ''' update a specific key of the dictionnary cache '''
        cache = self.get()
        cache[key] = value
        self.set(cache)