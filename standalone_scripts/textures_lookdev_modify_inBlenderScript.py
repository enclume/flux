import bpy
from bpy.app.handlers import persistent

def modify_lookdev_material(material_name: str, texture_filepath: str):
    ''' description TODO '''
    mat = bpy.data.materials[material_name]
    mat_nodes = mat.node_tree.nodes
    
    is_existing = {"lowres_output": None, "lowres_diffuse": None, "lowres_texture": None}
    
    for node in mat_nodes :
        if node.type == "OUTPUT_MATERIAL" and node.label == "Eevee lowres output":
            is_existing["lowres_output"] = node

        if node.type == "BSDF_DIFFUSE" and node.label == "Eevee lowres Diffuse BSDF":
            is_existing["lowres_diffuse"] = node

        if node.type == "TEX_IMAGE" and node.label == "Eevee lowres output":
            is_existing["lowres_texture"] = node
        
    if is_existing["lowres_output"] and is_existing["lowres_diffuse"] and is_existing["lowres_texture"] :
        print("lookdev already setuped")
        return True
        
    for node in mat_nodes :
        if node.type == "OUTPUT_MATERIAL" and node.label == "" and node.target == "ALL":
            #print(node.bl_idname)
            node.label = "Cycles output"
            node.target = "CYCLES"
            
            
    lowres_image = bpy.data.images.load(filepath=texture_filepath, check_existing=True)
    lowres_image.source = "TILED"
    lowres_image.reload()
    lowres_image.colorspace_settings.name = "ACES - ACEScg"


    if is_existing["lowres_output"] :
        lowres_output = is_existing["lowres_output"]
    else :
        lowres_output = mat_nodes.new("ShaderNodeOutputMaterial")

    lowres_output.location = (300.0, 700.0)
    lowres_output.label = "Eevee lowres output"
    lowres_output.target = "EEVEE"
    
    
    if is_existing["lowres_diffuse"] :
        lowres_diffuse = is_existing["lowres_diffuse"]
    else :
        lowres_diffuse = mat_nodes.new("ShaderNodeBsdfDiffuse")

    lowres_diffuse.location = (100.0, 700.0)
    lowres_diffuse.label = "Eevee lowres Diffuse BSDF"
    
    
    if is_existing["lowres_texture"] :
        lowres_texture = is_existing["lowres_texture"]
    else :
        lowres_texture = mat_nodes.new("ShaderNodeTexImage")

    lowres_texture.location = (-200.0, 700.0)
    lowres_texture.label = "Eevee lowres Image Texture"
    lowres_texture.image = lowres_image
    
    #print(dir(mat.node_tree.links))
    
    mat.node_tree.links.new(lowres_diffuse.outputs[0], lowres_output.inputs[0])
    mat.node_tree.links.new(lowres_texture.outputs[0], lowres_diffuse.inputs[0])
    
    
    return True


def modify_lookdev_materials(texture_filepath: str):
    ''' description TODO '''
    print("start modify_lookdev_materials")
    for mat in bpy.data.materials:
        #print()
        #print(mat.name)
        try :
            check = mat.name.startswith("mat-") and mat.name.split("-")[1] in bpy.data.filepath.split("\\")[-1]
        except :
            check = False

        if not check : continue

        the_tex = []
        for tex in texture_filepath :
            if mat.name in tex.split("\\")[-1] :
                the_tex.append(tex)

        #print('texture_filepath = ' + texture_filepath[0].split("\\")[-1])
        #print(f"{the_tex=}")

        if len(the_tex) == 0 : continue

        if len(the_tex) == 1 : 
            the_tex = the_tex[0]
        else :
            for t1, tex_1 in enumerate(the_tex.copy()) :
                if tex_1 in the_tex:
                    for t2, tex_2 in enumerate(the_tex.copy()) :
                        if t1 != t2 :
                            comp1 = tex_1.split("\\")[-1].split("_low")[0]
                            comp2 = tex_2.split("\\")[-1].split("_low")[0]
                            if comp1 in comp2 and tex_2 in the_tex:
                                the_tex.remove(tex_2)


            the_tex = the_tex[0]

        if not the_tex : continue

        if the_tex.split(".")[-2].isdigit():
            the_tex = the_tex.split(".")
            the_tex[-2] = "<UDIM>"
            the_tex = ".".join(the_tex)
        modify_lookdev_material(material_name = mat.name, texture_filepath=the_tex)

    return True



modify_lookdev_materials(texture_filepath = "<TEX_PATH>")
bpy.ops.wm.save_mainfile()