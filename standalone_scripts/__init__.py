''' standalone script manager '''

# initialize logger

import flux_logger

#from .. import flux_logger
log = flux_logger.get_flux_logger()

if __name__ == "__main__" :
    error = "all relative import will fail with a direct run of this script.\nRun instead this script : flux\\standalone_script_exe.py"
    log.fatal(error)
    raise Exception(error)



import tkinter as tk

from . import ui

#reload lib
import importlib
importlib.reload(flux_logger)
# importlib.reload(ui)


class script_element():
    def __init__(self, parent_manager, name: str, module_name: str = None, have_ui: bool = False, args: str = None) :
        self.parent_manager = parent_manager
        self.name = name
        self.module = module_name
        self.have_ui = have_ui
        if not module_name :
            self.module = name
        exec(f"from . import {self.module}")
        exec(f"self.module = {self.module}")

        self.root_frame = None

    def return_to_menu(self):
        if not self.root_frame :
            return
        self.parent_manager.script_list_container.pack(fill="both")
        self.root_frame.destroy()
        self.parent_manager.element_ui_container.pack_forget()

    def run(self):
        print(f"run {self.name}")
        if "draw" not in dir(self.module) :         
            self.module.execute()  
            return    
        
        self.parent_manager.script_list_container.pack_forget()
        tk_parent = self.parent_manager.element_ui_container
        tk_parent.pack(padx=1, pady=1, fill='both')
        sub_frame = tk.Frame(tk_parent)
        sub_frame.pack(padx=6, pady=6, fill='both')
        self.root_frame = sub_frame

        top_frame = tk.Frame(sub_frame)
        top_frame.pack(fill='both')
        BTN_return = tk.Button(top_frame, height=1, text="\u21A9 return", relief='groove', borderwidth=2, command=self.return_to_menu)
        BTN_return.pack(side='left')
        TXT_selection = tk.Label(top_frame, text=self.name, bg='gray', fg='white')
        TXT_selection.pack(side='left', fill='both', ipadx=12)

        border_frame = tk.Frame(sub_frame, relief='groove', border=2)
        border_frame.pack(fill='both')
        subsub_frame = tk.Frame(border_frame)
        subsub_frame.pack(padx=6, pady=6, fill='both')
        subsub_frame.configure()
        self.module.draw(subsub_frame)


    def draw(self, tk_parent, row = 0, col = 0):
        self.parent_manager.draw_top_menu()
        BTN_runBatchScript = tk.Button(tk_parent, width=30, height=2, text=f"Run {self.name}", borderwidth=1, command=self.run)
        BTN_runBatchScript.grid(padx=2, pady=1, row=row, column=col, sticky='WE', )



class standalone_scripts_manager() :
    def __init__(self, name = "standalone scripts manager") :
        self.name = name
        self.script_elements = {}
        self.wn_size = [350, 80]
        self.wn_root = None
        self.script_list_container = None
        self.element_ui_container = None
        self.tk_child_element = None


    def add_script_elements(self, module_name: str, name: str = None, have_ui: bool = False) :
        if not name :
            name = module_name
        if name in self.script_elements :
            name += "_1"
        index = 1
        while name in self.script_elements :
            name = name[:-len(str(index))]
            index += 1
            name += str(index)

        new_element = script_element(self, name, module_name, have_ui)

        self.script_elements.update({name : new_element})


    def register(self, module_name: str, display_name: str = None, have_ui: bool = False) :
        if not module_name :
            log.error("module_name is an empty thing")
            return False
        if type(module_name) is not str :
            log.error("\"module_name\" should be a string or a list of strings")
            return False
        
        self.add_script_elements(module_name, display_name, have_ui)


    def draw_registered_scripts_list(self) :
        tk_parent = self.script_list_container
        tk_parent.pack(fill="both", expand=True)
        LAB_ui = tk.LabelFrame(tk_parent, text="Modules (with UI)")
        LAB_ui.pack(padx=6, pady=6, fill="both")
        FRA_ui = tk.Frame(LAB_ui)
        FRA_ui.pack(padx=1, pady=1, fill="both", expand=True)

        LAB_scripts = tk.LabelFrame(tk_parent, text="Scripts (no UI)")
        LAB_scripts.pack(padx=6, pady=6, fill="both")
        FRA_scripts = tk.Frame(LAB_scripts)
        FRA_scripts.pack(padx=1, pady=1, fill="both", expand=True)

        nbr_el_inline = 3

        count_ui = 0
        count_script = 0
        for i, el in enumerate(self.script_elements.values()) :
            el: script_element
            row = int(i / nbr_el_inline)
            col = i % nbr_el_inline
            
            if el.have_ui : 
                el_parent = FRA_ui
                count_ui += 1
            else :
                el_parent = FRA_scripts
                count_script += 1

            el.draw(el_parent, row=row, col=col)

        if not count_ui : LAB_ui.destroy()
        if not count_script : LAB_scripts.destroy()


    def draw_top_menu(self) :
        tk_parent = self.wn_root
        bg_color = 'red'
        menubar  = tk.Menu(tk_parent, bg=bg_color)
        user_prefs = tk.Menu(menubar, tearoff=0) 
        debug_level = tk.Menu(menubar, tearoff=0) 
        debug_level.add_command(label="debug", command=lambda: flux_logger.set_level_all_loggers("DEBUG"))   
        debug_level.add_command(label="info", command=lambda: flux_logger.set_level_all_loggers("INFO"))   
        #debug_level.add_command(label="error", command=lambda: flux_logger.set_level_all_loggers("ERROR"))   
        user_prefs.add_cascade(label="debug level", menu=debug_level) 

        menubar.add_cascade(label="Preferences", menu=user_prefs)
        menubar.config(bg="GREEN", fg="WHITE")

        tk_parent.config(menu=menubar)   
        

    def draw(self) :
        rootWn = tk.Tk()
        rootWn.title(self.name)
        rootWn.minsize(self.wn_size[0], self.wn_size[1]) 
        #rootWn.tk_setPalette(background='gray90')   
        self.wn_root = rootWn  

        self.script_list_container = tk.Frame(rootWn)
        self.element_ui_container = tk.Frame(rootWn)

        self.draw_top_menu()
        self.draw_registered_scripts_list()


        rootWn.mainloop()