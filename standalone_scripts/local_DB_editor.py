import os
import tkinter as tk

from standalone_scripts import ui
import dependencies as dep
if not dep.is_dep_in_path :
    dep.add_dep_in_path()

import local_api
try : from ..dependencies import local_api #for dev purpose
except: pass


class custom_tk_screen :
    def __init__(self, tk_parent, cache: ui.tk_cache, lcl: local_api.Local_API) -> None :
        self.tk_parent = tk_parent
        self.cache = cache
        self.lcl = lcl

        self.container = tk.Frame(tk_parent)
        self.container.pack(fill="both", expand=True)


class db_info_screen(custom_tk_screen) :
    def __init__(self, tk_parent, cache: ui.tk_cache, lcl: local_api.Local_API) -> None :
        super().__init__(tk_parent, cache, lcl)
        tk.Label(self.tk_parent, text=self.cache.get()).pack()


class db_project_screen(custom_tk_screen) :
    def __init__(self, tk_parent, cache: ui.tk_cache, lcl: local_api.Local_API) -> None :
        super().__init__(tk_parent, cache, lcl)


class db_path_screen(custom_tk_screen) :
    def __init__(self, tk_parent, cache: ui.tk_cache, lcl: local_api.Local_API) -> None :
        super().__init__(tk_parent, cache, lcl)
        path = os.path.dirname(__file__)
        path = str(os.path.abspath(path)).split("\\")[:-1]
        path = "\\".join(path) + r"\database_access\local_bridge\test_local_db.json"
        self.db_path_var = tk.StringVar(value=path)
        self.db_path_report = tk.StringVar(value="")

        self.choose = self.draw_db_file_choose()
        self.show = None


    def validate_db_filepath(self, create_btn: tk.Button) :
        path = self.db_path_var.get()
        create_btn.grid_forget()
        if not path.endswith(".json") :
            self.db_path_report.set("the given db file is not a .json file")
            return
        if not os.path.exists(path) :
            self.db_path_report.set("the path does not exist") #TODO handle file creation
            create_btn.grid(row=0, column=1, padx=4, ipadx=4)
            return
        self.db_path_report.set("path ok")
        self.choose.pack_forget()
        self.show = self.draw_db_file_show()  

        if self.lcl and self.lcl.db_file_path != path :
            del self.lcl
            self.lcl = local_api.Local_API(path)

        test = db_info_screen(self.tk_parent, self.cache, self.lcl)
        return


    def create_new_db(self, create_btn: tk.Button):
        path = self.db_path_var.get()
        f = open(path, 'x')
        f.write(local_api.utils.get_default_db(True))
        self.validate_db_filepath(create_btn)
        return
    
    def edit_db_filepath(self) :
        self.show.destroy()
        self.choose.pack(fill="both", expand=True)
        return


    def draw_db_file_choose(self) :      
        container_choose = tk.Frame(self.container)
        container_choose.pack(fill="both", expand=True)

        db_filepath = self.db_path_var
        db_path_report = self.db_path_report

        lab = tk.Label(container_choose, text="Choose your DB json file location")
        lab.pack()
        ui.browse_path_entry(container_choose, "DB json filepath  ", db_filepath, dir_not_file=False)
        #ui.labeled_entry(self.container, "DB json filename", db_filename)
        report = tk.Label(container_choose, textvariable=db_path_report)
        report.pack(pady=6)
        buttons_cont = tk.Frame(container_choose)
        buttons_cont.pack()
        create = tk.Button(buttons_cont, text="create new", command=lambda:self.create_new_db(create))
        
        validate = tk.Button(buttons_cont, text="validate", command=lambda:self.validate_db_filepath(create))
        validate.grid(row=0, column=0, padx=4, ipadx=4)
        return container_choose
    

    def draw_db_file_show(self) :
        container_show = tk.Frame(self.container)
        container_show.pack(fill="both", expand=True)
        path = self.db_path_var.get()
        maxlen = 100
        if len(path) > maxlen :
            path = "..." + path[maxlen:]
        lab = tk.Label(container_show, text=f"DB filepath : ", font=('Segoe UI', 9, 'bold'))
        lab.pack(side='left')
        lab_path = tk.Label(container_show, text=f"{path}")
        lab_path.pack(side='left')

        btn_cont = tk.Frame(container_show, width=16)
        btn_cont.pack(fill='both', side='right', expand=True)
        btn = tk.Button(btn_cont, text="edit", command=self.edit_db_filepath)
        btn.pack()
        return container_show


def draw(tk_parent: tk.Widget = None) :
    need_mainloop = False

    if not tk_parent :
        tk_parent = tk.Tk()
        tk_parent.title('Blender Batch Script')
        tk_parent.minsize(200, 200)
        need_mainloop = True

    cache = ui.tk_cache()
    lcl = None

    step_0 = db_path_screen(tk_parent, cache, lcl)

    if need_mainloop :
        tk_parent.mainloop()


def execute() :
    draw()
    