''' dont import this file, it's just a dev unit, will be deleted at the end '''

import os
import shutil

def checker_v_folder_one_item(item, item_path):
    ''' 
    check if there is a v01 folder in an asset item task 
    (only look into 10_TEXTURE and 11_LOOK_DEV for now) 
    '''

    item_task = ["10_TEXTURE", "11_LOOK_DEV"]

    return_dict = {}

    for task in item_task :
        item_task_path = item_path + "\\" + task

        task_state = False

        if os.path.exists(item_task_path) :
            task_state = True
            return_dict.update({task: True})
            if "v01" not in os.listdir(item_task_path) : 
                print(f"{task} in {item}\n-> no v01 folder ({os.listdir(item_task_path)})\n")
                task_state = False
            


        return_dict.update({task: task_state})

    return return_dict

def get_item_textures(item_path, only_base_color = True) :
    ''' description TODO '''
    texture_folder_path = item_path + "\\10_TEXTURE\\v01"
    
    textures = []
    for texture in os.listdir(texture_folder_path):
        if texture.endswith(".png") :
            textures.append(texture)

    if only_base_color :
        for texture in textures.copy() :
            if "basecolor" not in texture.lower() or "basecolor_low" in texture.lower():
                textures.remove(texture)

    for i, texture in enumerate(textures) :
        textures[i] = texture_folder_path + "\\" + texture

    return textures

def get_item_lookdev(item_path) :
    ''' description TODO '''
    lookdev_folder_path = item_path + "\\11_LOOK_DEV\\v01"

    lookdev_blend = []
    for file in os.listdir(lookdev_folder_path):
        if file.endswith(".blend") and "lookdev" in file.lower() :
            lookdev_blend.append(file)

    if len(lookdev_blend) == 0 :
        return False
    if len(lookdev_blend) == 1 :
        return lookdev_folder_path + "\\" + lookdev_blend[0]
    
    print(f"--- multiple lookdev ! ---   ->{lookdev_blend}")
    return lookdev_folder_path + "\\" + lookdev_blend[0]


def resize_texture(texture_path, new_texture_path):
    ''' description TODO '''
    try :
        from PIL import Image
    except :
        import pip
        pip.main(['install', 'pillow'])
        from PIL import Image

    if not os.path.exists(texture_path) :
        print("texture_path does not exist")
        return False
    
    with Image.open(texture_path) as im:
        (width, height) = (im.width // 2, im.height // 2)
        while width > 512 or height > 512 :
            (width, height) = (width // 2, height // 2)

        im_resized = im.resize(size = (width, height))
        im_resized.save(new_texture_path)

    return True

def modify_texture(texture_path) :
    ''' description TODO '''
    new_texture_path = texture_path.split(".")
    new_texture_path[0] += "_low"
    new_texture_path = ".".join(new_texture_path)

    if os.path.exists(new_texture_path) :
        #print(f"need to delete {new_texture_path}")
        os.remove(new_texture_path)

    resize_texture(texture_path, new_texture_path)

    return new_texture_path


def modify_textures(textures_list: list[str]):
    ''' description TODO '''
    #print(textures_list)

    new_textures_path = []
    for texture_path in textures_list :
        new_texture_path = modify_texture(texture_path)        
        new_textures_path.append(new_texture_path)

    return new_textures_path


def modify_lookdev_blend(blendfile: str, textures_path: list[str]):
    ''' description TODO '''
    from . import blender_subprocess
    if not blendfile :
        print("blendfile is none")
        return False
    if not os.path.exists(blendfile) :
        print("blendfile path does not exist")
        return False
    
    print(blendfile)

    f = open(r"R:\01_R&D\GUILLAUME\flux\blender_libs\textures_lookdev_modify_inBlenderScript.py", "r")
    script = f.read() 

    script = script.replace("\"<TEX_PATH>\"", str(textures_path))

    blender_exe = r"C:\Program Files\Blender Foundation\Blender 3.6\blender.exe"

    blender_subprocess.openBlender_exeScript_background(blender_path=blender_exe, file_path=blendfile, script=script, debug=False)

    return True


def moulinette_convert_all_item_texture_lookdev():
    ''' 
    check if there is a v01 folder in all assets item task 
    (only look into 10_TEXTURE and 11_LOOK_DEV for now) 
    '''
    base_path = r"W:\01_PRODUCTIONS\012_RUFUS\1_PRE" # TODO hard coded

    for type_fold in os.listdir(base_path) :
        type_path = base_path + "\\" + type_fold
        if type_fold == "Library" or "BG" in type_fold or "PR" in type_fold or "CH" in type_fold :
            continue

        
        for item in os.listdir(type_path):
            if not int(item.split("_")[2]) > 241 : continue

            item_path = type_path + "\\" + item
            item_task_state = checker_v_folder_one_item(item, item_path)
                
            if not item_task_state["10_TEXTURE"] : continue
            
            print(f"\n-|o|{item} :")
            print("-- process textures") 
            new_textures_path = modify_textures(get_item_textures(item_path))

            if item_task_state["11_LOOK_DEV"] :
                print("-- process lookdev")
                modify_lookdev_blend(get_item_lookdev(item_path), new_textures_path)



                





if __name__ == '__main__' :

    os.system("cls")
    print("___start___")

    #modify_texture(r"W:\01_PRODUCTIONS\012_RUFUS\1_PRE\3_PR\RF_PR_0136_binoculars\10_TEXTURE\v01\mat-0136-binoculars_BaseColor.1001.png")

    moulinette_convert_all_item_texture_lookdev()

    print("\n___ END ___")



