import os
import json
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from typing import Union

from standalone_scripts import blender_subprocess
from standalone_scripts import ui

import config
import flux_logger

# initialize logger
log = flux_logger.get_flux_logger("flux standalone")
log.setLevel("INFO")


def get_exclude_state(to_test: str, exclude: list) -> bool :
    ''' 
    Test if the given string need to be excluded, based on a list of string to exclude 

    Args:
        - to_test : the string to test
        - exclude : the list with strings to exclude

    return :
        True if the string to test is not to exclude
    '''
    if not exclude or not to_test :
        log.info(f"return true : exclude is \"{exclude}\" and to_test is \"{to_test}\"")
        return True
    
    if to_test in exclude :
        return False
    for filter in exclude :
        if filter in to_test :
            return False
        
    return True


def get_version_in_item_name(item_name: str) -> list[int]:
    ''' get the versionning of an item based of his name '''
    v, r, w = None, None, None

    item_name = item_name.split("_")

    for part in item_name :
        if part[0] == "v" and part[1:3].isdigit() :
            v = int(part[1:3])
        if part[0] == "r" and part[1:4].isdigit() :
            r = int(part[1:4])
        if part[0] == "w" and part[1:5].isdigit() :
            w = int(part[1:5])

    if not v :
        return False
    if not r :
        return [v]
    if not w :
        return [v, r]

    return [v, r, w]


def get_version_dir_infos(dir_path: str, dir_version_letter: str = None) -> dict[str, list[str]] :
    ''' 
    get infos of a given directory 

    Args:
        - dir_path
        - dir_version_letter

    Return:
        a dictionnary with the folder's blendfile path and a list of child folders path
    '''
    if not os.listdir(dir_path) : return
    ext = ".blend"
    
    version_letters = config.path.get_version_letters()

    if not dir_version_letter or dir_version_letter not in version_letters :
        dir_version_letter = dir_path.split("\\")[-1].split("/")[-1][0]

    if dir_version_letter not in version_letters : 
        return

    dir_version_i = version_letters.index(dir_version_letter)
    if dir_version_i == len(version_letters) - 1 : 
        error = f"there is no sub version to this folder (folder version letter : \"{dir_version_letter}\")"
        print(error)
        next_letter = None
    else :
        next_letter = version_letters[dir_version_i + 1]

    blendfile = []
    childs = []    

    for item in os.listdir(dir_path) :
        item_path = os.path.join(dir_path, item)
        if os.path.isfile(item_path) :
            if not item.endswith(ext) : continue
            item_version = get_version_in_item_name(item.replace(ext, ""))
            if item_version and len(item_version) == (dir_version_i + 1) : 
                blendfile.append(item_path)

        if next_letter and item[0] == next_letter and item[1:].isdigit() :
            childs.append(item_path)


    if not blendfile and not childs : return
    if not blendfile : blendfile = None
    else : blendfile = blendfile[0] #TODO handle multiple case
    if not childs : childs = None

    return {"blendfile": blendfile, "childs": childs}


def get_version_subdirs(root_path: str, exclude: list = []) -> list :
    ''' description TODO '''

    to_return = []
    version_letters = config.path.get_version_letters()

    root_path_version = root_path.split("\\")[-1].split("/")[-1][0]
    root_path_version_i = version_letters.index(root_path_version)

    if root_path_version_i == len(version_letters) - 1 : 
        error = f"there is no sub version to this folder (folder version letter : \"{root_path_version}\")"
        log.error(error)
        return []
    
    next_letter = version_letters[root_path_version_i + 1]

    for dir_name in os.listdir(root_path) :
        dir_path = os.path.join(root_path, dir_name)
        
        if not (os.path.isdir(dir_path) and dir_name[0] == next_letter and 
            dir_name[1:].isdigit() and len(dir_name[1:]) == root_path_version_i + 3) :
            continue

        to_return.append(dir_path)

    log.debug(f"root_path : {root_path}, return {to_return}\n(root_path_version : {root_path_version}, next_letter : {next_letter}, exclude : {exclude})")

    return to_return


def get_all_file_in_dir(path: str, ext: str = ".blend", exclude: list = []) -> list :
    ''' description TODO '''
    if not os.listdir(path) : return []

    to_return = []

    for item in os.listdir(path) :
        item_path = os.path.join(path, item)
        if item.endswith(ext) and os.path.isfile(item_path) and get_exclude_state(item, exclude) :
            to_return.append(item_path)

    return to_return


def get_all_versions(root_path: str, BOO_v: bool = True, 
                     BOO_r: bool = True, BOO_w: bool = True, 
                     BOO_last: bool = False, exclude: list[str] = [], ext: str = ".blend") -> list[str] :
    ''' 
    description TODO 

    Args:
        - root_path : path to the v01 folder
        - ext : the wanted file extension
        - BOO_v, BOO_r, BOO_w : boolean, wanted version output
        - BOO_last : boolean, if True, give only the latest versions
        - exclude : the exclude list
        - ext : the file extension
    '''
    if not root_path.endswith("v01") :
        error = "root_path must end with \"v01\" !"
        raise Exception(error)
    
    if ext[0] != "." : 
        ext = "." + ext

    if not os.listdir(root_path) :
        return [None, None, None]

    version_dirs = [[root_path], [], []]
    
    for path in version_dirs[0] :
        version_dirs[1].extend(get_version_subdirs(path, exclude))

    for path in version_dirs[1] :
        version_dirs[2].extend(get_version_subdirs(path, exclude))

    to_return = [[], [], []]

    for i, version in enumerate(version_dirs) :
        for path in version :
            to_return[i].extend(get_all_file_in_dir(path, exclude=exclude))

    # just a security
    if not BOO_v : to_return[0] = None
    if not BOO_r : to_return[1] = None
    if not BOO_w : to_return[2] = None

    if BOO_last and to_return[0] : to_return[0] = [to_return[0][-1]]
    if BOO_last and to_return[1] : to_return[1] = [to_return[1][-1]]
    if BOO_last and to_return[2] : to_return[2] = [to_return[2][-1]]


    multi_to_return = to_return.copy()
    to_return = []

    for item in multi_to_return :
        if not item : continue
        to_return.extend(item)

    log.debug(f"to_return : {to_return}")
    return to_return


def get_selection(basepath: str, exclude: list = [], BOO_v: bool = True, BOO_r: bool = True, BOO_w: bool = True, BOO_last: bool = False):
    ''' description TODO '''
    log.debug(f"run get_selection (basepath : {basepath}, exclude : {exclude}, BOO_version : ({BOO_v}, {BOO_r}, {BOO_w}), BOO_last : {BOO_last})")
    to_return = []

    if not os.path.exists(basepath) :
        log.error("basepath does not exist")
        return []

    def recursive_get_selection(path, to_return: list, exclude):
        #print("in a recursive")
        if not os.path.exists(path) :
            error = f"path does not exist\n->({path})"
            log.error(error)
            raise Exception(error)

        for item in os.listdir(path) :
            if not get_exclude_state(item, exclude) : continue
            item_path = os.path.join(path, item)
            if item.endswith(".blend") :
                to_return.append(item_path)
            if not os.path.isdir(item_path) : continue
            if item == "v01" :
                # it's a version folder !
                to_return.extend(get_all_versions(root_path=item_path, BOO_v=BOO_v, BOO_r=BOO_r, BOO_w=BOO_w, BOO_last=BOO_last, exclude=exclude))
                continue

            recursive_get_selection(item_path, to_return, exclude)
        #print("out recursive")

    recursive_get_selection(basepath, to_return, exclude)

    # remove None and format path
    if True :
        bad_return = to_return.copy()
        to_return = []
        for item in bad_return :
            if not item : continue
            to_return.append(ui.format_paths(item))

    log.debug(to_return)
    return to_return


def get_exclude_list(entry_field: str) :
    ''' description TODO '''
    to_return = []

    entry_field = entry_field.split(";")

    for item in entry_field :
        if item.startswith("-") :
            to_return.append(item[1:])

    return to_return


def update_selection(cache: ui.tk_cache, basepath: str, boo_version: list[bool], boo_last: bool, exclude: str, force_update: bool = False) :
    inputs = {"basepath": basepath, "boo_version": boo_version, "boo_last": boo_last, "exclude": exclude}
    if cache.get("inputs") == inputs and not force_update :
        log.debug("cache already uptodate !")
        return cache.get("selection")
    
    selection = get_selection(basepath=basepath, exclude=get_exclude_list(exclude), 
                              BOO_v=boo_version[0], BOO_r=boo_version[1], 
                              BOO_w=boo_version[2], BOO_last=boo_last)
    
    cache.update({"selection": selection})
    cache.update({"inputs" : inputs})
    return selection


def draw(tk_parent = None) :
    need_mainloop = False

    if not tk_parent :
        tk_parent = tk.Tk()
        tk_parent.title('Blender Batch Script')
        tk_parent.minsize(200, 200)
        need_mainloop = True

    BOO_versionV = tk.BooleanVar()
    BOO_versionV.set(True)
    BOO_versionR = tk.BooleanVar()
    BOO_versionR.set(False)
    BOO_versionW = tk.BooleanVar()
    BOO_versionW.set(False)

    BOO_onlyLast = tk.BooleanVar()
    BOO_onlyLast.set(True)

    cache = ui.tk_cache()

    STR_includeByName = tk.StringVar()
    STR_includeByName.set("-turntable")
    STR_root_path = tk.StringVar()
    STR_root_path.set(f"The path to your selection root folder")
    #STR_root_path.set(f"W:\\01_PRODUCTIONS\\012_RUFUS\\1_PRE\\1_CH\\RF_CH_0003_rufus")
    STR_show_selection = tk.StringVar()

    #settings
    STR_blender_exe = tk.StringVar()
    STR_blender_exe.set("C:\\Program Files\\Blender Foundation\\Blender 3.6\\blender.exe")

    STR_script_path = tk.StringVar()
    script='The path to your script'
    STR_script_path.set(script)
    

    STR_info = tk.StringVar()
    STR_info.set("INFO : waiting for launch")
        

    def batchBoucle(): 
        #print("run batchBoucle")
        blender_path = STR_blender_exe.get()
        if not os.path.exists(blender_path) :
            log.error("blender path does not exist !")
            return
        
        script = STR_script_path.get().replace("\\", "/")
        if not os.path.exists(script) :
            log.error("Script path does not exist !")
            return
            
        script = f"import bpy; bpy.ops.script.python_file_run(filepath=\"{script}\")"
        #script = "print(\" - Hello Flux ! - \")"
        selection = update_selection(cache=cache, basepath=STR_root_path.get(), 
                                     boo_version=[BOO_versionV.get(), BOO_versionR.get(), BOO_versionW.get()], 
                                     boo_last=BOO_onlyLast.get(), exclude=STR_includeByName.get())
        
        log.info("start batch boucle")
        for file_path in selection :
            blender_subprocess.openBlender_exeScript_background(blender_path, file_path, script, debug=True)

        log.info("end of batch boucle")


    base_browse_path = r"W:\01_PRODUCTIONS\012_RUFUS" # TODO hard coded


    FRA_Main = tk.Frame(tk_parent, width=200, bg='gray94')
    FRA_Main.pack(padx=2, pady=0, side='left', fill='both')
    FRA_SideR = tk.Frame(tk_parent, width=100, height=100, background='white', relief='sunken', border=1)
    FRA_SideR.pack(padx=2, pady=8, side='right', fill='y')


    LAB_selection = tk.LabelFrame(FRA_Main, text="Selection")
    LAB_selection.pack(padx=0, pady=0, fill="both")
    FRA_path_info = tk.Frame(LAB_selection)
    FRA_path_info.pack(padx=4, pady=2, fill="both")
    TXT_path_info = tk.Label(FRA_path_info, text="selection path :")
    TXT_path_info.pack(padx=0, pady=2, side='left', fill='y')
    ENT_select_path = tk.Entry(FRA_path_info, textvariable=STR_root_path, width=120)
    ENT_select_path.pack(padx=2, pady=2, side='left', fill='x')
    BTN_browse = tk.Button(FRA_path_info, text='browse path', command=lambda: ui.browse(STR_root_path, base_browse_path, True))
    BTN_browse.pack(padx=2, pady=2, side='right', fill='y')
    FRA_selection = tk.Frame(LAB_selection)
    FRA_selection.pack(fill="both")


    CHK_versionV = tk.Checkbutton(FRA_selection, text="versions V", variable=BOO_versionV)
    CHK_versionV.grid(padx=2, pady=2, row=0, column=0, sticky="w")
    CHK_versionR = tk.Checkbutton(FRA_selection, text="versions R", variable=BOO_versionR)
    CHK_versionR.grid(padx=2, pady=2, row=0, column=1, sticky="w")
    CHK_versionW = tk.Checkbutton(FRA_selection, text="versions W", variable=BOO_versionW)
    CHK_versionW.grid(padx=2, pady=2, row=0, column=2, sticky="w")
    CHK_onlyLast = tk.Checkbutton(FRA_selection, text="only last version", variable=BOO_onlyLast)
    CHK_onlyLast.grid(padx=2, pady=2, row=0, column=3, sticky="w")


    FRA_includeByName = tk.LabelFrame(LAB_selection, text="Include (+) / exclude (-), \";\" separator ")
    FRA_includeByName.pack(padx=8, pady=2, fill="both")
    ENT_includeByName = tk.Entry(FRA_includeByName, textvariable=STR_includeByName, width=120)
    ENT_includeByName.pack(padx=2, pady=2, fill="both")


    FRA_script = tk.LabelFrame(FRA_Main, text="Script path")
    FRA_script.pack(padx=0, pady=2, fill="both")
    ENT_script = tk.Entry(FRA_script, textvariable=STR_script_path, width=120)
    ENT_script.pack(padx=2, pady=2, side='left', fill="x")
    BTN_browse = tk.Button(FRA_script, text='browse path', command=lambda: ui.browse(STR_script_path, base_browse_path))
    BTN_browse.pack(padx=2, pady=2, side='right', fill='y')

    FRA_blender = tk.LabelFrame(FRA_Main, text="Blender .exe path")
    FRA_blender.pack(padx=0, pady=2, fill="both")
    ENT_blender = tk.Entry(FRA_blender, textvariable=STR_blender_exe, width=120)
    ENT_blender.pack(padx=2, pady=2, side='left', fill="x")
    BTN_browse = tk.Button(FRA_blender, text='browse path', command=lambda: ui.browse(STR_blender_exe, r"C:\Program Files\Blender Foundation"))
    BTN_browse.pack(padx=2, pady=2, side='right', fill='y')

    #TXT_info = tk.Label(FRA_Main, textvariable=STR_info)
    #TXT_info.pack(padx=0, pady=2)

    #BTN_runBatchScript = tk.Button(tk_parent, width=30, height=2, text="Run Batch Script", borderwidth=1, command=batchBoucle)
    BTN_runBatchScript = tk.Button(FRA_Main, width=30, height=2, text="Run Batch Script", borderwidth=1, command=batchBoucle)
    BTN_runBatchScript.pack(padx=0, pady=8)


    def clear_sideTxt():
        ''' description TODO '''
        STR_show_selection.set("")

    #side R
    BTN_clear_selection = tk.Button(FRA_SideR, text="Clear show selection", borderwidth=1, command=clear_sideTxt)
    BTN_clear_selection.pack(side="top", padx=4, pady=4, fill='x')

    SCR_SideR_scroll = ui.scrollable_frame(FRA_SideR, padx=4)

    TXT_show_selection = tk.Label(SCR_SideR_scroll.frame, textvariable=STR_show_selection, bg='white', justify='left')
    TXT_show_selection.pack(padx=0, pady=0)

    def set_sideTxt():
        ''' description TODO '''
        selection = update_selection(cache=cache, basepath=STR_root_path.get(), 
                                     boo_version=[BOO_versionV.get(), BOO_versionR.get(), BOO_versionW.get()], 
                                     boo_last=BOO_onlyLast.get(), exclude=STR_includeByName.get())
        
        for i, item in enumerate(selection):
            if not item : continue
            item = ui.format_paths(item)
            selection[i] = item.split("\\")[-1]
        STR_show_selection.set("Selection :\n\n"+ "\n".join(selection))

    BTN_show_selection = tk.Button(FRA_SideR, text="Refresh show selection", borderwidth=1, command=set_sideTxt)
    BTN_show_selection.pack(padx=4, pady=4, fill='x')


    if need_mainloop :
        tk_parent.mainloop()


def execute() :
    draw()


if __name__ == "__main__" :
    os.system("cls")
    print("start executing batch_blender_modif")
    execute()