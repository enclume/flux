import gazu

# glpat-gWvBxu9kKR77A5-v9Bdh

gazu.set_host("https://nextframes-animation-studio.cg-wire.com/api")
gazu.log_in("nicolasced@gmail.com", "")
gazu.cache.enable()
# -----
project = gazu.project.get_project_by_name("Rufus") # TODO hard coded
print(project)

# find all comment for given asset

# print("------------ get comment v1")
# asset_id = gazu.asset.get_asset_by_name(project, "zTest")
# all_tasks_raw = gazu.task.all_tasks_for_asset(asset_id)
# task_id = ""
# for e in all_tasks_raw:
#     print("--")
#     print(e)
#     if e["task_type_name"] == "Modeling":
#         task_id = e
#         print("found")
#         break
# print(task_id)
# comments = gazu.task.all_comments_for_task(task_id)
# print(comments)

# method 2
# print("------------ get comment v2")
#
# task_type_id = gazu.task.get_task_type_by_name("Rigging")
# asset_id = gazu.asset.get_asset_by_name(project, "zTest")
# task_id = gazu.task.get_task_by_name(asset_id, task_type_id)
# print(task_id)
# comments = gazu.task.all_comments_for_task(task_id)
# comments2 = gazu.task.get_last_comment_for_task(task_id)
# print(comments)
# for c in comments:
#     print(c)
#
# print("---")
# print(comments2)
# for each in comments2.keys():
#     print(each, "---", comments2[each])
#
# print("------------ set comment 1")
# all_st = gazu.task.all_task_statuses()
# for e in all_st:
#     print(e)


# task_type_id = gazu.task.get_task_type_by_name("Rigging")
# status_id = gazu.task.get_task_status_by_short_name("WIP")
# asset_id = gazu.asset.get_asset_by_name(project, "zTest")
# task_id = gazu.task.get_task_by_name(asset_id, task_type_id)
# print(task_id)
# print(status_id)
# comments = gazu.task.add_comment(task_id, status_id, "Change status to work in progress / test 3")
# print(comments)

# seq = gazu.shot.all_sequences_for_project(project)
# print(seq)
# for each in seq:
#     print(each)
#
# shors = gazu.shot.all_shots_for_sequence(seq[2])

shors = gazu.task.all_task_statuses_for_project(project)

print("---")
print(shors)
for each in shors:
    print(each)

    # all_task_statuses


