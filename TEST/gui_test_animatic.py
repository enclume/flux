#import bpy
import os

def compare_path(path_1, path_2):
    path_1 = path_1.split("\\")
    path_2 = path_2.split("\\")
    
    for i in range(len(path_1)) :
        if path_1[i] != path_2[i]:
            return [i, "\\".join(path_1[:i])]
        

def get_animatic_path_from_item_path(workfile_path):
    animatic_path = []
    for item in workfile_path.split("\\") :
        if item.startswith("v") and item[1].isdigit() and item[2].isdigit() :
            break
        animatic_path.append(item)
        
    animatic_path[-1] = "00_ANIMATIC"
    
    animatic_name = "_".join(animatic_path[-2].split("_")[:2])
    animatic_name = f"RF_{animatic_name}_animatic.mp4"
    
    animatic_path.append(animatic_name)
    
    return "\\".join(animatic_path)


def get_animatic_path_in_blend_paths():
    #all_path = bpy.utils.blend_paths(absolute=True)
    all_path = "bpy.utils.blend_paths(absolute=True)"
    #current_path = bpy.data.filepath
    current_path = "bpy.data.filepath"

    animatic_path = []
        
    for path in all_path :
        if path.startswith("W:\\") and path.endswith(".mp4") :        
            if path not in animatic_path:
                animatic_path.append(path)

    if not animatic_path :
        print("animatic_path is empty")  
        return False 
    
    if len(animatic_path) == 1:
        animatic_path = animatic_path[0]
    else :
        print("multiple animatic_path found")
        
    return animatic_path
        

if __name__ == "__main__" :
    os.system("cls")
    if False :
        new_filepath = "W:\\01_PRODUCTIONS\\012_RUFUS\\2_PROD\\02_ANIM\\sq0580_anim\\sq0580_sh0040_anim\\01_LAYOUT\\v01\\r001\\w0001\\sq0580_sh0040_layout_v01_r001_w0001.blend"
            
        current_animatic_path = get_animatic_path_in_blend_paths()
        new_animatic_path = get_animatic_path_from_item_path(new_filepath)
            
        print(current_animatic_path)
        print()
        print(new_animatic_path)
        print()

    


    #blender_subprocess.openBlender_exeScript(file_path="", script="")

    all_sq = ["0010", "0020", "0030", "0050", "0060", "0090", "0130", "0170", "0190", "0225", "0240", "0280", "0285", "0300", "0325", "0335", 
            "0340", "0380", "0410", "0420", "0440", "0460", "0470", "0480", "0510", "0540", "0560", "0580", "0590", "0600", "0610", "0620", 
            "0640", "0650", "0655", "0660", "0670", "0680", "0690", "0700", "0730", "0750", "0755", "0760", "0770", "0780", "0790", "0800", 
            "0850", "0860", "0870", "0880", "0910", "0920", "0930", "0990", "1080", "1100", "1110", "1170", "1180", "1190"]
    print(all_sq)