import bpy
import os

def preview(filepath, publish = False):
    scene = bpy.context.scene

    #Change Settings
    scene.render.engine = "BLENDER_EEVEE"
    scene.render.image_settings.file_format = "FFMPEG"
    scene.render.image_settings.color_mode = "RGB"
    scene.render.ffmpeg.format = "QUICKTIME"
    scene.render.ffmpeg.codec = "H264"
    scene.render.ffmpeg.constant_rate_factor = "HIGH"
    scene.render.ffmpeg.ffmpeg_preset = "GOOD"
    scene.render.ffmpeg.audio_codec = "MP3"

    scene.render.film_transparent = False

    scene.render.filepath = filepath

    #Metadata
    scene.render.use_stamp_date = False
    scene.render.use_stamp_time = False
    scene.render.use_stamp_render_time = False
    scene.render.use_stamp_frame = True
    scene.render.use_stamp_frame_range = False
    scene.render.use_stamp_memory = False
    scene.render.use_stamp_hostname = False
    scene.render.use_stamp_camera = False
    scene.render.use_stamp_lens = False
    scene.render.use_stamp_scene = False
    scene.render.use_stamp_marker = False
    scene.render.use_stamp_filename = False
    scene.render.use_stamp_note = True
    scene.render.use_stamp = True

    #Render
    if publish:
        # Check range
        sound_count = 0
        for sequence in scene.sequence_editor.sequences:            
            if sequence.type == 'SOUND':
                sound_count += 1
        if sound_count == 1:
            scene.frame_start = 1
            scene.frame_end = sequence.frame_final_duration

        for screen in bpy.data.screens:
            for area in screen.areas:
                    if area.type == 'VIEW_3D':
                        for space in area.spaces:
                            if space.type == 'VIEW_3D':
                                space.overlay.show_overlays = False    
                                space.shading.type = 'MATERIAL'                                
                    if area.type == 'DOPESHEET_EDITOR':                        
                        for region in area.regions:
                            if region.type == 'WINDOW':
                                override = bpy.context.copy()                               
                                override = {'region': region, 'area': area}
                                bpy.ops.anim.previewrange_clear(override)

        bpy.context.space_data.region_3d.view_perspective = 'CAMERA'

        filename =  bpy.path.basename(filepath)
        filename = filename.rsplit(".", 1)[0]        
        scene.render.stamp_note_text = filename
        
        bpy.ops.render.opengl('INVOKE_DEFAULT', animation = True)

        filepath = filepath.split('//')
        dir_path = os.path.dirname(bpy.context.blend_data.filepath)
        path = os.path.join(dir_path, filepath[1])
        path = os.path.normpath(path)

    else:
        filename =  bpy.path.basename(bpy.context.blend_data.filepath)
        filename = filename.rsplit(".", 1)[0]        
        scene.render.stamp_note_text = filename

        bpy.ops.render.opengl('INVOKE_DEFAULT', animation = True)
            



        path = filepath
    
    #Open Folder
    path = os.path.dirname(path)
    os.startfile(path)