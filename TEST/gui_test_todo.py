'''
test perf texture

- OK kill zookeeper in turntable mstr
- OK ? import en fct des regles (-> item type->task) 
- library stuff
- coll en rouge

- name of the camera in the image burn

- light -> look trouhgt light
- alembic thing

- import master light set (1 master/sequence)

- les bouton dans l'asset manager sont chouette


- auto install color.io


alex things :
11/07/2023
- dès qu'un artiste fait une révision depuis flux, on a ce message d'erreur qui apparait
- ce serait également idéal pour le setdressing d'avoir un tool qui permetterait de relocaliser un chemin sans devoir passer via le blender file ( à voir si c'est possible). car on va surement devoir relocaliser certain asset présent dans les sets et c'est une horreur de trouver le bon asset dans cette liste . il n'y a pas l'air d'avoir la possibilité de relocaliser un fichier depuis l'outliner
- possibilité de push une image depuis la task uv's pour ne pas devoir aller la placer manuellement sur kitsu
- avoir une sorte d'alerte lorsqu'une nouvelle version du tool est dispo

____________________________
- avoir la possibilité de skip l'update de kitsu (-> ctrl+s basique ?)



- add setup lookdev : make override (selected & content) 1 sur la collection, 2 sur le material
'''
import os
os.system("cls")

days_fac = 20
Br_day = 245
Br = Br_day * days_fac

Br_108 = 1.08
taux_onss = 0.1307
taux_pp = 0.30

Br_inter = Br * Br_108
onss = Br * Br_108 * taux_onss
imp = Br - (Br * Br_108 * taux_onss)
pp = (Br - (Br * Br_108 * taux_onss)) * taux_pp

net = ((Br * (1 - (1 * Br_108 * taux_onss))) * (1 - taux_pp)) - (1.705 * days_fac)

print(f"net : {net}\n-> {(100/Br)*net:.02f}% du brut")