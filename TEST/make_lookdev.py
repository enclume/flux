import bpy, os

# already in other file
def transfer_scene_flux_ui_data(scene_from, scene_to): 
    A_flux_ui_data = scene_from.flux_ui
    B_flux_ui_data = scene_to.flux_ui
    
    for prop in A_flux_ui_data.__annotations__.keys():
        #print(prop, " : ", getattr(A_flux_ui_data, prop), " -> ", getattr(B_flux_ui_data, prop))
        
        setattr(B_flux_ui_data, prop, getattr(A_flux_ui_data, prop))
        
    return True

# already in other file
def get_scene_flux_ui_data(scene_from = ""):
    if not scene_from :
        scene_from = bpy.context.scene
    flux_ui_data = scene_from.flux_ui
    final_dict = {}
    
    for prop in flux_ui_data.__annotations__.keys():
        final_dict.update({prop: getattr(flux_ui_data, prop)})
        
    return final_dict

# already in other file
def set_scene_flux_ui_data(prop_dict, scene_to = ""):
    if not scene_to :
        scene_to = bpy.context.scene
        
    flux_ui_data = scene_to.flux_ui
    for key in prop_dict :
        value = prop_dict[key]
        setattr(flux_ui_data, key, value)
        
# already in other file
def get_collection(col_name: str, parent_col = None, scene = None, create_col: bool = True) :
    '''
    get the collection by his name (create it if don't exist)

    Args:
        col_name :
        parent_col :
        scene :
        create_col :

    Return:
        col
    '''
    if scene == None :
        scene = bpy.context.scene

    col = bpy.data.collections.get(col_name)
    
    if col : 
        return col
    if not create_col :
        return False
    
    col = bpy.data.collections.new(col_name)
    
    if not parent_col :
        scene.collection.children.link(col)
        return col
    
    # TODO if parent_col :
    return col

# already in other file
def set_collection_active(col_name: str) -> bool :
    '''
    set active the collection by his name

    Args:
        col_name :

    Return:
        True if the collection is succesfully set active
    '''
    if not bpy.data.collections.get(col_name) :
        #raise Exception("ERROR in set_collection_active : can't find col_name \"col_name\" in bpy.data.collections")
        layer_collection = bpy.context.view_layer.layer_collection
    else : 
        layer_collection = bpy.context.view_layer.layer_collection.children[col_name]
    bpy.context.view_layer.active_layer_collection = layer_collection

    return True

# already in other file
def link_collection(link_path, link_col_name, col_parent_name = ""):
    if col_parent_name == None :
        col_parent_name = bpy.context.scene.collection.name
        
    set_collection_active(col_name = col_parent_name)
    
    bpy.ops.wm.link(
            filepath = link_path.split("\\")[-1],
            directory = link_path + "\\Collection\\",
            filename = link_col_name
            )   
            
    return bpy.data.objects[link_col_name]


def append_all_scenes(filepath):
    with bpy.data.libraries.load(filepath, link=False) as (data_from, data_to):
        data_to.scenes = data_from.scenes
        
    return [bpy.data.scenes[scene.name] for scene in data_to.scenes]
    

def setup_lookdev_file(lookdev_master_filepath, relative_mod_filepath = ""):
    # garder en memoire le path original
    basePath = bpy.context.blend_data.filepath
    base_file_path = "\\".join(basePath.split("\\")[:-1])
    base_file_name = basePath.split("\\")[-1]
    
    #fullpath du dernier V de la mod
    if not relative_mod_filepath :
        mod_file_path = base_file_path.split("\\")
        while not mod_file_path[-1].startswith("v") :
            mod_file_path.pop()
        mod_file_path.pop()
        
        mod_file_path[-1] = "08_MODEL"
        
        mod_file_path = "\\".join(mod_file_path)
        
        #mod_last_v = get_item_path_last_version(path = mod_file_path, version_letter = "v", as_int = False)
        for file in os.listdir(mod_file_path + "\\v01"):
            mod_last_v = mod_file_path + "\\v01\\" + file
            if ".blend" in file and "model" in file :
                break
            else :
                mod_last_v = None
        
        if not mod_last_v : raise Exception("can't find mod_last_v")
        
    else :
        mod_last_v = relative_mod_filepath
        
    initial_scenes = []
    for scene in bpy.data.scenes :
        initial_scenes.append(scene.name)
    
    # get the lookdev master
    
    lookdev_scenes = append_all_scenes(lookdev_master_filepath)
    
    
    #link the mod
    #EXAMPLE link_col_name = "RF_CH_0003_rufus_MESHES" EXAMPLE
    #link_col_name = "_".join(base_file_name.split("_")[:4]) + "_MESHES"
    link_col_name = "Collection"
    
    link_col = bpy.data.collections.new("_".join(base_file_name.split("_")[:4]) + "_lookdev")
    bpy.context.scene.collection.children.link(link_col)
        
    linked_mod = link_collection(mod_last_v, link_col_name, col_parent_name = link_col.name)
    
    for scene in lookdev_scenes :
        transfer_scene_flux_ui_data(bpy.context.scene, scene)
        scene.collection.children.link(link_col)
        
    for scene_name in initial_scenes :
        scene =  bpy.data.scenes[scene_name]
        bpy.data.scenes.remove(scene)
            
        
    return


if __name__ == "__main__" :
    os.system("cls")
    '''
    toprint = bpy.context.scene
    
    print(dir(toprint))
    print()
    print(toprint)
    print()
    '''
    
    Scene_a = bpy.data.scenes["Scene"]
    Scene_b = bpy.data.scenes["Scene.001"]
    
    #transfer_scene_flux_ui_data(Scene_a, Scene_b)
    #transfer_scene_flux_ui_data(Scene_a, Scene_b)
    
    lookdev_master_filepath = r"W:\01_PRODUCTIONS\012_RUFUS\1_PRE\12_MS\RF_MS_0060_ms_lookdev\01_TEMPLATE\v01\RF_MS_0060_ms_lookdev_template_v01.blend"
    
    setup_lookdev_file(lookdev_master_filepath)
    
 