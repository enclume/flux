# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

''' utilitary module to avoid spam print in the blender console '''

import bpy

class timed_print :
    ''' class to handle timed print and to avoid to spam the console '''
    def __init__(self, wait_time: float = 4):
        ''' 
        class to handle timed print and to avoid to spam the console 

        Args:
            - wait_time: time (in seconde) to wait between two print
        '''
        self.message_queue = []
        self.wait_time = wait_time
        self.stop = False

        self.start_timer()
        
        
    def start_timer(self) :
        ''' start of the print process and of the blender timer event '''
        self.stop = False
        bpy.app.timers.register(self._check_can_print)


    def kill(self) :
        ''' kill the blender timer event, stop the print (can be restarted with self.start_timer())'''
        self.stop = True


    def print(self, message) :
        ''' add a message to the timed print queue '''
        if message not in self.message_queue:
            self.message_queue.append(message)


    def _check_can_print(self) :
        ''' utilitary function to print message in the queue or kill the timer if needed '''
        if self.message_queue :
            #print("timed_print : ")
            for i, msg in enumerate(self.message_queue) :
                #print(f" | msg {i+1} : {msg}")
                print(f"> {msg}")

            self.message_queue = []

        if self.stop : return
        return self.wait_time  