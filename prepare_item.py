# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''

operator to call when a scene need to be prepared for a specific task.

'''

import bpy
import json

from .items_operators import setup_task
from .config import prefs as prefs_from_config
from . import prefs

from bpy.app.handlers import persistent


# initialize logger
try:
    import flux_logger
except:
    from . import flux_logger 

log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)
importlib.reload(setup_task)
importlib.reload(prefs_from_config)


def setup_item_new(basePath: str, category: str, task: str) :
    ''' 
    here the new files are setuped (for a category and a task) 
    Args:
        - basePath: the future path of the wanted new file
        - category: the item category
        - task: the item task
    '''
    
    log.info(f'Starting preparation for the task \"{task}\"')
    task_setups = {
        "modeling": setup_task.setup_task_modeling, 
        "uvs": setup_task.setup_task_uvs,
        "rigging": setup_task.setup_task_rigging,
        "layout": setup_task.setup_task_layout,
        "blocking": setup_task.setup_task_blocking,
        "spline": setup_task.setup_task_spline,
        "polish": setup_task.setup_task_polish,
        "lighting": setup_task.setup_task_lighting,
        "generic": setup_task.setup_task_generic
        }

    if "asset" in category :
        if not bpy.context.blend_data.filepath :
            log.error("bpy.context.blend_data.filepath is none")
            return False
        
        item_task = task.lower()

        if item_task == "surfacing" :
            lookdev_master_filepath = prefs_from_config.get_template("lookdev")
            setup_task.setup_task_surfacing(lookdev_master_filepath=lookdev_master_filepath, basePath=basePath)
            return True
        
        if item_task in task_setups :
            task_setups[item_task](path = basePath)
            return True

        task_setups["generic"](path = basePath)
        return True


    if "shot" in category :        
        if item_task in task_setups :
            task_setups[item_task]()
            return True
        
        #task_setups["generic"]()
        return True

    log.info(f'The categorie ({category}) is not asset or shot, do nothing')
    return False


def create_item(load_from, basePath, cat) : # TODO cleanup a bit this
    @persistent
    def prepare_item_load_handler(self, context, 
                        basePath = basePath, 
                        cat = bpy.context.scene.flux_ui.current_category,
                        current_item = json.loads(bpy.context.scene.flux_ui.current_user_item)):
        # prepare item
        bpy.app.handlers.load_post.remove(prepare_item_load_handler)
        setup_item_new(basePath, cat, current_item['task'])

    bpy.app.handlers.load_post.append(prepare_item_load_handler)
    bpy.ops.wm.open_mainfile(filepath=load_from)
    
    return True
