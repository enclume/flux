# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Utils config function
'''

import os
import json
from typing import Union


try :
    from .. import config
    from .. import flux_logger 

    import importlib
    importlib.reload(flux_logger)
    importlib.reload(config)
except:
    import config
    import flux_logger

# initialize logger
log = flux_logger.get_flux_logger()


def load_json_file(json_path: str) -> dict :
    ''' load a json file from specified path and return the correspondant dictionnary '''

    if not os.path.exists(json_path) :
        try :
            import bpy
            json_path = os.path.join(bpy.utils.script_path_user(), 'addons', 'flux', json_path)
            if not os.path.exists(json_path) :
                log.error(f"json path does not exist ({json_path})")
                raise Exception
        except:
            log.error(f"json path does not exist ({json_path})")
            # print(f"json path does not exist ({json_path})")
            #print(json_path)
            raise Exception
    
    finalDict = open(json_path, "r")
    finalDict = json.load(finalDict)
    log.debug(f"json found and loaded (json_path : {json_path})")
    return finalDict


def format_str_to_python(entry: str) -> str :
    ''' format the entry string value to python (e.g.: \"_True\" string to True boolean) '''

    if type(entry) is not str :
        log.debug(f"entry type is not str, finish (entry type : {type(entry)})")
        return entry
    
    correspondance = {
        "_None": None,
        "_True": True,
        "_False": False
    }
    
    if entry not in correspondance :
        return entry
    
    return correspondance[entry]


def format_str_values(obj: Union[dict, list], recursive: bool = True) :
    '''
    format all string values in an iterable object (dict or list) recursively

    Args:
        - obj: the iterable to format (a dict or a list)
        - recursive: if True, the function is recursive
    '''
    if not (type(obj) is dict or type(obj) is list) :
        log.debug(f"obj type is not dict or list, finish (obj type : {type(obj)})")
        return
    
    for i, key in enumerate(obj) :
        # get the value
        value = key
        if type(obj) is dict :
            value = obj[key]

        # recursive if value is dict/list
        if recursive and (type(value) is dict or type(obj) is list) :
            format_str_values(value, recursive)

        # format the string
        if type(value) is str :
            value = format_str_to_python(value)

        # update the dict/list
        if type(obj) is dict :
            obj[key] = value
        else :
            obj[i] = value
            

# Old, will be deleted
def get_project_config_from_path(path: str) -> dict :
    ''' 
    get the project_config dictionnary from the json file path 

    Args:
        - path: the project_config path (by default (depreciated) on rufus_config.json)
    '''
    project_config = load_json_file(path)
    
    if not project_config :
        log.error(f"project_config = ({project_config})\n   ->(path : {path})")
        raise Exception
    return project_config
    

# Old, will be deleted
def get_project_config_from_name(name: str) -> dict :
    ''' 
    get the project_config dictionnary from the project name 
    
    Args:
        - name: the project name
    '''
    if not name.endswith("_config.json") : 
        name = f"{name.lower()}_config.json"
    return get_project_config_from_path(os.path.join("project_config", name))