# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
description TODO
'''

import os
from typing import Union

try :
    from .. import config
    from .. import flux_logger
    #from ..config.path import class_tree  

    import importlib
    importlib.reload(config)
    importlib.reload(flux_logger)
except :
    import config
    import flux_logger
    #from config.path import class_tree



# initialize logger
log = flux_logger.get_flux_logger()


class config_manager:
    instances = []
    instances_nbr = 0
    def __init__(self, name: str = "unnamed config_manager"):
        ''' 
        description TODO 

        Args:
            - name: the name of the config_manager
        '''
        self.name = name
        self.project_config = None
        self.project_config_indicator = None
        self.db = None

        self.root_path = None

        self.file_tree = None

        config_manager.instances.append(self)
        config_manager.instances_nbr += 1
        log.debug(f"new sanity_check_manager setuped : {self.name}")


    def set_project_config(self, project_config: Union[str, dict]) -> dict :
        ''' 
        description TODO 

        Args:
            - project_config: the project config dict or his name or his json path
        '''
        if type(project_config) is dict :
            self.project_config = project_config
            #print(f"project_config : {project_config}")
            return self.project_config

        if type(project_config) is not str :
            log.error(f"project_config entry need to be a dict or a str ! (project_config type : {type(project_config)})")
            return
        
        self.project_config_indicator = project_config 
        indicator = self.project_config_indicator

        if os.path.exists(indicator):
            project_config = self._get_project_config_path(indicator)
            self.project_config = project_config
            #print(f"project_config : {project_config}")
            return self.project_config
        
        project_config = self._get_project_config_name(indicator)

        config.utils.format_str_values(project_config)

        self.project_config = project_config
        #print(f"project_config : {project_config}")
        self.update_file_tree(self.root_path)
        return self.project_config


    def get_project_config(self) -> dict :
        ''' description TODO  '''
        if not self.project_config :
            log.warning(f"The project_config need to be set ! (project_config : {self.project_config})")
        return self.project_config
    

    def update_file_tree(self, root_path: str = None) :
        self.file_tree = config.path.class_tree.file_tree(config=self.project_config, root_path=root_path)
        return self.file_tree
    

    def _get_project_config_name(self, name: str) -> dict :
        ''' description TODO  '''

        if type(name) is not str :
            error = f"name need to be a str (name type : {type(name)})"
            log.error(error)
            raise Exception(error)

        before_name = []
        if "\\" in name :
            name:list = name.split("\\")
            before_name = name[:-1]
            name = name[-1]

            if before_name[-1] != "project_config" :
                before_name.append("project_config")

        if not name.endswith(".json") :
            if not name.endswith("_config") :
                name += "_config"
            name += ".json"

        if before_name :
            before_name = "\\".join(before_name)
        else :
            before_name = "project_config"

        path = "\\".join([before_name, name])

        return config.utils.load_json_file(path)


    def _get_project_config_path(self, path: str) -> dict :
        ''' description TODO  '''

        if type(path) is not str :
            error = f"path need to be a str (path type : {type(path)})"
            log.error(error)
            raise Exception(error)
        if not os.path.exists(path) :
            error = f"the given path does not exist (path : {path})"
            log.error(error)
            raise Exception(error)

        return config.utils.load_json_file(path)