# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
classes for file tree
'''

from typing import Union

try:
    from . import utils
    from ... import flux_logger 

    #reload lib
    import importlib
    importlib.reload(utils)
    importlib.reload(flux_logger)
except:
    import config.path.utils as utils
    import flux_logger

# initialize logger
log = flux_logger.get_flux_logger()


class TreeTag_Element :
    def __init__(self, name: str, tag_manager, tag_data: dict) :
        self.name = name
        self.manager = tag_manager
        self.tag_value = tag_data["tag_value"]
        self.children = None
        self.props = {}

        for key in tag_data :
            if key == "tag_value" : continue
            self.props.update({key: tag_data[key]})


    def get_tag_value(self, *args: dict, **kwargs) -> str :
        '''
        get the TreeTag element value

        Args:
            - args: any data dictionnary to replace tag 
            - kwargs: any data to replace tag 
        '''
        tags, seps = utils.get_tag_list_from_string(self.tag_value, True, False)

        data = kwargs
        for item in args :
            data.update(item)

        #print(f"\n-> get_tag_value ({self.tag_value})")
        #print("tags :", tags)
        #print("seps :", seps)
        #print("data :", data)
        
        for i, tag in enumerate(tags) :
            tag = tag.split(".")
            if tag[0] == "self" :
                tags[i] = self.props[tag[1]]
                continue
            if tag[0] == "data" :
                if tag[1] not in data :
                    if tag[1] in self.props and None in self.props[tag[1]] :
                        #this tag element can be skiped
                        tags[i] = ""
                        continue
                    error = f"{tag[1]} is needed in kwars or data dictionnary"

                    if tag[1] in self.props :
                        error += f"\n value type needed for {tag[1]} : {self.props[tag[1]]}"
                    log.error(error)
                    continue
                tags[i] = str(data[tag[1]])
                continue
            if tag[0] == "project_data" :
                self.manager.project_config["project_data"][tag[1]]

            log.error(f"unsupported first entity : {tag[0]}")
            
        proceded_value = utils.get_recomposed_tag(tags, seps)
        return proceded_value
    

class TreeTag_Manager :
    def __init__(self, config: dict) :
        self.config = config["tree_tag"]
        self.project_config = config
        self.tag_elements = {}
        self._fill_elems_from_config()

    def add_tag_element(self, name: str, tag_data: dict = None) -> TreeTag_Element:
        ''' add a tag element to the tag_elements dictionnary '''

        if name in self.tag_elements :
            error = f"the element \"{name}\" is already added to the manager"
            log.error(error)
            raise Exception(error)
        if not tag_data :
            if name not in self.config :
                error = "you need to give tag_data"
                log.error(error)
                raise Exception(error)
            tag_data = self.config[name]
        
        new_tag_el = TreeTag_Element(name, self, tag_data)
        self.tag_elements.update({name : new_tag_el})
        return new_tag_el
    
    def get_tag_value(self, element_name: str, *args: dict, **kwargs) -> str :
        '''
        get the TreeTag element value

        Args:
            - args: any data dictionnary to replace tag 
            - kwargs: any data to replace tag 
        '''
        tag_element: TreeTag_Element = self.tag_elements[element_name]

        data = kwargs
        for item in args :
            data.update(item)

        return tag_element.get_tag_value(data)
    
    def _fill_elems_from_config(self):
        for name in self.config :
            self.add_tag_element(name, self.config[name])
            


class tree_element :
    ''' class for tree element of a file tree '''
    def __init__(self, name: str, tags: dict, file_tree_p, parent, modular_ref: str = None) :
        '''
        tree element of a file tree

        Args: 
            - name: the name of the tree_element
            - tags: a dictionnary of tags (e.g.: {task: modeling})
            - file_tree_p: the parent file_tree 
            - parent: the parent tree_element
            - modular_ref: the modular reference if there is one
        '''
        self.name: str = name
        self.tags: dict = tags
        self.file_tree_p = file_tree_p
        self.parent = parent
        self.custom_props: dict = {}
        self.children: list = []
        self.modular_ref: str = modular_ref

        if self not in self.file_tree_p.all_children :
            self.file_tree_p.all_children.append(self)
        if self.parent and self not in self.parent.children :
            self.parent.children.append(self)


    def add_child(self, child) :
        ''' add a child to the tree element '''
        self.children.append(child)
        return child
    
    
    def create_child(self, name: str, tags: dict = {}, 
                     modular_ref:str = None) :
        ''' create and add a new child to the tree element '''

        new_child = tree_element(name, tags, self.file_tree_p, parent=self, 
                                 modular_ref=modular_ref)
        return new_child
    

    def get_parent_attr(self, attr_name: str, if_attr: str = None, recursive: bool = True) :
        ''' get an attribut from a parent tree element '''

        if type(self.parent) is not tree_element : 
            return "tralala"
        
        if if_attr and not self.get_parent_attr(attr_name=if_attr, recursive=False) :
            if recursive :
                return self.parent.get_parent_attr(attr_name, if_attr, recursive)
            return False
        
        value = getattr(self.parent, attr_name, False)
        if value : return value
     
        if attr_name in self.parent.tags : return self.parent.tags[attr_name]
        if attr_name in self.parent.custom_props : return self.parent.custom_props[attr_name]
        
        if recursive :
            return self.parent.get_parent_attr(attr_name, if_attr, recursive)
    
        return False
    

    def _get_value_from_tag(self, tag: str, data: dict) -> str :
        ''' get the tag value relative to the current tree element '''
        #print("_get_value_from_tag :", tag, data)

        tag = tag.split(".")

        if len(tag) == 1 or tag[0] == "data" : # look in given data
            data_tag = tag[-1].lower()
            if data_tag in data :
                #print("data :", data[data_tag].lower())
                return data[data_tag].lower()
        
        if tag[0] == "folder" or tag[0] == "tree" :
            if_attr = None
            if len(tag) == 3 : if_attr = tag[1]
            #print("tree :", self.get_parent_attr(tag[-1], if_attr=if_attr, recursive=True))
            return self.get_parent_attr(tag[-1], if_attr=if_attr, recursive=True)
        
        if tag[0] == "tree_tag" :
            if tag[1] not in data or data[tag[1]] in [None, False] :
                return ""
            return self.file_tree_p.tree_tag.get_tag_value(tag[1], data[tag[1]]) 
        
        if tag[0] == "project_data" :
            config_dict = self.file_tree_p.config
            for item in tag :
                config_dict = config_dict[item]
            #print("config_dict :", config_dict)
            return config_dict
        
        #print(f'->  <tag {tag[-1]}>')
        return f'<tag {tag[-1]}>'
       

    def get_untagged_name(self, data: dict, name: str = None) :
        ''' 
        get a name without tag

        Args:
            - data: the data dictionnary used to replace tags
            - name: the name to untag
        '''
        if not name : name = self.name
        if not utils.is_tagged_string(name) :
            return self.name
        
        tag_list, sep_list = utils.get_tag_list_from_string(name, True)

        for t, tag in enumerate(tag_list) :
            tag_list[t] = self._get_value_from_tag(tag, data)

        untagged = ""

        for i in range(max(len(tag_list), len(sep_list))):
            if i + 1 <= len(sep_list) :
                untagged += sep_list[i]
            if i + 1 <= len(tag_list) :
                untagged += tag_list[i]

        return untagged
    

    def get_matching_properties(self, data: dict) -> dict :
        ''' 
        description TODO 
         Args:
            - data: the data dictionnary to match
        '''
        match_list = []
        score = [0, 0, 0]

        if type(data) is not dict :
            log.error(f"data is not a dict ! (data type : {type(data)})")
            return False
        

        for key in data :
            if type(data[key]) is not str : continue
            value = data[key].lower()

            # test if key is in self
            attr = getattr(self, key, None)
            if attr and value == attr :
                match_list.append(f"child.{key}")
                score[0] += 1
                if key == 'name':
                    score[0] += 1

            # test if key is in self.tags
            if (key in self.tags and 
                (value == self.tags[key] or 
                 (value in self.tags[key] and 
                  type(self.tags[key]) is list))) :
                match_list.append(f"child.tags.{key}")
                score[1] += 1

            # test if key is in self.custom_props
            if key in self.custom_props and value == self.custom_props[key] :
                match_list.append(f"child.custom_props.{key}")
                score[2] += 1
        
        return {"list": match_list, "nbr": len(match_list), "score": score}
    

    def get_relevant_children(self, data: dict, only_tree_element: bool = True) -> list :
        ''' 
        get a list of relevant children relative to a given data dictionnary\n 
        keep only childrens with the higher number of matches

        Args :
            - data: the data dictionnary to match
            - only_tree_element: if True return only a list of tree_elements. Else return a list of child with corresponding matches
        '''
        relevant: dict = {}
        # will be like {2 : [{"child": child, "matches": {"list": match_list, "nbr": len(match_list), "score": score}}]}

        for child in self.children :
            matches = child.get_matching_properties(data)
            matches_nbr = matches["nbr"]

            if matches_nbr == 0 : continue 
            child_dict = {"child": child, "matches": matches}

            if matches_nbr in relevant : 
                # there is already a child with same nbr of matches, just add it
                relevant[matches_nbr].append(child_dict)
                continue
            
            if relevant :
                if relevant.keys()[0] > matches_nbr :
                    # there is already a child with higher nbr of matches, continue
                    continue

                # there is already a child but with lower nbr of matches, replace it
                relevant.clear()

            # there is no children or at least, with lower nbr of matches
            relevant.update({matches_nbr : [child_dict]})

        if not relevant :
            log.debug(f"can\'t find any relevant child (current tree_element : {self.name})")
            return False
        
        if len(relevant) > 1 : # TODO delete this ? -> just a security, maybe not necessary
            log.error(f"relevant dict is > 1 (len relevant : {len(relevant)})")
            big_key = 0
            for key in relevant.keys():
                if key > big_key :
                    big_key = key
            
            relevant = {big_key : relevant[big_key]}        
        
        key = list(relevant.keys())[0]
        relevant = relevant[key] # a list of dict

        if not only_tree_element :
            return relevant # a list of dict
        
        relevant_children = []
        for item in relevant :
            relevant_children.append(item["child"])

        return relevant_children # a list of tree_elements


    def get_most_relevant_child(self, data: dict) :
        ''' 
        get the most relevant child from a list of relevant children relative to a given data dictionnary

        Args:
            - data: the data dictionnary to match
        '''

        if len(self.children) == 1 :
            return self.children[0]
        
        relevant = self.get_relevant_children(data, False)

        if not relevant :
            log.debug(f"can\'t find any relevant child (current tree_element : {self.name})")
            return False

        if len(relevant) == len(self.children) and len(self.children) > 1 :
            log.debug(f"All children are relevant, can\'t choose (current tree_element : {self.name})")
            return False
        
        if len(relevant) == 1 :
            return relevant[0]["child"]
        
        best_child = [None, [0, 0, 0]] # [child, score]

        for item in relevant :
            item: dict # like {"child": child, "matches": {"list": match_list, "nbr": len(match_list), "score": score}}
            matches: list = item["matches"]
            score = matches["score"]

            for i in range(3) :
                if score[i] > best_child[1][i] :
                    best_child = [item["child"], score]
                    break

        return best_child[0]
    

    def get_most_relevant_child_recursive(self, data: dict) -> list :
        ''' 
        description TODO 

        Args:
            - data: the data dictionnary to match
        '''
        to_return = [self]
        next_child = self.get_most_relevant_child(data)
        if next_child :
            next_child_return = next_child.get_most_relevant_child_recursive(data)
            to_return.extend(next_child_return)
        
        return to_return
    

    def get_child_from_path_element(self, path_data: list[str]) -> list :
        ''' 
        get a list of child corresponding to a path 

        Args:
            - path_data: the path splitted in a list, without the root part
        '''
        child_list = []
        current_item = path_data[0]
        good_child = None

        # find the good child
        for child in self.children :
            name: str = child.name
            if len(self.children) == 1 or name == current_item :
                good_child = child
                break
            if utils.is_tagged_string(name) :
                tags, seps = utils.get_tag_list_from_string(name, True, True)

                for i, sep in enumerate(seps[1:]) :
                    tag = tags[i].split(".")
                    item_name = current_item.split(sep)

                    if len(tag) == 1 or len(tag) == 3 :
                        if len(tag) == 1 : tag = tag[0] 
                        elif len(tag) == 3 : tag = tag[1] 
                    item_name = sep.join(item_name[1:])

                good_child = child
                break

        if not good_child :
            return child_list
        
        child_list.append(good_child)

        if len(path_data) > 1 :
            child_list.extend(child.get_child_from_path_element(path_data[1:]))
        return child_list
    

    def print_children(self, indent_lvl: int = 0, recursive: bool = False) :
        ''' print all children of this tree element (can be recursive) '''

        idt = utils.get_indent(indent_lvl)
        if not self.children :
            print(f"{idt}{self.name} : " + "{no childrens}")
            return
        
        print(f"{idt}{self.name} : ")
        for child in self.children :
            if not recursive :
                print(f"{idt}  {child.name}")
                continue
            child.print_children(indent_lvl=indent_lvl+1, recursive = recursive)

    
    def get_tree_element_dict(self, recursive: bool = False, modular_use: bool = False) -> dict :
        ''' 
        get the tree element as a dictionnary\n
        (usefull for a json export) 
        '''
        if modular_use and self.modular_ref :
            return self.modular_ref

        element = {}

        element.update({"name": self.name})

        # here, put other properties TODO
        '''
        for prop in self.custom_props :
            value = self.custom_props[prop]
            element.update({"prop": value})
        '''
        element.update(self.custom_props)

        element.update({"tag": self.tags})
        element.update({"children": []})

        # here, put all modulars children
        for key in dir(self) :
            if "child_modular" in key :
                element.update({key: getattr(self, key)})

        for child in self.children :
            if not recursive :
                element["children"].append(child.name)
                continue
            element["children"].append(child.get_tree_element_dict(recursive))

        return element
    

class file_tree(tree_element) : 
    ''' class for file tree, owner of tree elements '''
    def __init__(self, config: dict = None, root_path: str = None) :
        '''
        file tree

        Args: 
            - root_path: the root path to the start of the file tree
            - config: the config tree dictionnary (e.g. from a json file)
        '''
        self.name: str = "file tree"
        self.root_path: str = root_path
        self.file_tree_p = self
        self.children: list[tree_element] = []
        self.all_children: list[tree_element] = []
        self.sep: str = "_" # separator for generated names
        self.config: dict = config
        self.tree_tag: TreeTag_Manager = None

        if self.config : 
            self.fill_tree_with_config(self.config)
            if (not self.root_path and 
                "project_data" in self.config and 
                "default_root" in self.config["project_data"]) :

                self.root_path = self.config["project_data"]["default_root"]

            self.tree_tag = TreeTag_Manager(self.config)


    def fill_tree_with_config(self, config: dict) :
        ''' take the config dictionnary as reference to fill the file tree '''

        if type(config) is not dict :
            log.error(f"config is not a dict (type : {type(config)})")
            return
        
        if "children" not in config :
            log.error(f"config need to have a \"children\" key (keys : {config.keys()})")
            return
        
        if type(config["children"]) is not list :
            log.error(f'config[\"children\"] is not a list (type : {type(config["children"])})')
            return
        
        self._add_child_recursive(self, config["children"])

    
    def get_path(self, path_data: dict, with_version: bool = True, non_folder_ext: str = None) -> str :
        ''' get a path corresponding to the given data '''

        path_list = self.get_path_list(path_data)

        utils.process_id(self.root_path, path_list, path_data)

        finalpath = [self.root_path]
        for item in path_list :
            finalpath.append(item.get_untagged_name(path_data))

        if "tree_type" in path_list[-1].custom_props and path_list[-1].custom_props["tree_type"] == "non_folder" :
            if non_folder_ext :
                if not non_folder_ext.startswith(".") :
                    non_folder_ext = f".{non_folder_ext}"
                finalpath[-1] += non_folder_ext
            else :
                log.error(f"the last path item is a non folder, you should give a non_folder_ext ! (non_folder_ext : {non_folder_ext})")
                finalpath = finalpath[:-1]

        finalpath = "\\".join(finalpath)
        return finalpath

    def get_path_list(self, path_data: dict) -> list[tree_element] :
        ''' 
        get a list of tree elements based on given data\n 
        the list can be used to generate a path

        Args: 
            - path_data: a dictionnary of data used to travel through the file_tree 
        '''
        path_list = self.get_most_relevant_child_recursive(path_data)

        path_list = path_list[1:] #remove the first element (the current file_tree)
        
        return path_list
    

    def extract_data(self, path_data: list[str], elems: list[tree_element]) -> dict :
        data = {}
        path_data = path_data.copy()
        current_item = -1

        for i, elem in enumerate(elems) :
            current_item += 1
            name = elem.name
            if utils.is_tagged_string(name) : continue
            data.update(elem.tags)
            path_data.remove(name)

        current_item = -1

        for i, elem in enumerate(elems) :
            name = elem.name

            if not utils.is_tagged_string(name) : continue
            current_item += 1

            tags, seps = utils.get_tag_list_from_string(name, True)

            name = ""
            for i, sep in enumerate(seps) :
                name += sep
                if not len(tags) > i : 
                    continue
                tag = tags[i]
                if tag in data :
                    tag = data[tag]
                else :
                    tag = f"<{tag}>"
                name += tag

            tags, seps = utils.get_tag_list_from_string(name, True, True)

            if not (seps and tags) : continue
            if len(seps) == 1 and len(tags) == 1 :
                item = path_data[current_item].replace(seps[0], "")
                if tags[0] not in data :
                    data.update({tags[0] : item})
                continue

            values = []
            for item in path_data[current_item:] :
                for sep in seps :
                    item = item.split(sep)
                    values.append(item[0])
                    if len(item[1:]) == 1 :
                        values.append(item[-1])
                        break
                    item = sep.join(item[1:])
                if len(values) == len(tags) : break
                values = []
                current_item += 1

            if not values :
                return data

            
            for i, tag in enumerate(tags) :
                if tag in data : continue

                if i >= len(values) :
                    error = f"try to get the values[{i}], but values is {values}.\n-> (path_data: {path_data})\n-> (elems: {elems})"
                    log.error(error)
                    raise Exception(error)
                data.update({tag : values[i]})

        return data
    

    def get_data_from_path(self, path: str) -> dict :
        ''' 
        get a list of tree elements based on given data\n 
        the list can be used to generate a path

        Args: 
            - path_data: a dictionnary of data used to travel through the file_tree 
        '''
        path_data = path.replace(f"{self.root_path}\\", "").split("\\")
        elements = self.get_child_from_path_element(path_data)
        
        data =  self.extract_data(path_data, elements)

        for tag in data.copy() :
            new_tag = tag.split(".")
            if len(new_tag) != 3 : continue
            new_tag = new_tag[1]
            value = data[tag]
            data.pop(tag)
            if new_tag not in data :
                data.update({new_tag: value})

        for key in data.copy() :
            if key.startswith("data.") :
                new_key = key.replace("data.", "")
                data.update({new_key: data[key]})
                data.pop(key)

        for key in data :
            value = data[key]
            if type(value) is list :
                value = str(value[0])
            data[key] = value

        version = []
        name_list = path_data[-1].split(".")[:-1]
        name_list = ".".join(name_list).split("_")

        for item in name_list :
            if ((item.startswith("v") and item[1:].isdigit() and len(item[1:]) == 2) or 
                (len(version) == 1 and item.startswith("r") and item[1:].isdigit() and len(item[1:]) == 3) or 
                (len(version) == 2 and item.startswith("w") and item[1:].isdigit() and len(item[1:]) == 4)) : 
                version.append(int(item[1:]))

        if version :
            data.update({"version": version})

        return data


    def print_tree(self) :
        ''' print the file tree '''
        print("file_tree : {")
        for child in self.children :
            child : tree_element
            child.print_children(indent_lvl=1, recursive=True)
        print("}")


    def get_tree(self) -> list[dict] :
        ''' 
        get the file tree in a list
        '''
        tree = []
        for child in self.children :
            child: tree_element
            tree.append(child.get_tree_element_dict(True, False))

        return tree
    

    def _get_modular_ref(self, modular_name: str, parent: tree_element) -> dict :
        ''' 
        get the modular reference relative to the given modular_name and the given parent of the futur child

        Args:
            - modular_name: the name of the modular reference
            - parent: the parent of the futur child

        Return:
            the descriptive dictionnary of the modular child
        '''

        if "child_modular" not in modular_name : 
            return False

        if type(parent) is not tree_element :
            log.error(f"parent is not a tree_element (type : {type(parent)})")
            return False    
        
        sub_lvl = modular_name.split("_").count("sub")
        modular_parent = parent
        for i in range(sub_lvl) :
            modular_parent = modular_parent.parent

        modular_ref = getattr(modular_parent, modular_name, False)
        if not modular_ref :
            log.error(f"there is no modular reference (modular_name : {modular_name}, parent : {parent.name})")
            return False

        return modular_ref


    def _get_child_info_from_config(self, child: dict) -> dict :
        ''' get formated child information from the config dictionnary '''
        
        if type(child) is not dict : 
            error = f"type(child) is not dict (child : {child}, type : {type(child)})"
            log.error(error)
            return False
        
        # format return
        child_old = child.copy()
        child = {"name": None, "tag": {}, "children": [], "child_modular": {}, "other": {}}
        for key in child_old :
            if key in ["name", "tag", "children"] : 
                child[key] = child_old[key]
            elif "child_modular" in key : 
                child["child_modular"].update({key: child_old[key]})
            else :
                child["other"].update({key: child_old[key]})

        return child


    def _add_child_recursive(self, parent: tree_element, config_list: list[Union[dict, str]]) :
        ''' 
        add recursivelly child to fill the file tree 

        Args:
            - parent: the parent/child receiver tree element 
            - config_list: a list of child to add (from a config file)
        '''

        if not config_list or not parent : return
        
        # loop in config_list children 
        for config_child in config_list :
            modular_ref = None
            if type(config_child) is str and "child_modular" in config_child :
                modular_ref = config_child
                config_child = self._get_modular_ref(config_child, parent)
                
            if type(config_child) is dict :
                # get a formated dict from the config_list item
                config_child = self._get_child_info_from_config(config_child)
                if not config_child : continue
                name = config_child["name"]
                tags = config_child["tag"]
                children = config_child["children"]

                # add the child
                new_child = parent.create_child(name, tags, modular_ref)

                # add modular child references
                for key in config_child["child_modular"] :
                    config_mod_ref = config_child["child_modular"][key]
                    setattr(new_child, key, config_mod_ref)

                if config_child["other"] :
                    for key in config_child["other"] :
                        value = config_child["other"][key]
                        new_child.custom_props.update({key: value})

                self._add_child_recursive(new_child, children)