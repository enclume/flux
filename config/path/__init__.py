# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

''' 
Path related stuff  
        
        -> give some contextual element and get the full path of this element 
'''
import os.path
from typing import Union
     

try:
    from . import class_tree
    from . import utils
    from ... import config
    from ... import flux_logger 

    #reload lib
    import importlib
    importlib.reload(flux_logger)
    importlib.reload(class_tree)
    importlib.reload(utils)
    importlib.reload(config)
except:
    # usefull for standalone app
    import config
    import config.path.class_tree as class_tree
    import config.path.utils as utils
    import flux_logger

# initialize logger
log = flux_logger.get_flux_logger()


def get_version_letters():
    return ["v", "r", "w"]


# old versionning stuff TODO
def get_version_parameter_digit_to_string(version_letter: str, version_int: int) -> str :
    ''' description TODO '''
    version_letters = get_version_letters()
    version_letter = version_letter.lower()

    if version_letter not in version_letters :
        error = f"version_letter not supported.\nSupported version_letters : {version_letters}"
        log.error(error)
        raise Exception(error)

    digit_nbr = version_letters.index(version_letter) + 2
    return version_letter + str(version_int).zfill(digit_nbr)


# old versionning stuff TODO
def convert_version_to_dict(version) -> dict :
    '''
    return a version dict {v: v**, r: r***, w: w****}
    '''

    version_letters = get_version_letters()
    new_version = {}
    if type(version) is str and version[0] in version_letters : 
        for l, letter in enumerate(version_letters) :
            if letter not in version : break
            new_version.update({letter: version[version.find(letter):(l+2)]})
            
        if not new_version :
            log.error("the version str is not right formated")
            raise Exception
        version = new_version

    if type(version) is list : 
        for i, item in enumerate(version):
            new_version.update({version_letters[i]: item})
        version = new_version

    if type(version) is dict : 
        for key in version :
            value = version[key]
            if type(value) is not int :
                if not str(value[0]).isdigit() :
                    value = int(value[1:])
                else : 
                    value = int(value)
            version[key] = get_version_parameter_digit_to_string(key, value)

        return version
    
    log.error("\nERROR in convert_version_to_string : the version type is not a list or a dict")
    raise Exception


# old versionning stuff TODO
def get_new_item_save_data(basePath: str, version_to_increment: str = "w", fullPath: bool = True) -> Union[str, tuple[str]] :
    ''' 
    get the full path (new path + new file name) of a new save based on a version increment 

    Args: 
        - basePath: the path of the (blend) file to save
        - version_to_increment: the version letter to increment (v, r or w)
        - fullPath: if False, return the new path and the new name. Else, return new path + \\ + new name
    '''
    version_letters = get_version_letters()
    version_to_increment = version_to_increment[0].lower()

    if version_to_increment not in version_letters :
        raise ValueError(f"version_to_increment (\"{version_to_increment}\") need to be in {version_letters}")
    
    item_data = utils.get_blend_file_path_data(basePath)
    version = {"v": item_data[1], "r": item_data[2], "w": item_data[3]}
    
    if version_to_increment == "w" :
        if version["w"] != "w0000" :
            version["w"] = utils.get_new_version_iterative(version["w"]) 
        elif version["r"] != "r000" :
            version["r"] = utils.get_new_version_iterative(version["r"]) 
            version["w"] = "w0001"
        else :
            version["v"] = utils.get_new_version_iterative(version["v"]) 
            version["r"] = "r001"
            version["w"] = "w0001"

    elif version_to_increment == "r" :
        if version["w"] == "w0000" :
            log.error("version_to_increment == r and version[\"w\"] == \"w0000\"")
            return None
        version["r"] = version["r"]
            
    elif version_to_increment == "v" :
        if not (version["w"] != "w0000" or version["r"] != "r000") :
            log.error("version_to_increment == v and version[\"r\"] == \"r000\"")
            return None
        version["v"] = version["v"]
            
    newName = item_data[4]
    newPath = item_data[0]
    
    for key in version :
        newName += "_" + version[key]
        newPath += "\\" + version[key]
        if key == version_to_increment :
            break
        
    newName += item_data[5]
    
    fullPath = newPath + "\\" + newName
    
    if fullPath :
        return fullPath
    return newPath, newName


def replace_task_in_filename(filename: str, task_overrider: str) -> str :
    '''
    replace the task part in the (blend) file name

    Args:
        - filename: the current filename
        - task_overrider: the new task

    Return:
        filename
    '''

    # TODO : modify funct to be adaptative
    initial_name = filename

    filename = initial_name.split("_")
    
    version = []
    for item in filename :
        if "v" in item :
            version.append(item)

    if not version : 
        if "." not in initial_name :
            return initial_name + "_" + task_overrider
        
        filename = initial_name.split(".")
        filename[-2] += "_" + task_overrider

        return ".".join(filename)
    

    version = filename.index(version[-1])
          
    filename[version-1] = task_overrider
    
    filename = "_".join(filename)
    
    return filename


#-------------------------------------------------
#high level   

#not used
def get_item_from_proxy_path(proxy_path: str, proxy_part: str = "PRX") -> str :
    '''
    get the item path based on the item proxy path

    Args:
        - proxy_path: the path of the proxy
    
    Return:
        the item path
    '''

    if proxy_part not in proxy_path :
        log.error(f"Can't process, the proxy identifier \"{proxy_part}\" not in the proxy path\n->(proxy path : {proxy_path})")
        return False

    # split the path and separate the name
    item_path = proxy_path.split("\\")
    item_name = item_path[-1]

    # separate the extension
    ext = item_name.split(".")[-1]
    item_name = ".".join(item_name.split(".")[:-1])

    # split the name
    item_name = item_name.split("_")

    # remove the proxy part
    item_name.remove(proxy_part)

    # recompose final path
    item_name = "_".join(item_name)
    item_name = ".".join([item_name, ext])
    item_path[-1] = item_name
    item_path = "\\".join(item_path)

    return item_path


# todo allow more than one digit
def is_proxy_from_item_path(item_path: str, proxy_part: str = "PRX#") :
    """
    is the given file path a proxy?
    Args:
        item_path ():
        proxy_part ():

    Returns:
        the item path an the proxy level
    """

    digit_len = 0
    proxy_part_tofind = proxy_part
    if "#" in proxy_part:
        proxy_part_tofind = proxy_part.replace("#", "")
        digit_len = 1

    if proxy_part_tofind not in item_path :
        return False

    file = os.path.basename(item_path)
    dir = os.path.dirname(item_path)
    file_parts = file.split(proxy_part_tofind)

    # proxy level
    proxy_level = -1
    if digit_len:
        proxy_level = int(file_parts[1][0])

    # reconstruct
    file_parts[0] = file_parts[0][:-1]
    if digit_len:
        file_parts[1] = file_parts[1][1:]
    orig_file = file_parts[0] + file_parts[1]

    orig_path = os.path.join(dir, orig_file)

    return [orig_path, proxy_level]
        


def get_proxy_from_item_path(item_path: str, is_proxy_multilevel=False, proxy_level=0, proxy_part: str = "PRX") -> str :
    '''
    get the item proxy path based on the item path

    Args:
        - item_path: the path of the item
        - is_proxy_multilevel: 
        - proxy_level: 
        - proxy_part: 
    
    Return:
        the item proxy path
    '''

    # print("GET PROXY PATH")
    # print(f"{item_path=}")
    # print(f"{is_proxy_multilevel=}")

    proxy_part_tofind = proxy_part
    if is_proxy_multilevel:
        proxy_part_tofind = proxy_part.replace("#", "")

    if proxy_part_tofind in item_path :
        log.info(f"the proxy identifier \"{proxy_part}\" is already in the item_path\n->(item path : {item_path})")
        print(f"WARNING already proxy given {item_path=}")
        return False
        # return item_path

    # split the path and separate the name
    proxy_path = item_path.split("\\")
    proxy_name = proxy_path[-1]

    # separate the extension
    ext = proxy_name.split(".")[-1]
    proxy_name = ".".join(proxy_name.split(".")[:-1])

    # split the name
    proxy_name = proxy_name.split("_")

    # extract versionning
    versionning_i = []
    for i, item in enumerate(proxy_name) :
        if item and item[0] == "v" and item[1:].isdigit() :
            versionning_i.append(i)

    versionning_i = versionning_i[-1]
    versionning = proxy_name[versionning_i:]
    proxy_name = proxy_name[:versionning_i]

    # add the proxy part
    if is_proxy_multilevel:
        proxy_part = proxy_part.replace("#", str(proxy_level))
        # proxy_part = proxy_part + str(proxy_level)
  
    proxy_name.append(proxy_part)

    # recompose final path
    proxy_name.extend(versionning)
    proxy_name = "_".join(proxy_name)
    proxy_name = ".".join([proxy_name, ext])
    proxy_path[-1] = proxy_name
    proxy_path = "\\".join(proxy_path)

    return proxy_path


def get_filepath_with_versionning(fullPath: list, fileName: str, item_version: dict, item_file_extention:str = ".blend") -> str :
    ''' 
    add the versionning to the given filepath 

    Args :
        - fullPath: a list of path element
        - fileName: the file name
        - item_version: item_data["version"]
        - item_file_extention:
    '''

    if item_version == False :
        return False
    
    for version in item_version :
        fullPath.append(item_version[version])
        fileName += "_" + item_version[version]

    if fileName :
        fileName += item_file_extention
        fullPath.append(fileName)

    fullPath = "\\".join(fullPath)
    return fullPath


def format_item_data(item_data: dict):
    ''' 
    format all strings values in an item_data dictionnary (recursively) 

    Args:
        - item_data: the dictionnary to format
    '''
    def _format_action(entry: str) :
        return entry.lower().replace(" ", "")
    def _format_item_data_recursive(entry):
        if not (type(entry) is list or type(entry) is dict) :
            return
        for i, key in enumerate(entry) :
            value = key
            if type(value) is dict : 
                value = entry[key]

            if type(value) is list or type(value) is dict :
                _format_item_data_recursive(value)

            if type(value) is str : 
                value = _format_action(value)

            if type(value) is dict : 
                entry[key] = value
            else : 
                entry[i] = value

    #_format_item_data_recursive(item_data)
    for key in item_data.keys() :
        value = item_data[key]
        if type(value) is str :
            item_data[key] = _format_action(value)
        if type(value) is list :
            for key2 in value :
                 if type(key2) is str :
                    key2 = _format_action(key2)


def get_filepath_from_item_data(item_data: dict, item_file_extention:str = ".blend", with_version: bool = True) -> str :
    ''' 
    get the file path relative to the item_data dictionnary 

    Args :
        - item_data: a dictionnary with all information about the item
        - item_file_extention: the final file extention
        - with_version: default ".blend"
    '''
    if not item_file_extention.startswith(".") : item_file_extention = f".{item_file_extention}"
    project_config = config.get_project_config().copy()
    format_item_data(item_data)
    tree = class_tree.file_tree(project_config, root_path=project_config["project_data"]["default_root"])
    path = tree.get_path(item_data, with_version, item_file_extention).split("\\")

    if not with_version :
        if path[-1].endswith(item_file_extention) :
            path = path[:-1]
        return "\\".join(path)
    
    path[-1] = path[-1][:-len(item_file_extention)]

    return get_filepath_with_versionning(fullPath=path[:-1], fileName=path[-1], item_version=item_data["version"], item_file_extention=item_file_extention)


def get_item_data_from_path(item_path: str) -> dict :
    ''' 
    get item data dictionnary relative to the given item file path 

    Args:
        - item_path: the path to the item
    '''
    project_config = config.get_project_config().copy()
    root_path = project_config["project_data"]["default_root"]
    tree = class_tree.file_tree(project_config, root_path=root_path)

    return tree.get_data_from_path(item_path)