from typing import Union

try:
    from ... import file_access
    from ... import config
    from ... import flux_logger 

    #reload lib
    import importlib
    importlib.reload(flux_logger)
    importlib.reload(config)
except:
    # usefull for standalone app
    import file_access
    import config
    import flux_logger

# initialize logger
log = flux_logger.get_flux_logger()


def find_value_in_obj(obj: Union[dict, list], valueToFind: str, valueMustBeEqual: bool = True) -> list :
    ''' 
    Loop recursively into all values of a dict or a list to find a specific string 

    Args: 
        - obj: the obj to test
        - valueToFind: the value to find in obj
        - valueMustBeEqual: if True, found value must be strictly equal to be keeped
    '''

    if type(obj) is not dict and type(obj) is not list :
        log.error("obj must be a dict or a list (obj is a " + type(obj) + "})")
        raise Exception
        
    if valueToFind.lower() not in str(obj).lower() :
        log.info(f"{valueToFind} not find in the given dict")
        return None
        
    foundObj = []
        
    for key in obj :
        value = key
        if type(obj) is dict :
            value = obj[key]

        if valueToFind not in str(value) : 
            continue

        if type(value) is dict or type(value) is list :
            # recursive research
            foundObj.extend(find_value_in_obj(value, valueToFind, valueMustBeEqual))
            continue

        if (valueMustBeEqual and value == valueToFind) or not valueMustBeEqual :
            foundObj.append(obj)
            continue

    return foundObj


def find_tag_in_obj(obj: Union[dict, list], tagToFind: str) -> list :
    ''' 
    Loop recursively into all values of a dict or a list to find a specific tag\n
    -> /!\\ specifict to a file tree
    '''

    if type(obj) is not dict and type(obj) is not list :
        log.error("obj must be a dict or a list (obj is a " + type(obj) + "})")
        raise Exception
            
    if not tagToFind.startswith("<") :
        tagToFind = "<" + tagToFind
    if not tagToFind.endswith(">") :
        tagToFind = tagToFind + ">"
        
    if tagToFind.lower() not in str(obj).lower() :
        log.info(f"{tagToFind} not find in the given dict")
        return None
        
    foundObj = []

    for key in obj :
        value = key
        if type(obj) is dict :
            value = obj[key]   

        if type(obj) is dict and key == "tag" and (value == tagToFind or (type(value) is list and value.count(tagToFind))) :
            if obj.get("childrens") :
                objCopy = obj.copy()
                objCopy.pop("childrens")
                foundObj.append(objCopy)
            else : 
                foundObj.append(obj)
        if (type(value) is dict or type(value) is list) and tagToFind in str(value) :
                foundObj.extend(find_tag_in_obj(value, tagToFind))
                                  
    return foundObj


def get_key_in_dict_from_prompt_correspondance(dict_target: dict, prompt: str) -> str :
    ''' return the key of the best key-value pair matching the prompt '''

    if not dict_target :
        log.error(f"dict_target = {dict_target}")
        raise Exception
    
    if type(dict_target) is not dict :
        log.error(f"dict_target is not a dict ! (type : {type(dict_target)}, dict_target : {dict_target}, prompt : {prompt})")
        return

    # print(f"{dict_target=}")
    dict_target = dict_target.copy()
        
    if (prompt not in str(dict_target) and 
        prompt.lower() not in str(dict_target) and 
        prompt.upper() not in str(dict_target)) :
        log.debug(f"prompt \"{prompt}\" not in dict_target")
        return None
    if prompt in dict_target.keys() :
        return prompt
    if prompt in dict_target.values() :
        return list(dict_target.keys())[list(dict_target.values()).index(prompt.lower())]

    found_item = []
    for key in dict_target :
        if type(dict_target[key]) is list and prompt.lower() in dict_target[key] :
            found_item.append(key)
            continue
        if (type(dict_target[key]) is str and 
            not dict_target[key].startswith("UNUSED_") and 
            (prompt.lower() == key.lower().split("_")[-1] or prompt.lower() == dict_target[key].lower())) :
            found_item.append(key)
            continue
        

    if not found_item :
        log.info(f"nothing found for {prompt} in {dict_target}")
        return None
    
    if "<" in found_item[0] and ">" in found_item[0] :
        return prompt

    if len(found_item) == 1 :
        return found_item[0]
        
    # TODO : handle the len > 1 list case
    log.info(f"multiple correspondance found for the prompt \"{prompt}\"\n   -> arbitrary return the first : {found_item[0]}")
    return found_item[0]


def find_tag_correspondance(item_data: dict, current_step_data: dict) -> bool :
    ''' 
    analye the given current_step_data dict to see if there is inside a tag dict that match item_data 

    Args :
        - item_data
        - current_step_data
        
    Return :
        True if there is a match with item_data
    '''
    item_data = item_data.copy()
    current_step_data = current_step_data.copy()
    tag_key_list = []

    if "tag" not in current_step_data.keys() :
        log.info(f"current_step dict have no \"tag\" key\n-> item_data : {item_data}\n-> current_step_data : {current_step_data}")
        return False
    
    tag_data = current_step_data["tag"]

    if type(tag_data) is not dict :
        error = f"current_step_data[\"tag\"] is not a dict (tag_data : {tag_data})"
        log.error(error)
        return False

    for tag_key in tag_data.keys():
        if tag_key in item_data.keys():
            tag_key_list.append(tag_key)

    if not tag_key_list :
        log.info("no tag found, return False")
        return False
    elif len(tag_key_list) > 1 :
        log.info("multiple tag found, arbitrary test the first")

    # assure good return if no correspondance
    # cedric add
    if not item_data[tag_key_list[0]]:
        return False

    return item_data[tag_key_list[0]] in tag_data[tag_key_list[0]]


def loop_in_children(current_step: dict, previous_step: dict, item_data: dict) -> dict :
    ''' 
    look for the relevant child in the given folder childrens 

    Args :
        - current_step
        - previous_step
        - item_data
        
    Return :
        the relevant child in the given folder childrens 
    '''

    current_step.copy()
    #print()
    #print(current_step)

    if "tree_type" in current_step and current_step["tree_type"] != "folder":
        return False #stop here if the current step is not a folder (a non folder can't have a child)
    
    if "children" not in current_step :
        return False
    
    to_return = {}

    for child in current_step["children"] :
        #to_return = False
        if child == "child_modular_0" :
            to_return = current_step[child]
            break
        
        if child == "sub_child_modular_0" :
            to_return = previous_step[child]
            break
             
        # here, the not specific case
        if find_tag_correspondance(item_data, child) :
            to_return = child
            break
                

    if not to_return : 
        return False

    if "tag" not in to_return.keys() : to_return.update({"tag": {}})
    if "tree_type" not in to_return.keys() : to_return.update({"tree_type": "folder"})
    return to_return.copy()


def get_folder_attr(item_Path: list, folder_tag_key: str, folder_attr: str) -> str :
    ''' 
    get a folder attribute (name, tag value...) based on a folder list and a folder tag key
    
    Args :
        - item_Path: the folder list
        - folder_tag_key: the tag key of the relevant folder
        - folder_attr: the attribute wanted
        
    Return :
        the folder attribute value
    '''
    to_return = False

    for item in item_Path : 
        # avoid not relevant folder
        if folder_tag_key not in item["tag"].keys() or folder_attr not in item.keys() :
            continue
        
        # get the folder attribute
        to_return = item[folder_attr]
        break

    if not to_return :
        return False
        
    # avoid dict and list return
    while type(to_return) is dict or type(to_return) is list :
        if type(to_return) is dict :
            to_return = list(to_return.values())[0]
        if type(to_return) is list :
            to_return = to_return[0]

    # final return
    return str(to_return)


def get_string_from_tag(tag: str, item_data: dict, item_path: list[dict], project_config: dict) -> str :
    ''' 
    process a tag to find the right correspondant string 

    Args :
        - tag: the tag to process
        - item_data: the item_data initial dictionnary
        - item_path: a list of folder used to reach the final filepath
        - project_config: the project_config dictionnary
        
    Return :
        the string corresponding to the tag
    '''    

    #process id tag (will be useless in the future, currently needed because of the old pipeline)
    if tag.lower() == "id" and "id" not in item_data.keys():
        #print(item_path)
        current_fullpath = []
        for item in item_path :
            if "<" in item["path_str"] and ">" in item["path_str"] :
                break
            current_fullpath.append(item["path_str"])

        current_fullpath = "\\".join(current_fullpath)
        the_id = file_access.get_item_id(current_fullpath, item_data["name"], get_folder_attr(item_path, "type", "name_2"), "RF")
        item_data.update({"id": the_id})
        log.debug(f"return : {the_id}")
        return the_id

    if "." not in tag :
        if tag.lower() in item_data.keys() :
            log.debug(f"return : {item_data[tag.lower()]}")
            return item_data[tag.lower()]
    else :
        try :
            tag = tag.split(".")
        except Exception as e:
            log.error(f"There is something bad\n -> tag : {tag}\nthe error : {e}")
            raise Exception

        if tag[0] == "project_data" :
            obj = project_config
            for item in tag :
                obj = obj[item]

            log.debug(f"return : {obj}")
            return obj
        
        if tag[0] == "folder":
            to_return = get_folder_attr(item_path, tag[1], tag[2])
            if to_return : 
                log.debug(f"return : {to_return}")
                return to_return
    
    #in case of error
    log.error(f"can't process tag \"{tag}\"")
    bad_return = "*"+".".join(tag)+"*"
    log.debug(f"return : {bad_return}")
    return bad_return


def get_untagged_string(string: str, item_data: dict, item_path: list, project_config: dict) -> str :
    ''' 
    get the untagged string from a string with tag into (<tag_exemple>_blablabla_<othertag>)\n
    a tag is relative to the initial item data, to the project configuration or relative to the previous folder 

    Args :
        - string: the string with tag into
        - item_data: the item data dictionnary
        - item_path: the previous folder list
        - project_config: the project config dictionnary

    Return :
        the untagged string
    '''

    if "<" not in string or ">" not in string :
        log.info("the given string is not a tagged string (need \"<\" and \">\")")
        return string
    
    
    string_list = []
    char_memory = ""
    for char in string :
        if char != "<" and char != ">" :
            char_memory += char
            continue
        if char == "<" :
            string_list.append({"str": char_memory, "tag_element": False})
            char_memory = ""
            continue
        if char == ">" :
            string_list.append({"str": char_memory, "tag_element": True})
            char_memory = ""
            continue

    string = ""

    for item in string_list :
        if item["tag_element"] :
            string += get_string_from_tag(item["str"], item_data, item_path, project_config)
            continue
        
        string += item["str"]


    return string


def old_get_filepath_from_item_data(item_data: dict, item_file_extention:str = ".blend", with_version: bool = True) -> str :
    ''' 
    get file path (and optionnaly file name) from item_data 
    
    Args :
        - project_config: ...
        - item_data: ...
        - item_file_extention: default ".blend"

    Return :
        the item file path
    '''
    project_config = config.get_project_config().copy()
    #security stuff
    if not project_config or type(project_config) is not dict :
        error = f"need a project_config dict in entry\n   ->(project_config is a {type(project_config)})"
        log.error(error)
        raise Exception(error)
    if not item_data or type(item_data) is not dict :
        error = f"need a item_data dict in entry\n   ->(item_data is a {type(item_data)})"
        log.error(error)
        raise Exception(error)
    
    
    item_data = item_data.copy()
    
    #conformation stuff
    config.path.format_item_data(item_data)

    if item_file_extention and not item_file_extention.startswith(".") : 
        item_file_extention = "." + item_file_extention

    #setup initial
    fileName = ""
    path_root = project_config["project_data"]["default_root"]
    item_path = [{"path_str": path_root, "tree_type": "folder", "tag": {}}]
    previous_step = None
    current_step = project_config

    #process
    while(item_path[-1] and item_path[-1]["tree_type"]=="folder" and "children" in current_step):
        #look for the right child
        child = loop_in_children(current_step=current_step, previous_step=previous_step, item_data=item_data)

        # cedric add
        # to not have a troncated path that exist but is not the good one
        if child == False:
            return False

        if not child :
            log.debug("no more child, break")
            break
        
        log.debug(f'new child : {child["name"]}')
        
        child.update({"path_str": child["name"]})   
        child.pop("name") 

        #add the child to item_path
        item_path.append(child)
        previous_step = current_step
        current_step = child

    #assemble final fullpath from item path
    fullPath = []
    for item in item_path :
        if "<" in item["path_str"] and ">" in item["path_str"] :
            item["path_str"] = get_untagged_string(item["path_str"], item_data, item_path, project_config)

        if item["tree_type"] != "folder" :
            fileName = item["path_str"]
            #if "versionning" not in item.keys() or item["versionning"] == True : pass
            break
        
        fullPath.append(item["path_str"])

    log.debug(f"fullpath without versionning : {fullPath}")

    if not with_version :
        fullPath = "\\".join(fullPath)
        return fullPath
    
    return config.path.get_filepath_with_versionning(fullPath=fullPath, fileName=fileName, item_version=item_data["version"], item_file_extention=item_file_extention)