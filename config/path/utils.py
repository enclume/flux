# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Utils stuff for config.path
'''

try:
    from . import class_tree
    from ... import file_access
    from ... import flux_logger 

    #reload lib
    import importlib
    importlib.reload(class_tree)
    importlib.reload(file_access)
    importlib.reload(flux_logger)
except:
    import flux_logger 
    import file_access
    import config.path.class_tree as class_tree

# initialize logger
log = flux_logger.get_flux_logger()


def get_indent(lvl: int, space: str = "   ") -> str :
        ''' 
        get an indent space for print purpose 

        Args:
            - lvl: the level of indentation
            - space: the space to repeate
        '''
        indent = ""
        for i in range(lvl) :
            indent += space
        return indent


def is_tagged_string(value: str) -> bool :
    ''' test if the given string is a tag '''
    if type(value) is not str :
        raise TypeError(f"value is not a string (type : {type(value)})")
    if "<" in value and ">" in value :
        return True
    return False


def get_tag_list_from_string(tagged_string: str, with_separators: bool = False, remove_empty_side: bool = False) :
    ''' 
    get a list from a given string with <TAG> inside 

    Args:
        - tagged_string: the string to process
        - with_separators: add the seperators list to the return
        - remove_empty_side: remove empty value from the start and the end of returned lists
    '''

    if not tagged_string or type(tagged_string) is not str :
        error = f"tagged_string need to be a string, not a {type(tagged_string)}"
        log.error(error)
        raise Exception(error)
    
    tag_list: list[str] = []
    sep_list: list[str] = []
    new_list_item: str = ""

    for char in tagged_string :
        if char == "<" :
            sep_list.append(new_list_item.lower())
            new_list_item = ""
            continue
        if char == ">" :
            tag_list.append(new_list_item.lower())
            new_list_item = ""
            continue
        new_list_item += char

    if new_list_item : sep_list.append(new_list_item.lower())

    if remove_empty_side :
        if tag_list :
            while not tag_list[0] : tag_list = tag_list[1:]
            while not tag_list[-1] : tag_list = tag_list[:-1]
        if sep_list :
            while not sep_list[0] : sep_list = sep_list[1:]
            while not sep_list[-1] : sep_list = sep_list[:-1]

    if with_separators :
        return tag_list, sep_list
    return tag_list


def get_recomposed_tag(tags: list, seps: list) -> str :
    if len(tags) != len(seps) : 
        error = "the two tags and seps lists need to have the same length !"
        log.error(error)
        raise Exception(error)
    
    recompo = ""
    for i, sep in enumerate(seps) :
        recompo += sep + tags[i]
    return recompo


def process_id(root_path: str, path_list: list, path_data: dict) -> str :
        ''' rustine for id handling '''
        if "name" not in path_data : return

        id_base_path = [root_path]
        id_name = path_data["name"].lower()
        id_type = ""

        for item in path_list :
            item: class_tree.tree_element
            if "type" in item.tags :
                id_type = item.tags["type"]
                if "name_2" in item.custom_props :
                    id_type = item.custom_props["name_2"]

            name = item.name
            tags = []
            if is_tagged_string(name) :
                tags = get_tag_list_from_string(name, False)

            for i, tag in enumerate(tags) :
                if tag.startswith("data.") :
                    tags[i] = tag.replace("data.", "")
            
            if "id" in tags or "data.id" in tags :
                break

            if tags :
                name = item.get_untagged_name(path_data)

            id_base_path.append(name)

        id_base_path = "\\".join(id_base_path)

        path_id = file_access.get_item_id(id_base_path, id_name, id_type)

        path_data.update({"id": path_id})
        return path_id



def get_blend_file_path_data(basePath: str) :
    ''' 
    get item data from the curent blend file path 
    (root item_path, version_v, version_r, version_w, base filename, file extension) 
    '''
    # TODO : fuction fonctionnal for other file type ? (not only .blend related ?)

    basePath = basePath.split("\\") 

    if len(basePath) < 2 : 
        log.error("basePath not formated in the right way, can't be as short")
        raise Exception
    
    version_r = "r000"
    version_w = "w0000"
    filename = basePath[-1]

    if basePath[-2].startswith("v") :
        root_item_path = "\\".join(basePath[:-2])
        version_v = basePath[-2]
        root_filename = filename.split("_")[:-1]
        
    elif basePath[-2].startswith("r") :
        root_item_path = "\\".join(basePath[0:-3])
        version_v = basePath[-3]
        version_r = basePath[-2]
        root_filename = filename.split("_")[:-2]
        
    elif basePath[-2].startswith("w") :    
        root_item_path = "\\".join(basePath[0:-4])
        version_v = basePath[-4]
        version_r = basePath[-3]
        version_w = basePath[-2]
        root_filename = filename.split("_")[:-3]
    else :
        log.error("basePath not formated in the right way, can't find the versionning")
        raise Exception
        
    root_filename = "_".join(root_filename)
    file_extension = filename[filename.rfind("."):]
    
    return root_item_path, version_v, version_r, version_w, root_filename, file_extension


def get_new_version_iterative(current_version: str) -> str :
    ''' get the incremented version of the given string version '''

    if not current_version or type(current_version) is not str :
        log.error(f"need a current_version string in entry\n   ->(current_version is a {type(current_version)})")
        raise Exception

    version_letter = current_version[0]
    version_digit = current_version[1:]
    digit_size = len(current_version) - 1
    version_digit = int(version_digit) + 1

    return version_letter + str(version_digit).zfill(digit_size)