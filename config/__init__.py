# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
description TODO
'''

import os
from typing import Union

try:
    from . import path
    from . import prefs
    from . import utils
    from . import class_config_manager
    from .. import flux_logger

    import importlib
    importlib.reload(flux_logger)
    importlib.reload(path)
    importlib.reload(prefs)
    importlib.reload(utils)
    importlib.reload(class_config_manager)
except:
    import config.path as path
    import config.class_config_manager as class_config_manager
    try:
        import config.prefs as prefs
    except:
        # there is a bpy import somewhere
        pass
    import config.utils as utils
    import flux_logger


# initialize logger
log = flux_logger.get_flux_logger()

# initialize config cache
if len(class_config_manager.config_manager.instances) == 0 :
    config_manager = class_config_manager.config_manager(name="config_manager")


def get_project_config(file_indicator: str = None) -> dict :
    ''' 
    get the project_config dictionnary\n
    (and if file_indicator is set, update the project config based on the json file path or the project name) 

    Args: 
        - file_indicator: the name of the project or the json file path
    '''
    if not file_indicator :
        return config_manager.get_project_config()
    if config_manager.project_config and file_indicator == config_manager.project_config_indicator :
        return config_manager.get_project_config()
    
    return config_manager.set_project_config(file_indicator)


def set_project_config(file_indicator: Union[str, dict]) -> dict :
    ''' 
    set the project_config dictionnary based on the json file path or the project name 

    Args: 
        - file_indicator: the name of the project or the json file path
    '''
    return config_manager.set_project_config(file_indicator)


def get_project_name(short: bool = False) -> str :
    """
    get project name fron json file, with cache support

    Args:
        - short: return short name
    """
    project_config = get_project_config()
    # print(f"{project_config=}")
    if short and "project_name_short" in project_config["project_data"] :
        project_name = project_config["project_data"]["project_name_short"]
    else :
        project_name = project_config["project_data"]["project_name"]

    return project_name


def get_project_db() -> dict :
    ''' 
    get the active db dict 

    Returns:
        {"host": the (url) path to the api, "api": the api type}
    '''
    return config_manager.db


def set_project_db(host: str, api: str) -> dict :
    ''' 
    set the active db dict 

    Returns:
        {"host": the (url) path to the api, "api": the api type}
    '''


    config_manager.db = {"host": host, "api": api}
    return config_manager.db

# TODO move this to be based on what give the DB (and not on the config file)
def get_status_list(get_default: Union[bool, str] = False) -> Union[str, list[list[str]]] :
    """
    get status list (or default), with cache support

    Args:
        - default: return string of default
    """
    project_config = get_project_config()
    # print(f"{project_config=}")
    if not project_config :
        return None
    # print(f"{get_default=}")
    if get_default :
        result = project_config["project_data"]["status_list_default_" + get_default]
    else :
        result = project_config["project_data"]["status_list"]

    # print(f"{result=}")
    return result




if __name__ == '__main__' :
    os.system("cls")
    #basePath = r"w:\01_PRODUCTIONS\012_RUFUS\1_PRE\1_CH\RF_CH_0003_rufus\08_MODEL\v01\r001\w0001\RF_CH_0003_rufus_model_v01_r001_w0001.blend"
    #print(get_new_item_save_data(basePath=basePath, version_to_increment = "w"))
    #basePath = r"w:\01_PRODUCTIONS\012_RUFUS\1_PRE\1_CH\RF_CH_0003_rufus\08_MODEL\v01\r001\RF_CH_0003_rufus_model_v01_r001.blend"
    #print(get_new_item_save_data(basePath=basePath, version_to_increment = "w"))
    #basePath = r"w:\01_PRODUCTIONS\012_RUFUS\1_PRE\1_CH\RF_CH_0003_rufus\08_MODEL\v01\RF_CH_0003_rufus_model_v01.blend"
    #print(get_new_item_save_data(basePath=basePath, version_to_increment = "w"))

    '''
    project_config_test = get_project_config_from_path(path = "project_config\\test_new_config.json")
    project_config = get_project_config_from_path()
    item_data_test = {"project": "rufus", "category": "assets", "type": "character", "name": "rufus", "task": "modeling", "version": [1, 1]}
    item_data_test["version"] = convert_version_to_dict(item_data_test["version"])
    #print(get_filepath_from_item_data(project_config=project_config, item_data=item_data_test, with_version=False))
    print(get_filepath_from_item_data(project_config=project_config_test, item_data=item_data_test))

    item_data_test = {"project": "rufus", "category": "shots", "departement": "animation", "seq": "sq0000", "shot": "sq0000_sh0000", "task": "layout", "version": [1]}
    item_data_test["version"] = convert_version_to_dict(item_data_test["version"])
    print(get_filepath_from_item_data(project_config=project_config_test, item_data=item_data_test))
    print(get_item_data_from_path())
    '''