# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

''' Prefs related stuff

        -> give some contextual element and get the full path of this element '''

import os

# not used
#try :
#    import path as path_from_config
#except :
#    from .. import path as path_from_config

try:
    from ... import config
    from ... import prefs
    from ... import flux_logger

    import importlib
    importlib.reload(config)
    importlib.reload(prefs)
    importlib.reload(flux_logger)
except:
    import config
    import prefs
    import flux_logger


# initialize logger  
log = flux_logger.get_flux_logger()


def get_prefs_from_project() -> dict :
    """
    get dict from json for current project name

    Returns:
        dict of all prefs
    """
    project_config = config.get_project_config()
    return project_config["prefs"]


def get_disk_action_template(cat: str="") -> str :
    """
    get disk action template. Only for the new disk action for now.
    Args:
        - cat: category

    Returns:
        file path 
    """
    prefs_path = get_prefs_from_project()
    disk_action_template = prefs_path["disk_action_new_template"]

    if not cat or cat not in disk_action_template :
        cat = "__default__"

    disk_action_template_final = disk_action_template[cat]

    # root = path_from_config.get_project_root()
    root = prefs.get_project_path_root()

    disk_action_template_final = disk_action_template_final.replace("<ROOT>", root)

    # print(disk_action_template)

    return disk_action_template_final


def get_template(name: str) -> str :
    ''' 
    get a template path 

    Args:
        - name: the name of the template
    '''
    prefs_path = get_prefs_from_project()
    templates: dict = prefs_path["templates"]
    if name not in templates :
        error = f"the given name is not in templates. Supported names : {list(templates.keys())}"
        log.error(error)
        raise Exception(error)
    
    template_path: str = templates[name]
    root = prefs.get_project_path_root()
    template_path = template_path.replace("<ROOT>", root)

    return template_path


def get_disk_action_from_task(cat: str, current_item: dict) -> list :
    """
    get disk action template. Only for the new disk action for now
    
    Args:
        cat: category
        current_item: ...

    Returns:
        file path
    """
    prefs_path = get_prefs_from_project()
    disk_action_template = prefs_path["disk_action_new_from"]

    if not cat:
        return None

    disk_action_previous = disk_action_template[cat]
    # print(f"{disk_action_previous=}")

    all_recorded_tasks = list(disk_action_previous.keys())
    # print(f"{all_recorded_tasks=}")
    # print(f"{current_item=}")

    if current_item["task"].lower() not in all_recorded_tasks:
        return None
    
    result_task = disk_action_previous[current_item["task"].lower()]
    result = dict(current_item)
    result["task"] = result_task[0]

    return [result, bool(result_task[1])]
        

def get_import_task_from_item_type(item_type: str) -> str :
    """
    get the item task needed for an import by the item type

    Args:
        item_type: ...

    Returns:
        item task
    """
    prefs_import = get_prefs_from_project()
    prefs_import = prefs_import["composer"]["import"]
    #task = path_from_config.get_key_in_dict_from_prompt_correspondance(prompt=item_type, dict_target=prefs_import)
    task = prefs_import[item_type]
    return task


if __name__ == "__main__" :
    os.system("cls")

    #print(get_import_task_from_item_type("element"))