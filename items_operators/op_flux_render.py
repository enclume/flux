# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
rendering related operator
'''

import bpy
import os
import ast
import json

from .. import config
# import ui
from ..blender_libs import render
from .. import ui_operators
from .. import ui
from .. import file_access
from .. import prefs
from .. import database_access

#reload lib
import importlib
importlib.reload(render)
importlib.reload(ui_operators)


class FLUX_OT_RenderFile(bpy.types.Operator):
    """
    item operator
    -> Render the current file
    """

    bl_idname = "flux.item_ot_render_file"
    bl_label = "Flux ITEM OT Render File"
    bl_options = {"INTERNAL"}
    bl_description = ("Render the current file")

    render_path: bpy.props.StringProperty(subtype="FILE_PATH", default="None")
    render_path_cache: bpy.props.StringProperty(subtype="FILE_PATH", default="_None")
    render_params: bpy.props.StringProperty(default="None")
    animation: bpy.props.BoolProperty(default=True)
    with_metadata: bpy.props.BoolProperty(default=True)
    openGL: bpy.props.BoolProperty(default=True)
    increment: bpy.props.BoolProperty(default=False)
    allow_preview_range: bpy.props.BoolProperty(default=True)

    preview_num: bpy.props.IntProperty(default=0)

    for_revision: bpy.props.BoolProperty(default=False)
    allow_choose_for_revision: bpy.props.BoolProperty(default=True)

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True
    

    def get_path(self):
        render_path = bpy.context.blend_data.filepath
        if not render_path:
            # self.report({'ERROR'}, 'You need to have your blendfile saved !')
            return None

        item = ui.get_current_user_item(get_category=True)
        last_vrw = file_access.get_item_last_version(item, version_letter="w", as_int=True)
        self.is_w = True
        if not last_vrw:
            self.is_w = False

        if self.for_revision:
            # print(item)
            # print(last_vrw)
            if not last_vrw:
                last_vrw = file_access.get_item_last_version(item, version_letter="r", as_int=True)
                if not last_vrw:
                    return None
            # print(last_vrw)
            item['version'] = [last_vrw['v'], last_vrw['r']]
            last_r_path = file_access.get_item_path2(item, only_file_path=True)
            # print(last_r_path)
            render_path = last_r_path
            # print(render_path)
            # return {"FINISHED"}

        render_path = render_path.replace(".blend", "_preview")
        preview_num = 1

        # ===================================
        all_ext = ["mp4","png"]

        while os.path.exists(f"{render_path}{preview_num:02d}.{all_ext[0]}") or os.path.exists(f"{render_path}{preview_num:02d}.{all_ext[1]}"):
            preview_num += 1

        # increment
        if not self.increment and preview_num > 1:
            preview_num -= 1

        if self.animation:
            ext = all_ext[0]
        else:
            ext = all_ext[1]

        render_path = f"{render_path}{preview_num:02d}.{ext}"

        # print(render_path)
        self.preview_num = preview_num

        return render_path
    

    def execute(self, context: bpy.types.Context):
        print("Start rendering file")
        render_path = self.render_path
        render_params = self.render_params
        # if render_path == "_None" :
        #     render_path = bpy.context.blend_data.filepath
        #     if not render_path :
        #         self.report({'ERROR'}, 'You need to have your blendfile saved !')
        #         return {"CANCELLED"}
        #     render_path = render_path.replace(".blend", "_preview")
        #     preview_num = 1
        #
        #     while os.path.exists(f"{render_path}{preview_num:02d}.mp4") :
        #         preview_num += 1
        #
        #     # increment
        #     if not self.increment:
        #         if preview_num > 1:
        #             preview_num -=1
        #
        #     render_path = f"{render_path}{preview_num:02d}"
        #
        #     print(render_path)

        if render_params == "None":
            render_params = None

        # delete file if not increment
        all_ext = [".mp4", ".png"]

        all_files = []
        for i, ext in enumerate(all_ext):
            if ext in render_path:
                all_files.append(render_path)
                all_files.append(render_path.replace(ext, all_ext[1-i]))

        if not self.increment:
            # remove ot be sure before to move on
            for file in all_files:
                # print("Check : ", file)
                if os.path.exists(file):
                    os.remove(file)
                    print("Delete : ", file)

        if self.openGL:
            render.launch_flux_playblast(render_path=render_path.split(".")[0], render_params=render_params,
                                         animation=self.animation, with_metadata=self.with_metadata,
                                         allow_preview_range=self.allow_preview_range)
        else:
            render.launch_flux_render(render_path=render_path.split(".")[0], render_params=render_params,
                                      animation=self.animation, openGL=self.openGL)

            # os.startfile(render_path + ".mp4")
            os.startfile(render_path)
        return {"FINISHED"}
    

    def invoke(self, context: bpy.types.Context, event):

        render_path = self.get_path()
        print(render_path)
        if not render_path:
            self.report({"ERROR"}, f"does not exist")
            return {"FINISHED"}

        return context.window_manager.invoke_props_dialog(self, width=500)

    def draw(self, context: bpy.types.Context):
        layout = self.layout
        col = layout.column()

        render_path = self.get_path()
        # print(render_path)
        if not render_path:
            self.report({"ERROR"}, f"does not exist")
            return {"FINISHED"}

        self.render_path = self.get_path()
        col.prop(self, "render_path", text="")
        col.separator()

        col.prop(self, "increment", text="Increment Versionning")
        col.prop(self, "allow_preview_range", text="Allow Preview Range")
        row = col.row()
        row.prop(self, "animation")
        row.prop(self, "openGL", text="openGL render (faster)")

        row = col.column()
        row.separator()
        if self.allow_choose_for_revision:
            row.prop(self, "for_revision", text="Render at Review Level", toggle=True)
        else:
            row.label(text="Render at review level")

        if not self.is_w and not self.for_revision and self.allow_choose_for_revision:
            row.separator()
            row.label(text="No wip level detected", icon="ERROR")


class FLUX_OT_CheckFile(bpy.types.Operator):
    """
    item operator
    -> Render the current file
    """

    bl_idname = "flux.item_ot_check_file"
    bl_label = "Flux ITEM OT Check File"
    bl_options = {"INTERNAL"}
    bl_description = ("Check current files")

    # render_path: bpy.props.StringProperty(subtype="FILE_PATH", default="None")
    # render_path_cache: bpy.props.StringProperty(subtype="FILE_PATH", default="_None")
    # render_params: bpy.props.StringProperty(default="None")
    # animation: bpy.props.BoolProperty(default=True)
    # with_metadata: bpy.props.BoolProperty(default=True)
    # openGL: bpy.props.BoolProperty(default=True)
    increment: bpy.props.BoolProperty(default=True)
    # allow_preview_range: bpy.props.BoolProperty(default=True)
    #
    # preview_num: bpy.props.IntProperty(default=0)
    #
    for_revision: bpy.props.BoolProperty(default=True)

    # kitsu infos on status and comment. Status is wip by default as it s the most used in the UI
    # task_status_name: bpy.props.StringProperty( default="WIP")
    task_status_name: bpy.props.EnumProperty(name="List of kitsu tasks",
        items=ui_operators.get_status_list_enum
        )

    user_comment: bpy.props.StringProperty(default="None")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def get_previews(self):
        render_path = bpy.context.blend_data.filepath
        if not render_path:
            self.report({'ERROR'}, 'You need to have your blendfile saved !')
            return {"CANCELLED"}

        if self.for_revision:
            item = ui.get_current_user_item(get_category=True)
            # print(item)
            last_vrw = file_access.get_item_last_version(item, version_letter="w", as_int=True)
            if not last_vrw:
                last_vrw = file_access.get_item_last_version(item, version_letter="r", as_int=True)
            # print(last_vrw)
            item['version'] = [last_vrw['v'], last_vrw['r']]
            last_r_path = file_access.get_item_path2(item, only_file_path=True)
            # print(last_r_path)
            render_path = last_r_path
            # print(render_path)
            # return {"FINISHED"}

            self.version_v = last_vrw['v']
            self.version_r = last_vrw['r']

        render_path = render_path.replace(".blend", "_preview")
        preview_num = 1

        print(render_path)

        # ===================================
        all_ext = [".mp4",".png"]

        # =========================
        all_prevs = []
        while os.path.exists(f"{render_path}{preview_num:02d}{all_ext[0]}") or os.path.exists(f"{render_path}{preview_num:02d}{all_ext[1]}"):
            for ext in all_ext:
                file = f"{render_path}{preview_num:02d}{ext}"
                if os.path.exists(file):
                    all_prevs.append(file)

            preview_num += 1

        return all_prevs

    def execute(self, context: bpy.types.Context):
        # todo securize
        current_item = json.loads(context.scene.flux_ui.current_user_item)

        #start set comment
        cat = context.scene.flux_ui.current_category
        current_item["category"] = cat

        user = prefs.get().db_fullname

        all_prevs_user = bpy.context.scene.flux_ui_previews_list
        all_prevs_user_sel = []

        for each in all_prevs_user :
            if each.is_sel :
                all_prevs_user_sel.append(each.file_path)
                print("Published previews selected:", each.file_path)

        database_access.set_formated_comment(cat, dict(current_item), self.task_status_name, self.user_comment, [self.version_v, self.version_r], 
                                             user=user, preview_path=all_prevs_user_sel, revision=self.version_r)

        return {"FINISHED"}
    

    def invoke(self, context: bpy.types.Context, event):

        all_prevs = self.get_previews()
        self.num_all_prevs = len(all_prevs)

        self.all_prevs = all_prevs

        bpy.context.scene.flux_ui_previews_list.clear()
        for each in all_prevs:
            newFile = context.scene.flux_ui_previews_list.add()
            newFile.file_name = os.path.basename(each)
            newFile.file_path = each

        return context.window_manager.invoke_props_dialog(self, width=500)


    def draw(self, context: bpy.types.Context):
        layout = self.layout
        col = layout.column()

        # col.label(text="Number of preview files to publish : " + str(self.num_all_prevs))

        all_files = context.scene.flux_ui_previews_list
        sel = 0
        for i, each in enumerate(all_files):
            if each.is_sel:
                sel += 1

        if not len(all_files):
            col.label(text=f"No Previews files founded", icon="ERROR")
            col.label(text=f"You'r comment will be published without previews!", icon="ERROR")
        else:
            col.label(text=f"Previews files to publish ({str(sel)}/{str(len(all_files))}):")
            col.separator()

            col.template_list("FLUX_UL_CheckFileList",  # name of UIList class containing the draw method
                              "CheckFileList",  # unique ID
                              bpy.context.scene,  # propGroup containing the list to display
                              "flux_ui_previews_list",
                              bpy.context.scene,  # index
                              "flux_ui_previews_list_index",
                              rows=1,
                              type="DEFAULT",
                              sort_lock=True)
            
            col.separator()

            # row = layout.row()
            wm = context.window_manager
            col.template_icon_view(wm, "previews_thumbnails", show_labels=True)

        col.separator()
        col.operator("flux.itemmenu_exe", icon='FILEBROWSER',
                      text="Check Preview folder (Review)").what = 'WINEXPLORER_REVISION'


        col.separator()
        col.prop(self, "task_status_name", text="Status")
        col.prop(self, "user_comment", text="Comment")


class FLUX_UL_CheckFileList(bpy.types.UIList):
    '''
    Composer List Builder
    '''
    # VGROUP_EMPTY = 1 << 0

    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        if self.layout_type not in {'DEFAULT', 'COMPACT'} :
            return

        row = layout.row(align=True)

        icon_load = "CHECKBOX_DEHLT"
        if item.is_sel : icon_load = "CHECKBOX_HLT"
        row.prop(item, "is_sel", text="", emboss=False, icon=icon_load)

        icon = "OUTLINER_DATA_CAMERA"
        if ".png" in item.file_name : icon = "OUTLINER_OB_IMAGE"
        row.prop(item, "file_name", text="", emboss=False, icon=icon)


class FLUX_OT_RenderCleanLayout(bpy.types.Operator):
    """
    item operator
    -> Render the current file
    """

    bl_idname = "flux.item_ot_render_clean_layout"
    bl_label = "Flux ITEM OT Render Clean Layout"
    bl_options = {"INTERNAL"}
    bl_description = ("""Clean layout render : a openGL render of the layout in colour without anything else obstructing the image.
                      \n->(mov output, with no metadata, CHAR & PROP collection hidden)""")

    overwrite: bpy.props.BoolProperty(default=False)
    openfile: bpy.props.BoolProperty(default=False)

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool :
        return True
    
    def execute(self, context: bpy.types.Context) :
        print("Start rendering file")
        print(f"(overwrite : {self.overwrite}, openfile : {self.openfile})")

        # handle render path
        render_path = bpy.context.blend_data.filepath
        if not render_path :
            self.report({'ERROR'}, 'You need to have your blendfile saved !')
            return {"CANCELLED"}
        render_path = render_path.replace(".blend", "_clean_")
        preview_num = 1

        while os.path.exists(f"{render_path}{preview_num:02d}.mov") :
            preview_num += 1

        # render path increment
        if self.overwrite and preview_num > 1 :
            preview_num -=1

        render_path = f"{render_path}{preview_num:02d}"

        print(f"render path : {render_path}")

        # start render
        render_filepath = render.layout_clean_render(render_path = render_path, openfile = False)
        
        if self.openfile :
            dir_path = "\\".join(render_path.split("\\")[:-1])
            #os.startfile(render_path + ".mov")
            os.startfile(dir_path)
        return {"FINISHED"}
    
    
    def invoke(self, context: bpy.types.Context, event):
        return context.window_manager.invoke_props_dialog(self)


    def draw(self, context: bpy.types.Context):
        layout = self.layout
        col = layout.column()
        col.prop(self, "overwrite", text="overwrite last version")
        col.prop(self, "openfile", text="open folder after render")


class FLUX_OT_RenderAlphaLayout(bpy.types.Operator):
    """
    item operator
    -> Render the current file
    """

    bl_idname = "flux.item_ot_render_alpha_layout"
    bl_label = "Flux ITEM OT Render Alpha Layout"
    bl_options = {"INTERNAL"}
    bl_description = ("""Alpha layout render""")

    render_path: bpy.props.StringProperty(subtype="FILE_PATH", default="_None")
    overwrite: bpy.props.BoolProperty(default=False)
    openfolder: bpy.props.BoolProperty(default=False)

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool :
        return True
    
    def execute(self, context: bpy.types.Context) :
        print("Start rendering file")
        print(f"(overwrite : {self.overwrite}, openfolder : {self.openfolder})")

        # handle render path
        blendfile_path = bpy.context.blend_data.filepath
        if not blendfile_path :
            self.report({'ERROR'}, 'You need to have your blendfile saved !')
            return {"CANCELLED"}
        
        blendfile_name = blendfile_path.split("\\")[-1].replace(".blend", "")
        mstr_folder = blendfile_path.split("\\")[:-1]
        mstr_folder = "\\".join(mstr_folder)
        if "layout" in blendfile_name.lower() :
            render_name = f"{blendfile_name}_alpha_"
        else :
            render_name = f"{blendfile_name}_layout_alpha_"
        render_folder = os.path.join(mstr_folder, render_name)
        version_num = 1

        while os.path.exists(f"{render_folder}{version_num:02d}") :
            version_num += 1

        # render path increment
        if self.overwrite and version_num > 1 :
            version_num -=1

        render_folder = f"{render_folder}{version_num:02d}"
        render_name = f"{render_name}{version_num:02d}"
        render_path = os.path.join(render_folder, render_name)

        print(f"render path : {render_path}")

        if not os.path.exists(render_folder) :
            os.makedirs(render_folder)

        # start render
        render_filepath = render.layout_alpha_images_render(render_path=render_path)
        
        if self.openfile :
            dir_path = "\\".join(render_path.split("\\")[:-1])
            #os.startfile(render_path + ".mov")
            os.startfile(dir_path)
        
        return {"FINISHED"}
    
    def invoke(self, context: bpy.types.Context, event):
        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context: bpy.types.Context):
        layout = self.layout
        col = layout.column()
        col.prop(self, "overwrite", text="overwrite last version")
        col.prop(self, "openfolder", text="open folder after render")


class FLUX_OT_OpenRenderedFile(bpy.types.Operator):
    """
    item operator
    -> Open image/mp4 Files From current open blender file on the rule:
        blenderFile_previewXX.EXT
        where EXT is either mp4, jpg, jpeg
    """

    bl_idname = "flux.item_ot_open_rendered_file"
    bl_label = "Flux ITEM OT Open Rendered Render File"
    bl_options = {"INTERNAL"}
    bl_description = ("Render the current file")

    file_path: bpy.props.StringProperty(subtype="FILE_PATH", default="None")
    files_to_open_len : bpy.props.IntProperty(default=0)
    open_only_last: bpy.props.BoolProperty(default=True)
    files_to_open : bpy.props.StringProperty(subtype="FILE_PATH", default="None")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):
        # print("launch flux render")

        if self.file_path == "None":
            self.file_path = bpy.data.filepath

        all_files = ui_operators.get_preview_file(self.file_path)

        for each in all_files:
           os.startfile(each)
        return {"FINISHED"}
    

    def invoke(self, context: bpy.types.Context, event):
        if self.file_path == "None":
            self.file_path = bpy.data.filepath
        all_files = ui_operators.get_preview_file(self.file_path)

        if all_files:
            self.files_to_open_len = len(all_files)

        if self.files_to_open_len:
            self.files_to_open = str(all_files)

        return context.window_manager.invoke_props_dialog(self, width=500)


    def draw(self, context):
        layout = self.layout
        col = layout.column()

        if not self.files_to_open_len :
            col.label(text="No files to see :( ")
            return
        
        col.label(text= "Files to see : " + str(self.files_to_open_len))
        if self.files_to_open_len > 1 :
            col.prop(self, "open_only_last", text="Open Only Last")


    def execute(self, context: bpy.types.Context):
        if not self.files_to_open_len :
            self.report({'ERROR'}, 'Nothing to open')
            return {"CANCELLED"}
        
        print("open...")

        all_files = self.files_to_open
        all_files = ast.literal_eval(all_files)

        # print(all_files)

        if self.open_only_last:
            all_files = [all_files[-1]]

        for each in all_files:
            print("..." + each)
            os.startfile(each)

        return {"FINISHED"}


