# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
turntable related operator
'''

import bpy
import os

#from ..standalone_scripts import blender_subprocess
from . import turntable
from . import setup_task
from ..config import path as path_from_config

#reload lib
import importlib
#importlib.reload(blender_subprocess)
importlib.reload(turntable)
importlib.reload(setup_task)
importlib.reload(path_from_config)


class FLUX_OT_MakeTurntable(bpy.types.Operator):
    """
    item operator
    -> Create the turntable blend file of the current item
    """

    bl_idname = "flux.item_ot_make_turntable"
    bl_label = "Flux ITEM OT Make Turntable"
    bl_options = {"INTERNAL"}
    bl_description = ("Create the turntable blend file of the current item")

    current_item: bpy.props.StringProperty(default = "default current_item")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):
        #print("\n\nMake turntable :")
        #print(f"current_item = {self.current_item}\n\n")

        basePath = bpy.context.blend_data.filepath # TODO hard coded
        turntable_master_filepath = r"W:\01_PRODUCTIONS\012_RUFUS\1_PRE\12_MS\RF_MS_0386_ms_turntable\01_TEMPLATE\v01\RF_MS_0386_ms_turntable_template_v01.blend"
        light_master_filepath = r"W:\01_PRODUCTIONS\012_RUFUS\1_PRE\12_MS\RF_MS_0387_ms_lightset\01_TEMPLATE\v01\RF_MS_0387_ms_lightset_template_v01.blend"

        #print("start making turntable")
        turntable_mp4_filepath = turntable.make_turntable(basePath=basePath, turntable_master_filepath=turntable_master_filepath, light_master_filepath=light_master_filepath)
        bpy.context.scene.flux_ui.item_preview_path = turntable_mp4_filepath
        
        self.report({"INFO"}, "Turntable created")

        return {"FINISHED"}
    

class FLUX_OT_SetupTurntable(bpy.types.Operator):
    """
    item operator
    -> Setup turntable related stuff in the current blend file
    """

    bl_idname = "flux.item_ot_setup_turntable"
    bl_label = "Flux ITEM OT Setup Turntable"
    bl_options = {"INTERNAL"}
    bl_description = ("Setup turntable related stuff in the current blend file")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context): # TODO hard coded
        turntable_master_filepath = r"W:\01_PRODUCTIONS\012_RUFUS\1_PRE\12_MS\RF_MS_0386_ms_turntable\01_TEMPLATE\v01\RF_MS_0386_ms_turntable_template_v01.blend"
        light_master_filepath = r"W:\01_PRODUCTIONS\012_RUFUS\1_PRE\12_MS\RF_MS_0387_ms_lightset\01_TEMPLATE\v01\RF_MS_0387_ms_lightset_template_v01.blend"

        turntable.setup_turntable(turntable_master_filepath=turntable_master_filepath, light_master_filepath=light_master_filepath)
        
        self.report({"INFO"}, "Turntable setuped")

        return {"FINISHED"}
    
    def invoke(self, context: bpy.types.Context, event):
        return context.window_manager.invoke_confirm(self, event)
    

class FLUX_OT_SetupModeling(bpy.types.Operator):
    """
    item operator
    -> Setup modeling related stuff in the current blend file
    """

    bl_idname = "flux.item_ot_setup_modeling"
    bl_label = "Flux ITEM OT Setup Modeling"
    bl_options = {"INTERNAL"}
    bl_description = ("Setup modeling related stuff in the current blend file")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):
        if not bpy.context.blend_data.filepath :
            self.report({"ERROR"}, "you need to save before")
            return {"FINISHED"}
        
        setup_task.setup_task_modeling(path=bpy.context.blend_data.filepath)

        self.report({"INFO"}, "Modeling setuped")

        return {"FINISHED"}
    
    def invoke(self, context: bpy.types.Context, event):
        return context.window_manager.invoke_confirm(self, event)


class FLUX_OT_QuitTurntable(bpy.types.Operator):
    """
    item operator
    -> Quit the turntable blend file and go in the relative item file
    """

    bl_idname = "flux.item_ot_quit_turntable"
    bl_label = "Flux ITEM OT Quit Turntable"
    bl_options = {"INTERNAL"}
    bl_description = ("Quit the turntable blend file and go in the relative item file")

    current_item: bpy.props.StringProperty(default = "default current_item")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):
        print("\n\nQuit turntable")
        basePath = bpy.context.blend_data.filepath
        file_path = "\\".join(basePath.split("\\")[:-1])
        blendfiles = []
        for item in os.listdir(file_path) :
            if item.endswith(".blend") and (file_path + "\\" + item) != basePath :
                blendfiles.append(item)

        if not blendfiles :
            return {"CANCELLED"}

        bpy.ops.wm.save_mainfile()
        bpy.ops.wm.open_mainfile(filepath=file_path + "\\" + blendfiles[0])
        #print(f"current_item = {self.current_item}\n\n")

        return {"FINISHED"}


class FLUX_OT_RenderTurntable(bpy.types.Operator):
    """
    item operator
    -> Render the turntable if the turntable blend file exist
    """

    bl_idname = "flux.item_ot_render_turntable"
    bl_label = "Flux ITEM OT Render Turntable"
    bl_options = {"INTERNAL"}
    bl_description = ("Render the turntable if the turntable blend file exist")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True
    
    def execute(self, context: bpy.types.Context):
        blend_Path = bpy.context.blend_data.filepath
        blend_name = blend_Path.split("\\")[-1]
        base_path = "\\".join(blend_Path.split("\\")[:-1])

        file_to_render = blend_Path
        
        if not "turntable" in blend_name :
            turntable_blend_name = path_from_config.replace_task_in_filename(blend_name, "turntable")
            file_to_render = base_path + "\\" + turntable_blend_name
            if not turntable_blend_name in os.listdir(base_path) :
                self.report({"ERROR"}, "Can't find the turntable blend file")
                return {"FINISHED"}
        
        if file_to_render == blend_Path :
            self.report({"INFO"}, "Turntable render launched")
            bpy.ops.render.render(animation=True, use_viewport=True)
        else :
            self.report({"INFO"}, "Turntable render launched")
            #script = f'import bpy; bpy.ops.wm.console_toggle(); print("start render from " + r"{blend_Path}"); bpy.ops.render.render(animation=True, use_viewport=True)'
            print("start render")
            #blender_subprocess.blender_render_commandline(file_path=file_to_render, blender_path = bpy.app.binary_path)

        self.report({"INFO"}, "Turntable render finished")
        os.startfile(base_path)
        return {"FINISHED"}
        


        