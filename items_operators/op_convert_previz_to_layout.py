# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Convert previz seq to layout shot
'''

import bpy
import os
import ast
import json
import shutil

from .. import config
# import ui
from ..blender_libs import render
from .. import ui_operators
from .. import ui
from .. import file_access
from .. import prefs
from .. import database_access

#reload lib
import importlib
importlib.reload(render)
importlib.reload(ui_operators)


class FLUX_OT_ConvertPrevizToLayout(bpy.types.Operator):
    """
    Convert previz seq to layout shot
    """

    bl_idname = "flux.item_ot_convert_previz_to_layout"
    bl_label = "Flux ITEM OT Convert Previz To Layout"
    bl_options = {"INTERNAL"}
    bl_description = ("Convert Previz To Layout : copy previz to layout scenes.")

    # render_path: bpy.props.StringProperty(subtype="FILE_PATH", default="None")
    # render_path_cache: bpy.props.StringProperty(subtype="FILE_PATH", default="_None")
    # render_params: bpy.props.StringProperty(default="None")
    # animation: bpy.props.BoolProperty(default=True)
    # with_metadata: bpy.props.BoolProperty(default=True)
    # openGL: bpy.props.BoolProperty(default=True)
    # increment: bpy.props.BoolProperty(default=False)
    # allow_preview_range: bpy.props.BoolProperty(default=True)
    #
    # preview_num: bpy.props.IntProperty(default=0)
    #
    # for_revision: bpy.props.BoolProperty(default=False)
    # allow_choose_for_revision: bpy.props.BoolProperty(default=True)

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):
        print("--Start convert--")

        # print(f"{self.current_item=}")
        current_file = bpy.data.filepath
        shots = bpy.context.scene.flux_ui_tool_previz_to_shot_list

        # get max for feedback
        wm = bpy.context.window_manager
        wm.progress_begin(0,100)

        # start
        i = 0
        for shot in shots:
            if not shot.shot_is_sel :
                continue

            dir = os.path.dirname(shot.shot_path)
            if not os.path.exists(dir) : os.makedirs(dir)

            # copy
            shutil.copyfile(current_file, shot.shot_path)

            # add comment
            comment = f"Layout from previz, from {os.path.basename(current_file)}"
            version = json.loads(shot.file_version)
            shot_item = json.loads(shot.shot_item)
            project_config = config.get_project_config()
            wip_status = project_config["project_data"]["status_list_default_wip"]
            # print(f"{shot_item=}")
            # print(f"{wip_status=}")
            # print(f"{formated_comment=}")
            # print(f"{shot.shot_base_path=}")
            database_access.set_formated_comment(shot_item['category'], shot_item, wip_status, comment, version, 
                                                    local=shot.shot_base_path)

            print(f"{shot.shot_name} : copy current file to : {shot.shot_path}")

            wm.progress_update(i)
            i += 1

        wm.progress_end()

        print("--End convert--")
        self.report({"INFO"}, f"Convertion done for {str(i)} shots. Open them with the item manager")
        return {"FINISHED"}
    

    def invoke(self, context: bpy.types.Context, event):

        # render_path = self.get_path()
        # print(render_path)
        # if not render_path:
        #     self.report({"ERROR"}, f"does not exist")
        #     return {"FINISHED"}

        current_item = ui.get_current_user_item(get_category=True)
        # print(f"{current_item=}")
        self.current_item = current_item

        current_shots = database_access.get_shots(current_item['seq'])
        # print(f"{current_shots=}")

        # check
        project_config = prefs.get_project_config_dict()
        previz_tag = project_config["project_data"]["previz_tag"]
        # print(f"{previz_tag=}")

        if not previz_tag in current_item['shot']:
            self.report({"ERROR"}, f"{previz_tag} should be in the name of the current shot")
            return {"FINISHED"}


        bpy.context.scene.flux_ui_tool_previz_to_shot_list.clear()
        for shot in current_shots:
            if previz_tag in shot :
                continue
            
            newShot = context.scene.flux_ui_tool_previz_to_shot_list.add()
            newShot.shot_name = shot

            shot_item = dict(current_item)
            shot_item['shot'] = shot
            # todo what if last version is not on w?
            last_version = file_access.get_item_last_version(shot_item, as_int=True)
            # print(f"{last_version=}")

            if last_version:
                last_version['w']+= 1
                version = last_version
                newShot.shot_is_already_exist = True
            else:
                version = [1,1,1]
                newShot.shot_is_already_exist = False

            shot_item['version'] = version
            newShot.file_version = json.dumps(version)
            newShot.shot_item = json.dumps(shot_item)

            shot_path = file_access.get_item_path2(shot_item)
            newShot.shot_path = shot_path[1]
            newShot.shot_base_path = shot_path[0]

            # print(f"{shot=} / {shot_path=} / {last_version=}")

        return context.window_manager.invoke_props_dialog(self, width=500)


    def draw(self, context: bpy.types.Context):
        layout = self.layout
        line = layout.column()

        line.label(text="select destination shot for copy previz", icon="FILE")

        line.template_list("FLUX_UL_ConvertPrevizShotList",  # name of UIList class containing the draw method
                          "ConvertPrevizShotList",  # unique ID
                          bpy.context.scene,  # propGroup containing the list to display
                          "flux_ui_tool_previz_to_shot_list",
                          bpy.context.scene,  # index
                          "flux_ui_tool_previz_to_shot_list_index",
                          rows=1,
                          type="DEFAULT",
                          sort_lock=True
                          )

        current_sel_shot = bpy.context.scene.flux_ui_tool_previz_to_shot_list[bpy.context.scene.flux_ui_tool_previz_to_shot_list_index]

        if current_sel_shot.shot_is_already_exist :
            line.label(text="There is already some files for this shot. Wip created :", icon="ERROR")
        else :
            line.label(text="A new wip will be created for this file", icon="FILE")
        line.label(text=os.path.basename(current_sel_shot.shot_path))

        line.separator()


class FLUX_UL_ConvertPrevizShotList(bpy.types.UIList):
    '''
    Composer List Builder
    '''
    # VGROUP_EMPTY = 1 << 0

    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):

        if self.layout_type not in {'DEFAULT', 'COMPACT'} :
            return

        row = layout.row(align=True)

        icon_load = "CHECKBOX_DEHLT"
        if item.shot_is_sel: icon_load = "CHECKBOX_HLT"
        row.prop(item, "shot_is_sel", text="", emboss=False, icon=icon_load)

        icon_exist = "CHECKMARK"
        if item.shot_is_already_exist: icon_exist = "ERROR"
        row2 = row.column()
        row2.prop(item, "shot_is_already_exist", text="", emboss=False, icon=icon_exist)
        row2.enabled = False

        row.prop(item, "shot_name", text="", emboss=False)