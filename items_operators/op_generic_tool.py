# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
generic dynamic operator
'''

import bpy
import os

from .. import blender_libs

# initialize logger
from .. import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)
importlib.reload(blender_libs)


class FLUX_OP_GeonodeSetSceneCam(bpy.types.Operator):
    """
    Loop through all geonodes to set the scene camera in relevant nodes
    """

    bl_idname = "flux.item_op_geonode_set_scene_cam"
    bl_label = "Flux ITEM OP Geonode Set Scene Cam"
    bl_options = {"INTERNAL"}
    bl_description = ("Loop through all geonodes to set the scene camera in relevant nodes")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        if len(bpy.data.node_groups) == 0 :
            return False
        return True
    
    def add_driver_single_var(self, receiver_element, receiver_str_prop, target, data_path, target_type = "OBJECT", expr = "var"):
        try :
            receiver_element.driver_remove(receiver_str_prop)
        except :
            pass
        
        newDriver = receiver_element.driver_add(receiver_str_prop).driver
        newDriver.expression = expr
        var = newDriver.variables.new()
        var.type = "SINGLE_PROP"
        var.targets[0].id_type = target_type
        var.targets[0].id = target
        var.targets[0].data_path = data_path
        
        return newDriver


    def get_data_from_label(self, node_label, context):
        label_data = node_label.split(".")[1:]
        bpy_data = context.scene
        
        for data in label_data :
            bpy_data = getattr(bpy_data, data, None)
            if bpy_data == None :
                return None
            
        return bpy_data


    def proceed_node_OBJECT_INFO(self, node, set_data):
        if node.type != "OBJECT_INFO" :
            print("error : node.type is not \"OBJECT_INFO\"")
            return False
        
        if "bpy_types.Object" not in str(type(set_data)) : 
            print("error : set_data type is not an object")
            return False
        
        for input in node.inputs :
            if input.type == "OBJECT" :
                input.default_value = set_data
        
        return True


    def proceed_node_VALUE(self, node, set_data, context):
        if node.type != "VALUE" :
            print("error : node.type is not \"VALUE\"")
            return False
        
        if type(set_data) is not int and type(set_data) is not float : 
            print("error : set_data type is not an int or a float")
            return False
        
        output = node.outputs[0]
        output.default_value = set_data
        self.add_driver_single_var(receiver_element=output, receiver_str_prop="default_value", target=context.scene, data_path=node.label.replace("flux_scene.", ""), target_type="SCENE")
        
        return True


    def proceed_node(self, node, context):
        if not node.label.startswith("flux_scene"):
            return False
        
        #print(node.label)
        #print(node.type)
        
        set_data = self.get_data_from_label(node.label, context)
        if set_data == None :
            print("Can't get data from label")
            return False
            
        #print(set_data)
        #print(type(set_data))
        
        if node.type == "OBJECT_INFO" :
            self.proceed_node_OBJECT_INFO(node, set_data)
                
        elif node.type == "VALUE" :
            self.proceed_node_VALUE(node, set_data, context)
                
        else :
            print(f"unsupported node type : {node.type}")
            
        #print()
        
        return True

    def execute(self, context: bpy.types.Context):
        if len(bpy.data.node_groups) == 0 :
            self.report({"INFO"}, "No geonodes found")
            return {"FINISHED"}
        
        there_is_no_geonode = 0

        def switch_cam_in_geonode(geonode):
            for node in geonode.nodes :
                if node.label.startswith("flux_scene") :
                    self.proceed_node(node, context)
            
            return True

        for geonode in bpy.data.node_groups :
            if geonode.type == "GEOMETRY" :
                there_is_no_geonode += 1
                switch_cam_in_geonode(geonode)

        if there_is_no_geonode == 0 :
            self.report({"INFO"}, "No geonodes found")
        else :
            self.report({"INFO"}, "All geonodes have been processed")
        return {"FINISHED"}
    

class FLUX_OP_GeonodeToggleBoundingBox(bpy.types.Operator):
    """
    Loop through all geonodes to set the scene camera in relevant nodes
    """

    bl_idname = "flux.item_op_geonode_toggle_bounding_box"
    bl_label = "Flux ITEM OP Geonode Toggle Bounding Box"
    bl_options = {"INTERNAL"}
    bl_description = ("Loop through all geonodes to toggle the bounding box option in relevant nodes")

    toggle_value: bpy.props.BoolProperty(default=True)

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        if len(bpy.data.node_groups) == 0 :
            return False
        return True
    

    def update_toggle_value_to_minority_value(self):
        v_true = 0
        v_false = 0
        for geonode in bpy.data.node_groups :
            if geonode.type != "GEOMETRY" : continue

            for node in geonode.nodes :
                if node.label.lower() != "flux_boundingbox_onoff" or node.type not in ["GROUP"] : 
                    continue

                for input in node.inputs :
                    if not (input.type == "BOOLEAN" and input.name.lower() == "Bounding Box ON/OFF".lower()) : 
                        continue
                    if type(input.default_value) is not bool :
                        continue
    
                    if input.default_value :
                        v_true += 1
                    else :
                        v_false += 1
            
        if v_true < v_false :
            self.toggle_value = True
        if v_true > v_false :
            self.toggle_value = False

    
    def proceed_node(self, node):            
        if node.type not in ["GROUP"] :
            log.info(f"unsupported node type : {node.type}")
            return

        for input in node.inputs :
            if not (input.type == "BOOLEAN" and input.name.lower() == "Bounding Box ON/OFF".lower()) : 
                continue
            
            input.default_value = self.toggle_value


    def toggle_bounding_box_in_geonode(self, geonode):
            for node in geonode.nodes :
                if node.type is not "GROUP" : continue
                if node.label.lower() == ("flux_boundingbox_onoff") or node.node_tree.name.lower() == "flux_boundingbox_onoff" :
                    self.proceed_node(node)


    def execute(self, context: bpy.types.Context):
        if len(bpy.data.node_groups) == 0 :
            self.report({"INFO"}, "No geonodes found")
            return {"FINISHED"}

        geonode_proceeded = 0

        for geonode in bpy.data.node_groups :
            if geonode.type == "GEOMETRY" :
                #print(f"\n\n_______________\n{geonode.name} :")
                geonode_proceeded += 1
                self.toggle_bounding_box_in_geonode(geonode)

        if geonode_proceeded == 0 :
            self.report({"INFO"}, "No geonodes found")
        else :
            self.report({"INFO"}, "All geonodes have been processed")
        return {"FINISHED"}
    
    
    def invoke(self, context: bpy.types.Context, event):
        self.update_toggle_value_to_minority_value()
        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context: bpy.types.Context):
        layout = self.layout
        col = layout.column()
        col.prop(self, "toggle_value", text="Value to set")
    

class FLUX_OP_OpenBlendFolder(bpy.types.Operator):
    """
    Open the folder of the current blend file
    """

    bl_idname = "flux.item_op_open_blend_folder"
    bl_label = "Flux ITEM OP Open Blend Folder"
    bl_options = {"INTERNAL"}
    bl_description = ("Open the folder of the current blend file")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True
    
    def execute(self, context: bpy.types.Context):
        filepath = bpy.context.blend_data.filepath
        if filepath == "" :
            self.report({"INFO"}, "Your file is not saved")
            return {"FINISHED"}
        filepath = filepath.split("\\")[:-1]
        filepath = "\\".join(filepath)
        os.startfile(filepath)
        return {"FINISHED"}
    

class FLUX_OT_ExportCamAlembic(bpy.types.Operator):
    """
    item operator
    -> Export the render cam in alembic
    """

    bl_idname = "flux.item_ot_export_cam_alembic"
    bl_label = "Flux ITEM OT Export Cam Alembic"
    bl_options = {"INTERNAL"}
    bl_description = (""" Export the scene render camera in alembic """)

    render_path: bpy.props.StringProperty(subtype="FILE_PATH", default="_None")
    overwrite: bpy.props.BoolProperty(default=False)
    openfolder: bpy.props.BoolProperty(default=False)
    clip_modifier: bpy.props.IntProperty(default=1)
    global_scale: bpy.props.FloatProperty(default=1.0)

    scene_cam = None

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool :
        if context.scene.camera :
            return True
        return False
    
    def execute(self, context: bpy.types.Context) :
        print("Start Exporting Camera")
        
        init_selection = blender_libs.get_current_selection(context)
        blender_libs.set_selection([self.scene_cam])

        init_cam_clip = [self.scene_cam.data.clip_start, self.scene_cam.data.clip_end]
        self.scene_cam.data.clip_start *= self.clip_modifier
        self.scene_cam.data.clip_end *= self.clip_modifier

        blendfile_path = context.blend_data.filepath
        if not blendfile_path :
            self.report({'ERROR'}, 'You need to have your blendfile saved !')
            return {"CANCELLED"}
        
        root_folder = blendfile_path.split("\\")[:-1]
        root_folder = "\\".join(root_folder)
        file_name = blendfile_path.split("\\")[-1].replace(".blend", "")
        filepath = f"{file_name}_camera_"
        filepath = os.path.join(root_folder, filepath)
        version_num = 1
        ext = "abc"

        while os.path.exists(f"{filepath}{version_num:02d}.{ext}") :
            version_num += 1

        # increment
        if self.overwrite and version_num > 1:
            version_num -=1

        filepath = f"{filepath}{version_num:02d}.{ext}"

        print(filepath)

        if not filepath :
            return {"FINISHED"}
        
        bpy.ops.wm.alembic_export(filepath=filepath, selected=True, uvs=False, 
                                  packuv=False, normals=False, vcolors=False, 
                                  orcos=False, use_instancing=True, global_scale=self.global_scale, 
                                  export_hair=False, export_particles=False, 
                                  export_custom_properties=False)

        # reset changes
        self.scene_cam.data.clip_start = init_cam_clip[0]
        self.scene_cam.data.clip_end = init_cam_clip[1]
        blender_libs.set_selection(init_selection)

        if self.openfolder :
            dir_path = "\\".join(filepath.split("\\")[:-1])
            #os.startfile(render_path + ".mov")
            os.startfile(dir_path)
        
        return {"FINISHED"}
    
    
    def invoke(self, context: bpy.types.Context, event):
        self.scene_cam = context.scene.camera
        return context.window_manager.invoke_props_dialog(self)


    def draw(self, context: bpy.types.Context):
        layout = self.layout
        col = layout.column()
        col.label(text=f"Camera to export : {self.scene_cam.name}")
        col.prop(self, "overwrite", text="overwrite last version")
        col.prop(self, "openfolder", text="open folder after export")    
        col.prop(self, "clip_modifier", text="multiply clip near and far")    
        col.prop(self, "global_scale", text="Alembic global_scale factor")    