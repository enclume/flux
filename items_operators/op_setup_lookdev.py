# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
lookdev related operator
'''

import bpy

from . import setup_task

#reload lib
import importlib
importlib.reload(setup_task)


class FLUX_OT_SetupLookdev(bpy.types.Operator):
    """
    item operator
    -> Setup the lookdev file of the current item
    """

    bl_idname = "flux.item_ot_setup_lookdev"
    bl_label = "Flux ITEM OT Setup Lookdev"
    bl_options = {"INTERNAL"}
    bl_description = ("Setup the lookdev file of the current item")

    current_item: bpy.props.StringProperty(default = "default current_item")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):
        print("\n\Setup Lookdev :")
        # TODO hard coded
        lookdev_master_filepath = r"W:\01_PRODUCTIONS\012_RUFUS\1_PRE\12_MS\RF_MS_0060_ms_lookdev\01_TEMPLATE\v01\RF_MS_0060_ms_lookdev_template_v01.blend"
        setup_task.setup_task_surfacing(lookdev_master_filepath=lookdev_master_filepath)
        
        self.report({"INFO"}, "Lookdev setuped")

        return {"FINISHED"}