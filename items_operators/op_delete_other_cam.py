# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Convert previz seq to layout shot
'''

import bpy
import os
import ast
import json
import shutil

from .. import config
# import ui
from ..blender_libs import render
from .. import ui_operators
from .. import ui
from .. import file_access
from .. import prefs
from .. import database_access

#reload lib
import importlib
importlib.reload(render)
importlib.reload(ui_operators)


class FLUX_OT_DeleteOtherCams(bpy.types.Operator):
    """
    Convert previz seq to layout shot
    """

    bl_idname = "flux.item_ot_delete_other_cams"
    bl_label = "Flux ITEM OT Delete Other Cam"
    bl_options = {"INTERNAL"}
    bl_description = ("Delete unselected cam in scene")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):
        # get sel objs
        selected_objects = bpy.context.selected_objects
        # print(selected_objects)
        for each in selected_objects:
            if each.type != "CAMERA":
                self.report({'ERROR'}, 'Please select only camera to keep')
                return {"CANCELLED"}

        if not selected_objects:
            self.report({'ERROR'}, 'Select at least one cam')
            return {"CANCELLED"}

        all_cams = [ob for ob in list(bpy.context.scene.objects) if ob.type == 'CAMERA']

        print(all_cams)

        i = 0
        for each in list(all_cams):
            if each not in selected_objects:
                print("Delete : ", each.name)
                bpy.data.objects.remove(each, do_unlink=True)
                i += 1

        self.report({"INFO"}, f"Delete {str(i)} cameras, keep {selected_objects}")
        return {"FINISHED"}

