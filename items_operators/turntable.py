# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
turntable related stuff
'''

import bpy
import os

from .. import blender_libs
try:
    import config.path as path_from_config
except:
    from ..config import path as path_from_config

# initialize logger
from .. import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)
importlib.reload(blender_libs)
importlib.reload(path_from_config)


def make_turntable(basePath: str, turntable_master_filepath: str, light_master_filepath: str) :
    '''
    /!\ OLD METHODE -> setup in another file
    Setup the current blend file into a turntable blend file

    Args:
        basePath : the file path of the initial blend file
        turntable_master_filepath : the file path of the master blend file

    Return:
        True if the turntable blend file is succesfully setuped
    '''

    #save current file
    bpy.ops.wm.save_mainfile()

    #process file naming
    baseName = basePath.split("\\")[-1]
    turntable_filePath = "\\".join(basePath.split("\\")[:-1])
    turntable_name = path_from_config.replace_task_in_filename(baseName, "turntable")

    # save as review [TODO]
    # (save as version)

    # save as turntable
    # (save turntable in review) [TODO, -> and think to update the version turntable]
    if baseName != turntable_name :
        bpy.ops.wm.save_as_mainfile(filepath = turntable_filePath + "\\" + turntable_name)
        
    # make current file a real turntable (import turntable scene, etc.)      
    mod_scene = bpy.context.scene
    turn_scene = bpy.data.scenes.get("Scene_turn")
           
    if not turn_scene :
        mod_scene.name = "Scene_mod"
        turn_scene = blender_libs.scene.append(turntable_master_filepath, "Scene")
        turn_scene.name = "Scene_turn"
        bpy.context.window.scene = turn_scene

        linked_light = blender_libs.collection.link(light_master_filepath, "LIGHT_MODEL", "LIGHT_SET")   
        turn_scene.world = blender_libs.world_link(light_master_filepath, "world_model")
        
    blender_libs.scene.flux_ui_data_transfer(mod_scene, turn_scene)    
    col_model = blender_libs.collection.pack_all_in("ASSET_COL", col_exclude = ["TURNTABLE_COL", "LIGHT_SET"], scenefrom = mod_scene, sceneto = turn_scene)
        
        
    # find cam position
    cam = bpy.data.objects["preview_cam"]
    cam_new_loc = blender_libs.find_cam_location(cam = cam, asset_collection = bpy.data.collections["ASSET_COL"])
    
    scale_factor = cam_new_loc.length - cam.location.length
    linked_light.scale *= ((scale_factor - 1) * 0.1) + 1
    cam.location = cam_new_loc

    # parent asset obj to turn_pivot
    for obj in col_model.all_objects :
        if not obj.parent :
            obj.parent = bpy.data.objects["turntable_pivot"]
        
    # set file output path
    turntable_mp4_filepath = turntable_filePath + "\\" + turntable_name.replace(".blend", ".mp4")
    turn_scene.render.filepath = turntable_mp4_filepath

    #save current file
    bpy.ops.wm.save_mainfile()

    return turntable_mp4_filepath


def setup_turntable(turntable_master_filepath: str, light_master_filepath: str, basePath: str = None, find_cam_pos: bool = False) :
    '''
    Setup the current blend file into a turntable blend file

    Args:
        - turntable_master_filepath : the file path of the turntable master blend file
        - light_master_filepath : the file path of the light master blend file
        - basePath : the file path of the initial blend file
        - find_cam_pos : if True, find a better position for the camera

    Return:
        True if the turntable blend file is succesfully setuped
    '''
    #save current file
    #bpy.ops.wm.save_mainfile()


    mod_scene = bpy.context.scene
    initial_scene_name: str = mod_scene.name
    if not mod_scene.name.endswith("_OLD") :
        mod_scene.name = initial_scene_name + "_OLD"

    master_col = blender_libs.collection.set_master(scene = mod_scene, path = basePath)

    turn_col = blender_libs.collection.get("Turntable")
    turn_col.color_tag = "COLOR_01"

    turn_scene = bpy.data.scenes.get(initial_scene_name)
           
    if not turn_scene :
        turn_scene = blender_libs.scene.append(turntable_master_filepath, "Scene")
        turn_scene.name = initial_scene_name
        bpy.context.window.scene = turn_scene

        linked_light = blender_libs.collection.link(light_master_filepath, "LIGHT_MODEL", "LIGHT_SET")  
        linked_light.parent = bpy.data.objects["turntable_pivot"]
        turn_scene.world = blender_libs.world_link(light_master_filepath, "world_model")
   
    blender_libs.scene.flux_ui_data_transfer(mod_scene, turn_scene) 

    turn_scene.collection.children.link(turn_col)

    for col in turn_scene.collection.children :
        if col.name == turn_col.name : continue
        turn_col.children.link(col)
        turn_scene.collection.children.unlink(col)

    turn_scene.collection.children.link(master_col)
    bpy.data.scenes.remove(mod_scene)

        
    # find cam position
    if find_cam_pos :
        cam = bpy.data.objects["preview_cam"]
        cam_new_loc = blender_libs.find_cam_location(cam = cam, asset_collection = master_col)
        
        scale_factor = cam_new_loc.length - cam.location.length
        linked_light.scale *= ((scale_factor - 1) * 0.1) + 1
        cam.location = cam_new_loc


    # parent asset obj to turn_pivot
    '''
    for obj in col_model.all_objects :
        if not obj.parent :
            obj.parent = bpy.data.objects["turntable_pivot"]
    '''
        
    # set file output path
    render_filepath: str = bpy.context.blend_data.filepath
    turn_scene.render.filepath = render_filepath.replace(".blend", "")
    return True

    
    
    
if __name__ == '__main__' :
    os.system("cls")

    print("\n---START---\n")
    
    basePath = bpy.context.blend_data.filepath
    turntable_master_filepath = r"W:\01_PRODUCTIONS\012_RUFUS\1_PRE\12_MS\RF_MS_0386_ms_turntable\01_TEMPLATE\v01\RF_MS_0386_ms_turntable_template_v01.blend"
    light_master_filepath = r"W:\01_PRODUCTIONS\012_RUFUS\1_PRE\12_MS\RF_MS_0387_ms_lightset\01_TEMPLATE\v01\RF_MS_0387_ms_lightset_template_v01.blend"
    
    make_turntable(basePath = basePath, turntable_master_filepath = turntable_master_filepath, light_master_filepath = light_master_filepath)
    #TODO : render the turntable
        
    print("\n----END----\n")
