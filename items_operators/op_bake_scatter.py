# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
exemple of dynamic operator
'''
import bpy
import bmesh
import mathutils
import math
import os
import time

from .. import file_access
from .. import blender_libs


import importlib
importlib.reload(file_access)
importlib.reload(blender_libs)


def rename_collection_to_lod(name, lod=True):
    """
    find lod collection from not lod collection
    Args:
        name: name to parse

    Returns:lod collection
    """

    name_split = name.split("_")
    # if ("MESHES" in name or "lookdev" in name) and lod:
    #     name_split[2] = "0000"
    #     result = "_".join(name_split)
    # else:
    result = name

    # if lod:
    #     if not "_lod" in result:
    #         if "_MESHES" in result:
    #             result = result.replace("_MESHES", "_lod5_MESHES")
    #         if "_lookdev" in result:
    #             result = result.replace("_lookdev", "_lod5_lookdev")
    result = result.replace("_#AXIS", "")

    return result


def get_obj_under_col(col):
    """
    get first object under collection
    Args:
        col: given collection

    Returns:first object founded

    """

    objs = list(col.objects)
    while len(objs) == 0:

        col = list(col.children)[0]
        objs = list(col.objects)

    return objs[0]


class FLUX_OP_ImportTriaxis(bpy.types.Operator):
    """
    Import triaxis on the scene
    """

    bl_idname = "flux.bakescatter_import_triaxis"
    bl_label = "Flux ITEM OP Bake Scatter Import tri axis"
    bl_options = {"INTERNAL"}
    bl_description = ("Bake scatter")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):

        path = context.scene.flux_ui_bakescatter_triaxisPath

        if path[0:1] != "@":
            raise Exception("OS path are Not supported Yet")
        
        path = path[1:]
        path_array = path.split("/")

        current_item = {}

        current_item["category"] = path_array[0]
        current_item["type"] = path_array[1]
        current_item["name"] = path_array[2]
        current_item["task"] = path_array[3]
        current_item["version"] = [1]

        triaxis_filePath = file_access.get_item_path2(current_item, only_file_path=True)

        print(triaxis_filePath)

        if os.path.exists(triaxis_filePath):

            # set active top one
            scene_collection = bpy.context.view_layer.layer_collection
            bpy.context.view_layer.active_layer_collection = scene_collection

            result = blender_libs.import_item(item_data=current_item, link=False,
                                                make_lib_override=False, parent_col=None)
            print(f"{result=}")

            bpy.context.scene.flux_ui_bakescatter_triaxisCollection = result.name            

        return {"FINISHED"}


class FLUX_OP_SwitchGeoToTriaxis(bpy.types.Operator):
    """
    switch geo to tri axis on the geonode
    """

    bl_idname = "flux.bakescatter_switch_geo_triaxis"
    bl_label = "Flux ITEM OP Bake Scatter switch geo to triaxis"
    bl_options = {"INTERNAL"}
    bl_description = ("Bake scatter")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):

        # Replace 'YourObjectName' with the name of the object you want to work with

        obj = bpy.context.scene.flux_ui_bakescatter_geonodeObj
        if not obj:
            self.report({"ERROR"}, "Enter a geonode obj")
            return {"CANCELLED"}

        base_name = "Triaxis_"

        if obj is None:
            print(f"Object with the name '{obj}' not found.")
            return {"FINISHED"}
        
        
        # Iterate through the object's modifiers
        for modifier in obj.modifiers:
            if modifier.type != 'NODES':
                continue
            
            # Check if the modifier is a Geometry Nodes modifier
            if modifier.node_group is None:
                print("This modifier doesn't have a node group.")
                continue

            
            # Get the list of nodes in the Geometry Nodes modifier
            nodes = modifier.node_group.nodes
            # Iterate through the nodes and display their name and type
            i = 0
            to_switch = []
            for node in nodes:
                # go for all collections infos
                if node.type != "COLLECTION_INFO":
                    continue
                
                # print(node.name, node.inputs[0].default_value, node.inputs[1].name,
                #       node.inputs[1].default_value)
                j = 0
                print(f"--> call for : {i} - {node.inputs[0].default_value}")
                # if separate children is ok
                if node.inputs[1].default_value:
                    col = node.inputs[0].default_value
                    # print(list(col.all_objects))

                    for child in list(col.all_objects):
                        print(f"    --> child : {j} - {child}")
                        # print(f"    -->       : {j} - {child.instance_type}")
                        j+=1

                else:
                    j=1

                # delete unwanted objs
                for j_del in range(j,10):
                    # print(f"         --> delette {j_del=}")
                    to_remove = bpy.data.objects[base_name + str(i) + str(j_del)]
                    bpy.data.objects.remove(to_remove, do_unlink=True)

                # --------------------------
                # rename tri axis collection
                col_tri = bpy.data.collections.get(base_name + str(i))
                col_tri.name = node.inputs[0].default_value.name + "_#AXIS"
                # print(f"---------------- {col_tri=}")

                # if separate childrem == True, then rename the childs
                if node.inputs[1].default_value:
                    print("      ", col_tri ,  "is group, childs will be renamed")
                    col_no_meshes = bpy.data.collections[node.inputs[0].default_value.name]
                    sub_objs_of_orig = col_no_meshes.all_objects
                    sub_objs_of_axis = col_tri.objects

                    # print(f"{sub_objs_of_orig=}")
                    # print(f"{sub_objs_of_axis=}")

                    # process children
                    for id_ob, each in enumerate(list(sub_objs_of_axis)):
                        each.name = sub_objs_of_orig[id_ob].name + "_#AXIS"

                # iterate
                to_switch.append([node, col_tri])
                i += 1

            # delete unwantned slot
            print("start deleting all from ", i)
            for i_del in range(i, 30):
                for j_del in range(10):
                    to_remove = bpy.data.objects[base_name + str(i_del) + str(j_del)]
                    bpy.data.objects.remove(to_remove, do_unlink=True)

                to_remove = bpy.data.collections[base_name + str(i_del)]
                bpy.data.collections.remove(to_remove)

            # finally replace
            print("replace collection infos inputs...")
            for each in to_switch:
                print("  > ", each)
                each[0].inputs[0].default_value = each[1]

        return {"FINISHED"}


class FLUX_OP_BakeTriAxis(bpy.types.Operator):
    """
    bake tri axis on node group
    """

    bl_idname = "flux.bakescatter_bake_triaxis"
    bl_label = "Flux ITEM OP Bake Scatter switch geo to triaxis"
    bl_options = {"INTERNAL"}
    bl_description = ("Bake scatter")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):
        obj = bpy.context.scene.flux_ui_bakescatter_geonodeObj
        # print(obj)
        modifier_to_apply = False

        if obj is None:
            print(f"Object with the name '{obj}' not found.")
            self.report({"ERROR"}, f"Give an object ('{obj}')")
            print("BAKED")
            return {"FINISHED"}
        
        
        # Iterate through the object's modifiers
        for modifier in obj.modifiers:
            if modifier.type != 'NODES':
                continue
            
            # Check if the modifier is a Geometry Nodes modifier
            if modifier.node_group is None:
                print("This modifier doesn't have a node group.")
                self.report({"ERROR"}, "This modifier doesn't have a node group.")
                continue

            modifier_to_apply = modifier

            # Get the list of nodes in the Geometry Nodes modifier
            nodes = modifier.node_group.nodes
            # Iterate through the nodes and display their name and type
            i = 0
            to_switch = []
            for node in nodes:
                # go for all collections infos
                # print(node)
                if node.type != "GROUP":
                    continue
               
                all_input = list(node.inputs)
                # print(all_input)

                idx = None
                for i, each in enumerate(all_input):
                    if each.name == "Realize Instance":
                        # print(each, i)
                        idx = i

                if not idx == None:
                    node.inputs[idx].default_value = True


        if modifier_to_apply:
            bpy.ops.object.select_all(action='DESELECT')
            bpy.context.view_layer.objects.active = obj
            bpy.ops.object.modifier_apply(modifier=modifier_to_apply.name)

        print("BAKED")
        return {"FINISHED"}


class FLUX_OP_PrintLod(bpy.types.Operator):
    """
    exemple of blender operator to create button on item operator UI
    Im sorry, Im not sure im in good shape here, mother dying... code is messy

    UNUSED NOW. KEEP IT FOR BACKUP.

    """

    bl_idname = "flux.bakescatter_printlod"
    bl_label = "Flux ITEM OP Bake Scatter do it dude"
    bl_options = {"INTERNAL"}
    bl_description = ("Bake scatter")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):

        single_obj_to_dup = False
        try:
            single_obj_to_dup = bpy.data.objects['PlaneOut']
        except:
            pass

        # -----------------------------
        # if we go for the all complex experience, not only one object to duplicate
        # objs_tu_dup = []
        print("===PRINT OBJ LOD TO IMPORT===")
        print("===and add sub collection to scene collection used as group===")
        if single_obj_to_dup :
            return {"FINISHED"}
        
        need_to_import = []
        import_parent = []
        main_col = bpy.data.collections[bpy.context.scene.flux_ui_bakescatter_triaxisCollection]

        for child in list(main_col.children):
            print(f"process scanning of : {child=}")
            # if only one subchilds
            if "MESHES" in child.name:
                name_to_dup = child.name
                name_to_dup = rename_collection_to_lod(name_to_dup)
                need_to_import.append(name_to_dup)
                import_parent.append("")

            # grp is linked obj
            elif "grp_" in child.name:
                sub_childs_of_parents = list(child.all_objects)
                # print(f"{sub_childs_of_parents=}")

                temp = []
                for each in sub_childs_of_parents:
                    temp.append(each.name)
                sub_childs_of_parents = temp

                # print(f"{sub_childs_of_parents=}")

                for e in sub_childs_of_parents:
                    # print(">", e.name)
                    # new = rename_collection_to_lod(e.name)
                    need_to_import.append(rename_collection_to_lod(e))
                    import_parent.append(rename_collection_to_lod(child.name))

            # usergrp is user group where more objets are
            elif "USERGRP_" in child.name:
                # print("user grp", child)
                sub_childs_of_parents = list(child.all_objects)
                # print(f"{sub_childs_of_parents=}")

                temp = []
                for each in sub_childs_of_parents:
                    temp.append(each.name)
                sub_childs_of_parents = temp

                # print(f"{sub_childs_of_parents=}")

                for e in sub_childs_of_parents:
                    # print(">", e.name)
                    # new = rename_collection_to_lod(e.name)
                    need_to_import.append(rename_collection_to_lod(e, lod=False))
                    import_parent.append(rename_collection_to_lod(child.name, lod=False))

            else:
                print("You shouldnt be here")

            # else:
            #     name_of_parent = child.name
            #     name_of_parent = rename_collection_to_lod(name_of_parent)
            #     col_of_parent = bpy.data.collections.get(name_of_parent)
            #
            #     sub_childs_of_parents = list(col_of_parent.children)
            #
            #     # print(f"{sub_childs_of_parents=}")
            #     #check if lod is already in
            #     lod_present = False
            #     if len(sub_childs_of_parents):
            #         if "lod5" in sub_childs_of_parents[0].name:
            #             lod_present = True
            #
            #     # =============
            #     if lod_present:
            #         for each in sub_childs_of_parents:
            #             need_to_import.append(each.name)
            #             import_parent.append("")
            #     else:
            #         if not sub_childs_of_parents:
            #             sub_childs_of_parents = list(col_of_parent.objects)
            #         else:
            #             temp = []
            #             for each in sub_childs_of_parents:
            #                 obj_under = get_obj_under_col(each)
            #                 temp.append(obj_under)
            #
            #             sub_childs_of_parents = temp
            #
            #         print("  APPEND MULT > ", list(sub_childs_of_parents))
            #         for e in list(sub_childs_of_parents):
            #             # print(">", e.name)
            #             new = rename_collection_to_lod(e.name)
            #             need_to_import.append(new)
            #             import_parent.append(name_of_parent)

        # print("---")
        # print(f"{need_to_import=}")
        # print(f"{import_parent=}")

        futur_parent = []
        for id, each in enumerate(need_to_import):
            col = bpy.data.collections.get(each)

            if "grp_" in import_parent[id]:
                col = bpy.data.objects.get(each)
            else:
                col = bpy.data.collections.get(each)

            if not col == None:
                if import_parent[id]:
                    print(">> already in scene: ", each, "  > Will be child of : ", import_parent[id])
                    if import_parent[id] not in futur_parent:
                        futur_parent.append(import_parent[id])

                else:     print(">> already in scene: ", each)
            else:
                if import_parent[id]:
                    print(">> ++ You need to import : ", each, "  > Will be child of : ", import_parent[id])
                else : print(">> ++ You need to import : ", each)

            # that was on the old scene, where I needed to manually replace the new optimized objects
            # place in the futur parent collection f any
            # print(f"{futur_parent=}")
            # for each in futur_parent:
            #     print("care of putting children under ", each)
            #     col_each = bpy.data.collections.get(each)
            #     # print(col_each)
            #     child_is_col = True
            #     childs = col_each.children
            #     if not childs:
            #         child_is_col = False
            #         childs = col_each.objects
            #     # print(list(childs))
            #     # childs = list(childs).reverse()
            #     for child in childs:
            #         # print("==")
            #         # print(child)
            #         child_lod = rename_collection_to_lod(child.name)
            #         child_lod_col = bpy.data.collections.get(child_lod)
            #         # print(child_lod)
            #         # print(child_lod_col)
            #         col_each.children.link(child_lod_col)
            #
            #         # unlink old collection
            #         if child_is_col:
            #             col_each.children.unlink(child)
            #             context.scene.collection.children.link(child)
            #         else:
            #             col_each.objects.unlink(child)
            #             context.scene.collection.objects.link(child)

        return {"FINISHED"}


class FLUX_OP_BakeScatter(bpy.types.Operator):
    """
    finally bake the scatter
    """

    bl_idname = "flux.bakescatter_do"
    bl_label = "Flux ITEM OP Bake Scatter do it dude"
    bl_options = {"INTERNAL"}
    bl_description = ("Bake scatter")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):
        # get current mesh
        current_mesh = bpy.context.scene.flux_ui_bakescatter_geonodeObj.data
        print(current_mesh)

        # create empty bmesh, add current mesh into empty bmesh
        current_bm = bmesh.new()
        current_bm.from_mesh(current_mesh)
        # look up table
        current_bm.verts.ensure_lookup_table()
        current_bm.faces.ensure_lookup_table()

        # if PlaneOut exist, for test purpose
        single_obj_to_dup = False
        try:
            single_obj_to_dup = bpy.data.objects['PlaneOut']
        except:
            pass

        # -----------------------------
        # if we go for the all complex experience, not only one object to duplicate
        objs_tu_dup = []
        print("===TRY TO RECREATE THE CORRECT TABLE OF OBJECTS TO DUP===")
        if not single_obj_to_dup:
            main_col = bpy.data.collections[bpy.context.scene.flux_ui_bakescatter_triaxisCollection]
            # print(main_col)
            # print(main_col.children)
            for child in list(main_col.children):
                print("--> ", child.name)

                sub_childs = list(child.objects)
                # if only one subchilds, no separate_children
                if len(sub_childs) == 1:
                    name_to_dup = rename_collection_to_lod(child.name)
                    col_to_dup = bpy.data.collections.get(name_to_dup)
                    obj_under = get_obj_under_col(col_to_dup)

                    print("   - APPEND name > ", name_to_dup)
                    print("   - APPEND obj  > ", obj_under)

                    objs_tu_dup.append([obj_under])
                # if more than one, then we go to look for childs, => separate_children
                else:
                    name_of_parent = rename_collection_to_lod(child.name)
                    # print(f"{child=}")
                    # print(f"{name_of_parent=}")
                    col_of_parent = bpy.data.collections.get(name_of_parent)

                    sub_childs_of_parents = list(col_of_parent.children)
                    # print(f"first {sub_childs_of_parents=}")
                    # then its objects
                    if not sub_childs_of_parents:
                        sub_childs_of_parents = list(col_of_parent.objects)
                    else:
                        temp = []
                        for each in sub_childs_of_parents:
                            obj_under = get_obj_under_col(each)
                            temp.append(obj_under)
                        sub_childs_of_parents = temp

                    print("   - APPEND MULT > ", list(sub_childs_of_parents))

                    objs_tu_dup.append(list(sub_childs_of_parents))

            print("--obj to dup--")
            for idup, each in enumerate(objs_tu_dup) :
                if len(each) == 1 :
                    print(f"V={idup} : {each}")
                    continue
                
                print(f"V={idup} : ")
                for jdup, eachsub in enumerate(each) :
                    print(f"   => U={jdup} : {eachsub}")

        # return {"FINISHED"}
        #----
        # get location of face (atm it relative to object origin)
        vertices = current_bm.verts
        faces = current_bm.faces
        uv_layer = current_bm.loops.layers.uv.verify()
        #
        counter = 0
        tot = 100
        length = len(vertices)/4

        # output
        msg = f"SCATTER BAKER : go for {int(length)} duplications..."
        print(msg)
        self.report({'INFO'}, msg)


        # progress bar
        wm = bpy.context.window_manager

        # progress from [0 - 1000]
        wm.progress_begin(0, tot)
        prog_i = 1
        prog_j = 1

        then = time.time()  # Time before the operations start

        # new ones
        new_name = "dup"
        # New Collection
        dup_coll = bpy.data.collections.new("Instances")
        # Add collection to scene collection
        bpy.context.scene.collection.children.link(dup_coll)

        # deselect to be sure
        bpy.ops.object.select_all(action='DESELECT')

        for i, vert in enumerate(vertices):
            if i % 4 != 0 :
                continue

            if not single_obj_to_dup:
                # first get uv coordinate
                face = faces[counter*3]
                loop = face.loops[0]
                loop_uv = loop[uv_layer]
                u = int(loop_uv.uv[0]*10)
                v = int(loop_uv.uv[1]*10)
                # print(f"{i=} - {vert.co=} - {loop_uv.uv} - VU = {v}-{u}")
                bpy.context.scene.cursor.location = vert.co
                # duplicate new object

                # print(f"{objs_tu_dup[v][u]=}")
                # print(f"{objs_tu_dup[v][u].instance_type == 'COLLECTION':=}")

                new_name = objs_tu_dup[v][u].name + "_INST"
                # if its a collection
                if objs_tu_dup[v][u].instance_type == 'COLLECTION':

                    # print(objs_tu_dup[v][u])

                    dup = blender_libs.object.instance_col(objs_tu_dup[v][u].name, new_name + str(v) + str(u), dup_coll)
                # if not, just usual
                else:
                    dup = blender_libs.object.instance(objs_tu_dup[v][u], new_name + str(v) + str(u), dup_coll)
            else:
                dup = blender_libs.object.instance(single_obj_to_dup, new_name + str(counter), dup_coll)

            # --------------------------------------------
            # translation
            dup.location[0] = vert.co.x
            dup.location[1] = vert.co.y
            dup.location[2] = vert.co.z

            # --------------------------------------------
            # deal with rotation
            # orig
            point_0 = vert.co
            point_z = vertices[i+1].co
            point_x = vertices[i+2].co
            point_y = vertices[i+3].co
            # deducte vectors
            vector_x = mathutils.Vector(point_x) - mathutils.Vector(point_0)
            vector_y = mathutils.Vector(point_y) - mathutils.Vector(point_0)
            vector_z = mathutils.Vector(point_z) - mathutils.Vector(point_0)
            # prepapre scale
            theo_scale = vector_x.length

            # normalize them
            vector_x.normalize()
            vector_y.normalize()
            vector_z.normalize()

            # rotation
            rotation_matrix = mathutils.Matrix(
                (
                    (vector_x.x, vector_y.x, vector_z.x, 0),
                    (vector_x.y, vector_y.y, vector_z.y, 0),
                    (vector_x.z, vector_y.z, vector_z.z, 0),
                    (0, 0, 0, 1)
                )
            )

            # Convert the rotation matrix to Euler angles
            # go to quaternion to be more efficient
            quat_rotation = rotation_matrix.to_quaternion()
            dup.rotation_mode = 'QUATERNION'
            dup.rotation_quaternion[0] = quat_rotation[0]
            dup.rotation_quaternion[1] = quat_rotation[1]
            dup.rotation_quaternion[2] = quat_rotation[2]
            dup.rotation_quaternion[3] = quat_rotation[3]

            # --------------------------------------------
            # scale
            # print(theo_scale)
            if abs(theo_scale - 1) > 0.01:
                dup.scale[0] = theo_scale
                dup.scale[1] = theo_scale
                dup.scale[2] = theo_scale

            # counter
            counter += 1
            prog = (counter * tot) / length
            prog = int(prog)
            wm.progress_update(prog)

            # diaplay on console for safety
            if prog > (prog_i*10)*prog_j:

                # percent done (i know its the same up!
                percent = (prog_i*10)*prog_j
                # now time
                now = time.time()  # Time after it finished
                # execution time
                time_exec = now - then
                # left time
                time_left = time_exec * (100/percent)

                # output
                msg = f" > DONE : {percent}%, in {round(time_exec,2)}s, (eta :{round(time_left,2)}s)"
                print(msg)
                # self.report({'INFO'}, msg)
                prog_j += 1

        # progres end
        wm.progress_end()
        # free bmesh
        current_bm.to_mesh(current_mesh)
        current_bm.free()

        # prin t and time output
        now = time.time()
        time_exec = now - then

        # ---- delete
        bpy.data.objects.remove(bpy.context.scene.flux_ui_bakescatter_geonodeObj, do_unlink=True)
        bpy.data.meshes.remove(current_mesh)

        # blender_libs.collection.delete_hierarchy(bpy.data.collections[bpy.context.scene.flux_ui_bakescatter_triaxisCollection])

        # group from instances
        dup_coll.color_tag='COLOR_04'

        # group collection to delete
        # create the to delete collection in case I have to remove the delete for crash
        bpy.data.collections[bpy.context.scene.flux_ui_bakescatter_triaxisCollection].color_tag='COLOR_01'
        del_col = bpy.data.collections.new("TO_DELETE")
        bpy.context.scene.collection.children.link(del_col)
        bpy.context.scene.collection.children.unlink(bpy.data.collections[bpy.context.scene.flux_ui_bakescatter_triaxisCollection])
        del_col.children.link(bpy.data.collections[bpy.context.scene.flux_ui_bakescatter_triaxisCollection])
        del_col.color_tag='COLOR_01'

        # delete
        blender_libs.collection.delete_hierarchy_recursive(del_col)

        # output
        msg = f"Processed {counter} objects, in {round(time_exec, 2)}s"
        print(msg)
        self.report({'INFO'}, msg)

        return {"FINISHED"}
    

class FLUX_OP_Constraint_Billboard(bpy.types.Operator):
    """
    constraint the billboard to the camera
    """

    bl_idname = "flux.bakescatter_constraint_billoboard"
    bl_label = "Flux ITEM OP Constraint Billboard"
    bl_options = {"INTERNAL"}
    bl_description = ("Bake scatter")

    just_delete: bpy.props.BoolProperty(
        default=False)

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):

        # set objs
        camera = bpy.context.scene.flux_ui_camera_lod_constraint
        inst_col = bpy.data.collections[bpy.context.scene.flux_ui_camera_lod_instance_col]
        pattern = bpy.context.scene.flux_ui_camera_lod_pattern

        # all objects under instance
        instances = inst_col.objects

        # list
        for inst in instances:
            if pattern not in inst.name :
                continue
            
            # print(inst.constraints)

            # remove old constraint
            ctrs = inst.constraints
            for ctr in list(ctrs):
                inst.constraints.remove(ctr)

            if not self.just_delete:
                # add new one
                constraint = inst.constraints.new(type='LOCKED_TRACK')
                constraint.target = camera

        return {"FINISHED"}


class FLUX_OP_PlayAll(bpy.types.Operator):
    """
    constraint the billboard to the camera
    """

    bl_idname = "flux.bakescatter_play_all"
    bl_label = "Flux ITEM OP Constraint Billboard"
    bl_options = {"INTERNAL"}
    bl_description = ("Bake scatter")

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def execute(self, context: bpy.types.Context):

        obj = bpy.context.scene.flux_ui_bakescatter_geonodeObj
        if not obj:
            self.report({"ERROR"}, "Enter a geonode obj")
            return {"CANCELLED"}

        bpy.ops.flux.bakescatter_import_triaxis()
        bpy.ops.flux.bakescatter_switch_geo_triaxis()
        bpy.ops.flux.bakescatter_bake_triaxis()
        bpy.ops.flux.bakescatter_do()

        return {"FINISHED"}