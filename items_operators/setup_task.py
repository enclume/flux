# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
setup a task in a given blend file
'''

import bpy
import os

from .. import blender_libs
from .. import config
from . import turntable

# initialize logger
from .. import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)
importlib.reload(blender_libs)
importlib.reload(config)
importlib.reload(turntable)


def setup_task_end() :
    bpy.ops.file.make_paths_absolute()
    return True


def setup_task_generic(path: str = None):
    ''' setup the task in a generic way '''

    log.debug("start setuping a generic file in a generic way")
    blender_libs.collection.set_master(path = path)
    return setup_task_end()


def setup_task_modeling(path: str = None):
    ''' setup the modeling task '''

    log.debug("start setuping a modeling (and uvs) file")
    master_col = blender_libs.collection.set_master(path = path)
    mesh_col_name = master_col.name.split("_")
    mesh_col_name[-1] = "MESHES"
    mesh_col_name = "_".join(mesh_col_name)
    mesh_col = blender_libs.collection.get(mesh_col_name)

    try :
        master_col.children.link(mesh_col)
    except :
        log.debug("mesh_col linking to master_col.children failed (maybe already linked)")

    for col in master_col.children :
        if col.name != mesh_col.name :
            mesh_col.children.link(col)
            master_col.children.unlink(col)
    
    for obj in master_col.objects :
        mesh_col.objects.link(obj)
        master_col.objects.unlink(obj)
        
    turntable_master_filepath = config.prefs.get_template("turntable")
    light_master_filepath = config.prefs.get_template("light")
    turntable.setup_turntable(turntable_master_filepath=turntable_master_filepath, light_master_filepath=light_master_filepath, basePath = path)
    return setup_task_end()


def setup_task_uvs(path: str = None):
    ''' setup the uvs task '''
    return setup_task_modeling(path = path)


def setup_task_surfacing(lookdev_master_filepath: str, basePath: str = "", relative_mod_filepath: str = "") :
    ''' setup the surfacing task '''

    log.debug("start setuping a surfacing file")

    # garder en memoire le path original
    if not basePath :
        basePath = bpy.context.blend_data.filepath
    base_file_path = "\\".join(basePath.split("\\")[:-1])
    base_file_name = basePath.split("\\")[-1]
    
    #fullpath du dernier V de la mod
    if not relative_mod_filepath :
        mod_file_path = base_file_path.split("\\")
        while not mod_file_path[-1].startswith("v") :
            mod_file_path.pop()
        mod_file_path.pop()
        
        mod_file_path[-1] = "08_MODEL"
        
        mod_file_path = "\\".join(mod_file_path)
        
        mod_last_v = None
        for file in os.listdir(mod_file_path + "\\v01"):
            if not (".blend" in file and "model" in file) :
                continue
            mod_last_v = mod_file_path + "\\v01\\" + file
            break
        
        if not mod_last_v : 
            error = "can't find mod_last_v"
            log.error(error)
            raise Exception(error)
        
    else :
        mod_last_v = relative_mod_filepath
        
    initial_scenes = [scene.name for scene in bpy.data.scenes]
    
    bpy.context.view_layer.update()
    # get the lookdev master
    lookdev_scenes = blender_libs.scene.append_all(lookdev_master_filepath)
    
    
    #link the mod
    #EXAMPLE link_col_name = "RF_CH_0003_rufus_MESHES" EXAMPLE
    #OLD link_col_name = "_".join(base_file_name.split("_")[:4]) + "MESHES"
    link_col_name = blender_libs.collection.get_master_name_from_path(path = basePath, override_task = "MESHES")
        
    link_col_parent = blender_libs.collection.get(col_name=blender_libs.collection.get_master_name_from_path(path = basePath))

    try :  
        linked_mod = blender_libs.collection.link(mod_last_v, link_col_name, link_col_parent.name, instance_collections = False)
    except :
        log.error(f'can\'t link collection \"{link_col_name}\", try with \"{"_".join(base_file_name.split("_")[:4]) + "_model"}\"')
        try :
            linked_mod = blender_libs.collection.link(mod_last_v, "_".join(base_file_name.split("_")[:4]) + "_model", link_col_parent.name, instance_collections = False)
        except :
            log.error(f'can\'t link collection \"{"_".join(base_file_name.split("_")[:4]) + "_model"}\", try with \"Collection\"')
            linked_mod = blender_libs.collection.link(mod_last_v, "Collection", link_col_parent.name, instance_collections = False)
    
    for scene in lookdev_scenes :
        blender_libs.scene.flux_ui_data_transfer(bpy.context.scene, scene)
        scene.collection.children.link(link_col_parent)
        
    for scene_name in initial_scenes :
        scene =  bpy.data.scenes[scene_name]
        bpy.data.scenes.remove(scene)

    blender_libs.scene.set_active(bpy.data.scenes["Scene_Studio"])

    log.debug("end of setuping a surfacing file")
    return setup_task_end()


def setup_task_rigging(path = None):
    ''' setup the rigging task '''

    log.debug("start setuping a rigging file")
    blender_libs.collection.set_master(path = path)

    return setup_task_end()


def setup_task_layout():
    ''' setup the layout task '''

    log.debug("start setuping a layout file")
    #blender_libs.collection.set_master()
    return True


def setup_task_blocking():
    ''' setup the blocking task '''

    log.debug("start setuping a blocking file")
    #blender_libs.collection.set_master()
    return True


def setup_task_spline():
    ''' setup the spline task '''

    log.debug("start setuping a spline file")
    #blender_libs.collection.set_master()
    return True


def setup_task_polish():
    ''' setup the polish task '''

    log.debug("start setuping a polish file")
    #blender_libs.collection.set_master()
    return True


def setup_task_lighting():
    ''' setup the lighting task '''

    log.debug("start setuping a lighting file")
    #blender_libs.collection.set_master()
    return True