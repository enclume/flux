# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen


'''
Flux Pipeline Manager

Custom call for project
'''


import bpy

from .. import op_make_turntable
from .. import op_bake_scatter
from ...items_operators import op_flux_render
from ...items_operators import op_generic_tool
from ...items_operators import op_convert_previz_to_layout
from ...items_operators import op_delete_other_cam
from ... import ui as main_ui

#reload lib
import importlib
importlib.reload(op_bake_scatter)
importlib.reload(op_make_turntable)
importlib.reload(op_flux_render)
importlib.reload(op_generic_tool)
importlib.reload(op_convert_previz_to_layout)
importlib.reload(op_delete_other_cam)
importlib.reload(main_ui)

# used for
bpy.types.Scene.flux_ui_bakescatter_triaxisPath = bpy.props.StringProperty(default = "@assets/Element/Triaxis_baker/Modeling")
bpy.types.Scene.flux_ui_bakescatter_geonodeObj = bpy.props.PointerProperty(type=bpy.types.Object)
bpy.types.Scene.flux_ui_bakescatter_triaxisCollection = bpy.props.StringProperty(default = "")
bpy.types.Scene.flux_ui_bakescatter_show_ui = bpy.props.BoolProperty(default = False)
bpy.types.Scene.flux_ui_bakescatter_adv_ui = bpy.props.BoolProperty(default = False)

bpy.types.Scene.flux_ui_camera_lod_show_ui = bpy.props.BoolProperty(default = False)
bpy.types.Scene.flux_ui_camera_lod_pattern = bpy.props.StringProperty(default = "_lod5_")
bpy.types.Scene.flux_ui_camera_lod_constraint = bpy.props.PointerProperty(type=bpy.types.Object)
bpy.types.Scene.flux_ui_camera_lod_instance_col = bpy.props.StringProperty(default="Instances")


def ui(layout: bpy.types.UILayout, cat: str, current_item: dict):
    """
    Build the UI specific for a project name.
    Args:
        layout: layout herited from Tools UI
        cat: current category
        current_item: current item, in dictionnary
    """
    # ui init
    row = layout.column(align=True)

    # =====================================
    # uncomment if needed to check
    # print(type(layout))
    # print(cat)
    # print(current_item)
    # print(type(current_item))
    # row.label(text=f"Hello World")

    # check if item is valid
    item_valid = main_ui.is_item_valid()
    # print(f"{item_valid=}")

    # ----------------------------------------
    # will be erased after confirm
    # # =======ALWAYS===========================
    # if cat == "assets":
    #     box = row.box()
    #     row2 = box.column(align=True)
    #
    #     # image render preview stuffs
    #     op = row2.operator(op_flux_render.FLUX_OT_RenderFile.bl_idname, text="Render Previews",icon="RESTRICT_RENDER_OFF")
    #     op.increment = True
    #     op.for_revision = True
    #     row2.operator("flux.itemmenu_exe", icon='FILEBROWSER',
    #                   text="Check Preview folder (revision)").what = 'WINEXPLORER_REVISION'
    #     row2.operator(op_flux_render.FLUX_OT_CheckFile.bl_idname, text="Post Comment/Previews (revision)", icon="OUTPUT")
    #
    #     row.separator(factor=2)
    # ----------------------------------------

    row.operator(op_generic_tool.FLUX_OP_GeonodeSetSceneCam.bl_idname, text="Geonode set scene camera", icon="GEOMETRY_NODES")
    row.operator(op_generic_tool.FLUX_OP_GeonodeToggleBoundingBox.bl_idname, text="Geonode toggle bounding box", icon="GEOMETRY_NODES")
    # jlai mis ds le popup menu de l'item, plus pratique :)
    #row.operator(op_generic_tool.FLUX_OP_OpenBlendFolder.bl_idname, text="Open BlendFile Folder", icon="FILEBROWSER")

    # =======ASSETS===========================
    if cat == "assets":
        # row.separator(factor=2)
        # this is display if item is not valid
        #
        # row.operator(op_flux_render.FLUX_OT_RenderFile.bl_idname, text="Import Turntable", icon="RESTRICT_RENDER_OFF")

        if False and not(item_valid and current_item["task"] == "Surfacing"):
            row.operator(op_make_turntable.FLUX_OT_SetupTurntable.bl_idname, text="Setup Turntable", icon="MODIFIER")
            row.operator(op_flux_render.FLUX_OT_OpenRenderedFile.bl_idname, text="Open Turntable", icon="PLAY")

        row.separator(factor=2)
        row.operator(op_make_turntable.FLUX_OT_SetupModeling.bl_idname, text="Setup Modeling", icon="MODIFIER")


        # how to go in context, with item valid
        if item_valid:
            if current_item["type"] == "Prop":
                pass
                # row.label(text="exemple of prop context hello world")
            # how to go in context
            if current_item["task"] == "Modeling":
                pass
                # row.label(text="exemple of Modeling context hello world")


        # new
        row.separator(factor=2)

        # bake scatter
        ic = "GEOMETRY_NODES"
        if bpy.context.scene.flux_ui_bakescatter_show_ui:
            ic = "DOWNARROW_HLT"

        row.prop(bpy.context.scene, "flux_ui_bakescatter_show_ui", text="Bake Scatter (Expand)", toggle  = True, icon=ic)
        if bpy.context.scene.flux_ui_bakescatter_show_ui:
            box = row.box()
            rowScatter = box.column()
            rowScatter.label(text=" Bake Vegetation GeoNode to instances")
            rowScatter.label(text="        - use MESHES/grp_ for point collection in assets")
            rowScatter.label(text="        - use USERGRP_ for prefix scene user collections")
            rowScatter.separator()

            # advanced UI?
            rowScatter.prop(bpy.context.scene, "flux_ui_bakescatter_adv_ui", text="Advanced")

            #
            rowScatter.prop_search(bpy.context.scene, "flux_ui_bakescatter_geonodeObj", bpy.context.scene, "objects", text="Ground GeoNode")
            if bpy.context.scene.flux_ui_bakescatter_adv_ui:
                rowScatter.prop(bpy.context.scene, "flux_ui_bakescatter_triaxisPath", text="Triaxe Path")
                rowScatter.operator(op_bake_scatter.FLUX_OP_ImportTriaxis.bl_idname, text="1- Import Triaxis", icon="EVENT_A")
                rowScatter.prop(bpy.context.scene, "flux_ui_bakescatter_triaxisCollection", text="Triaxe Collection")
                rowScatter.operator(op_bake_scatter.FLUX_OP_SwitchGeoToTriaxis.bl_idname, text="2- Switch Geo to triAxis", icon="EVENT_B")
                rowScatter.operator(op_bake_scatter.FLUX_OP_BakeTriAxis.bl_idname, text="3- Bake triAxis on GeoNode", icon="EVENT_C")
                # rowScatter.operator(op_bake_scatter.FLUX_OP_PrintLod.bl_idname, text="4-check lod5 objects", icon="EVENT_D")
                rowScatter.operator(op_bake_scatter.FLUX_OP_BakeScatter.bl_idname, text="3- Bake back vegetation on TriAxis", icon="EVENT_D")

            #
            if not bpy.context.scene.flux_ui_bakescatter_adv_ui:
                rowScatter.separator()
                rowScatter.operator(op_bake_scatter.FLUX_OP_PlayAll.bl_idname, text="Bake in one step", icon="PLAY")

        # billboard to cameras
        row.separator(factor=2)
        ic = "CON_CAMERASOLVER"
        if bpy.context.scene.flux_ui_camera_lod_show_ui:
            ic = "DOWNARROW_HLT"
        row.prop(bpy.context.scene, "flux_ui_camera_lod_show_ui", text="Constraint LOD5 to cam (Expand)", toggle  = True, icon=ic)
        if bpy.context.scene.flux_ui_camera_lod_show_ui:
            box = row.box()
            rowCamCtr = box.column()
            rowCamCtr.prop_search(bpy.context.scene, "flux_ui_camera_lod_constraint", bpy.context.scene, "objects", text="Camera")
            rowCamCtr.prop(bpy.context.scene, "flux_ui_camera_lod_instance_col", text="Instances Collection")
            rowCamCtr.prop(bpy.context.scene, "flux_ui_camera_lod_pattern", text="LOD5 Pattern")
            rowCamCtr.operator(op_bake_scatter.FLUX_OP_Constraint_Billboard.bl_idname, text="Constraint LOD5 to camera", icon="CON_CAMERASOLVER")
            rowCamCtr.operator(op_bake_scatter.FLUX_OP_Constraint_Billboard.bl_idname, text="Delete Constraints", icon="CON_CAMERASOLVER").just_delete=True

    # =======SHOTS===========================
    if cat == "shots":
        row.separator(factor=2)
        # row.operator(op_flux_render.FLUX_OT_RenderFile.bl_idname, text="Render Playblast", icon="RESTRICT_RENDER_OFF")
        #row.label(text="Hello world")
        row.operator(op_flux_render.FLUX_OT_RenderFile.bl_idname, text="Render Playblast", icon="RESTRICT_RENDER_OFF")

        if  current_item != "None" and current_item["task"].lower() == "layout":
            row.separator()
            row.label(text="layout utils")
            row.operator(op_flux_render.FLUX_OT_RenderCleanLayout.bl_idname, text="Clean Layout Render", icon="FILE_MOVIE")
            row.operator(op_flux_render.FLUX_OT_RenderAlphaLayout.bl_idname, text="Alpha Layout Render", icon="FILE_IMAGE")
            row.operator(op_generic_tool.FLUX_OT_ExportCamAlembic.bl_idname, text="Export Camera Alembic", icon="CAMERA_DATA")

        if  current_item != "None" and current_item["task"].lower() == "layout":
            row.separator()
            row.label(text="sequence previz > shots layout")
            row.operator(op_convert_previz_to_layout.FLUX_OT_ConvertPrevizToLayout.bl_idname, text="Convert previz to shot layout", icon="VIEW_CAMERA")
            row.separator()
            row.operator(op_delete_other_cam.FLUX_OT_DeleteOtherCams.bl_idname, text="Keep unselected cams", icon="VIEW_CAMERA")
