# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen
'''
draw text message on top of blender
'''

import bpy
import blf
import os

# initialize logger
from .. import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)
 
#font_id = blf.load(r'C:\Users\Marteau\Desktop\Roboto\Roboto-Medium.ttf')
    
class DrawMsg_3Dview:
    ''' Draw a text message on the 3D viewport '''

    def __init__(self, context, message: str = "Hello World", color_RGBA: list = [1, 0, 0, 0.75], position_XYZ: list = [100, 100, 0]):
        self.message = message
        self.color_RGBA = color_RGBA
        self.position_XYZ = position_XYZ
        self.size = 60
        self.dpi = 72
        self.draw_space = bpy.types.SpaceView3D
        self.handle = self.draw_space.draw_handler_add(
                   self.draw_text_callback,(context,),
                   'WINDOW', 'POST_PIXEL')
        
    def redraw_areas(self):
        ''' Redraw the interface '''
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        return
                   
    def draw_text_callback(self, context) :
        ''' Create the text to draw '''

        font_id = 0
        color_R, color_G, color_B, color_A = self.color_RGBA
        position_X, position_Y, position_Z = self.position_XYZ

        # draw some text
        blf.color(font_id, color_R, color_G, color_B, color_A)
        blf.position(font_id, position_X, position_Y, position_Z)
        blf.size(font_id, self.size, self.dpi)
        blf.draw(font_id, self.message )
    
    def remove_handle(self, redraw = True):
        ''' Remove the text '''

        self.draw_space.draw_handler_remove(self.handle, 'WINDOW')
        if redraw : self.redraw_areas()

    def update_message(self, context, new_message):
        ''' Update the text with a new message '''

        self.message = new_message
        self.remove_handle(redraw = False)
        self.handle = self.draw_space.draw_handler_add(
            self.draw_text_callback,(context,), 
            'WINDOW', 'POST_PIXEL')
        
        self.redraw_areas()
        
        


def remove_warning_text():
    ''' Remove the warning text '''
    dns = bpy.app.driver_namespace
    try :
        dm = dns.get("dm")
        if dm and "RNA_HANDLE" in str(dm.handle) : 
            log.debug("remove draw handler")
            dm.remove_handle()
    except :
        log.debug("no draw handler to remove")


def add_warning_text(context = bpy.context):
    ''' add the warning text "Locked file - don't save" on all 3D viewport '''
    dns = bpy.app.driver_namespace
    remove_warning_text()
    #print("is_item_locked : " + str(bpy.context.scene.flux_ui.is_item_locked))
    if bpy.context.scene.flux_ui.is_item_locked :
        dns["dm"] = DrawMsg_3Dview(context, "\u26A0 LOCKED FILE - DON'T SAVE \u26A0")

        try :
            for area in bpy.context.screen.areas :
                area.tag_redraw()
        except :
            pass




if __name__ == "__main__" :
    os.system("cls")
        
    context = bpy.context             
    dns = bpy.app.driver_namespace

    #pour enlever le msg
    dm = dns.get("dm")
    if dm and "RNA_HANDLE" in str(dm.handle) : 
        dm.remove_handle()
        
    #pour ajouter le msg
    dns["dm"] = DrawMsg_3Dview(context, "un p'tit Walfis ?")

    #pour refresh l'interface
    for area in bpy.context.screen.areas:
        area.tag_redraw()