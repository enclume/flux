import datetime
import os.path
from typing import Union

try: 
    from ..import shotgun_bridge
    from ... import database_access
    from ... import config
    from ... import dependencies as dep
    from ... import file_access
    from ... import flux_logger

    #reload lib
    import importlib
    #importlib.reload(utils)
    #importlib.reload(database_access)
    #importlib.reload(config)
    #importlib.reload(dep)
    #importlib.reload(file_access)
    #importlib.reload(flux_logger)
    
except: 
    import database_access.shotgun_bridge as shotgun_bridge
    import database_access
    import config
    import dependencies as dep
    import file_access
    import flux_logger


# set logger
log = flux_logger.get_flux_logger()


if not dep.is_dep_in_path():
    try:
        import shotgun_api3
        database_access.utils.print_framed("shotgun_api3 is already loaded, Be carefull")
    except:
        pass

if not dep.is_dep_in_path():
    dep.add_dep_in_path()


def get_current_version() -> list[int] :
    try :
        import shotgun_api3
    except :
        log.error('can\'t import shotgun_api3')
        return
    version = shotgun_api3.__version__
    version = version.split(".")
    for i, item in enumerate(version):
        version[i] = int(item)
    return version


try :
    import shotgun_api3
    import gazu
except :
    database_access.db_access_startup()
    import shotgun_api3
    import gazu

try :
    from ...dependencies import shotgun_api3 #for dev purpose
except:
    pass


class shotgun_manager :
    def __init__(self, host, username, password) -> None:
        self.host = host
        self.script_name = "Flux pipeline"
        self.script_key = "tbrsera0zrfyacu)fsxYknljr"
        self.sg = shotgun_api3.Shotgun(
            self.host,
            script_name=self.script_name,
            api_key=self.script_key)
        
        self.user = self.sg.authenticate_human_user(username, password)


def check_db_auth() :
    ''' 
    check if there is an user authentified to the db.\n
    if not, raise an Exception
    '''
    if not shotgun_bridge.is_db_auth():
        error = "ERROR : Not logged to db"
        log.error(error)
        raise Exception(error)