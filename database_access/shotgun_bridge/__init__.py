# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Description TODO
'''

import datetime
import os.path
from typing import Union

try: 
    from . import utils
    from ... import database_access
    from ... import config
    from ... import dependencies as dep
    from ... import file_access
    from ... import flux_logger

    #reload lib
    import importlib
    importlib.reload(utils)
    #importlib.reload(database_access)
    importlib.reload(config)
    importlib.reload(dep)
    importlib.reload(file_access)
    importlib.reload(flux_logger)
    
except: 
    import database_access.shotgun_bridge.utils as utils
    import database_access
    import config
    import dependencies as dep
    import file_access
    import flux_logger


# set logger
log = flux_logger.get_flux_logger()


if not dep.is_dep_in_path():
    try:
        import shotgun_api3
        database_access.utils.print_framed("shotgun_api3 is already loaded, Be carefull")
    except:
        pass

if not dep.is_dep_in_path():
    dep.add_dep_in_path()


def get_supported_version() -> list[int] :
    return [3, 4, 0]


def is_bridge_current_version_supported() -> bool :
    ''' test if the imported db bridge python api is supported  '''

    current_v = utils.get_current_version()
    supported_v = get_supported_version()

    if len(current_v) != len(supported_v) :
        log.warning(f"the shotgun_api3 current and supported versions does'nt have the same length (current: {current_v}, supported: {supported_v}) ")
        return False

    for i, item in enumerate(current_v) :
        if item < supported_v[i] :
            return False
    return True


try :
    import shotgun_api3
    import gazu
except :
    database_access.db_access_startup()
    import shotgun_api3
    import gazu

try :
    from ...dependencies import shotgun_api3
except:
    pass

sg_manager = None

def get_sg_manager() :
    global sg_manager
    if not sg_manager :
        log.warning("sg_manager is None, think to login !")
    return sg_manager

def get_sg() :
    sg_manager = get_sg_manager()
    return sg_manager.sg

def get_db_url():
    sg_manager = get_sg_manager()
    return sg_manager.host

#bridge
def db_login(host: str, username: str, password: str, use_db_cache: bool = True):
    host = "https://nextframes.shotgrid.autodesk.com/"
    global sg_manager
    sg_manager = utils.shotgun_manager(host, username, password)
    return sg_manager


def get_entities(entity: str, filters: list = [], specific_fields: list = None, only_one: bool = False) -> Union[list[dict], dict] :
    ''' 
    get all specified entities 

    Args:
        - entity: the wanted entity
        - filters: a sg filter list
        - specific_fields: a sg field list (if None, return all fields)
    '''
    sg = get_sg()
    if specific_fields : fields = specific_fields
    else : fields = list(sg.schema_field_read(entity).keys())

    if only_one :
        return sg.find_one(entity, filters=filters, fields=fields)

    return sg.find(entity, filters=filters, fields=fields)


def get_db_current_user() :
    ''' get the current logged user '''
    sg_manager = get_sg_manager()
    user_id = sg_manager.user
    if not user_id :
        return
    user_id = user_id["id"]
    filters = [["id", "is", user_id]]
    user = get_entities('HumanUser', filters, only_one=True)

    #needed/used keys
    user.update({"full_name": user["name"]})
    user.update({"first_name": user["firstname"]})
    user.update({"last_name": user["lastname"]})
    return user
    return user[-1]


def is_db_auth() -> bool :
    ''' return True if a db session exist '''
    try:
        get_db_current_user()
        return True
    except:
        return False
    

def db_end_user_session():
    sg = get_sg()
    sg.close()
    global sg_manager
    sg_manager = None


def gazu_clear_cache():
    ''' gazu feature '''
    return


def gazu_update_cache(use_cache):
    ''' gazu feature '''
    return

#bridge
def get_project_by_name(project_name: str, field: list[str] = None) -> dict :
    entity = 'Project'
    filters = [["name", "is", project_name]]
    project = get_entities(entity, filters, field, only_one=True)
    return project
    if len(project) > 1 :
        print(len(project))
    return project[-1]


def get_project_id_filter(only_project_dict: bool = False) -> Union[dict, list] :
    project_name = config.get_project_name()
    project = get_project_by_name(project_name, ["id"])
    if only_project_dict :
        return {"type": "Project", "id": project["id"]}
    return ["project", "is", {"type": "Project", "id": project["id"]}]


def get_sequence_by_name(name: str, field: list[str] = None) -> dict :
    filters = [["code", "is", name], get_project_id_filter()]
    seq = get_entities('Sequence', filters, field, only_one=True)
    return seq
    return seq[-1]


def get_shot_by_name(seq_name: str, shot_name: str, field: list[str] = None) :
    seq = get_sequence_by_name(seq_name, ["id"])
    filters = [["code", "is", shot_name], ["sg_sequence", "is", {"type": "Sequence", "id": seq["id"]}], get_project_id_filter()]
    shot = get_entities("Shot", filters, field, only_one=True)
    return shot
    return shot[-1]

#bridge
def get_asset_types_name() -> list[str] :
    ''' get all asset types name '''
    utils.check_db_auth()

    sg = get_sg()
    asset_types = sg.schema_read(get_project_id_filter(True))
    asset_types = asset_types["Asset"]["sg_asset_type"]["properties"]["valid_values"]["value"]
    asset_types = sorted(asset_types)
    return asset_types

#bridge
def get_assets_name(asset_type_name: str) -> list[str] :
    ''' get all assets name for a given type '''
    utils.check_db_auth()

    filters = [["sg_asset_type", "is", asset_type_name], get_project_id_filter()]
    assets_names = get_entities('Asset', filters=filters, specific_fields=["code"])

    for i, asset in enumerate(assets_names) :
        assets_names[i] = asset["code"]

    assets_names = sorted(assets_names)
    return assets_names


def get_asset_by_name(asset_name, fields: list[str] = None) -> dict :
    ''' get an asset by name '''
    utils.check_db_auth()

    filters = [["code", "is", asset_name], get_project_id_filter()]
    asset = get_entities('Asset', filters, fields)

    if len(asset) == 1 :
        return asset[0]
    
    if len(asset) == 0 :
        log.info("no asset found")
        return asset
    
    log.info("multiple assets found")
    return asset

#bridge
def get_asset_tasks_name(asset_name: str, get_step_name: bool = False) -> list[str] :
    ''' get all asset tasks for a given asset name '''
    utils.check_db_auth()

    asset_tasks = get_asset_by_name(asset_name, ["tasks"])["tasks"]
    
    for i, task in enumerate(asset_tasks) :
        if not get_step_name :
            asset_tasks[i] = task["name"]
            continue
        filters = [["id", "is", task["id"]]]
        asset_tasks[i] = get_entities("Task", filters=filters)[0]['step']['name']

    return asset_tasks


def upload_file(entity: str, entity_id: int, file_path: str, field_name = None, display_name = None, tag_list = None) :
    ''' 
    Return: 
        the id of the file (sg type : Attachement)
    '''
    #print('start upload')
    return get_sg().upload(entity, entity_id, file_path, field_name, display_name, tag_list)


def upload_file_to_version(version_id: int, file_path: str) :
    return upload_file('Version', version_id, file_path, "sg_uploaded_movie", 'test_picture')
    

def add_preview_to_db(preview_path: Union[str, list], version_id: dict, revision: Union[str, int] = None) :
    ''' 
    add preview(s) to the DB

    Args:
        - preview_path: a path or a list of path of preview file
        - version_id:
        - comments:
        - revision: Force preview revision to this number
    '''
    #print("enter add_preview_to_db")
    if type(preview_path) is str :
        preview_path = [preview_path]

    file_ids = []
        
    for p_path in preview_path :
        #print(f"process path : {p_path}")
        if not os.path.exists(p_path) :
            log.error(f"the preview path does\'nt exist (->{p_path})")
            continue

        '''gazu_revision = None
        if revision :
            gazu_revision = int(revision)
            print(f"Flux : Force preview revision to {revision} for {p_path}")'''

        file_id = None
        for i in range(5) :
            if file_id :
                break
            file_id = upload_file_to_version(version_id["id"], p_path)
        
        if not file_id :
            log.warning(f"fail of file upload (->{p_path})")
        file_ids.append(file_id)
    
    return file_ids

#bridge
def get_task_id_from_asset(asset_name: str, task_name: str) -> dict :
    asset = get_asset_by_name(asset_name, ["tasks"])
    task_id = None
    for task in asset['tasks'] :
        if task["name"] == task_name :
            task_id = task["id"]
            break

    if task_id :
        filters = [["id", "is", task_id], get_project_id_filter()]
    else :
        filters = [["content", "is", task_name], get_project_id_filter()]

    task = get_entities('Task', filters=filters)
    if len(task) > 1 :
        log.warning("multiple task found")
    return task[0]

#bridge
def get_task_id_from_shot(seq_name: str, shot_name: str, task_name: str) -> dict :
    shot = get_shot_by_name(seq_name, shot_name, ["task"])
    task_id = None
    for task in shot['tasks'] :
        if task["name"] == task_name :
            task_id = task["id"]
            break

    if task_id :
        filters = [["id", "is", task_id], get_project_id_filter()]
    else :
        filters = [["content", "is", task_name], get_project_id_filter()]

    task = get_entities('Task', filters=filters)
    if len(task) > 1 :
        log.warning("multiple task found")
    return task[0]


def get_task_statut_by_name(status_name: str) :
    filters = [["code", "is", status_name]]
    status = get_entities('Status', filters=filters)[0]
    
    if not status :
        error = f"Cannot set status (status_name : {status_name})"
        log.error(error)
        raise Exception(error)
    return status


def get_entity_from_current_item(current_item):
    if current_item["category"].lower() in ["asset", "assets"] :
        return get_asset_by_name(current_item["name"])
    if current_item["category"].lower() in ["shot", "shots"] :
        return get_shot_by_name(current_item["seq"], current_item["shot"])

    log.error(f'current_item["category"] not supported ->({current_item["category"]})')
    return False


def get_task_from_current_item(current_item: dict) -> dict :
    ''' description TODO '''

    if "category" not in current_item :
        error = f"you need a category key in the current_item dictionnary"
        log.error(error)
        return
    
    if current_item["category"] not in ["assets", "shots"] :
        log.info(f'current_item[\"category\"] not supported !\n   -> ({current_item["category"]})')

    # get asset
    if current_item["category"] == "assets":
        return get_task_id_from_asset(current_item["name"], current_item["task"])
    # shots
    elif current_item["category"] == "shots":
        return get_task_id_from_shot(current_item["seq"], current_item["shot"], current_item["task"])
    
    log.info("oups !?")
    return False


def create_new_version(version_name, entity_p, task, status, description) -> dict :
    ''' 
    create a new sg version 

    Args:
        - version_name: the name of the version to create
        - entity_p: the entity receiver of the new sg version
        - task: the task receiver of the new sg version
        - status: the status of the new version
        - description: the description text of the new version

    Returns:
        the sg version dict 
    '''
    user = get_db_current_user()[0]
    data = {'project':  get_project_id_filter(True),
        'code': version_name,
        'description': description,
        #'sg_path_to_frames': '/v1/gun/s100/010/frames/anim/100_010_animv1_jack.#.jpg',
        'sg_status_list': status["code"],
        'entity': {'type': entity_p["type"], 'id': entity_p['id']},
        'sg_task': {'type': 'Task', 'id': task['id']},
        'user': {'type': user['type'], 'id': user['id']}
    }
    
    #print(f"data : {data}")
    
    return get_sg().create('Version', data=data)


def add_comment_to_db(entity_p: dict, task: dict, status: str, comment: str) -> dict :
    ''' 
    add a comment in the DB for the given task of the given entity

    Args:
        - entity_p: the entity receiver
        - task: the task to comment
        - status: the status of the task
        - comment: the comment text in a formated way

    Returns:
        the sg version dict 
    '''
    comment = comment.split("||")
    version = database_access.utils.get_version_from_comment(comment[-1])
    for i, key in enumerate(version) :
        value = version[key]
        value = str(value).zfill(i+2)
        version[key] = f"{key}{value}"

    name = [entity_p["code"], task["content"]]
    name.extend(list(version.values()))
    name = "_".join(name) #TODO better name
    return create_new_version(name, entity_p, task, status, comment[0])

#bridge
def set_comment_db(current_item: dict, task_status: str, comment: str, 
                   preview_path: Union[str, list] = None, revision: Union[int, str] = None) -> dict :
    ''' 
    add a comment in the DB for the current item with the given status 

    Args:
        - current_item: the current item dictionnary
        - task_status: the task status name
        - comment: (in formated way, like comment || v1, r1, etc...)
        - preview_path: if set, the preview(s) path to upload to DB
        - revision: an integer corresponding to the number of the revision (more a kitsu thing)

    Returns:
        the version dictionnary
    '''
    # get status
    status_id = get_task_statut_by_name(task_status)
    task_id = get_task_from_current_item(current_item)
    entity_p = get_entity_from_current_item(current_item)

    # set comment
    version = add_comment_to_db(entity_p, task_id, status_id, comment)
    
    if not preview_path : return True
    add_preview_to_db(preview_path, version, None) 
    return version

#bridge
def get_comments_db(task: dict, only_last: bool, db_reversed: bool = True) -> list[dict] :
    ''' 
    get all comments (or only last) for a given task 

    Args:
        - task: the task dictionnary
        - only_last: if True return only the last comment
        - db_reversed: if True the first comment in the list is the last made in the db
    
    Returns:
        a list of comments dictionnary
    '''
    comments = task['sg_versions']
    comments.extend(task['notes'])

    sorted_comments = {}

    for i, item in enumerate(comments) :
        filters = [["id", "is", item["id"]], get_project_id_filter()]
        item = get_entities(item["type"], filters=filters)[0]
        comments[i] = item
        date: datetime.datetime = item["created_at"]
        sorted_comments.update({date.isoformat():item})

    sorted_comments = dict(sorted(sorted_comments.items()))

    comments = list(sorted_comments.values())

    if only_last : 
        comments = [comments[-1]]
    if db_reversed :
        comments.reverse()
    
    for i, comment in enumerate(comments) : #TODO formated result
        #text = "the comment ||v:1/r:1/w:1 - From r:1"
        versionning = None
        text = ''
        previews = []
        attachment_files = []

        if comment["type"] == "Note" :
            text = comment["content"]
        elif comment["type"] == "Version" :
            text = comment["description"]
            #versionning = comment["code"] TODO

        if versionning :
            text += f"||{versionning}"

        comment.update({"text": text})
        comment.update({"task_status": {"short_name" : comment["sg_status_list"]}}) 
        comment.update({"updated_at" : comment["updated_at"].isoformat()})
        name = comment["updated_by"]["name"].split(" ") #TODO improve this
        comment.update({"person": {"first_name": name[0], "last_name": name[-1]}})
        comment.update({"previews": previews})
        comment.update({"attachment_files": attachment_files})
    
    return comments

#bridge
def get_sequences_name() -> list[str] :
    ''' get all sequences name of the project '''

    sequences = get_entities('Sequence', [get_project_id_filter()])

    for i, sequence in enumerate(sequences):
        sequences[i] = sequence["code"]

    sequences = sorted(sequences)
    return sequences

#bridge
def get_shots_name(seq_name: str) -> list[str] :
    '''
    get all shots name for a given sequence

    Args:
        - seq_name: the sequence name
    '''
    seq = get_sequence_by_name(seq_name, ["id"])
    filters = [["sg_sequence", "is", {"type": "Sequence", "id": seq["id"]}], get_project_id_filter()]
    shots = get_entities('Shot', filters, ["code"])

    for i, shot in enumerate(shots):
        shots[i] = shot["code"]
    shots = sorted(shots)

    return shots

#bridge
def get_shot_tasks_name(seq_name: str, shot_name: str, filter_supported: bool = False) -> list[str] :
    '''
    get all tasks name for a given shot

    Args:
        - seq_name:
        - shot_name: 
        - filter_supported: if True, keep only the supported tasks
    '''
    tasks = get_shot_by_name(seq_name, shot_name, ['tasks'])['tasks']

    all_tasks = []
    for task in tasks :
        all_tasks.append(task["name"])
    all_tasks = sorted(all_tasks)

    if not filter_supported :
        return all_tasks
    
    current_item = {"seq": seq_name, "shot": shot_name, "category": "shots", "task": None}
    all_tasks_filtered = []
    for task in all_tasks :
        current_item["task"] = task
        base_folder = file_access.get_item_path2(current_item, only_base_folder=True)
        
        if base_folder and seq_name in base_folder and shot_name in base_folder : #TODO improve this
            all_tasks_filtered.append(task)

    return all_tasks_filtered

#bridge
def get_shot_infos(seq_name: str, shot_name: str) -> dict :
    '''
    get infos for a given shot

    Returns:
        a dict with all shots infos
        - act
        - fps
        - frame_in : used
        - frame_out : used
        - shot_type
        - difficulty
        - briefing_lighting
    '''
    shot = get_shot_by_name(seq_name, shot_name)

    if not shot : return False

    infos = {
        "act": None,
        "fps": None,
        "frame_in": None, #cut_in / sg_cut_in / head_in / sg_head_in ?
        "frame_out": None,
        "shot_type": None,
        "difficulty": None,
        "briefing_lighting": None
        } #TODO

    return infos

#bridge
def get_shot_url(seq_name: str, shot_name: str) -> str :
    ''' get the DB url of a shot (to be able to open it in an internet browser) '''

    shot = get_shot_by_name(seq_name, shot_name, ["id"])
    host = get_db_url()
    url = host + "detail/Shot/" + str(shot["id"])

    return url

#bridge
def get_asset_url(asset_name: str) -> str :
    ''' get the DB url of an asset (to be able to open it in an internet browser) '''

    asset = get_asset_by_name(asset_name, ["id"])
    host = get_db_url()
    url = host + "detail/Asset/" + str(asset["id"])

    return url