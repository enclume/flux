# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Flux Pipeline Manager

manage Database acces
Specifically Kitsu

all acces here are depend to the prefs get in the prefs
'''
import os
from typing import Union

try: 
    from . import gazu_bridge
    from . import shotgun_bridge
    from . import utils
    from .. import config
    from .. import dependencies as dep
    from .. import file_access
    from .. import flux_logger
    from ..config import path
    

    #reload lib
    import importlib
    importlib.reload(gazu_bridge)
    importlib.reload(shotgun_bridge)
    importlib.reload(utils)
    importlib.reload(config)
    importlib.reload(dep)
    importlib.reload(file_access)
    importlib.reload(flux_logger)
    importlib.reload(path)
    
except: 
    import database_access.utils as utils
    import config
    import dependencies as dep
    import file_access
    import flux_logger
    import config.path as path

# set logger
log = flux_logger.get_flux_logger()

all_bridges = {"kitsu": gazu_bridge, "shotgrid": shotgun_bridge}


def get_supported_db_api() -> list[str] :
    ''' get all supported api '''
    global all_bridges
    return list(all_bridges.keys())

# initialize depedencies
if not dep.is_dep_in_path():
    try:
        import gazu
        utils.print_framed("Gazu is already loaded, Be carefull")
    except:
        pass

if not dep.is_dep_in_path():
    dep.add_dep_in_path()


bridge = None

#bridge = shotgun_bridge

def get_bridge() :
    #return gazu_bridge
    project_db = config.get_project_db()
    # print(f"{project_db=}")
    if not project_db :
        return
    
    bridge_name = str(project_db["api"])

    global all_bridges
    if bridge_name.lower() not in all_bridges :
        error = f"the current api name is not supported\n(-> given: {bridge_name}, supported: {get_supported_db_api()})"
        log.error(error)
        raise Exception(error)

    global bridge
    if not bridge :
        bridge = all_bridges[bridge_name.lower()]

    return all_bridges[bridge_name.lower()]


#bridge
def db_access_startup() :
    bridge = get_bridge()
    if not bridge :
        return
    if not bridge.is_bridge_current_version_supported() :
        utils.print_framed("Gazu is not the good version (0.9.11)")


try :
    import gazu
except :
    db_access_startup()
    import gazu

#bridge
def get_db_current_user() :
    ''' get the current logged user '''
    return get_bridge().get_db_current_user()


def is_db_auth() -> bool :
    ''' return True if a db session exist '''
    # initialement dans prefs.py, déplacé ici pour éviter l'import bpy
    try:
        get_db_current_user()
        return True
    except:
        return False

#bridge
def db_login(username, password, use_db_cache = True) :
    ''' log to the DB api '''
    host = config.get_project_db()["host"]
    if not username or not password :
        print(f"username or/and password is none (username: {username}, password: {password})")
        return 
    return get_bridge().db_login(host, username, password, use_db_cache)

#bridge
def db_end_user_session():
    ''' logout the current user '''
    return get_bridge().db_end_user_session()

#bridge
def clear_cache():
    ''' clear the db cache (more a gazu thing) '''
    return get_bridge().gazu_clear_cache()

#bridge
def update_cache(use_cache):
    ''' update the db cache (more a gazu thing) '''
    return get_bridge().gazu_update_cache(use_cache)

#bridge
def get_assets_types() -> list[str] :
    ''' get all asset types name '''
    return get_bridge().get_asset_types_name()

#bridge
def get_assets_names(asset_type :str) -> list[str] :
    ''' get all assets name for a given type '''
    return get_bridge().get_assets_name(asset_type)

#bridge
def get_assets_tasks(asset_name: str) -> list[str] :
    ''' get all asset tasks for a given asset name '''
    return get_bridge().get_asset_tasks_name(asset_name)

#bridge
def get_project() -> dict :
    ''' get the DB project '''
    project_name = config.get_project_name()
    project = get_bridge().get_project_by_name(project_name)
    #log.debug(f"project_name : {project_name}, project : {project}")

    if not project: 
        error = f"Not valid database project ({project_name} is not in db)"
        log.error(error)
        raise ValueError(error)
    return project

#bridge
def get_task_id_from_current_item(current_item: dict) -> dict :
    ''' 
    get the task dict for a given "current_item" dictionnary.\n

    Needed keys:
        - "category": ("assets" or "shots")
        - "name" (assets) / "seq" and "shot" (shots)
        - "task"

    '''
    if "category" not in current_item :
        error = f"you need a category key in the current_item dictionnary"
        log.error(error)
        return
    
    if current_item["category"] not in ["assets", "shots"] :
        log.info(f'current_item[\"category\"] not supported !\n   -> ({current_item["category"]})')

    # get asset
    if current_item["category"] == "assets":
        name = current_item["name"].replace("_", " ")
        return get_bridge().get_task_id_from_asset(name, current_item["task"])
    # shots
    elif current_item["category"] == "shots":
        return get_bridge().get_task_id_from_shot(current_item["seq"], current_item["shot"], current_item["task"])
    
    log.info("oups !?")
    return False


def get_formated_flux_metadata_path(path: str) :
    end_path = "\\flux_metadata.txt"
    if os.path.isdir(path) :
        path += end_path
    if os.path.isfile(path) and not path.endswith(end_path) :
        error = f"the path should end with \"{end_path}\" (-> {path})"
        log.error(error)
        raise Exception(error)
    return path


def set_comment_db(current_item: dict, task_status: str, comment: str, 
                   preview_path: Union[str, list] = None, revision: Union[int, str] = None) -> bool :
    ''' 
    add a comment in the DB for the current item with the given status 

    Args:
        - current_item: the current item dictionnary
        - task_status: the task status name
        - comment: (in formated way, like comment || v1, r1, etc...)
        - preview_path: if set, the preview(s) path to upload to DB
        - revision: an integer corresponding to the number of the revision (more a kitsu thing)

    Returns:
        the version dictionnary
    '''
    return get_bridge().set_comment_db(current_item, task_status, comment, preview_path, revision)


def format_comment(comment: str, versionning: Union[list, dict], from_r: Union[str, int] = None) -> str :
    '''
    format the given comment in a readable and parsable way : "comment || v1 / r1 / w1"\n

    Args:
        - comment: raw comment in string
        - versionning: versionning in dict of array
        - from_r: from wich revision is coming from (used only for version)

    Returns:
        the comment formatted (comment ||v:0/r:0/w:0 - From r:0)
    '''
    if type(versionning) is list :
        if not versionning or len(versionning) > 3 :
            error = f"versionning length should be 1, 2 or 3 (versionning length: {len(versionning)})"
            log.error(error)
            raise ValueError(error)
        
        version_letter = path.get_version_letters()
        
        while len(version_letter) > len(versionning) :
            version_letter.pop()
        
        versionning_dict = {}
        for i, key in enumerate(version_letter) :
            value = versionning[i]
            versionning_dict.update({key: value})
    else :
        #print(f"versionning is a dict : {versionning}")
        versionning_dict = versionning.copy()
          
    formated = []
    for key in versionning_dict :
        formated.append(f"{key}:{versionning_dict[key]}")

    formated = "/".join(formated)
    formated = comment + " ||" + formated

    # if its a "v", add the tag from revision
    if len(versionning_dict) == 1 and from_r != None :
        formated += f" - From r:{from_r}"

    return formated


#bridge
def set_comment(category: str, current_item: dict, status: str, comment: str, 
                user: str = "", preview_path=None, revision: Union[int, str] = None, local: str = False) :
    """
    set a comment in DB or in local for the current item

    Args:
        - category: the current_item category
        - current_item: a descriptive dict of the current item
        - status: the task status for the task link to the new comment
        - comment: (in formated way, like "comment || v1, r1, etc...")
        - preview_path: if there is a preview (ignored in local)
        - revision: if there is a revision (ignored in local)
        - local: Is to write in local. If yes, this should be a folder path, if not, (then write in db/kitsu) should be False
    """
    if local :
        local = get_formated_flux_metadata_path(local)
        utils.set_comment_local(local, status, comment, user)
        return False
    
    # put the category inside the current item
    current_item = current_item.copy()
    current_item.update({"category": category})
    
    set_comment_db(current_item, status, comment, preview_path, revision)

    return True


def set_formated_comment(category: str, current_item: dict, status: str, comment: str, versionning: Union[list, dict], from_r: Union[str, int] = None,
                         user: str = "", preview_path=None, revision: Union[int, str] = None, local: str = False) :
    
    comment = format_comment(comment, versionning, from_r)
    return set_comment(category, current_item, status, comment, user, preview_path, revision, local)


#bridge
def get_comment(category: str, current_item: dict,
                versionning: Union[list, dict] = None, force_last_with_no_match_versionning: bool = False, 
                only_last: bool = False, local: Union[list, bool] = ["w"], local_path: str = None) -> list :
    """
    description TODO

    Args:
        - category: clear enough no?
        - current_item: item in formated dict
        - versionning: if not none, return only the comment for the given versionning. Can be an array of 3 or dict
        - only_last: Return only last comment.
        - local: which level of versionning to get from local.
                - local = ["w"] will look for w on local, the rest on kitsu.
                - local = ["w", "r"] will look for w and r on local, the rest on kitsu.
                - local = [] or None will look for everything on kitsu.
                - local = True will look for everything on local

                local refer to the metadata file.

        - local_path: path of where to find the metadata file. this should be the absolute final path name

    Returns:
        array of array of infos :
        [[0=id, 1=comment (without versionning tag), 2=versionnning , 3=status (short) , 4=update time, 5=person, 6=preview, 7=attachement], etc...]
    """
    if local and local not in [["w"], ["w", "r"], True] :
        error = f"local need to be [\"w\"], [\"w\", \"r\"], True or []/None\n   ->(local: {local})"
        log.error(error)
    if local and not local_path :
        error = f"get_comment with local need a local_path\n   ->(local: {local}, local_path: {local_path})"
        log.error(error)
        raise Exception(error)
    
    current_item = current_item.copy()
    current_item.update({"category": category})

    task_id = get_task_id_from_current_item(current_item)

    if versionning == "None": versionning = None
    # get raw comments
    comments: list = get_bridge().get_comments_db(task_id, only_last=(only_last and not versionning))
    if local:
        local_path = get_formated_flux_metadata_path(local_path)
        local_comments = utils.get_comment_local(local_path)
        if local_comments:
            comments.extend(local_comments)

    result = []
    # process
    for i, comment in enumerate(comments):
        # test if its a flux local meta data
        if comment and type(comment) is list and comment[0] and comment[0] == "fluxMeta" :
            comment_formated = utils.get_formated_comment(comment[1])

            current = ["local", 
                comment_formated[0], comment_formated[1], 
                comment[2], comment[3], comment[4], [], []]

        else :
            comment_formated = utils.get_formated_comment(comment["text"])
            # if a revision_from, we add it to the version dictionnary temporarly. 
            # It will be read later and parse, delete.
            if comment_formated[2]:
                comment_formated[1]["from"] = comment_formated[2]

            current = [comment["id"], 
                comment_formated[0], comment_formated[1], 
                comment["task_status"]["short_name"], 
                comment["updated_at"], 
                comment["person"]["first_name"] + " " + comment["person"]["last_name"], 
                comment["previews"], 
                comment["attachment_files"]]

        if not versionning:
            result.append(current)
            continue

        # print(f"> versionning : {versionning}, comment_formated[1] : {comment_formated[1]}")

        if comment_formated[1] and len(versionning) == len(comment_formated[1].keys()) :
            version_letters = path.get_version_letters()
            is_equal = True
            for key in comment_formated[1] :
                value = comment_formated[1][key]
                index = version_letters.index(key)

                if value != versionning[index] :
                    is_equal = False
                    break

            if is_equal :
                result.append(current)            

        # if versionning and last one and force
        # force writing none to version, to future detect. 
        # We are sure at this point that the version is not matching anyway.
        if i == len(comments)-1 and force_last_with_no_match_versionning and not result :
            current[2] = "None"
            result.append(current)            

    return result

#bridge
def get_seqs() -> list[str] :
    ''' get all sequences name of the project '''
    return get_bridge().get_sequences_name()

#bridge
def get_shots(seq) -> list[str] :
    '''
    get all shots name for a given sequence

    Args:
        - seq: the sequence name
    '''
    return get_bridge().get_shots_name(seq)

#bridge
def get_shots_tasks(seq: str, shot: str, filter: bool = False) -> list[str] :
    '''
    get all tasks name for a given shot

    Args:
        - seq_name:
        - shot_name: 
        - filter_supported: if True, keep only the supported tasks
    '''
    return get_bridge().get_shot_tasks_name(seq, shot, filter)

#bridge
def get_shots_infos(seq, shot) -> dict :
    '''
    get infos for a given shot

    Returns:
        a dict with all shots infos
        - act
        - fps
        - frame_in : used
        - frame_out : used
        - shot_type
        - difficulty
        - briefing_lighting
    '''
    return get_bridge().get_shot_infos(seq, shot)

#bridge
def get_shot_url(seq, shot) -> str :
    ''' get the DB url of a shot (to be able to open it in an internet browser) '''
    return get_bridge().get_shot_url(seq, shot)

#bridge
def get_asset_url(asset_name: str) -> str :
    ''' get the DB url of an asset (to be able to open it in an internet browser) '''
    return get_bridge().get_asset_url(asset_name)