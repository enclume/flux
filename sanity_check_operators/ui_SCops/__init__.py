# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
call for sanity check operators, in a dyn way.

'''

import bpy
import json

from . import class_SCmanager
from .. import functions_SCops as fct
from ... import prefs
from ... import ui as main_ui
from ... import blender_libs

# initialize logger
from ... import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(class_SCmanager)
importlib.reload(fct)
importlib.reload(flux_logger)
importlib.reload(prefs)
importlib.reload(main_ui)
importlib.reload(blender_libs)


if class_SCmanager.sanity_check_manager.instances == 0 :
    # startup of the sanity check manager
    SC_manager = class_SCmanager.sanity_check_manager(name="Main SC manager")


# test items
#SC_manager.add_check_element(name="test_need_an_refresh")
#SC_manager.add_check_element(name="test_GOOD", description = "always good", report = "OK")
#SC_manager.add_check_element(name="test_BAD", description = "always bad", report = "y'a qqch qui va pas")
#SC_manager.add_check_element(name="test_shot_specific", description = "shot", report = "OK", filters = {"category" : "shots"})


class FLUX_UL_SanityCheckElementList(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname):
        cat = bpy.context.scene.flux_ui.current_category
        current_item = bpy.context.scene.flux_ui.current_user_item
        item_valid = False

        if current_item != None and current_item != "None":
            current_item = json.loads(current_item)
        item_valid = main_ui.is_item_valid(current_item)

        if not item_valid : 
            log.debug("item is not valid")
            return
        
        current_item_filters = current_item.copy()
        current_item_filters.update({"category": cat})
        
        SC_manager.draw_list_element(item.name, p_layout=layout, current_item=current_item_filters)


class FLUX_SC_AutoFixElement(bpy.types.Operator):
    ''' Refresh flux_ui_scList '''

    bl_idname = "flux.sc_auto_fix_element"
    bl_label = "Flux SC Auto Fix Element"
    bl_options = {"INTERNAL"}
    bl_description = ("Auto fix the sanity check element")

    def execute(self, context: bpy.types.Context) :
        scene = bpy.context.scene

        scList_element = scene.flux_ui_scList[scene.flux_ui_sc_list_index]

        element = SC_manager.check_elements[scList_element.name]

        if element["source"] == None :
            log.error(f'element \"{element["name"]}\" have no source')
        if element["source"].fct_autofix == None :
            log.error(f'element \"{element["name"]}\" have no fct_autofix')

        element["source"].fct_autofix()

        bpy.ops.flux.sc_panel_refresh()

        log.debug(f'element \"{element["name"]}\" is auto fixed')
        return {"FINISHED"}


class FLUX_SC_RunElementList(bpy.types.Operator):
    ''' Refresh flux_ui_scList '''

    bl_idname = "flux.sc_auto_fix_element_list"
    bl_label = "Flux SC Auto Fix Element"
    bl_options = {"INTERNAL"}
    bl_description = ("Auto fix the sanity check element")

    name:bpy.props.StringProperty(default="None")
    autofix:bpy.props.BoolProperty(default=False)

    def execute(self, context: bpy.types.Context) :

        element = SC_manager.check_elements[self.name]

        if element["source"] == None :
            log.error(f'element \"{element["name"]}\" have no source')
        if self.autofix and not element["source"].fct_autofix :
            log.error(f'element \"{element["name"]}\" have no fct_autofix')

        if self.autofix:
            element["source"].fct_autofix() 

        bpy.ops.flux.sc_panel_refresh() 
        
        log.debug(f'element \"{element["name"]}\" is auto fixed')
        return {"FINISHED"}


class FLUX_SC_PanelRefresh(bpy.types.Operator):
    ''' Refresh the sanity check panel '''

    bl_idname = "flux.sc_panel_refresh"
    bl_label = "Flux SC Panel Refresh"
    bl_options = {"INTERNAL"}
    bl_description = ("Refresh the sanity check panel")

    def execute(self, context: bpy.types.Context) :
        # TODO
        cat = bpy.context.scene.flux_ui.current_category
        current_item = bpy.context.scene.flux_ui.current_user_item
        item_valid = False

        if current_item != None and current_item != "None":
            current_item = json.loads(current_item)
        item_valid = main_ui.is_item_valid(current_item)

        if not item_valid : 
            log.debug("item is not valid")
            return {"FINISHED"}
        
        current_item_filters = current_item.copy()
        current_item_filters.update({"category": cat})
        
        SC_manager.last_current_item = current_item_filters
        SC_manager.update_all_check_elements(current_item_filters)

        print("Flux : Sanity Check panel refreshed")
        return {"FINISHED"}


class FLUX_PT_SanityCheckOpsPanel(bpy.types.Panel):
    ''' Flux Sanity Check UI panel '''
    bl_label = "Flux Sanity Check"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:

        if not prefs.get_project_name():
            return False

        if prefs.get().is_db_logged and prefs.is_show_sanity_check() : 
            return True
        return False

    def draw(self, context: bpy.types.Context):
        cat = bpy.context.scene.flux_ui.current_category
        current_item = bpy.context.scene.flux_ui.current_user_item

        # label title_text construtor
        title_text = ":("
        item_valid = False
        if current_item != None and current_item != "None":
            current_item = json.loads(current_item)
            item_valid = main_ui.is_item_valid(current_item)
        if item_valid:
            title_text = ":) > " + cat.capitalize()
            for each in current_item.keys():
                if each not in ["name", "seq"]:
                    title_text += " > " + current_item[each].capitalize()

        # init layout
        layout = self.layout
        box = layout.box()
        row_title = box.column(align=True)
        row = row_title.row()
        row.label(text=title_text, icon="FAKE_USER_ON")

        # help
        from ... import ui
        op = row.operator(ui.FLUX_OP_Help.bl_idname, text="", icon="HELP", emboss=False)
        op.what = "SANITY"

        row_title.separator()
        if not item_valid :
            row_title.label(text="Please select an Item with flux")
            return

        row_title2 = row_title.column(align=True)
        # refresh disk
        row_title2.operator(
            FLUX_SC_PanelRefresh.bl_idname,
            icon="FILE_REFRESH",
            text="Check All",
            emboss=True,
        )
        row_title2.scale_y=1.5

        #
        current_item_filters = current_item.copy()
        current_item_filters.update({"category": cat})

        if len(bpy.context.scene.flux_ui_scList) != SC_manager.to_display :
            blender_libs.timer.run_fct(SC_manager.refresh_all_flux_ui_scList_element)

        if SC_manager.need_first_update or SC_manager.last_current_item != current_item_filters :
            SC_manager.last_current_item = current_item_filters
            SC_manager.update_all_check_elements(current_item_filters)
            if bpy.context.scene.flux_ui_sc.is_clean != SC_manager.is_clean :
                blender_libs.timer.run_fct(SC_manager.set_final_is_clean)

            if SC_manager.need_first_update :
                SC_manager.need_first_update = False

        # row_title.operator(
        #     "flux.sc_panel_refresh",
        #     icon="FILE_REFRESH",
        #     text="CHECK",
        #     emboss=True,
        # )

        SC_manager.draw_sc_report(p_layout=box)

        #SC_manager.draw(p_layout=box, current_item=current_item_filters)
        box.template_list(listtype_name="FLUX_UL_SanityCheckElementList",
                             list_id="",
                             dataptr=context.scene,
                             propname="flux_ui_scList",
                             active_dataptr=context.scene,
                             active_propname="flux_ui_sc_list_index",
                             item_dyntip_propname='description',
                             rows=1,
                             maxrows=5,
                             type='DEFAULT', 
                             sort_lock=True) 
        
        SC_manager.draw_element_report(p_layout=box)


class FLUX_SC_Popup(bpy.types.Operator):
    '''
    Flux popup message for sanity check, when you cannot autofix
    '''
    bl_idname = "flux.sc_popup_message"
    bl_label = "Cannot autofix, buddy..."
    bl_options = {"REGISTER"}
    bl_description = "Flux popup message for sanity check, when you cannot autofix"

    message: bpy.props.StringProperty(default="test")

    def execute(self, context):

        print(self.message)
        # self.report({"ERROR"}, f"{self.message}")
        return {"FINISHED"}

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        col.label(text=self.message)

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self, width=500)