# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

import bpy
import json
import textwrap

from .. import ui_SCops
from . import class_SCmanager
from .. import functions_SCops as fct
from ... import prefs
from ... import sanity_check_operators as san_check_ops
from ... import ui as main_ui

# initialize logger
from ... import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(ui_SCops)
importlib.reload(class_SCmanager)
importlib.reload(fct)
importlib.reload(flux_logger)
importlib.reload(prefs)
importlib.reload(san_check_ops)
importlib.reload(main_ui)

    
class label_textwrap():
    ''' description TODO '''
    def __init__(self, p_layout, text='', text_ctxt='', translate=True, icon='NONE', icon_value=0):
        self.text = text
        self.text_ctxt = text_ctxt
        self.translate = translate
        self.icon = icon
        self.icon_value = icon_value
        self.p_layout = p_layout

        self.chars_factor = 8.5
        self.width_factor = 1
        self.init_indent = 4

        self.draw()


    def remap(self, old_val, old_min, old_max, new_min, new_max):
        ''' description TODO '''
        return (new_max - new_min) * (old_val - old_min) / (old_max - old_min) + new_min


    def get_text_lines(self):
        ''' description TODO '''
        text = self.text.split("\n")
        text_lines = []

        init_indent = ''

        for i, item in enumerate(text):
            if i == 0 and self.icon != 'NONE':
                init_indent = self.init_indent

            context = bpy.context
            w = context.region.width

            mult = self.remap(w, 150, 1600, 1, 2)
            chars = int((w * mult) / self.chars_factor)   # 7 pix on 1 character
            chars = int((w * 1) / self.chars_factor)   # 7 pix on 1 character
            wrapper = textwrap.TextWrapper(width=chars, initial_indent = init_indent)
            text_lines += wrapper.wrap(text=item)

        return text_lines


    def draw_line(self, text_line, line_index):
        ''' description TODO '''
        row = self.p_layout.row()

        icon='NONE'
        icon_value=0

        if line_index == 0 :
            icon=self.icon
            icon_value=self.icon_value
            if self.icon != 'NONE'and text_line.startswith(self.init_indent):
                text_line = text_line[len(self.init_indent):]

        row.label(text=text_line, text_ctxt=self.text_ctxt, translate=self.translate, icon=icon, icon_value=icon_value)
        
        row.scale_y = 0.6


    def draw(self):
        ''' description TODO '''
        self.init_indent = ''.zfill(self.init_indent).replace("0", " ")
        text_lines = self.get_text_lines()
        for line_index, text_line in enumerate(text_lines):
            self.draw_line(text_line, line_index)