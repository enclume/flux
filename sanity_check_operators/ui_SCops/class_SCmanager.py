# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

import bpy
import json

from . import SC_utils
from .. import ui_SCops
from .. import functions_SCops as fct
from ... import prefs
from ... import sanity_check_operators as san_check_ops
from ... import blender_libs

# initialize logger
from ... import flux_logger 
log = flux_logger.get_flux_logger()


#reload lib
import importlib
importlib.reload(SC_utils)
importlib.reload(ui_SCops)
importlib.reload(fct)
importlib.reload(flux_logger)
importlib.reload(prefs)
importlib.reload(san_check_ops)
importlib.reload(blender_libs)


class sanity_check_manager:
    ''' Sanity Check panel manager '''
    instances = 0
    def __init__(self, name = "unnamed sanity_check_manager"):
        self.name = name
        self.check_elements = {}
        self.add_all_check_elements()
        self.need_first_update = True
        self.last_current_item = False
        self.to_display = 0
        self.bad_element = -1
        self.is_clean = False

        sanity_check_manager.instances += 1

        log.debug(f"new sanity_check_manager setuped : {self.name}")


    def add_check_element(self, name: str, description: str = None, report: str = "to check", filters: dict = {}, source = None):
        ''' 
        add a check element to the current sanity check manager

        Args:
            - name : the name of the check element
            - description : the description of the check element
            - report : the report of the check element ("OK", "to check", "issue description")
            - filters : show filters relative to the current_items
            - source : the source object of the check element
        '''

        if not description :
            description = "no description"

        new_element = {"name": name, 
                       "description": description, 
                       "report": report, 
                       "filters": filters, 
                       "source": source}
        self.check_elements.update({name: new_element})

        log.debug(f"check element added to {self.name} : {new_element}")
        return new_element


    def add_flux_ui_scList_element(self, element, scene = None):
        ''' Add element (from self.check_elements) to flux_ui_scList '''
        if not scene :
            scene = bpy.context.scene

        newItem = scene.flux_ui_scList.add()
        newItem.name = element["name"]
        newItem.description = element["description"]
        newItem.report = element["report"]
        newItem.filters = element["filters"]
        newItem.statut = False

        if element["report"] == "OK" :
            newItem.statut = True

        return newItem


    def add_all_check_elements(self) :
        ''' add the check elements defined in san_check_ops\\__init__.py '''
        sc_elements_list = san_check_ops.sc_elements_list
        log.debug(f"sc_elements_list ({len(sc_elements_list)}) : {sc_elements_list}")

        for el in sc_elements_list:
            if el.name in self.check_elements.keys() :
                log.info(f"{el.name} already in self.check_elements")
                log.debug(f"  -> {self.check_elements.keys()}")
                continue

            log.info(f"add {el.name}")
            report = el.get_check_report()
            if report != "OK" : report="to check"

            new_element = self.add_check_element(name=el.name, description=el.description, 
                                                 report=report, filters=el.filters, source=el)
            
            if prefs.get() and prefs.get().is_db_logged :
                try :
                    self.add_flux_ui_scList_element(new_element)
                except :
                    log.error("fail to add_flux_ui_scList_element")

        return self.check_elements


    def compare_filters(self, element, current_item) :
        ''' compare element filters to the current item to see if the element should be displayed '''

        if not element["filters"] or not current_item :
            return True

        if "add" in element["filters"].keys() :
            filters = element["filters"]["add"]

            for key in filters :
                if key not in current_item.keys() : continue

                if type(filters[key]) is list :
                    filters_list = filters[key].copy()
                    for i, item in enumerate(filters_list) :
                        filters_list[i] = item.lower()
                    if str(current_item[key]).lower() not in filters_list :
                        return False
                elif str(filters[key]).lower() != str(current_item[key]).lower() :
                    return False
                
        if "sub" in element["filters"].keys() :
            filters = element["filters"]["sub"]
            for key in filters :
                if key not in current_item.keys() : continue

                if type(filters[key]) is list :
                    filters_list = filters[key].copy()
                    for item in filters_list :
                        filters_list[item] = item.lower()
                    if str(current_item[key]).lower() in filters_list :
                        return False
                elif str(filters[key]).lower() == str(current_item[key]).lower() :
                    return False
                
        return True
    

    def update_is_clean(self):
        ''' update self.is_clean (if is_clean is True, all check elements have an "OK" repport) '''
        for element in self.check_elements.values() :
            if element["report"] != "OK" :
                self.is_clean = False
                return False
            
        self.is_clean = True
        return True
    

    def update_all_check_elements(self, current_item) :
        ''' 
        update all check elements reports 
        refresh_all_flux_ui_scList_element
        update_is_clean
        '''

        self.to_display = 0
        self.bad_element = 0
        for element in self.check_elements.values() :
            filter_result = self.compare_filters(element, current_item)
            if filter_result :
                self.to_display += 1

            if not element["source"] :
                log.debug("no source for " + element["name"] + " -> set to OK")
                element["report"] = "OK"
                continue

            if filter_result :
                element["report"] = element["source"].get_check_report()
                if element["report"] != "OK" :
                    self.bad_element +=1


        blender_libs.timer.run_fct(self.refresh_all_flux_ui_scList_element)
        self.update_is_clean()

        log.debug("all check elements reports are updated")
        return True


    def print_check_elements(self):
        ''' print all check elements currently in the current sanity check manager '''
        print(f"\n~~ check_elements for {self.name} ~~")
        for element in self.check_elements.values() :
            print(f"  {element}")


    def get_state_icon(self, report):
        ''' get the right state icon based on the check element report '''
        state_icons = ["ERROR", "RECOVER_LAST", "CHECKMARK"]
        if report == "OK" :
            return state_icons[2]
        if report == "to check" :
            return state_icons[1]
        
        return state_icons[0]
    
    def get_state_color(self, report):
        ''' description TODO '''
        if report == "OK":
            return "load_list_color_v"
        if report == "to check":
            return "load_list_color_r"
        return "load_list_color_w"
    

    def refresh_all_flux_ui_scList_element(self):
        ''' description TODO '''
        current_item = fct.fct_utils.get_current_item()
        scene = bpy.context.scene

        scene.flux_ui_scList.clear()

        elements = self.check_elements.values()

        for element in elements :
            if not self.compare_filters(element, current_item) : continue
            
            newItem = scene.flux_ui_scList.add()
            newItem.name = element["name"]
            newItem.description = element["description"]
            newItem.report = element["report"]
            newItem.filters = json.dumps(element["filters"])
            newItem.statut = False

            if element["report"] == "OK" :
                newItem.statut = True
        

    def clear_flux_ui_scList_element(self):
        ''' clear the scene collection property flux_ui_scList '''
        bpy.context.scene.flux_ui_scList.clear()
        

    def set_final_is_clean(self):
        ''' set the scene property is_clean '''
        bpy.context.scene.flux_ui_sc.is_clean = self.is_clean
        

    def draw_single_element(self, element, p_layout, current_item: dict = {}):
        ''' 
        [OLD draw Function]
        draw function for a single check element 

        Args:
            - p_layout : the parent layout object
            - current_item : the current_item used for the filtering
        '''
        if not self.compare_filters(element, current_item) :
            return
            
        name = element["name"]
        report = element["report"]
        side_arrow="RIGHTARROW"
        if report != "OK" :
            side_arrow="DOWNARROW_HLT"

        row = p_layout.row(align=True)
        row.label(text=f"{name}", icon=side_arrow)
        row.label(text="", icon=self.get_state_icon(element["report"]))
        if report != "OK" :
            row = p_layout.row(align=True)
            row.label(text=f"      {report}")


    def draw(self, p_layout, current_item: dict = {}):
        ''' 
        [OLD draw Function]
        draw function for all check elements in the current sanity check manager 

        Args:
            - p_layout : the parent layout object
            - current_item : the current_item used for the filtering
        '''
        need_separator = False

        for element in self.check_elements.values() :
            if element["report"] != "OK" :
                self.draw_single_element(element, p_layout, current_item)
                need_separator = True

        if need_separator :
            row_gap = p_layout.row(align=True) 
            row_gap.separator()

        for element in self.check_elements.values() :
            if element["report"] == "OK" :
                self.draw_single_element(element, p_layout, current_item)


    def draw_list_element(self, element_name, p_layout, current_item: dict = {}):
        ''' 
        draw function for a single check element in the SClist

        Args:
            - element_name : the name of the element to display
            - p_layout : the parent layout object
            - current_item : the current_item used for the filtering
        '''
        # this is done if optimisation is needed, we can switch back to the old UI (without color and buildin operators)
        simple = False

        element = self.check_elements[element_name]
        if not self.compare_filters(element, current_item) :
            return
        
        name = element["name"]
        report = element["report"]
        sideL_icon=self.get_state_icon(report)

        if simple: 
            # sideL_icon="RIGHTARROW"
            row = p_layout.row(align=True)
            row.label(text=f"{name}", icon=sideL_icon)
            return
        

        sideL_icon = self.get_state_icon(report)

        # row = p_layout.row(align=True)
        split = p_layout.split(factor=0.02)

        # color
        split.prop(bpy.context.scene.flux_ui_utils, self.get_state_color(report), text="")

        # layout
        row = split.row(align=True)
        row.label(text=f"{name}", icon=sideL_icon)

        # autofix if needed
        if (element["report"] != "OK" and 
            element["source"] and 
            element["source"].fct_autofix) : 

            op = row.operator(ui_SCops.FLUX_SC_RunElementList.bl_idname, icon='MODIFIER_DATA', text="")
            op.name = element["name"]
            op.autofix = True

        # check item
        op2 = row.operator(ui_SCops.FLUX_SC_RunElementList.bl_idname, icon='FILE_REFRESH', text="")
        op2.name = element["name"]
        op2.autofix = False


    def draw_element_report(self, p_layout):
        ''' 
        draw function for the report of a single check element on the bottom of the SClist

        Args:
            - element_name : the name of the element to display
            - p_layout : the parent layout object
            - current_item : the current_item used for the filtering
        '''
        list_index = bpy.context.scene.flux_ui_sc_list_index
        if not bpy.context.scene.flux_ui_scList : return
        try :
            list_element = bpy.context.scene.flux_ui_scList[list_index]
        except Exception as error :
            log.debug(error)
            return

        element = self.check_elements[list_element.name]
        name = element["name"] 
        #description = element["description"]
        report = element["report"]

        # -------

        report_frame = p_layout

        # SC_utils.label_textwrap(report_frame, text=f"{name}", icon="RIGHTARROW")
        row = report_frame.row()
        row.separator

        if (element["report"] not in ["OK", "to check"] and 
            element["source"] and 
            element["source"].fct_autofix) : 

            row.operator(
                ui_SCops.FLUX_SC_AutoFixElement.bl_idname,
                icon="MODIFIER_DATA",
                text="Auto Fix",
                emboss=True)

        if element["report"] == "OK" :
            row.label(text=f"The \"{name}\" check is OK !")
            return
        if element["report"] == "to check" :
            row = report_frame.row()
            row.label(text=f"The \"{name}\" check need to be refreshed")
            row = report_frame.row()
            op = row.operator(ui_SCops.FLUX_SC_RunElementList.bl_idname, icon='FILE_REFRESH', text="refresh")
            op.name = element["name"]
            op.autofix = False
            return
        
        SC_utils.label_textwrap(report_frame, text=f"{report}", icon=self.get_state_icon(report))

        # row = report_frame.row()
        # row.separator

        # autofix
        # row = report_frame.row()

        return
    

    def draw_sc_report(self, p_layout):
        ''' 
        draw function for the global SC report

        Args:
            - element_name : the name of the element to display
            - p_layout : the parent layout object
            - current_item : the current_item used for the filtering
        '''
        text = "it's OK !"
        icon = 'FUND'
        if self.bad_element :
            text = f"{self.bad_element} issue"
            if self.bad_element != 1 : text += "s"
            icon = 'CANCEL'

        row = p_layout.row()
        row.alignment = 'CENTER'
        row.label(text=text, icon=icon)
        row.scale_y = 0.7