# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
#Sanity check related things
-> all Sanity_Check_Element are defined here
'''
from . import functions_SCops as fct
from . import class_SCelement

# initialize logger
from .. import flux_logger
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(class_SCelement)
importlib.reload(flux_logger)
importlib.reload(fct)

from .class_SCelement import Sanity_Check_Element

#variable where SC element are stored
sc_elements_list = []


#exemple add a sanity check element
#sc_elements_list.append(Sanity_Check_Element(name="my name to show", 
#                                             function=fct.some_function, #-> warning, no "()" at the end !
#                                             description="my funny description to show", 
#                                             filters={})) #-> can be "{"category": "assets"}"


#
#filters :
#"category": ["assets", "shots"]
#"type": ["background", "character", "element", "fx2d", "fx3d", "matte painting", "prop", "template"]
#"task": ["concept", "fx2d", "fx3d", "lightrig", "matte painting", "modeling", "rigging", "set dressing", "surfacing", "uvs"]
#"task": ["blocking", "color script", "compositing", "conformation", "layout", "lighting", "polish", "rendering", 
#         "shot fx", "shot matte painting", "shot pre matte paint", "shot set dressing", "spline", "storyboard"]
#
#

#sc_elements_list.append(Sanity_Check_Element(name="sc_check_nbr_objects", 
#                                             function=fct.sc_check_nbr_objects,
#                                             fct_autofix=fct.sc_check_animatic, 
#                                             description="check if there is 4 objects in the scene", 
#                                             filters={},
#                                             filters_exclude={}))

sc_elements_list.append(Sanity_Check_Element(name="master collection", 
                                             function=fct.sc_check_master_collection, 
                                             fct_autofix=fct.sc_autofix_master_collection,
                                             description="check if the master collection is set properly", 
                                             filters={"category": "assets"}))

sc_elements_list.append(Sanity_Check_Element(name="animatic", 
                                             function=fct.sc_check_animatic, 
                                             fct_autofix=fct.sc_autofix_animatic,
                                             description="check if the animatic is set properly",
                                             filters={"category": "shots"}, 
                                             filters_exclude={}))


sc_elements_list.append(Sanity_Check_Element(name="render parameters", 
                                             function=fct.sc_check_render_params, 
                                             fct_autofix=fct.sc_autofix_render_params, 
                                             description="check if the render parameters are good", 
                                             filters={"category": "shots"}, 
                                             filters_exclude={}))

sc_elements_list.append(Sanity_Check_Element(name="visibility (viewport vs render)", 
                                             function=fct.sc_check_visibility, 
                                             fct_autofix=fct.sc_autofix_visibility,
                                             description="check if objects and collections have their viewport and render visibility set equally",
                                             filters={}, 
                                             filters_exclude={}))


# check_mod
sc_elements_list.append(Sanity_Check_Element(name="objects naming", 
                                             function=fct.sc_check_mod_naming, 
                                             fct_autofix=fct.sc_autofix_mod_naming,
                                             description="check if objects are named properly", 
                                             filters={"task": ["modeling", "surfacing"]}, 
                                             filters_exclude={}))

# REMOVE "__" TO UNHIDE
sc_elements_list.append(Sanity_Check_Element(name="objects topology", 
                                             function=fct.sc_check_mod_topology, 
                                             description="check if objects have good topology", 
                                             filters={"task": "modeling__"}, 
                                             filters_exclude={}))

# REMOVE "__" TO UNHIDE
sc_elements_list.append(Sanity_Check_Element(name="objects UVs", 
                                             function=fct.sc_check_mod_uv, 
                                             description="check if objects have good UVs", 
                                             filters={"task": "modeling__"}, 
                                             filters_exclude={}))

sc_elements_list.append(Sanity_Check_Element(name="objects transforms", 
                                             function=fct.sc_check_mod_obj_transform, 
                                             fct_autofix=fct.sc_autofix_mod_obj_transform,
                                             description="check if objects have good transforms", 
                                             filters={"task": "modeling"}, 
                                             filters_exclude={}))


# check_layout
sc_elements_list.append(Sanity_Check_Element(name="frame range", 
                                             function=fct.sc_check_frame_range, 
                                             fct_autofix=fct.sc_autofix_frame_range,
                                             description="check if the frame range is set properly", 
                                             filters={"category": "shots"}, 
                                             filters_exclude={}))

sc_elements_list.append(Sanity_Check_Element(name="camera locked", 
                                             function=fct.sc_check_locked_camera, 
                                             fct_autofix=fct.sc_autofix_locked_camera,
                                             description="check if the camera is locked", 
                                             filters={"task": "layout"}, 
                                             filters_exclude={}))

# check set dress
sc_elements_list.append(Sanity_Check_Element(name="Cam+Light outside main collection",
                                             function=fct.sc_check_cameralight_outside_maincollection,
                                             fct_autofix=fct.sc_autofix_cameralight_outside_maincollection,
                                             description="Check if Camera and Light are outside the main collection",
                                             filters={"task": "Set Dressing"},
                                             filters_exclude={}))


