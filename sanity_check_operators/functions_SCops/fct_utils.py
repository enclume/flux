# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Sanity check utils functions 
(indirectly used for a report or an auto fix)

'''
import bpy
import json

from ... import database_access
from ... import file_access
from ... import ui as main_ui

# initialize logger
from ... import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(database_access)
importlib.reload(file_access)
importlib.reload(flux_logger)
importlib.reload(main_ui)


def get_current_item(version=[1, 1, 1]) -> dict :
    ''' get the current item based on the user input '''

    cat = bpy.context.scene.flux_ui.current_category
    current_item = bpy.context.scene.flux_ui.current_user_item
    item_valid = False

    if current_item == None or current_item == "None": return False
    
    current_item = json.loads(current_item)
    item_valid = main_ui.is_item_valid(current_item)

    if not item_valid : return False

    current_item = current_item.copy()
    current_item.update({"category": cat})
    current_item.update({"version": version})
    
    return current_item


def get_current_item_path(current_item = None) -> str :
    ''' get the current item path based on the current item '''

    if not current_item :
        current_item = get_current_item()

    return file_access.get_item_path(item_data=current_item)


def get_good_render_params():
    ''' description TODO '''

    good_render_params = {
        "eevee": {
            "taa_samples" : 8,
            "taa_render_samples" : 8,
            "use_bloom" : False,
            "use_gtao" : True,
            "use_motion_blur" : False,
            "use_ssr" : False,
            "use_ssr_refraction" : False
        },
        "frame_step" : 1,
        "render" : {
            "fps" : 24,
            "fps_base" : 1.0,
            "resolution_x" : 2048,
            "resolution_y" : 858,
            "resolution_percentage" : 100,
            "use_stamp" : False
        },
        "use_preview_range" : False,
    }
    return good_render_params


def get_current_shot_frame_info(override_f_in: int = None):
    ''' description TODO '''

    current_item = get_current_item()
    if not current_item or current_item["category"] != "shots" :
        return False
    
    seq, shot = current_item["seq"], current_item["shot"]

    info = database_access.get_shots_infos(seq=seq, shot=shot)

    if not info :
        return False
    
    if not override_f_in :
        return [info["frame_in"], info["frame_out"]]
    
    override_f_in = int(override_f_in)
    
    f_in = int(info["frame_in"])
    f_out = int(info["frame_out"])

    offset = override_f_in - f_in
    new_f_out = f_out + offset

    log.debug(f"offset = {offset} : {override_f_in} - {f_in} (override_f_in - f_in)")

    return [override_f_in, new_f_out]


def get_current_shot_frame_end(override_f_in: int = None):
    ''' description TODO '''

    frame_info = get_current_shot_frame_info(override_f_in)
    
    if not frame_info :
        return False
    
    return frame_info[1]
    


def is_lod_rig_obj(to_test):
    ''' description TODO '''
    if not str(to_test).startswith("<bpy_struct, Object"):
        log.error(f"to_test need to be an object\n->(type(to_test) is {type(to_test)})")
        return False
    
    if not to_test.animation_data :
        return False
    
    if len(to_test.animation_data.drivers) == 0 :
        return False

    for driver in to_test.animation_data.drivers :
        if driver.data_path == "hide_viewport" :
            return True
        
    return False

def is_lod_rig_col(to_test):
    ''' description TODO '''
    if not str(to_test).startswith("<bpy_struct, Collection"):
        log.error(f"to_test need to be a collection\n->(type(to_test) is {type(to_test)})")
        return False
    
    if not to_test.objects :
        return False
    
    for obj in to_test.objects :
        if not is_lod_rig_obj(obj) :
            return False
        
    return True

def is_lod_rig(to_test):
    ''' 
    test if the given object or collection is rig LOD related 

    Args :
        to_test : the object or the collection to test
    '''

    if not str(to_test).startswith("<bpy_struct, Object") and not str(to_test).startswith("<bpy_struct, Collection"):
        log.error(f"the thing to test is not an object or a collection\n->(type(to_test) is {type(to_test)})")
        return False
    
    if str(to_test).startswith("<bpy_struct, Collection") :
        return is_lod_rig_col(to_test)
    
    return is_lod_rig_obj(to_test)