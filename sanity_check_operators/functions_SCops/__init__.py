# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Sanity check functions
(functions used for "get SC element report" and "autofix" if bad report)

'''
import bpy
import bmesh
import mathutils
import os

from . import fct_utils
from ... import blender_libs
from ...blender_libs import animatics

# initialize logger
from ... import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(animatics)
importlib.reload(blender_libs)
importlib.reload(fct_utils)
importlib.reload(flux_logger)

'''
def sc_check_exemple_function() -> str :

    "do something"
    try :
        bad code
    except :
        return "to check"

    if all_right :
        return "OK"
    if not_good :
        return "it's not good because blablabla"
'''   


def sc_check_master_collection() -> str :
    ''' check the master collection '''
    
    context = blender_libs.get_bpy_context()
    if not context : return "to check" 

    futur_blendpath = None
   
    if not context.blend_data.filepath or not context.blend_data.filepath.startswith("W:") :
        futur_blendpath = fct_utils.get_current_item_path()

    good_master_name = blender_libs.collection.get_master_name_from_path(path=futur_blendpath)

    good_master_col = bpy.data.collections.get(good_master_name)

    if not good_master_col :
        return f"there should be a collection in the file with this name : {good_master_name}"
    
    if good_master_name not in context.scene.collection.children :
        return f"the collection {good_master_name} should be at the root of the outliner hierarchy"

    return "OK"

def sc_autofix_master_collection():
    ''' autofix for the master collection '''

    context = blender_libs.get_bpy_context()
    if not context : return "to check" 

    futur_blendpath = None
   
    if not context.blend_data.filepath or not context.blend_data.filepath.startswith("W:") :
        futur_blendpath = fct_utils.get_current_item_path()

    return blender_libs.collection.set_master(path=futur_blendpath)


def sc_check_model():
    '''
    1. Naming
        • OK Impossible | Duplicated Names
        • OK | Shape Names
        • OK | Namespaces -> replace(" ", "_") and specials character -> replace with "-"

    2. Topology -> add-ons Mesh Check
        • . | Triangles
        • . | Ngons
        • . | Open Edges
        • . | Hard Edges
        • . | Lamina faces
        • . | None manifold Edges
        • . | Starlike
        • . | Unmerged vertices
    3. UVs
        • . | Self Penetrating UVs
        • . | Missing UVs
        • . | UV Range 
        • . | Cross Border

    4. General
        • OK | Layers -> collection name ? -> master col
        • OK | Origin to 3D cursor
        • OK | All transform
        '''

    return


def sc_check_mod_naming():
    ''' check if objects are named properly '''

    to_return = []
    is_alnum = []
    diff_name = []
    errors = []
    for obj in bpy.data.objects :
        try :
            name = obj.name
        except Exception as error:
            error = f"{obj} : {error}"
            errors.append(error)
            log.info(error)
            continue

        char_list = ["-", ".", "_"]
        for item in char_list :
            name = name.replace(item, "")

        if not name.isalnum() :
            is_alnum.append(obj)

        if getattr(obj.data, "name", False) == False :
            continue
        if obj.data.name != obj.name and obj.data.users == 1 :
            diff_name.append(obj)
        
    if is_alnum :
        if len(is_alnum) == 1 :
            to_return.append(f"One object has spaces or specials characters in its name ({is_alnum[0].name})")
        else :
            to_return.append(f"{len(is_alnum)} objects have spaces or specials characters in their name")

    if diff_name :
        if len(diff_name) == 1 :
            to_return.append(f"One object has its subobject named differently ({diff_name[0].name})")
        else :
            to_return.append(f"{len(diff_name)} objects have their subobject named differently")

    if errors :
        errors = ", ".join(errors)
        errors = f"there are some errors : {errors}"
        to_return.append(errors)

    if to_return :
        to_return = f"{len(is_alnum) + len(diff_name)} naming issue(s) :\n" + "\n".join(to_return)
        return to_return
    
    return "OK"

def sc_autofix_mod_naming():
    ''' autofix objects naming '''

    for obj in bpy.data.objects :
        if " " in obj.name :
            obj.name = obj.name.replace(" ", "_")
        
        if not obj.name.isalnum() :
            new_name = ""
            char_list = ["-", ".", "_"]
            for char in obj.name :
                if char.isalnum() or char in char_list :
                    new_name += char
                else :
                    new_name += "-"
            obj.name = new_name

        if getattr(obj.data, "name", False) == False :
            continue

        if obj.data.name != obj.name and obj.data.users == 1 :
            obj.data.name = obj.name

    return True


def check_object_topology(obj):
    ''' description TODO '''

    if obj.type not in ["MESH"] :
        return False
    
    # bmesh.ops.find_doubles
    # bpy.ops.mesh.select_non_manifold(extend=True, use_wire=True, use_boundary=True, use_multi_face=True, use_non_contiguous=True, use_verts=True)
    # bpy.ops.uv.select_overlap(extend=False)
    
    if bpy.context.object.mode == 'EDIT':
        obj = bpy.context.object
        me = obj.data
        bm = bmesh.from_edit_mesh(me)

        info_str = ""
        tris = ngons = 0

        for f in bm.faces:

            v = len(f.verts)
            if v == 3:
                tris += 1
            elif v > 4:
                ngons += 1

        bmesh.update_edit_mesh(me)
        info_str = f"Ngons: {ngons} | Tris: {tris}"
    
    return True


def sc_check_mod_topology():
    ''' check the mod topology '''

    for obj in bpy.data.objects :
        break
        if obj.type == "MESH" : 
            check_object_topology(obj)

    return "OK"


def sc_check_mod_uv():
    ''' check the mod UVs '''

    for obj in bpy.data.objects :
        break

    return "OK"


def sc_check_mod_obj_transform():
    ''' check the obj transforms '''

    # -> reset all transforms (no rot, loc to world center, scale to 1 and no delta)
    to_return = []
    multi_users = False

    for obj in bpy.data.objects :
        if obj.type not in ["MESH"] : continue

        to_return_line = []
        # location
        if obj.location != mathutils.Vector((0.0, 0.0, 0.0)) :
            to_return_line.append("location")
        if obj.delta_location != mathutils.Vector((0.0, 0.0, 0.0)) :
            to_return_line.append("delta location")

        # rotation
        rotation = obj.rotation_euler
        d_rotation = obj.delta_rotation_euler
        if obj.rotation_mode == "QUATERNION" :
            rotation = obj.rotation_quaternion
            d_rotation = obj.delta_rotation_quaternion

        for rot in rotation :
            if rot != 0 :
                to_return_line.append("rotation")
                break

        for rot in d_rotation :
            if rot != 0 :
                to_return_line.append("delta rotation")
                break
        
        # scale
        if obj.scale != mathutils.Vector((1.0, 1.0, 1.0)) :
            to_return_line.append("scale")
        if obj.delta_scale != mathutils.Vector((1.0, 1.0, 1.0)) :
            to_return_line.append("delta scale")

        if to_return_line :
            multi_users = True
            to_return_line = ", ".join(to_return_line)
            to_return_line = f"> {obj.name} ({to_return_line})"
            to_return.append(to_return_line)


    if to_return :
        to_return = "\n".join(to_return)
        to_return = "some objects do not have their transformation applied :\n" + to_return
        if multi_users :
            multi_users = "WARNING: There are some multi-user objects, AutoFix will not work for them.\n"
            to_return = multi_users + to_return
        return to_return

    return "OK"

def sc_autofix_mod_obj_transform():
    ''' autofix the obj transforms '''

    context = blender_libs.get_bpy_context()
    if not context : return "context not defined"

    user_selection = context.selected_objects
    to_return = []

    for obj in bpy.data.objects : obj.select_set(False) 

    for obj in bpy.data.objects : 
        if obj.type not in ["MESH"] : continue

        obj.select_set(True)
        try :
            bpy.ops.object.transform_apply(location=True, rotation=True, scale=True, properties=True, isolate_users=False)
        except Exception as e :
            to_return.append(str(e))

        obj.select_set(False)

    for obj in bpy.data.objects : obj.select_set(False) 
    for obj in user_selection : obj.select_set(True)

    if to_return :
        to_return = "\n".join(to_return)
        log.error(to_return)
        return to_return

    return True


def sc_check_surfacing():
    '''
    • . | Check images exist
    • . | Check images packed values
    • OK | Check images paths are valid
    • ? . | Check material name convention
    • OK | (G) Check the name of meshes -> existe déjà en Mod
    • OK | (G) Check objects special characters -> existe déjà en Mod
    • ? . | Check the number of materials slots
    • . | Check turntable scene done
    • . | Check Material override (some assets still have the modeling link)
    '''

    return


def sc_check_setdressing():
    '''
    • . | Check File purged
    • . | Check cam still present
    • . | Check light still present
    • . | (G) Check collection outside the main one -> already OK with check master collection ?
    '''

    return


def sc_check_layout():
    '''
    - OK, mais pas 100% accurate, car l'animatic n'est pas gérée de façon constante | Check if the correct animatic clip is loaded 
    in the sequences of the blender layout file.

    - impossible for now | Check if all the different 3D assets are the latest v01 version.
    (For props, characters, elements, set dressing. This might be difficult to check, but I did have something 
    like this in previous productions and it is quite useful. How to check this I don't really know. 
    Checking it with the breakdown list on Kitsu will probably not work because sometimes we add extra or different assets 
    to a layout scene, which might not be in the breakdown list.)

    - need to use flux, it's all | Does the file have the correct file name and is it saved in the correct directory?
    (Self explanatory. For this we need to have saved the file already, but still a useful one.)
    '''
    return


def sc_check_locked_camera():
    ''' description TODO '''

    context = blender_libs.get_bpy_context()
    if not context : return "to check"

    camera = context.scene.camera

    if camera == None :
        return "the scene camera is not defined"
    
    lock_L = camera.lock_location
    lock_R = camera.lock_rotation

    if False not in lock_L and False not in lock_R :
        return "OK"
    
    axes = ["X", "Y", "Z"]
    
    to_return = []

    for i, lock in enumerate(lock_L) :
        if not lock :
            to_return.append(f"{axes[i]} location") 

    for i, lock in enumerate(lock_R) :
        if not lock :
            to_return.append(f"{axes[i]} rotation")

    return "are not locked : " + ", ".join(to_return)

def sc_autofix_locked_camera():
    ''' autofix description TODO '''

    context = blender_libs.get_bpy_context()
    if not context : return "to check"

    camera = context.scene.camera

    if camera == None :
        return "the scene camera is not defined"
    
    lock_L = camera.lock_location
    lock_R = camera.lock_rotation

    for i in range(3) :
        lock_L[i] = True
        lock_R[i] = True
    
    return True


def sc_check_frame_range():
    ''' check the frame range '''

    # print("sc_check_frame_range")

    context = blender_libs.get_bpy_context()
    if not context : return "to check"

    good_start = 101
    good_end = fct_utils.get_current_shot_frame_end(good_start)

    report = []

    if context.scene.frame_start != good_start :
        report.append(f"the start frame should be the frame {good_start}")
    if good_end and context.scene.frame_end != good_end :
        report.append(f"the end frame should be the frame {good_end}")

    if report : 
        return "\n".join(report)

    if not good_end:
        return "No Shot Info Founded. Cannot be fix"

    return "OK"

def sc_autofix_frame_range():
    ''' autofix the frame range '''

    context = blender_libs.get_bpy_context()
    if not context : return "to check"

    good_start = 101
    good_end = fct_utils.get_current_shot_frame_end(good_start)

    context.scene.frame_start = good_start 

    if good_end :
        context.scene.frame_end = good_end 
    # else:
    #     return "No Shot Info. Cannot fix."

    return True



def sc_check_animatic(auto_fix = False) -> str :
    ''' check the animatic '''

    # print("sc_check_animatic")

    context = blender_libs.get_bpy_context()
    if not context : return "to check"        

    # check if all needed data are ok   
    current_item = fct_utils.get_current_item()

    if not current_item : 
        error = f"can't get the current item (current_item = {current_item})"
        log.info(error)
        return error
    
    if current_item["category"] != "shots" : 
        error = "the current item category is not shots !"
        log.error(error)
        return error
    
    # check if it's a bad animatics case
    if animatics.utils.get_is_bad_animatic(sq = current_item["seq"][-4:], sh = current_item["shot"][-4:]) :
        log.info(f"It's a bad animatic case, just waiting for a final file")
        return "OK"
    
    # get right animatic path
    good_animatic_path = animatics.utils.get_animatic_fullpath_from_data(seq = current_item["seq"][-4:], shot = current_item["shot"][-4:], file_extension = "mov")
    
    current_animatic_path = animatics.switcher.get_animatic_path_in_blend_paths()

    if current_animatic_path == False :
        error = f"CAN\'T AUTOFIX\nCan't find the current animatic filepath in the blend file\nThe good animatic path should be {good_animatic_path}"
        log.debug(f"{error}\n(current_animatic_path = {current_animatic_path})")
        return error
    
    if not os.path.exists(good_animatic_path) or not os.path.exists(current_animatic_path) :
        errors = []
        if not os.path.exists(good_animatic_path) :
            error = f"good_animatic_path doesn't exist !\n->({good_animatic_path})"
            log.error(error)
            errors.append(error)
        
        if not os.path.exists(current_animatic_path) :
            error = f"current_animatic_path doesn't exist !\n->({current_animatic_path})"
            log.error(error)
            errors.append(error)

        errors = "\n".join(errors)
        return errors

    if current_animatic_path == good_animatic_path :
        log.info("current animatic path is already the good one !")
        return "OK"

    if not auto_fix :
        return f"The current animatic path is not the good one\nThe good animatic path : {good_animatic_path}"
    

    animatics.switcher.set_animatic_path(current_animatic_path, good_animatic_path)
    bpy.ops.file.make_paths_absolute()
    log.info("animatic filepaths succesfully replaced in this file")
    return True

def sc_autofix_animatic():
    ''' description TODO '''
    result = sc_check_animatic(auto_fix=True)
    return result


def sc_check_render_params() -> str :
    ''' check the render parameters '''

    context = blender_libs.get_bpy_context()
    if not context : return "to check"

    good_render_params = fct_utils.get_good_render_params()
    
    scene = context.scene

    def test_obj_param(obj, param: str, value: str):
        try :
            attr = getattr(obj, param)
        except : 
            log.info(f"you give a parameter to test, but the object don't have it\n-> object : {obj}\n-> parameter : {param}")
            return False
        
        if str(attr) == str(value) :
            return True
        
        return False


    error_list = []

    for param in good_render_params :
        if good_render_params[param] == "_pass" :
            continue
        if type(good_render_params[param]) is dict :
            for sub_param in good_render_params[param] :
                if good_render_params[param][sub_param] == "_pass" :
                    continue
                if type(good_render_params[param][sub_param]) is dict :
                    log.error("there is a subsub params dict")
                    continue
                if not test_obj_param(getattr(scene, param), sub_param, good_render_params[param][sub_param]) :
                    error_list.append(f"scene.{param}.{sub_param} should be \"{good_render_params[param][sub_param]}\"")
        elif not test_obj_param(scene, param, good_render_params[param]) :
            error_list.append(f"scene.{param} should be \"{good_render_params[param]}\"")

    if error_list :
        return "\n".join(error_list)

    return "OK"

def sc_autofix_render_params() -> str :
    ''' autofix the render parameters '''

    context = blender_libs.get_bpy_context()
    if not context : return "to check"

    good_render_params = fct_utils.get_good_render_params()
    
    scene = context.scene

    for param in good_render_params :
        if good_render_params[param] == "_pass" :
            continue
        if type(good_render_params[param]) is dict :
            for sub_param in good_render_params[param] :
                if good_render_params[param][sub_param] == "_pass" :
                    continue
                if type(good_render_params[param][sub_param]) is dict :
                    log.error("there is a subsub params dict")
                    continue
                if param in dir(scene) and sub_param in dir(getattr(scene, param)) :
                    setattr(getattr(scene, param), sub_param, good_render_params[param][sub_param])

        elif param in dir(scene) :
            setattr(scene, param, good_render_params[param])

    return True


def sc_check_visibility(auto_fix=False):
    ''' check if there is object or collection have their viewport and render visibility set equally '''

    context = blender_libs.get_bpy_context()
    if not context : return "to check"

    to_return = []

    for col in context.scene.collection.children_recursive :
        if fct_utils.is_lod_rig(col) :
            col.hide_render = True
            continue
        if col.hide_viewport != col.hide_render :
            if auto_fix:
                col.hide_viewport = col.hide_render
            else:
                to_return.append(f"collection \"{col.name}\" : hide_viewport is {col.hide_viewport} and hide_render is {col.hide_render}")

    for obj in context.scene.objects :
        if fct_utils.is_lod_rig(obj) :
            obj.hide_render = True
            continue
        elif obj.hide_viewport != obj.hide_render :
            if auto_fix:
                obj.hide_viewport = obj.hide_render

                print("auto", obj, obj.hide_viewport ,obj.hide_render)
                if obj.hide_viewport != obj.hide_render:
                    print("still dif")
                    obj.hide_render = obj.hide_viewport # you really don't want to have rigs LOD visible in renders

            else:
                to_return.append(f"object \"{obj.name}\" : hide_viewport is {obj.hide_viewport} and hide_render is {obj.hide_render}")


    if not to_return :
        return "OK"
    
    before = f"some elements ({len(to_return)}) don't have their viewport and render visibility set equally :\n"
    to_return = before + "\n".join(to_return)

    return to_return

def sc_autofix_visibility():
    ''' description TODO '''
    result = sc_check_visibility(auto_fix=True)
    return result


def sc_check_cameralight_outside_maincollection(auto_fix=False):
    '''
    check camera light outside of the main collection
    Returns:

    '''


    context = blender_libs.get_bpy_context()
    if not context:
        return "to check"

    futur_blendpath = None
    if not context.blend_data.filepath or not context.blend_data.filepath.startswith("W:"):
        futur_blendpath = fct_utils.get_current_item_path()

    master_name = blender_libs.collection.get_master_name_from_path(path=futur_blendpath)
    master_col = bpy.data.collections.get(master_name)

    # check the same as master collection bidule
    if not master_col:
        return f"there should be a collection in the file with this name : {master_name}"
    if master_name not in context.scene.collection.children:
        return f"the collection {master_name} should be at the root of the outliner hierarchy"

    # now it's fine
    cams = [ob for ob in list(bpy.context.scene.objects) if ob.type == 'CAMERA']
    lights = [ob for ob in list(bpy.context.scene.objects) if ob.type == 'LIGHT']
    # print(f"-- {cams=}")
    camlights = cams + lights
    # print(f"--- {camlights=}")

    # parse
    collections = []
    obj_related = []
    for each in camlights:
        # print(f"{each=}")
        user_collections = each.users_collection

        scene_collection = []
        for col in user_collections:
            if context.scene.user_of_id(col): scene_collection.append(col)

        if len(scene_collection) > 1: return f"the object {each} should be under ONE collection only"
        # ---
        if scene_collection[0] not in collections or scene_collection[0].name == master_name:
            collections.append(scene_collection[0])
            obj_related.append(each)
    # print(f"{collections=}")
    # print(f"{obj_related=}")
    bads = []
    bads_parents = []
    bads_obj = []


    for i, col in enumerate(collections):
        obj = obj_related[i]
        # --
        all = blender_libs.collection.get_all_parents(col, return_name=True, include_input=True)
        # print(f"{all=}")
        # case where the object is directly under main collection
        if all[0] == master_name:
            # print("MAIN")
            bads_obj.append(obj)
            bads.append("")
            bads_parents.append("")
        else:
            # not bad obj
            bads_obj.append("")

            if master_name in all:
                # print(f"{all=}")
                bads.append(all[0])

                if len(all)>1:
                    bads_parents.append(all[1])
                else:
                    bads_parents.append("")

    print(f"{bads=}")
    print(f"{bads_parents=}")
    print(f"{obj_related=}")
    # autofix
    if auto_fix:

        for i, bad in enumerate(bads):
            # if its a collection
            # print(bad)
            if not bad:
                # active collection is the scene
                scene_collection = bpy.context.view_layer.layer_collection
                context.view_layer.active_layer_collection = scene_collection

                # create flux collection if not here
                trash_collection_name = "FLUX_COLLECTION"
                trash_collection = bpy.data.collections.get(trash_collection_name)
                # if it doesn't exist create it
                if trash_collection is None:
                    trash_collection = bpy.data.collections.new(trash_collection_name)
                # if it is not linked to scene colleciton treelink it
                if not context.scene.user_of_id(trash_collection):
                    context.collection.children.link(trash_collection)

                # link
                trash_collection.objects.link(obj_related[i])
                master_col.objects.unlink(obj_related[i])


            else:
                # print(bad)
                # link to the scene
                bad_col = bpy.data.collections.get(bad)
                context.scene.collection.children.link(bad_col)
                # unlink from origin
                if bads_parents[i]:
                    bad_parent_col = bpy.data.collections.get(bads_parents[i])
                    bad_parent_col.children.unlink(bad_col)

        # print("AUTO")
        return "OK"
    # no autofix
    else:

        # -------
        if bads:
            return f"Cam/Lights are under main collection. Collection to moves : {str(bads)}"

        return "OK"

def sc_autofix_cameralight_outside_maincollection():
    '''
    check camera light outside of the main collection
    Returns:

    '''

    result = sc_check_cameralight_outside_maincollection(auto_fix=True)

    return result