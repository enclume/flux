# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Sanity check element class

'''

# initialize logger
from .. import flux_logger 
log = flux_logger.get_flux_logger()

#reload lib
import importlib
importlib.reload(flux_logger)

class Sanity_Check_Element:
    ''' 
    Check element class 

    Args:
        - name : the name of the SC element
        - function : the function to get the report
        - fct_autofix : the function to fix the issue (if there is a bad report)
        - description : the description of the SC element
        - filters : item data filters to show the SC element in the SC list
        - filters_exclude : item data filters to not show the SC element in the SC list
    '''
    def __init__(self, name: str, function, fct_autofix = None, description: str = None, filters: dict = {}, filters_exclude: dict = {}):
        self.name = name
        self.description = description
        self.function = function
        self.fct_autofix = fct_autofix
        self.filters = None

        if filters or filters_exclude :
            self.filters = {"add":filters, 
                            "sub":filters_exclude}

    def get_formated_report(self, report):
        ''' description TODO '''      
        if report == True or str(report).lower() == "ok" : return "OK"
        if str(report).lower() == "to check" : return "to check"
        if not report : return "Error"
        return report

    def get_check_report(self):
        ''' description TODO '''
        try :
            report = self.function()
            log.debug(report)
        except Exception as e:
            report = "to check"
            log.info(f"report set to \"to check\" because of :\n{e}")

        return self.get_formated_report(report)
    
    def autofix(self, check_report: bool = True):
        ''' description TODO '''
        if check_report :
            report = self.get_check_report()
            if report == "OK" :
                log.debug("start to auto fix but report is already OK")
                return report

        report_fix = self.fct_autofix()
        report_fix = self.get_formated_report(report_fix)
        report = self.get_check_report()

        if report_fix != "OK" :
            if report == "OK" :
                report = report_fix
            else :
                report = f"{report} | And during auto fix : {report_fix}"
        
        return report
        