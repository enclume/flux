# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

''' run this script to publish the flux addon on the server '''

import json
import shutil
import os

def get_flux_current_tag(source):
    ''' description TODO '''
    
    source = os.path.join(source, "__init__.py")
    f = open(source)
    f = f.read()
    bl_info = "{"

    pos = f.find("bl_info = {") + len("bl_info = {")

    for char in f[pos:] :
        bl_info += char
        if char == "}" : break

    if  bl_info[-3] == "," :
        bl_info = bl_info[:-3] + bl_info[-2:]

    bl_info = bl_info.replace("(", "[").replace(")", "]")
    bl_info = json.loads(bl_info)

    #bl_info = {"version" : (0, 5, 5)}
    current_tag = ""

    for digit in list(bl_info["version"]):
        current_tag += f".{digit}"

    return current_tag[1:]

def make_publish(target, source, name = "flux"):
    ''' description TODO '''

    if not os.path.exists(target) :
        print(f"target path does not exist\n-> ({target})")
    if not os.path.exists(source) :
        print(f"source path does not exist\n-> ({source})")
    if not os.path.exists(target) or not os.path.exists(source) :
        print("stop the process")
        return False
    
    output_filename = os.path.join(target, name)
    shutil.copytree(source, output_filename, ignore=shutil.ignore_patterns("*.pyc", "tmp*", ".git*", "*.idea", "__pycache__", "TEST"))
    shutil.make_archive(output_filename, format='zip', root_dir=target, base_dir = name)
    shutil.rmtree(output_filename)

    for item in os.listdir(target):
        if "version" not in item or "." not in item : continue

        item = item.split(".")
        if len(item) != 3 or not item[0][-1].isdigit() or not item[1].isdigit() or not item[2].isdigit() :
            continue
        
        item = ".".join(item)
        print("find a version file !")
        print(f"old : {item}")
        digits = get_flux_current_tag(source)
        new_item = "version " + digits

        os.rename(os.path.join(target, item), os.path.join(target, new_item))

        print(f"new : {new_item}")

    print("publish done !")
    return True

if __name__ == "__main__" :

    target = "R:/02_TOOLS"
    name = "flux"
    source = "C:/Users/Marteau/AppData/Roaming/Blender Foundation/Blender/3.6/scripts/addons/flux"

    make_publish(target, source)

    target = r"W:\01_PRODUCTIONS\012_RUFUS\0_DEV\00_PIPELINE\Flux"

    make_publish(target, source)

    '''
    output_filename = os.path.join(target, name)
    shutil.copytree(source, output_filename, ignore=shutil.ignore_patterns("*.pyc", "tmp*", ".git*", "*.idea", "__pycache__", "TEST"))
    shutil.make_archive(output_filename, format='zip', root_dir=target, base_dir = name)
    shutil.rmtree(output_filename)
    '''

