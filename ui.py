# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
#
# (c) 2023, Paul Jadoul, Cedric Nicolas,  Guillaume Geelen

'''
Flux Pipeline Manager
__
Manage UI
'''

import bpy
import json
import os
import ast

from . import proxy
from . import prefs
from . import ui_operators
from . import ui_edit_link
from typing import Any, List, Union, Dict, Tuple

from . import database_access
from . import config

import sys
import traceback

from . import file_access
from .config import path as path_from_config
import webbrowser

#reload lib
from .items_operators import op_flux_render
import importlib
importlib.reload(database_access)
importlib.reload(file_access)
importlib.reload(ui_edit_link)


def set_global_ui_refresh(value: bool):
    """
    Set global force check value. Setted at any part of the addons, allows to refresh the UI
    Args:
        value: Value to set (True/False
    """
    global _flux_ui_global_refresh
    # print("set value to", value)
    _flux_ui_global_refresh = value


def get_global_ui_refresh() -> bool:
    """
    Get current refresh value
    Returns:
        bool of the state (True/False)
    """
    global _flux_ui_global_refresh
    try:
        return _flux_ui_global_refresh
    except:
        return True

# ================================================
# cache variable
# ================================================
def set_cache_force_check(value: bool):
    """
    Set global force check value. Setted at any part of the addons, allows to refresh the UI
    Args:
        value: Value to set (True/False
    """
    global _flux_ui_force_check
    # print("set value to", value)
    _flux_ui_force_check = value


def get_cache_force_check() -> bool:
    """
    Get current refresh value
    Returns:
        bool of the state (True/False)
    """
    global _flux_ui_force_check
    try:
        return _flux_ui_force_check
    except:
        return True

# ================================================
# utils def
# ================================================
def set_enum(self, context) -> bpy.props.EnumProperty:
    '''
    normalized def to dynamically set the list of items to display.
    This is only in format def(self, context).
    Input is managed via global var
    '''
    global _flux_ui_cache_display_list
    # print(f"debug {_flux_ui_cache_display_list=}")
    try:
        r = array_to_enum(_flux_ui_cache_display_list)
    except:
        r = []
    return r


def enum_members_from_instance(rna_item, prop_str):
    """
    get enum list from enum prop.

    enum_members_from_instance(bpy.context.scene.flux_ui, "current_category")
    return ['assets', 'shots']

    also with
    https://blender.stackexchange.com/questions/58030/how-to-get-all-available-entries-of-an-enumproperty
    Args:
        rna_item: ie bpy.context.scene.flux_ui
        prop_str: ie current_category
    Returns:
        [members]
    """
    rna_type = type(rna_item)
    prop = rna_type.bl_rna.properties[prop_str]
    #r = [e.identifier for e in prop.enum_items]
    r = []
    for e in prop.enum_items:
        r.append(e.identifier)
    return r


def spopup_set_items_list(input: str, for_composer: bool) -> None:
    '''
    establish the list of all elements that will be display via the search popup,
    Set popup item via global var

    Args:
        input:     what level of input (type, name, task, seq, etc...)
        composer:  is it for composer or not (and always get assets then)

        global _flux_ui_cache_display_list is used as return
    '''
    global _flux_ui_cache_display_list

    # force asset type if composer mode
    if for_composer : cat = enum_members_from_instance(bpy.context.scene.flux_ui, "current_category")[0]
    else            : cat = bpy.context.scene.flux_ui.current_category

    # debug
    # print(f"spopup {cat=}")
    # print(f"spopup {input=}")
    # for my_item in bpy.context.scene.flux_ui_composer_list:
    #     print(my_item.item_name, my_item.item_load)

    # ---
    insert_asset = False

    # ---- composer ----
    if for_composer:
        load_type = bpy.context.scene.flux_ui_composer.load_type
        # print(f"spopup {load_type=}")

        current_item = get_current_user_item(for_composer=True)

        # if list we will insert this later
        if load_type == "list":
            # only if we are in the task moment
            if input == "task":
                all_list = bpy.context.scene.flux_ui_composer_list
                if not len(all_list):
                    insert_asset = False
                # print(f"{list(all_list)=}")
                # ---
                insert_asset = bpy.context.scene.flux_ui_composer_list[0].item_name
                # print("spopup")
    # if usual ui
    else:
        current_item = get_current_user_item()

    # if current item is None, reset it, and read it from def
    if current_item == None:
        current_item = reset_user_item(for_composer)

    if prefs.is_debug_mode():
        print(f"for composer  {for_composer}")
        print(f"current_item 1 {current_item}")

    # current_item = json.loads(current_item)
    if insert_asset:
        current_item["name"] = insert_asset

    is_log = prefs.get().is_db_logged

    if prefs.is_debug_mode():
        print(f"current_item 2 {current_item}")
        print(f"is_log : {is_log}")
        print(f"cat : {cat}")
        print(f"input : {input}")

    # if is log
    if cat == "assets":
        if input == "type":
            _flux_ui_cache_display_list = database_access.get_assets_types()
        if input == "name":
            asset_type = current_item["type"]
            _flux_ui_cache_display_list = database_access.get_assets_names(asset_type)
        if input == "task":
            asset_name = current_item["name"]
            _flux_ui_cache_display_list = database_access.get_assets_tasks(asset_name)
    elif cat == "shots":
        if input == "seq":
            _flux_ui_cache_display_list = database_access.get_seqs()
        if input == "shot":
            seq_name = current_item["seq"]
            _flux_ui_cache_display_list = database_access.get_shots(seq_name)
        if input == "task":
            seq_name = current_item["seq"]
            shot_name = current_item["shot"]

            _flux_ui_cache_display_list =  database_access.get_shots_tasks(seq_name, shot_name, filter=prefs.get().filter_task_item)
    else:
        _flux_ui_cache_display_list = []


def array_to_enum(input: list) -> list[Tuple[str, str, str]]:
    """
    Convert array to enum blender style
    """
    result = []
    for each in input:
        result.append( (each, each, "") )
    return result


def reset_user_item(for_composer=False) -> str:
    """
    reset items for current category in a normative way.
    Start from descritpion and fill with None at each key.
    Args:
        for_composer: is it for composer var?
    """

    if for_composer: category = enum_members_from_instance(bpy.context.scene.flux_ui, "current_category")[0]
    else           : category = bpy.context.scene.flux_ui.current_category

    all_items_description = json.loads(bpy.context.scene.flux_ui.items_description)
    item_description = all_items_description[category]

    # if reset
    current_item_dict = {}
    # reset, write none in the corrent format (good num of inputs)
    # print(" -set_category_items : reset!")
    for each in item_description:
        current_item_dict[each] = "None"

    result = json.dumps(current_item_dict)

    # final write to prop
    if for_composer:
        bpy.context.scene.flux_ui_composer.current_user_item = result
    else:
        bpy.context.scene.flux_ui.current_user_item = result

    return result

def set_current_user_item(category : str, what : str , value: str, for_composer=False) -> None:
    """
    Set the current item in the prop
    """
    # determine if reset
    reset = False
    if not for_composer:
        if bpy.context.scene.flux_ui.current_category_cache != category or bpy.context.scene.flux_ui.current_user_item == "None":
            reset = True

        # once reset is setted, store the category in cache for later
        bpy.context.scene.flux_ui.current_category_cache = bpy.context.scene.flux_ui.current_category
    else:
        if bpy.context.scene.flux_ui_composer.current_user_item == "None":
            reset = True

    if not for_composer:
        all_items_description = json.loads(bpy.context.scene.flux_ui.items_description)
        item_description = all_items_description[bpy.context.scene.flux_ui.current_category]
    else:
        all_items_description = json.loads(bpy.context.scene.flux_ui.items_description)
        item_description = all_items_description[list(all_items_description.keys())[0]]

    # print(f"set current user - {for_composer=} - {item_description=}")
    # print(for_composer)
    # print(item_description)
    # print(what)

    # if reset
    current_item_dict = {}
    if reset:
        # reset, write none in the corrent format (good num of inputs)
        # print(" -set_category_items : reset!")
        for each in item_description:
            current_item_dict[each] = "None"

    else:
        # we assume here that the current_item is ok and load it
        if not for_composer:
            current_item_dict = get_current_user_item()
        else:
            current_item_dict = get_current_user_item(for_composer=True)

            # security check
            if "seq" in list(current_item_dict.keys()):
                current_item_dict = reset_user_item(for_composer=True)
                print("FLUX : Current item reset (forced) for composer.")

    # print(f" -set_category_items : read dict : {for_composer}")
    # print(f" -set_category_items : read dict : {current_item_dict}")

    # final write with error catching
    # print(current_item_dict)

    if what not in list(current_item_dict.keys()):
        raise Exception(f"ERROR : cannot set current item with {what}")

    current_item_dict[what] = value

    # final write to prop
    if not for_composer:
        bpy.context.scene.flux_ui.current_user_item = json.dumps(current_item_dict)
    else:
        bpy.context.scene.flux_ui_composer.current_user_item = json.dumps(current_item_dict)


def set_current_user_item_cache():

    global _flux_ui_cache_current_items
    current_items = _flux_ui_cache_current_items

    current_items_str = json.dumps(current_items)
    print(current_items_str)
    bpy.context.scene.flux_ui.current_user_item_cache = current_items_str


# todo rename with get_user_item
# todo generalize more this, by search on uses "context.scene.flux_ui_composer.current_user_item"
def get_current_user_item(for_composer: bool = False, cache: bool = False, get_category: bool = False) -> dict :
    """
    get the current car items from props
    """
    # once reset is setted, store the category in cache for later
    if for_composer :
        current_item = bpy.context.scene.flux_ui_composer.current_user_item
    else:
        if cache : current_item = bpy.context.scene.flux_ui.current_user_item_cache
        else : current_item = bpy.context.scene.flux_ui.current_user_item

    # print(f"get {current_item=}")

    if current_item == "None" :
        return None

    current_item: dict = json.loads(current_item)
    # security check
    if get_category :
        cat = bpy.context.scene.flux_ui.current_category
        current_item["category"] = cat
    elif "category" in list(current_item.keys()):
        current_item.pop("category")

    return current_item


def is_category_changed(context) -> bool :
    """
    return true if category is changed by user. Use to force refresh
    """
    reset = False
    flux_ui = context.scene.flux_ui
    if (flux_ui.current_category_cache != flux_ui.current_category or 
        flux_ui.current_user_item == "None"):
        reset = True

    return reset


def is_same_dict(dict_a: dict, dict_b: dict) -> bool:
    """
    Compare two dict. (same entries with the same values)
    Args:
        dict_a: compare a
        dict_b: compare b

    Returns:
        object: same? as bool
    """

    dict_a = dict(dict_a)
    dict_b = dict(dict_b)

    same = True
    for each in list(dict_a.keys()):
        if each in list(dict_b.keys()):
            if dict_a[each] != dict_b[each]:
                same = False
        else:
            same = False

    return same


def is_item_valid(current_item: dict = None, check_for_cat = True) -> bool:
    """
    Check if item is valid
    Args:
        current_item: given current item. If None take one from prop
        check_for_cat: check if match with current category from prop
    Returns:
        True/False
    """
    # print(f"{current_item=}")
    if current_item == None:
        current_item = get_current_user_item()
        # check
        if not current_item : return False

    is_item_valid_bool = True

    current_items_values = list(current_item.values())
    if current_items_values == None or 'None' in current_items_values :
        is_item_valid_bool = False

    # print(f"{current_items_values=}")

    flux_ui = bpy.context.scene.flux_ui
    if check_for_cat:
        cat = flux_ui.current_category

        desc = json.loads(flux_ui.items_description)
        desc = desc[cat]

        for i, each in enumerate(desc):
            if each != list(current_item.keys())[i]:
                is_item_valid_bool = False

    return is_item_valid_bool

# todo make it work with proxy_multilevel = False
def is_proxy_available_in_folder(current_linked):
    """
    check if there is proxy in folder. Art least one for multilevel
    Args:
        current_linked (): item to check. prop formated

    Returns: bool

    """
    # multilevel?
    proxy_multilevel = prefs.get().proxy_is_multilevel
    # print(proxy_multilevel)

    is_proxy_available = False
    # if is prosy, then look for orig. If not, look for recorded path in usual place
    if current_linked.is_proxy:
        link_path = current_linked.link_path_orig
    else:
        link_path = current_linked.link_path

    # print(prefs.get().proxy_level_prefs)
    proxy_range = json.loads(prefs.get().proxy_level_prefs)
    for level in range(proxy_range[0], proxy_range[1] + 1):
        proxy_path = proxy.get_from_item_path(link_path, level=level)
        if os.path.exists(proxy_path):
            if not proxy_multilevel :
                is_proxy_available = True
                break

            if type(is_proxy_available) is not list :
                is_proxy_available = [level]
            else:
                is_proxy_available.append(level)
            
    return is_proxy_available


# ================================================
# ops
# ================================================
class FLUX_PT_SessionManagementPanel(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_label = "Flux Session Manager"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def draw(self, context: bpy.types.Context):
        layout = self.layout
        col = layout.column(align=False)
        col.alignment = "CENTER"

        # if not already login
        if not prefs.get().is_db_logged :
            row = col.row(align=True)
            row.label(text=f"Not logged :(", icon="X")
            op = row.operator(FLUX_OP_Help.bl_idname, text="", icon="QUESTION")
            op.what = ""
            op = row.operator(FLUX_OP_Help.bl_idname, text="", icon="HELP")
            op.what = "SESSION"


            col.label(text=f"Recorded Host  : {prefs.get().db_host}", icon="QUESTION")
            col.label(text=f"Recorded Log: {prefs.get().db_email}", icon="USER")
            op = col.operator(ui_operators.FLUX_DB_session_start.bl_idname, text="Login", icon="PLAY")
            op.from_prefs = False
            op = col.operator(ui_operators.FLUX_DB_session_end.bl_idname, text="Clear Current User Data", icon="TRASH")
            op.empty_all_prefs = True

        # if already login
        else :
            # row = box.row()
            row = col.row(align=True)
            row.label(text=f"Logged, Project {prefs.get().project_name.capitalize()} :) ", icon="FUND")
            op = row.operator(FLUX_OP_Help.bl_idname, text="", icon="QUESTION")
            op.what = ""
            op = row.operator(FLUX_OP_Help.bl_idname, text="", icon="HELP")
            op.what = "SESSION"

            col.label(text=f"Host at  : {prefs.get().db_host}", icon="QUESTION")
            col.label(text=f"Logged in: {prefs.get().db_email}", icon="USER")
            col.operator(
                ui_operators.FLUX_DB_session_end.bl_idname, text="Logout", icon="PANEL_CLOSE"
            )
            # col.prop(context.scene.flux_ui, "use_kitsu_cache" , text="use cache")
            col.operator(
                ui_operators.FLUX_DB_clearCache.bl_idname, text="refresh cache", icon="FILE_REFRESH"
            )
            # col.prop(context.preferences.addons["flux"].preferences, "show_composer" , text="Show Composer")
            # col.prop(context.preferences.addons["flux"].preferences, "show_sanity_check" , text="Show Sanity Check")
            #
            # if not prefs.get().root_ok:
            #     col.label(text=f"Error with : " + prefs.get().project_path_root, icon="ERROR")

        # todo check in wich circonstance show tools (log+proj)
        # show tab
        col.label(text="Show:")
        r = col.row(align=False)
        if prefs.get().is_db_logged and prefs.get_project_name():
            r.prop(context.preferences.addons["flux"].preferences, "show_composer" , text="Composer", toggle=True)
        r.prop(context.preferences.addons["flux"].preferences, "show_linkeditor" , text="LinkEditor", toggle=True)

        if prefs.get().is_db_logged :
            if prefs.get_project_name():
                r = col.row(align=False)
                r.prop(context.preferences.addons["flux"].preferences, "show_sanity_check" , text="SanityCheck", toggle=True)
            r.prop(context.preferences.addons["flux"].preferences, "show_tools" , text="Tools", toggle=True)

            if not prefs.get().root_ok:
                col.label(text=f"Error with : " + prefs.get().project_path_root, icon="ERROR")

        # project setted?
        if not prefs.get_project_name():
            col.label(text=f"Error with : No setted Project", icon="ERROR")


class FLUX_MT_ItemMenu(bpy.types.Menu):
    bl_label = "Flux Special Menu, miam"

    def draw(self, _context):

        # guess (again) for publish offline
        current_items = get_current_user_item()
        is_matching_r_exist = False
        if current_items:
            # todo clean current_items_r = dict(current_items)
            current_items["project"] = prefs.get_project_name()
            current_items["category"] = bpy.context.scene.flux_ui.current_category
            current_items["version"] = [1]
            tree = file_access.get_item_versionning_tree(current_items)
            # file_path_for_item = file_access.get_item_path2(current_items["category"], current_items, only_file_path=True)
            # print(file_path_for_item)
            # print(tree)

            if tree:
                all_r = list(tree['v01'].keys())
                all_r = int(all_r[-1][1:])
                current_items_r = dict(current_items)
                current_items_r["version"] = [1, all_r]
                matching_r = file_access.get_item_path2(current_items_r, only_file_path=True)
                is_matching_r_exist = os.path.exists(matching_r)
            else:
                is_matching_r_exist = False


        # begin
        cat = bpy.context.scene.flux_ui.current_category
        layout = self.layout
        row = layout.column()
        row.label(text = "Bookmarks")

        bkms = prefs.bookmarks_get()
        if not len(bkms):
            row.label(text = " (Add with the RED HEART button)")
        # print(bkms)
        for bk in bkms:
            if isinstance(bk, str):
                bk = json.loads(bk)
            # print(bk)
            all_items_part = list(bk.values())
            print(all_items_part)
            text = f"{all_items_part[4]} / {all_items_part[3]} / {all_items_part[0]} / {all_items_part[1]} / {all_items_part[2]}"
            op = row.operator("flux.item_bkm_call", icon='FUND', text=text)
            op.for_composer=False
            op.item = json.dumps(bk)

        if len(bkms):
            op = row.operator("flux.item_bkm_call", icon='ORPHAN_DATA', text="-Clear Bookmarks List-")
            op.for_composer=False
            op.item = "REMOVE"

        row.separator()

        row.operator("flux.itemmenu_exe", icon='FILEBROWSER', text="Open Current Item Folder").what = 'WINEXPLORER_BASE'
        row.operator("flux.item_op_open_blend_folder", text="Open currently open BlendFile Folder", icon="FILEBROWSER")
        # row.operator("flux.itemmenu_exe", icon='FILEBROWSER', text="Open win explorer (base folder)").what = 'WINEXPLORER_BASE'
        # row.operator("flux.itemmenu_exe", icon='FILEBROWSER', text="Open win explorer here (full)").what = 'WINEXPLORER_FULL'
        row.operator("flux.itemmenu_exe", icon='OUTLINER_DATA_LIGHT', text="Open Kitsu item URL").what = 'KITSU'
        if cat == "shots":
            row.separator()
            row.operator("flux.itemmenu_exe", icon='QUESTION', text="Get shot infos").what = 'SHOT_INFOS'

        if is_matching_r_exist:
            row.separator()
            save3 = row.operator(ui_operators.FLUX_DISK_ActionComfirm.bl_idname, text="Publish OFFLINE", icon="FUND")
            save3.action     = "SAVE"
            save3.sub_action = "PUBLISH_OFFLINE"


class FLUX_MT_ComposerMenu(bpy.types.Menu):
    bl_label = "Flux Special Menu Composer, miam"

    def draw(self, _context):
        cat = "assets"

        layout = self.layout
        row = layout.column()

        row.label(text = "Bookmarks For Composer")

        bkms = prefs.bookmarks_get(for_composer=True)
        if not len(bkms):
            row.label(text = " (Add with the RED HEART button)")
        # print(bkms)
        for bk in bkms:
            if isinstance(bk, str):
                bk = json.loads(bk)
            # print(bk)
            all_items_part = list(bk.values())
            # print(all_items_part)

            # if fron list
            disp_name = all_items_part[1]
            if "[" in all_items_part[1]:
                # print(all_items_part[1])

                disp_name = ast.literal_eval(all_items_part[1])
                disp_name = f"{disp_name[1]}, etc... ({str(len(disp_name))})"

            # display line
            text = f"{all_items_part[4]} / {all_items_part[3]} / {all_items_part[0]} / {disp_name} / {all_items_part[2]}"
            op = row.operator("flux.item_bkm_call", icon='FUND', text=text)
            op.for_composer=True
            op.item = json.dumps(bk)

        if len(bkms):
            op = row.operator("flux.item_bkm_call", icon='ORPHAN_DATA', text="-Clear Bookmarks List-")
            op.for_composer=True
            op.item = "REMOVE"

        # todo open it for composer
        # row.separator()
        #
        # row.operator("flux.itemmenu_exe", icon='FILEBROWSER', text="Open Current Item Folder").what = 'WINEXPLORER_BASE'
        # row.operator("flux.item_op_open_blend_folder", text="Open currently open BlendFile Folder", icon="FILEBROWSER")
        # # row.operator("flux.itemmenu_exe", icon='FILEBROWSER', text="Open win explorer (base folder)").what = 'WINEXPLORER_BASE'
        # # row.operator("flux.itemmenu_exe", icon='FILEBROWSER', text="Open win explorer here (full)").what = 'WINEXPLORER_FULL'
        # row.operator("flux.itemmenu_exe", icon='OUTLINER_DATA_LIGHT', text="Open Kitsu item URL").what = 'KITSU'
        # if cat == "shots":
        #     row.separator()
        #     row.operator("flux.itemmenu_exe", icon='QUESTION', text="Get shot infos").what = 'SHOT_INFOS'



class FLUX_OP_ItemMenu_Exe(bpy.types.Operator):
    bl_idname = "flux.itemmenu_exe"
    bl_label = "Flux item menu exe"
    bl_description = "get infos on item"

    what: bpy.props.StringProperty(name="None")

    def execute(self, context: bpy.types.Context):

        current_item = dict(get_current_user_item())
        go = is_item_valid(current_item)

        # ---------------------------------
        cat = bpy.context.scene.flux_ui.current_category
        cat_cache = bpy.context.scene.flux_ui.current_category_cache

        # ---------------------------------
        current_item["category"] = cat
        # print(f"{current_item=}")

        # ---------------------------------
        if cat != cat_cache:
            go = False

        if not go:
            self.report({"ERROR"}, "Check your inputs")
            return {"CANCELLED"}

        if self.what == "SHOT_INFOS":
            seq_name = current_item["seq"]
            shot_name = current_item["shot"]

            infos = database_access.get_shots_infos(seq_name, shot_name)
            print(f"database_access.get_shots_infos({seq_name}, {shot_name})={infos}")

            self.report({'INFO'}, str(infos))
            return {"FINISHED"}

        if self.what == "WINEXPLORER_BASE":
            item_path = file_access.get_item_path2(current_item, only_base_folder=True) + "//v01"
            if not os.path.exists(item_path):
                item_path = file_access.get_item_path2(current_item, only_base_folder=True)
            if not os.path.exists(item_path):
                item_path = os.path.dirname(item_path)
                if not os.path.exists(item_path):
                    self.report({"ERROR"}, f"{item_path} doesn't exist")
                    return {"CANCELLED"}

            os.startfile(item_path)

        if self.what == "WINEXPLORER_REVISION":
            item = get_current_user_item()
            # print(item)
            item["category"] = context.scene.flux_ui.current_category
            last_vrw = file_access.get_item_last_version(item, version_letter="w", as_int=True)
            # print(last_vrw)
            item['version'] = [last_vrw['v'], last_vrw['r']]
            last_r_path = file_access.get_item_path2(item, only_file_path=True)
            dir = os.path.dirname(os.path.abspath(last_r_path))
            os.startfile(dir)

        elif self.what == "WINEXPLORER_FULL":
            print("todo")

        elif self.what == "KITSU":
            if cat == "shots":
                seq_name = current_item["seq"]
                shot_name = current_item["shot"]
                url = database_access.get_shot_url(seq_name, shot_name)

                webbrowser.open(url)
                self.report({'INFO'}, "Open URL")
                return {"FINISHED"}

            else:
                name = current_item["name"]
                url = database_access.get_asset_url(name)

                webbrowser.open(url)
                self.report({'INFO'}, "Open URL")
                return {"FINISHED"}

        return {"FINISHED"}

class FLUX_OP_Help(bpy.types.Operator):
    bl_idname = "flux.help"
    bl_label = "Flux help"
    bl_description = "open help"

    what: bpy.props.StringProperty(name="")

    def execute(self, context: bpy.types.Context):

        help_path_root = bpy.utils.script_path_user() + '/addons/flux/doc/pdf'

        if   self.what == "ITEM": file_help = "flux-item-manager.pdf"
        elif self.what == "LINK": file_help = "link-editor.pdf"
        elif self.what == "CONFIG": file_help = "configuration.pdf"
        elif self.what == "INSTALL": file_help = "installation.pdf"
        elif self.what == "TOOLS": file_help = "tools-manager.pdf"
        elif self.what == "SESSION": file_help = "session-manager.pdf"
        elif self.what == "COMPOSER": file_help = "scene-composer.pdf"
        elif self.what == "SANITY": file_help = "sanity-check.pdf"
        elif self.what == "PROXY": file_help = "proxy-workflow.pdf"
        elif self.what == "FILE": file_help = "file-workflow.pdf"
        else: file_help = "flux.pdf"

        path = os.path.join(help_path_root, file_help)

        os.startfile(path)
        return {"FINISHED"}

    @classmethod
    def description(cls, context, event):
        opt = getattr(event, "what")
        if not opt: return f"Main Help"
        elif opt == "FILE": return f"File Workflow Versionning Help"
        elif opt == "PROXY": return f"Proxy Workflow Versionning Help"
        else: return f"Help for current panel"


class FLUX_PT_ItemManagementPanel(bpy.types.Panel):
    """Creates a Panel in the Object properties window with all the save/load buttons for asset and shot"""
    bl_label = "Flux Item Manager"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:

        if not prefs.get_project_name():
            return False
        if prefs.get().is_db_logged: return True
        else: return False

    def draw(self, context: bpy.types.Context):
        # init var
        is_matching_r_exist = False
        is_matching_v_exist = False
        erase_v = True

        # init layout
        layout = self.layout
        box = layout.box()
        row = box.row(align=True)
        row.label(text="Browser", icon="FILEBROWSER")

        # help
        op = row.operator(FLUX_OP_Help.bl_idname, text="", icon="QUESTION", emboss=False)
        op.what = "FILE"

        op = row.operator(FLUX_OP_Help.bl_idname, text="", icon="HELP", emboss=False)
        op.what = "ITEM"

        # refresh disk
        row.operator(ui_operators.FLUX_DB_clearCache.bl_idname,icon="FILE_REFRESH",text="",emboss=False)

        row2 = row.column()
        ic = "HEART"

        if is_item_valid() and prefs.bookmarks_is_item_exist() : 
            ic = "FUND"
        row2.operator(ui_operators.FLUX_ITEM_BKM_AddRemove.bl_idname,icon=ic,text="",emboss=False).for_composer=False
        row2.enabled = is_item_valid()

        row.menu("FLUX_MT_ItemMenu", icon='DOWNARROW_HLT', text="")

        # category button (ie shot/asset)
        row = box.row(align=True)
        row.prop(context.scene.flux_ui, "current_category", expand=True)
        col = box.column(align=True)

        # -------------------------------------
        # guess all button to display from items_description list
        # get descriptions
        all_items_description = json.loads(context.scene.flux_ui.items_description)

        # print(f"{context.scene.flux_ui.current_category=}")
        # print(f"{context.scene.flux_ui.current_category_cache=}")

        # catch error
        if not context.scene.flux_ui.current_category in all_items_description.keys():
            raise Exception(f"FLUX ERROR : Categories and descriptions doesnt match : {context.scene.flux_ui.current_category}")

        # get current item desc
        item_description = all_items_description[context.scene.flux_ui.current_category]

        # nice text
        col.label(text=" > Select your " + context.scene.flux_ui.current_category)

        # is reset?
        reset = False
        if context.scene.flux_ui_utils.no_reset == False:
            reset = is_category_changed(context)

        # get values of dict as array if not none
        current_items = get_current_user_item()
        # print(f"{current_items=}")
        if current_items != None : current_items_values = list(current_items.values())
        else : current_items_values = None

        # is item valid? Is there any None in the values?
        item_valid = is_item_valid()

        #print(f"{item_valid=}")
        #print(f"{reset=}")
        #print(f"{item_description=}")

        # apply buttons
        enable_button = True
        for i, button in enumerate(item_description):
            # display
            display_text = "Select " + button.capitalize()
            if not reset:
                if not current_items_values == None:
                    if current_items_values[i] != "None":
                        display_text = current_items_values[i]
                    # if current_item == None
                    else:
                        if i == 2:
                            if current_items_values[1] == "None":
                                enable_button = False
                else:
                    if current_items_values[0] == "None" and current_items_values[1] == "None" and current_items_values[2] == "None":
                        if i > 0:
                            enable_button = False
            else:
                if i > 0:
                    enable_button = False
            # do it
            col = box.column(align=True)
            list_btn = col.operator(ui_operators.FLUX_DB_choose_from_list.bl_idname, text=display_text, icon="DOWNARROW_HLT")
            list_btn.what = button
            list_btn.composer = False

            # task
            col.enabled = enable_button

        # #proxy
        proxy_enable = prefs.get().proxy_enable
        proxy_multilevel = prefs.get().proxy_is_multilevel
        is_proxy = context.scene.flux_ui.is_proxy
        proxy_level = context.scene.flux_ui.proxy_level

        # --
        if proxy_enable:
            if context.scene.flux_ui.current_category == "assets":
                col = box.column(align=False)

                # if proxy_multilevel: # removed
                col = col.row(align=True)
                col.prop(context.scene.flux_ui, "is_proxy", text="Proxy", toggle=True, emboss=True, icon="MOD_SUBSURF")
                if proxy_multilevel:
                    col2 = col.column(align=True)
                    col2.prop(context.scene.flux_ui, "proxy_level", text="Level")
                    col2.enabled = is_proxy

                # help on proxy
                op = col.operator(FLUX_OP_Help.bl_idname, text="", icon="QUESTION", emboss=True)
                op.what = "PROXY"

                col.enabled = item_valid
            else:
                col = box.column(align=False)
                col.label(text="")
                is_proxy = False

        # layout actions
        col = box.column(align=True)
        col.label(text=" > Select your Action")

        # if valid item, test if file exist and put it to cache
        file_path_for_item = False
        file_path_for_item_valid_wip = True
        # bpy.data.filepath

        # ====================
        # cache
        # ====================
        #  as bl not allows to write in bpy.context in this context, I use global, even its evil. But blender fond. use it too after seeking
        #  I used it previously but from one file to another it works, here not. Mysterious, ans I want to go on
        # ====================
        global _flux_ui_cache_current_items
        global _flux_ui_cache_proxy
        global _flux_ui_cache_proxy_level
        global _flux_ui_cache_matching_v
        global _flux_ui_cache_matching_r
        global _flux_ui_cache_file_path_for_item
        global _flux_ui_cache_file_path_for_item_valid_wip

        # conditions
        check_file = False

        # first check if the var exist. If not, it's the first time we go for it
        if '_flux_ui_cache_current_items' not in globals():
            check_file = True
        # else, check if need to refresh vars
        else:
            if current_items == None or _flux_ui_cache_current_items == None:
                check_file = True
            else:
                same = is_same_dict(dict(current_items), dict(_flux_ui_cache_current_items))
                if not same:
                    check_file = True

            # proxy
            if context.scene.flux_ui.enable_proxy:
                if is_proxy != _flux_ui_cache_proxy:
                    check_file = True
                if context.scene.flux_ui.proxy_level != _flux_ui_cache_proxy_level:
                    check_file = True

        # force check
        if get_cache_force_check():
            check_file = True
            set_cache_force_check(False)

        # temporary force
        if not prefs.is_use_ui_cache():
            check_file = True

        # ====================
        # go
        # ====================
        if item_valid:
            # ----------------------
            # check file
            if check_file:
                # if prefs.is_use_ui_cache(): print("Flux : Checķ file(s) for main ui")
                file_item = dict(current_items)

                file_item["project"] = prefs.get_project_name()
                file_item["category"] = context.scene.flux_ui.current_category

                # ------------------------------------------
                # ask to return main folder and item path.
                #   If folder return[0] is false, then the item doesnt exist at all.
                #   If return[1] is False, then the last item doesn't exist at all
                if is_proxy:
                    # put it in the same format as non proxy
                    file_path_for_item = [file_access.get_item_path2(file_item, only_base_folder=True),
                                          ui_operators.find_last_proxy_file(file_item, level=proxy_level)]
                else:
                    file_path_for_item = file_access.get_item_path2(file_item)
                # print(f"start {file_path_for_item=}")

                # print(f"{file_item=}")
                # print(f"get file : {file_path_for_item=}")
                #  if not base folder = no valid item = folder base doesnt exist AT ALL
                if not file_path_for_item[0]:
                    col.enabled = False
                    col = box.column(align=True)
                    col.label(text="   > Invalid Given Item ")
                    return None
                # file path normalisation
                file_path_for_item = file_path_for_item[-1]

                # if prefs.is_debug_mode():
                # print(f"{file_path_for_item=}")

                # ------------------------------------------
                # if no file path for item, at this stage,
                # it can be because there is no wip but revision
                last_v = file_access.get_item_last_version(file_item, as_int=True)
                # print(f"base {last_v=}")

                if not file_path_for_item:
                    tree = file_access.get_item_versionning_tree(file_item)
                    # print(f"base {tree=}")

                    # todo make it work for else than v01
                    if tree:
                        current_v_inside = tree['v01']
                        current_v_inside_up = tree

                        # print(f"1 -{current_v_inside=}")
                        # print(f"1 -{len(current_v_inside_up)=}")

                        all_keys = list(current_v_inside.keys())
                        # print(f"1 -{all_keys=}")
                        if len(all_keys):

                            version_str = list(current_v_inside.keys())[-1]
                            version_int = ui_operators.version_str_to_int(version_str)

                            test_item = dict(file_item)
                            test_item["version"] = [1, version_int]

                            # file_path_for_item = file_access.get_item_path(test_item)
                            file_path_for_item = file_access.get_item_path2(test_item, only_file_path=True)

                            if is_proxy:
                                file_path_for_item = proxy.get_from_item_path(file_path_for_item, level=proxy_level, check=True)

                            # print(f"1 -{file_path_for_item=}")

                            file_path_for_item_valid_wip = False
                            # file_path_for_item = "test"
                        elif len(current_v_inside_up.keys()):

                            test_item = dict(file_item)
                            test_item["version"] = [1]

                            # file_path_for_item = file_access.get_item_path(test_item)
                            file_path_for_item = file_access.get_item_path2(test_item, only_file_path=True)

                            if is_proxy:
                                file_path_for_item = proxy.get_from_item_path(file_path_for_item, level=proxy_level, check=True)

                            file_path_for_item_valid_wip = False

                # update to cache
                _flux_ui_cache_file_path_for_item = file_path_for_item
                _flux_ui_cache_file_path_for_item_valid_wip = file_path_for_item_valid_wip
                _flux_ui_cache_current_items = dict(current_items)
                _flux_ui_cache_proxy = is_proxy
                _flux_ui_cache_proxy_level = context.scene.flux_ui.proxy_level

                # if check file and file path to check
                # print(f"1032 -{file_path_for_item=}")

                if file_path_for_item :
                    if file_path_for_item_valid_wip :
                        only_file = file_path_for_item.split('\\')[-1]

                        # potential version
                        matching_r = file_access.get_new_item_save_path(file_path_for_item, new_version="r")
                        # potential review
                        matching_v = file_access.get_new_item_save_path(file_path_for_item, new_version="v")

                        is_matching_r_exist = os.path.exists(matching_r)
                        is_matching_v_exist = os.path.exists(matching_v)

                        _flux_ui_cache_matching_r = is_matching_r_exist
                        _flux_ui_cache_matching_v = is_matching_v_exist

                        _flux_ui_cache_file_path_for_item = file_path_for_item
                else :
                    is_matching_r_exist = False
                    is_matching_v_exist = False
                    file_path_for_item = False

                    _flux_ui_cache_matching_r = False
                    _flux_ui_cache_matching_v = False
                    _flux_ui_cache_file_path_for_item = False

                #force global check.
                set_global_ui_refresh(True)

            # ----------------------
            # not check file
            else :
                # update cache
                file_path_for_item_valid_wip = _flux_ui_cache_file_path_for_item_valid_wip
                file_path_for_item = _flux_ui_cache_file_path_for_item

                # try excepet only in first passage.
                try :
                    is_matching_r_exist = _flux_ui_cache_matching_r
                    is_matching_v_exist = _flux_ui_cache_matching_v
                except :
                    is_matching_r_exist = False
                    is_matching_v_exist = False

        # creation button
        # print(f"final draw : {file_path_for_item=}")
        is_proxy_str = ""
        if is_proxy: is_proxy_str = " (Proxy)"

        if not file_path_for_item:
            colCreate = box.column(align=True)

            if is_proxy:
                colCreate.label(text=" +-> No Wip Proxy")
                colCreate.label(text=" +-> Proxy is created from the current open scene, at last wip level")
            else:
                colCreate.label(text=" +-> Wip doesn't exist yet")
                colCreate.label(text=" :p")

            if is_proxy:
                create1 = colCreate.operator(ui_operators.FLUX_DISK_ActionComfirm.bl_idname, text="Create New Proxy", icon="FILE_NEW")
                create1.action     = "SAVE"
                create1.sub_action = "AS+1"
            else:
                create1 = colCreate.operator(ui_operators.FLUX_DISK_ActionComfirm.bl_idname, text="Create New", icon="FILE_NEW")
                create1.action     = "NEW"
                create1.sub_action = "NONE"

        # load buttons
        else:
            colLoad = box.column(align=True)

            # get only file name to avoid too big print
            only_file = file_path_for_item.split('\\')[-1]

            # usual case, wip and revision are present
            if file_path_for_item_valid_wip:

                is_r = "No"
                if is_matching_r_exist:is_r = "Yes"
                is_v = "No"
                if is_matching_v_exist: is_v = "Yes"

                colLoad.label(text=f" +-> last wip :{only_file}")
                colLoad.label(text=f" +-> Reviewed : {is_r} / Published : {is_v}")

                # ui
                rowLoad = colLoad.row(align=True)
                load1 = rowLoad.operator(ui_operators.FLUX_DISK_ActionComfirm.bl_idname, text=f"Load Last Wip{is_proxy_str}", icon="FILE_FOLDER")

                # load1.is_proxy = is_proxy
                load1.action     = "LOAD"
                load1.sub_action = "WIP_LAST"
                load2 = rowLoad.operator(ui_operators.FLUX_DISK_ActionComfirm.bl_idname, text=f"Load{is_proxy_str}...", icon="FILE_FOLDER")
                load2.action     = "LOAD"
                load2.sub_action = "LIST"


            # no wip present, but revision and wip
            else:
                colLoad.label(text=f" +-> No wip but revision exist")
                colLoad.label(text=f" +-> Create a wip please!")

                # ui
                rowLoad = colLoad.row(align=True)
                rowLoad1 = rowLoad.column()
                rowLoad1.operator(ui_operators.FLUX_DISK_ActionComfirm.bl_idname, text="Load Last Wip", icon="FILE_FOLDER")
                rowLoad1.enabled = False
                rowLoad2 = rowLoad.column()
                load2 = rowLoad2.operator(ui_operators.FLUX_DISK_ActionComfirm.bl_idname, text="Load...", icon="FILE_FOLDER")
                load2.action     = "LOAD"
                load2.sub_action = "LIST"

        # save buttons
        colSaveWip = box.column(align=True)
        colSaveWip.scale_y = 2

        if is_proxy:
            save1 = colSaveWip.operator(ui_operators.FLUX_DISK_ActionComfirm.bl_idname, text="Work File : Save Proxy File", icon="FILE_TICK")
        else:
            save1 = colSaveWip.operator(ui_operators.FLUX_DISK_ActionComfirm.bl_idname, text="Work File : Save As +1", icon="FILE_TICK")
        # save1.is_proxy = is_proxy
        save1.action     = "SAVE"
        save1.sub_action = "AS+1"

        rowPub = box.row(align=True)

        reviewCol = rowPub.column()
        save2 = reviewCol.operator(ui_operators.FLUX_DISK_ActionComfirm.bl_idname, text=f"Review{is_proxy_str}", icon="HEART")
        save2.action     = "SAVE"
        save2.sub_action = "REVIEW"

        pubCol = rowPub.column()

        # print(bpy.data.filepath)

        # allow off line when you have a blank scene.
        allow_offline = False
        if not bpy.data.filepath and is_matching_r_exist:
            allow_offline = True
            # print("no file path")

        text_pub = f"Publish{is_proxy_str}"
        if allow_offline: text_pub = "Publish (OFFLINE)"

        save3 = pubCol.operator(ui_operators.FLUX_DISK_ActionComfirm.bl_idname, text=text_pub, icon="FUND")
        save3.action     = "SAVE"
        if allow_offline: save3.sub_action = "PUBLISH_OFFLINE"
        else:             save3.sub_action = "PUBLISH"

        # refresh visibility of buttons
        if current_items_values != None:
            if 'None' in current_items_values :
                if not file_path_for_item : colCreate.enabled = False
                else : colLoad.enabled = False
                colSaveWip.enabled = False
                rowPub.enabled = False
        # if no file in here
        if not file_path_for_item or not bpy.data.filepath:
            if not file_path_for_item:
                colSaveWip.enabled = False
            reviewCol.enabled = False
            pubCol.enabled = False

        # check visibility if v or r exist
        # if is_matching_v_exist:
        # print(is_matching_r_exist)
        if not erase_v:
            reviewCol.enabled = False
            pubCol.enabled = False
        else:
            #if we have a review, then we can do a offline publish
            if is_matching_r_exist:
                # print("here")
                reviewCol.enabled = False
                pubCol.enabled = True
                # rowPub.enabled = True
            else:
                pubCol.enabled = False

        # final check for disable all at beginning.
        if not item_valid:
            if not file_path_for_item:
                colCreate.enabled = False
            else:
                colLoad.enabled = False
            colSaveWip.enabled = False
            rowPub.enabled = False

        # if is proxy, review is always on
        if is_proxy:
            reviewCol.enabled = True

        # if no wip present we cannot review it
        if not file_path_for_item_valid_wip:
            reviewCol.enabled = False

        # offlineCol = box.column(align=False)
        # offlineCol.alignment = 'LEFT'
        # 
        # icon = "RIGHTARROW_THIN"
        # if context.scene.flux_ui_utils.show_offline_subpanel: icon="DOWNARROW_HLT"
        # offlineCol.prop(context.scene.flux_ui_utils, "show_offline_subpanel", text="OFFLINE previews/publish", icon="RIGHTARROW_THIN", emboss=False)
        # 
        # if context.scene.flux_ui_utils.show_offline_subpanel:
        #     offlineCol.label(text="test")



class FLUX_PT_ItemManagementPanel_subPanelOffline(bpy.types.Panel):
    bl_label = "OFFLINE previews/publish"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_parent_id = "FLUX_PT_ItemManagementPanel"
    bl_options = {'DEFAULT_CLOSED'}

    # def draw_header(self,context):
    #     self.layout.prop(context.scene.camera.data, "show_background_images", text="")

    def draw(self, context):

        # init vars
        global _flux_ui_cache_file_path_for_item
        file_path_for_item = _flux_ui_cache_file_path_for_item
        is_valid = is_item_valid()

        layout = self.layout

        # -------------------------------------
        # image render preview stuffs
        box = layout.box()
        row = box.column(align=True)
        row.label(text="Multiple Previews for Review", icon="VIEWZOOM")
        op = row.operator(op_flux_render.FLUX_OT_RenderFile.bl_idname, text="Render Previews",icon="RESTRICT_RENDER_OFF")
        op.increment = True
        op.for_revision = True
        op.allow_choose_for_revision = True

        op = row.operator(op_flux_render.FLUX_OT_CheckFile.bl_idname, text="Post Comment/Previews", icon="OUTPUT")
        op.task_status_name = config.get_status_list(get_default="review")
        # enabled ?
        row.enabled = is_valid
        if is_valid:
            row.enabled = bool(file_path_for_item)

        if bpy.context.scene.flux_ui.is_proxy:
            row.enabled = False

        #
        # todo clean current_items_r = dict(current_items)
        current_item = get_current_user_item(get_category=True)
        is_matching_r_exist = False
        if is_valid:
            # print(current_item)
            tree = file_access.get_item_versionning_tree(current_item)
            # print(tree)

            if tree:
                all_r = list(tree['v01'].keys())
                all_r = int(all_r[-1][1:])
                current_items_r = dict(current_item)
                current_items_r["version"] = [1, all_r]
                matching_r = file_access.get_item_path2(current_items_r, only_file_path=True)
                is_matching_r_exist = os.path.exists(matching_r)
                # print(is_matching_r_exist)
            else:
                is_matching_r_exist = False

            # -------------------------------------
            # offline publish
            box = layout.box()
            row = box.column(align=True)
            row.label(text="Publish without open", icon="ANIM_DATA")
            row.operator(op_flux_render.FLUX_OT_RenderFile.bl_idname, text="OFFLINE Publish",icon="FUND")
            row.enabled = is_matching_r_exist

            if bpy.context.scene.flux_ui.is_proxy:
                row.enabled = False


class FLUX_PT_SceneComposerPanel(bpy.types.Panel):
    """Creates a Panel in the Object properties window with all the save/load buttons for asset and shot"""
    bl_label = "Flux Scene Composer"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_order = 10

    # --
    link_lib_override: bpy.props.BoolProperty(default=True)

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:

        if not prefs.get_project_name():
            return False

        if prefs.is_show_composer():
            if prefs.get().is_db_logged: return True
            else: return False
        else:
            return False


    def draw(self, context: bpy.types.Context):

        # init var
        is_matching_r_exist = False
        is_matching_v_exist = False
        erase_v = True

        # get values of dict as array if not none
        current_items = get_current_user_item(for_composer=True)
        # print(current_items)
        if current_items != None : current_items_values = list(current_items.values())
        else: current_items_values = None


        # is item valid? Is there any None in the values?
        item_valid = True
        if current_items_values == None:
            item_valid = False
        else:
            # care about list
            current_items_values_copy = list(current_items_values)
            if context.scene.flux_ui_composer.load_type == "list":
                current_items_values_copy.pop(1)
            if 'None' in current_items_values_copy:
                item_valid = False


        # print(f"{item_valid=}")

        # init layout
        layout = self.layout
        box = layout.box()
        row = box.row(align=True)
        row.label(text="Composer", icon="OUTLINER_OB_CAMERA")
        # help
        op = row.operator(FLUX_OP_Help.bl_idname, text="", icon="HELP", emboss=False)
        op.what = "COMPOSER"

        row2 = row.column()
        ic = "HEART"

        # ------
        if item_valid:
            if prefs.bookmarks_is_item_exist(for_composer=True): ic = "FUND"
        row2.operator(ui_operators.FLUX_ITEM_BKM_AddRemove.bl_idname,icon=ic,text="",emboss=False).for_composer=True
        row2.enabled = item_valid

        # support for list
        # if context.scene.flux_ui_composer.load_type == "list":
        #     row2.enabled = False


        row.menu("FLUX_MT_ComposerMenu", icon='DOWNARROW_HLT', text="")

        # current category if we keep the choice from asset/shot, to display assets from casting
        # row = box.row(align=True)
        # row.prop(context.scene.flux_ui, "current_category", expand=True)
        # col = box.column(align=True)

        # -------------------------------------
        # guess all button to display from items_description list
        # get descriptions
        all_items_description = json.loads(context.scene.flux_ui.items_description)
        # get current item desc
        item_description = all_items_description[list(all_items_description.keys())[0]]

        # nice text
        # category button (ie shot/asset)
        col = box.column(align=True)

        row = col.row(align=True)
        row.prop(context.scene.flux_ui_composer, "load_type", expand=True)

        for i in range(2): col.separator()

        col.label(text=" > Select your Asset")


        if prefs.is_debug_mode():
            print(f" > Composer : {current_items=} / {item_valid=}")

        # print(context.scene.flux_ui_composer.load_type)

        enable_button = True
        # apply buttons
        for i, button in enumerate(item_description):
            # display
            #  if name and list... then go to list for name!
            if i == 1 and context.scene.flux_ui_composer.load_type == "list":
                # -------
                col = box.column(align=True)
                row_tl1 = col.row()

                row_tl1.template_list("FLUX_UL_composerList",  # name of UIList class containing the draw method
                                      "ComposerList",          # unique ID
                                      bpy.context.scene,       # propGroup containing the list to display
                                      "flux_ui_composer_list",
                                      bpy.context.scene,       # index
                                      "flux_ui_composer_list_index",
                                      rows=1,
                                      type="DEFAULT",
                                      sort_lock=True
                                      )

                col_tl2 = row_tl1.column(align=True)

                col_tl2.prop(context.scene.flux_ui_composer, "show_only_unchecked", text="", toggle=True, icon="CHECKBOX_DEHLT")
                col_tl2.prop(context.scene.flux_ui_composer, "show_only_checked", text="", toggle=True, icon="CHECKBOX_HLT")
                col_tl2.prop(context.scene.flux_ui_composer, "show_only_heart", text="", toggle=True, icon="HEART")
                col_tl2.prop(context.scene.flux_ui_composer, "show_only_funded", text="", toggle=True, icon="FUND")

                col_tl2.separator()
                col_tl2.prop(context.scene.flux_ui_composer, "show_only_broken", text="", toggle=True, icon="ORPHAN_DATA")
                col_tl2.prop(context.scene.flux_ui_composer, "show_only_question", text="", toggle=True, icon="QUESTION")

                for i in range(3):
                    col_tl2.separator()

                col_tl2.operator(ui_operators.FLUX_OT_RefreshComposerList.bl_idname, text="", icon="FILE_REFRESH")

            # in all other cases, use the normal selector display
            else:
                display_text = "Select " + button.capitalize()
                if not current_items_values == None:
                    if current_items_values[i] != "None":
                        display_text = current_items_values[i]

                # do it
                col = box.column(align=True)
                list_btn = col.operator(ui_operators.FLUX_DB_choose_from_list.bl_idname, text=display_text, icon="DOWNARROW_HLT")
                list_btn.what = button
                list_btn.composer = True

            # security check where from old files, composer had a bad current_item
            if current_items and "seq" in list(current_items.keys()):
                # todo check why we pass though here on a new scene. to ee it uncomment this line
                # print(" YOU R NOT SUPPOSE TO BE HERE (2) -- force reset current item in composer recuservily")
                # print("bad")
                current_items = {'type': 'None', 'name': 'None', 'task': 'None'}

            # print(f"composer {current_items=}")

            # =======================================================
            # is button enable
            enable_button = True
            # one
            if current_items_values == None:
                if i > 0:
                    enable_button = False
            else:
                if context.scene.flux_ui_composer.load_type == "one":
                    if i > 0:
                        if current_items == None or current_items == "None":
                            enable_button = False
                        else:
                            if current_items_values[0] == "None" and current_items_values[1] == "None" and current_items_values[2] == "None":
                                enable_button = False
                    if i == 2:
                        if current_items_values[1] == "None":
                            enable_button = False
                # list
                else:
                    if i > 0:
                        if current_items == None or current_items == "None":
                            enable_button = False
                        else:
                            if current_items_values[0] == "None":
                                enable_button = False
            col.enabled = enable_button

        col.separator()

        # --------------------------------------
        #  display action button
        # check collections button
        col_check = box.column(align=True)
        row_check = col_check.row(align=True)
        row_check.operator(ui_operators.FLUX_OT_CheckComposerFilesCollection.bl_idname, text="Check Asset(s) Collection", icon="OUTLINER_COLLECTION")
        row_check.scale_y = 2

        row_check = col_check.row(align=True)
        checker_icon = "LIBRARY_DATA_BROKEN"
        msg = "At least one bad / untested asset"
        if context.scene.flux_ui_composer.load_type == "one":
            msg = "Invalid asset"

        if context.scene.flux_ui_composer.ok_for_load:
            checker_icon = "FUND"
            msg = "All good :)"
        elif not context.scene.flux_ui_composer.at_least_one_checked:
            checker_icon = "QUESTION"
            msg = "Please Check Asset(s)"

        row_check.label(text=msg, icon = checker_icon)

        col_action1 = box.column(align=True)
        row_action = col_action1.row(align=True)
        row_action.label(text=" > Select your Action")

        row_action = col_action1.row(align=True)
        row_action.separator()

        row_action = col_action1.row(align=True)
        row_action.prop(context.scene.flux_ui_composer, "ui_link_lib_override", text="Library override")

        # col_action = box.column(align=True)
        row_action = col_action1.row(align=True)
        op_link = row_action.operator(ui_operators.FLUX_DISK_AppendLink.bl_idname, text="Link", icon="LINKED")
        op_link.link = True
        op_link.link_lib_override = context.scene.flux_ui_composer.ui_link_lib_override
        row_action.scale_y = 2

        col_action2 = box.column(align=True)
        row_action = col_action2.row(align=True)
        # col2 = box.column(align=True)
        op_append = row_action.operator(ui_operators.FLUX_DISK_AppendLink.bl_idname, text="Append", icon="APPEND_BLEND")
        op_append.link = False

        # enable action button
        if not context.scene.flux_ui_composer.ok_for_load:
            col_action1.enabled = False
            col_action2.enabled = False
        else:
            col_action1.enabled = True
            col_action2.enabled = True

        # enable check button
        col_check.enabled = False
        if current_items not in [None, "None"] and current_items['task'] != "None":
            col_check.enabled = True

        # =================
        # todo csche deeper test, add refresh, and detection of last wip
        # todo add from casting
        # =================
        # todo options v r w pour le selecteur list

        return None


class FLUX_PT_LinkEditorPanel(bpy.types.Panel):
    """Creates a Panel in the Object properties window with all the save/load buttons for asset and shot"""
    bl_label = "Flux Link Editor"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_order = 9

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        if prefs.is_show_linkeditor(): return True
        else: return False

    def remap(self, old_val, old_min, old_max, new_min, new_max):
        ''' description TODO '''

        if old_val < old_min: return new_min
        if old_val > old_max: return new_max

        return (new_max - new_min) * (old_val - old_min) / (old_max - old_min) + new_min

    def draw(self, context: bpy.types.Context):

        # init layout
        layout = self.layout
        box = layout.box()
        line = box.column(align=True)
        row = line.row(align=True)
        row.label(text="Link Editor", icon="LINKED")

        op = row.operator(FLUX_OP_Help.bl_idname, text="", icon="HELP", emboss=False)
        op.what = "LINK"

        row.operator(ui_operators.FLUX_OT_LINK_refresh.bl_idname,icon="FILE_REFRESH",text="",emboss=False)

        #1
        col = line.column(align=True)
        #2
        row_tl1 = col.row()
        #
        row_tl1_line = row_tl1.column()

        # print(context.region.width)
        w = context.region.width
        f_remap = self.remap(w, 150, 400, 0.01, 0.5)
        n_sel = context.scene.flux_ui_linkeditor.is_sel_num

        #https://blender.stackexchange.com/questions/30444/create-an-interface-which-is-similar-to-the-material-list-box

        row_tl1_line_split = row_tl1_line.split(factor=f_remap)
        #
        row_tl1_line_split.separator()
        #
        # row_tl1_line_colL = row_tl1_line_split.row()
        # row_tl1_line_colL.prop(context.scene.flux_ui_linkeditor, "show_sidebar", text="", icon="MENU_PANEL")

        row_tl1_line_col = row_tl1_line_split.row()
        row_tl1_line_col.prop(context.scene.flux_ui_linkeditor, "edit_mode", expand=True)

        # -----
        # if n_sel:
        #     row_tl1_line_col.enabled = False

        all_linked = list(context.scene.flux_ui_link_list)
        if not all_linked:
            row_tl1_line.label(text=" > No links listed")
            row_tl1_line2 = row_tl1_line.column()
            row_tl1_line2.operator(ui_operators.FLUX_OT_LINK_refresh.bl_idname,icon="FILE_REFRESH",text="Refresh Link List",emboss=True)
            row_tl1_line2.scale_y = 2
        else:
            row_tl1_line.template_list("FLUX_UL_linkList",  # name of UIList class containing the draw method
                                  "LinkList",          # unique ID
                                  bpy.context.scene,       # propGroup containing the list to display
                                  "flux_ui_link_list",
                                  bpy.context.scene,       # index
                                  "flux_ui_link_list_index",
                                  rows=1,
                                  type="DEFAULT",
                                  sort_lock=True
                                  )

        col_tl2 = row_tl1.column(align=True)

        # col_tl2.separator(factor=1)
        col_tl2.separator(factor=4)

        col_tl2.prop(context.scene.flux_ui_linkeditor, "show_only_unchecked", text="", toggle=True, icon="CHECKBOX_DEHLT")
        col_tl2.prop(context.scene.flux_ui_linkeditor, "show_only_checked", text="", toggle=True, icon="CHECKBOX_HLT")

        col_tl2.separator(factor=1)
        col_tl2.prop(context.scene.flux_ui_linkeditor, "show_short", text="", toggle=True, icon="FULLSCREEN_EXIT")
        col_tl2.prop(context.scene.flux_ui_linkeditor, "show_file", text="", toggle=True, icon="FILE_TICK")

        col_tl2.separator(factor=1)
        col_tl2.operator(ui_operators.FLUX_LINK_PROXY_printInfos.bl_idname,icon="GHOST_DISABLED",text="",emboss=True)
        # -----
        line_splitA = line.split(factor=f_remap)

        line_splitA1 = line_splitA.row(align=True)
        line_splitA1.label(text="   ", icon="BLANK1")
        line_splitA2 = line_splitA.row(align=True)

        # item fct
        isolate_icn = "RADIOBUT_ON"
        if context.scene.flux_ui_linkeditor.isolate_objsvis:  isolate_icn = "CANCEL"
        # -----
        op = line_splitA2.operator(ui_operators.FLUX_LINK_op.bl_idname, text="Isolate", icon=isolate_icn ,emboss=True)
        op.action = "ISOLATE"
        op.item = ""
        op = line_splitA2.operator(ui_operators.FLUX_LINK_op.bl_idname, text="sel.", icon="RESTRICT_SELECT_OFF",emboss=True)
        op.action = "SELECT"
        op.item = ""
        op = line_splitA2.operator(ui_operators.FLUX_LINK_op.bl_idname, text="", icon="FILE_REFRESH",emboss=True)
        op.action = "RELOAD"
        op.item = ""
        op = line_splitA2.operator(ui_operators.FLUX_LINK_op.bl_idname, text="", icon="TRASH",emboss=True)
        op.action = "REMOVE"
        op.item = ""

        for i in range(5):
            line_splitA2.separator()
        if not n_sel:
            line_splitA2.enabled = False

        # fcts 1
        line.separator()
        line_splitB1 = line.row(align=True)
        line_splitB1.operator(ui_operators.FLUX_LINK_op.bl_idname, text="Get from sel", icon="QUESTION",emboss=True).action = "GET"
        for i in range(5):
            line_splitB1.separator()

        line.separator()

        # ======================
        # proxies
        proxy_multilevel = prefs.get().proxy_is_multilevel
        all_linked = list(context.scene.flux_ui_link_list)
        is_proxy_available = False

        at_least_one_false = False
        at_least_one_sel = False
        is_there_shot_level = False

        # cache
        cache_update = False

        # internal cache
        global _flux_ui_linked_cache_link_list_index
        global _flux_ui_linked_cache_link_list_len
        global _flux_ui_linked_cache_is_there_shot_level
        global _flux_ui_linked_cache_is_proxy_available
        global _flux_ui_linked_cache_link_item
        global _flux_ui_linked_cache_previz_tag

        # external cache
        at_least_one_sel = bool(context.scene.flux_ui_linkeditor.is_sel_num)
        at_least_one_false = not bool(context.scene.flux_ui_linkeditor.is_proxy_for_all_sel)

        if "_flux_ui_linked_cache_link_list_index" not in globals():
            cache_update = True
        else:
            if _flux_ui_linked_cache_link_list_index != context.scene.flux_ui_link_list_index:
                cache_update = True
            if _flux_ui_linked_cache_link_list_len != len(all_linked):
                cache_update = True

        # print(f"{cache_update=}")

        # update
        if cache_update:
            if all_linked:
                current_linked = all_linked[context.scene.flux_ui_link_list_index]
                _flux_ui_linked_cache_link_list_index = context.scene.flux_ui_link_list_index
                _flux_ui_linked_cache_link_list_len = len(all_linked)
                # print(f"{current_linked=}")
                # print(f"{current_linked.link_path=}")
                is_proxy_available = is_proxy_available_in_folder(current_linked)
                _flux_ui_linked_cache_is_proxy_available = is_proxy_available

                # if at least one selected, we put on enabled the proxy tab
                is_there_shot_level = False
                for each in all_linked:
                    # if each.is_sel:
                        # at_least_one_sel = True
                        # is_proxy_available = True
                        # is_this_proxy_available = is_proxy_available_in_folder(each)
                        # if not is_this_proxy_available: at_least_one_false = True
                    # check id there is shot level
                    if each.is_shot_level:
                        is_there_shot_level = True
                _flux_ui_linked_cache_is_there_shot_level = is_there_shot_level

                # print(f"{current_linked.link_path=}")
                link_item = file_access.get_item_from_path(current_linked.link_path)
                # print(f"{link_item=}")
                _flux_ui_linked_cache_link_item = link_item

                # previz tag
                project_config = prefs.get_project_config_dict()
                previz_tag = project_config["project_data"]["previz_tag"]
                _flux_ui_linked_cache_previz_tag = previz_tag
        # use cache
        else:
            if all_linked:
                current_linked = all_linked[_flux_ui_linked_cache_link_list_index]
            is_proxy_available = _flux_ui_linked_cache_is_proxy_available
            is_there_shot_level = _flux_ui_linked_cache_is_there_shot_level
            link_item = _flux_ui_linked_cache_link_item
            previz_tag = _flux_ui_linked_cache_previz_tag

        #
        if at_least_one_sel:
            if at_least_one_false:
                is_proxy_available = False

        # ===================================
        # PROXY BOX
        # https://blender.stackexchange.com/questions/32335/how-to-implement-custom-icons-for-my-script-addon
        box_line = line.box()
        line_box_line = box_line.column()
        # box_line.enabled = bool(is_proxy_available)

        # infos
        if is_proxy_available:
            msg = "Proxy"
            if proxy_multilevel:
                # if its bool is set earlier, and its multi selection
                if isinstance(is_proxy_available, bool):
                    msg += " (Multiselection)"
                # if netm the is_proxy_available contains the possible choise as array
                else:
                    msg += " (Available Levels: "
                    for each in is_proxy_available:
                        msg+= str(each)
                        if not each == is_proxy_available[-1]:
                            msg+= " / "
                    msg += " )"
        else:
            if at_least_one_sel and  at_least_one_false:
                msg = "At leat one item without proxy"
            else:
                msg = "No Proxy for selected item"

        # show main label of UI
        # line_box_line.label(text=msg)
        icon = "RIGHTARROW"
        if context.scene.flux_ui_linkeditor.show_proxy_panel:
            icon = "DOWNARROW_HLT"
        line_box_line.prop(context.scene.flux_ui_linkeditor, "show_proxy_panel", text=msg, icon=icon, emboss=False )

        # --------------------------------------------------
        if context.scene.flux_ui_linkeditor.show_proxy_panel:
            # state
            if all_linked:
                msg = "Proxy is "
                if current_linked.is_proxy:
                    msg += "ON"
                    icon = "MESH_CUBE"
                    if proxy_multilevel:
                        msg+= " / Level : " + str(current_linked.proxy_level)
                else:
                    msg += "OFF"
                    icon = "MESH_UVSPHERE"
            else:
                msg = ":/"
                icon = "DOT"

            # --------------------------------------------------------------------------------------
            # line2_box_line is all button under show proxy panel, excepet proxy asset to shot panel
            line2_box_line = line_box_line.column()
            line2_box_line.enabled = bool(is_proxy_available)

            line2_box_line.label(text=msg, icon=icon)

            # line_splitC1 = line.row(align=True)
            # if proxy_multilevel: line_box_line2 = line2_box_line.row(align=True)
            # else: line_box_line2 = line2_box_line  # removed

            line_box_line2 = line2_box_line.row(align=True)

            line_box_line2.prop(context.scene.flux_ui_linkeditor, "is_proxy", text="Is Proxy?", toggle=True, emboss=True, icon="MOD_SUBSURF")
            if proxy_multilevel:
                col2 = line_box_line2.column(align=True)
                col2.prop(context.scene.flux_ui_linkeditor, "proxy_level", text="Level?")
                col2.enabled = context.scene.flux_ui_linkeditor.is_proxy
                line_box_line2.operator(ui_operators.FLUX_LINK_PROXY_refresh_props.bl_idname, text="", icon="FILE_REFRESH")

                line2_box_line.operator(ui_operators.FLUX_LINK_PROXY_switch.bl_idname, text="Apply Settings", icon="PLAY",emboss=True).indep_button = True

            # help on proxy
            op = line_box_line2.operator(FLUX_OP_Help.bl_idname, text="", icon="QUESTION", emboss=True)
            op.what = "PROXY"

            box_line2 = box_line.box()
            line_box_line = box_line2.column()
            icon = "RIGHTARROW"
            if context.scene.flux_ui_linkeditor.show_convert_to_shot_panel:
                icon = "DOWNARROW_HLT"
            line_box_line.prop(context.scene.flux_ui_linkeditor, "show_convert_to_shot_panel", text="Convert Link to Shot Level", icon=icon, emboss=False)
            if context.scene.flux_ui_linkeditor.show_convert_to_shot_panel:

                if all_linked:
                    # enabled
                    current_user = get_current_user_item(get_category=True)
                    if current_user['category'] != "shots": en_out = False
                    else: en_out = True

                    # enabled
                    en_in = False
                    if all_linked:
                        if current_linked.is_shot_level: en_in = True

                    # -----------------------------------------------
                    # you can have only one shot level per scene
                    en_in2 = True
                    display_one_shot_level_msg = False
                    if is_there_shot_level and not current_linked.is_shot_level:
                        en_in2 = False
                        display_one_shot_level_msg = True
                    # -----------------------------------------------


                    # print(current_user)
                    # layout init
                    line_box_line2 = line_box_line.column(align=True)
                    # lable line
                    line_box_line2.label(text=f"Link '{current_linked.link_collection}' file will be copied to :")
                    shot_ref = "None"
                    # print(current_user)
                    is_previz = False
                    if current_user['category'] == "shots":
                        if not display_one_shot_level_msg:
                            if "seq" in list(current_user.keys()):
                                if previz_tag in current_user['shot']:
                                    shot_ref = f"Current Shot is previz. Cannot be copied!"
                                    is_previz = True
                                else:
                                    shot_ref = f"{current_user['seq']} / {current_user['shot']} / proxy"
                            else:
                                shot_ref = f"None"
                            if is_item_valid(current_user, check_for_cat=True): line_box_line2.label(text=f"  {shot_ref}")
                            else: line_box_line2.label(text=f"  Please select a shot...")
                        else:
                            line_box_line2.label(text=f"  Only one shot level per scene!")

                    else: line_box_line2.label(text=f"  Please select a shot...")
                    # op
                    op = line_box_line2.operator(ui_operators.FLUX_LINK_PROXY_asset2shot.bl_idname, text="Convert Link asset > shot", icon="FILE_MOVIE",emboss=True)
                    op.shot_message = shot_ref
                    line_box_line2.enabled = (not en_in) * en_out * en_in2
                    if len(all_linked) == 0 : line_box_line2.enabled = False
                    if is_previz: line_box_line2.enabled = False


                    line_box_line3 = line_box_line.column(align=True)
                    line_box_line3.operator(ui_operators.FLUX_LINK_PROXY_shot2asset.bl_idname, text="Convert Back Link shot > asset", icon="FILE_3D",emboss=True)
                    line_box_line3.enabled = en_in * en_out

                else:
                    line_box_line.label(text="No links...")

        # ---------------------------------------------------
        # infos
        line.separator()
        # label the state of the asset
        # all_linked = list(bpy.context.scene.flux_ui_link_list)
        # current_linked = None
        if all_linked:
            # current_linked = all_linked[bpy.context.scene.flux_ui_link_list_index]
            # print(all_linked)
            # print(current_linked)
            # link_item = file_access.get_item_from_path(current_linked.link_path)
            # print(f"{current_linked.link_path=} - {link_item=}")

            # print(f"{link_item}")
            shot_level = False
            if link_item:
                if 'name' in list(link_item.keys()):
                    msg = f"Asset : {link_item['name']} / {link_item['type']} / {link_item['task']}"
                else:
                    shot_level = True
                    msg = f"Link at shot level : {link_item['seq']} / {link_item['shot']} / {link_item['task']}"

                # old parsing method
                # for i, each in enumerate(link_item.values()):
                #     if i > 0:
                #         # print(f"{each=}")
                #         if isinstance(each, list):
                #             each = each[0]
                #
                #         msg+= each.capitalize()
                #         if i+1 < len(link_item.values()):
                #             msg += " / "

            else:
                msg = "Collection : " + current_linked.link_collection
            line.label(text=msg, icon="FILE_3D")
            line.label(text="File : " + os.path.basename(current_linked.link_path), icon="FILE_TICK")

            # print(os.path.basename(current_linked.link_path))

            # infos from kitsu
            if link_item and database_access.is_db_auth():
                if shot_level: status = [["", "(shot level)", "", "(shot level)"]]
                else:
                    status = database_access.get_comment("assets", link_item, versionning=[1], local=None,
                                                 only_last=True)
            else: status = False
            # print(status)
            if status:
                comment = status[0][1]
                status = status[0][3]
            else:
                status = "No Db infos available"
                comment = status
            line.label(text=f"STATUS : {status}", icon="LIGHT")
            line.label(text=f"COMMENT : {comment}", icon="LIGHT")
            line.separator()

        # ----------------------------------------
        # edit link

        global flux_editlink_origfile
        # if not flux_editlink_origfile: print("not")
        if not 'flux_editlink_origfile' in globals():
            flux_editlink_origfile = ""
            # print("overrite")
        # print(f"{flux_editlink_origfile=}")

        box_line3 = line.box()
        line_box_line = box_line3.column()
        icon = "RIGHTARROW"
        if context.scene.flux_ui_linkeditor.show_edit_link:
            icon = "DOWNARROW_HLT"
        line_box_line.prop(context.scene.flux_ui_linkeditor, "show_edit_link", text="Edit link", icon=icon, emboss=False)
        if context.scene.flux_ui_linkeditor.show_edit_link:
            # display edit link panel
            ui_edit_link.draw_proxy_link_edit(context, line_box_line)

        return None