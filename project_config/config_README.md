# Config README : config json file explanation
> This file will explain you the logic behind a <project_name>_config.json file

## project_data
> Here are the project specific information
- project_name
- project_name_short
- default_root
- ...
- kitsu api link?

## prefs
> TODO
- disk_action_new_template: this is really rough now. put your absolute path in code here, based on root project
- disk_action_new_from: describe dependencies from other tasks. Value is : [task (str), open_it (bool)]
- composer

## versionning
> TODO

## children
> Here begins the description of the project file tree

a list of sub folder of the file tree